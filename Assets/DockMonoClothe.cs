using InteractionSystem;
using PlayerSystem;
using UnityEngine;

[RequireComponent(typeof(ItemDock))]
public class DockMonoClothe : MonoBehaviour
{
    [SerializeField] private MonologueSO _monologue;
    private Interactable _myItemDock;
    private static bool _wasPlayed = false;
    private void Awake()
    {
        _myItemDock = GetComponent<ItemDock>();
    }
    private void OnEnable()
    {
        _myItemDock.OnInteract.AddListener(OnPutToDockMono);
    }
    private void OnDisable()
    {
        _myItemDock.OnInteract.RemoveListener(OnPutToDockMono);
    }
    private void OnPutToDockMono(Player p_player)
    {
        if (!p_player.IsOwner) return;
        if (!_wasPlayed)
            {
                MonologueManager.instance.PlayMonologue(_monologue);
                _wasPlayed = true;
            }
    }
}
