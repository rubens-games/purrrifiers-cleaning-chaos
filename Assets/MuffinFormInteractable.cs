using DG.Tweening;
using InteractionSystem;
using PlayerSystem;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

public class MuffinFormInteractable : Interactable
{
    [Inject] private PlayerManager _playerManager;
    [SerializeField] private FoodSO _cakeSO;
    [SerializeField] private BowlInteractable _bowlInteractable;
    [SerializeField] private OvenControler _ovenControler;
    private ItemState _stateFromCakeRaw;
    private int _readyMuffins = 0;

    public int ReadyMuffins
    {
        get => _readyMuffins;
        set => _readyMuffins = value;
    }

    [SerializeField] private List<ItemData> _muffinsItem;
    [SerializeField] private List<GameObject> _cakes;

    private void OnEnable()
    {
        OnInteract.AddListener(IncomingItem);
    }

    private void OnDisable()
    {
        OnInteract.RemoveListener(IncomingItem);
    }

    public async void IncomingItem(Player p_player)
    {
        if (p_player.pickUpControllerRight.CurrentPickup.TryGetComponent(out ItemData _dataHoldable)) //weryfikacja
        {
            if (_dataHoldable.ItemSO != _cakeSO) return; //czy dobry item
        }

        if (_readyMuffins > 0) return;

        _stateFromCakeRaw = _dataHoldable.CurrentState;

        // foreach (var item in _muffinsItem)
        // {
        //     item.gameObject.SetActive(true);
        // }

        foreach (var cake in _cakes)
            cake.SetActive(true);

        _bowlInteractable.ResetBowl(); //resetuje ciasto i craft
        _ovenControler.PutMuffinsInside();
    }

    public void BakedMuffins(bool p_result)
    {
        foreach (var cake in _cakes) cake.SetActive(false);

        foreach (var item in _muffinsItem)
        {
            item.CurrentState = p_result && _stateFromCakeRaw == ItemState.good ? ItemState.good : ItemState.poisoned;
            item.gameObject.SetActive(true);
        }

        _readyMuffins = _muffinsItem.Count; //Full tacka
    }

    public override bool CanInteractWithPlayer(Player player)
    {
        if (!player) return false;
        
        var isAnyMuffinActive = _muffinsItem.Any(muffin => muffin.gameObject.activeSelf);

        if (isAnyMuffinActive) return false;

        if (!player.pickUpControllerRight.CurrentPickup ||
            !player.pickUpControllerRight.CurrentPickup.TryGetComponent(out ItemData dataHoldable)) return false;
        
        return dataHoldable.ItemSO is FoodSO;
    }
}
