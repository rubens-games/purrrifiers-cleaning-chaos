using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemData : MonoBehaviour
{
    [SerializeField] private ItemSO _itemSO;
    public ItemSO ItemSO
    {
        get => _itemSO;
    }

    public ItemState CurrentState
    {
        get => _currentState;
        set => _currentState = value;
    }

    [SerializeField] private ItemState _currentState;

    public void ClotheToDry()
    {
        transform.GetChild(1).gameObject.SetActive(false);
        transform.GetChild(2).gameObject.SetActive(true);
    }
    public void DeleteDirt()
    {
        transform.GetChild(0).gameObject.SetActive(false);
    }
}
public enum ItemState
{
    none,
    dirty,
    wet,
    clean,
    good,
    rotten,
    poisoned,
    paperTrash,
    plasticTrash,
    glassTrash,
    mixTrash,
    paperBin,
    plasticBin,
    glassBin,
    mixBin
}
