using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using DG.Tweening;

public class SinkManager : MonoBehaviour
{
    [SerializeField] private GameObject _kran;
    private bool _isRunningWater = false;
    [SerializeField] private ParticleSystem _foam;
    [SerializeField] private VisualEffect _water;
    private AudioSource _audioSource;
    public bool IsRunningWater
    {
        get => _isRunningWater;
        set
        {
            _isRunningWater = value;
            
        }
    }
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }
    public void TurnWater()
    {
        _isRunningWater = !_isRunningWater;

        if (_isRunningWater)
        {
            _foam.Play();
            _water.Play();
            _kran.transform.DOLocalRotate(new Vector3(90f, 0f, 0f), 0.5f);
            _audioSource.Play();
        }
        else
        {
            _foam.Stop();
            _water.Stop();
            _kran.transform.DOLocalRotate(new Vector3(0f, 0f, 0f), 0.5f);
            _audioSource.Stop();
        }
    }
}
