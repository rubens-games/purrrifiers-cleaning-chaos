using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraController : MonoBehaviour
{
    public float LookSpeed = 3.0f; //Do barrel roll!
    public float MoveSpeed = 10.0f; //Normal Speed
    public float FastMoveSpeed = 50.0f; //Need 4 Speed

    private float _rotationX = 0.0f;
    private float _rotationY = 0.0f;

    private Vector3 _initialPosition; //Cameras starting position
    private Quaternion _initialRotation; //Cameras sexual orientation

    void Start()
    {
        //Starting pos camera
        _initialPosition = transform.position;
        _initialRotation = transform.rotation;

        //Angles on start
        Vector3 eulerAngles = transform.eulerAngles;
        _rotationX = eulerAngles.y;
        _rotationY = eulerAngles.x;
    }

    void Update()
    {
        //Reset camera on start
        if (Time.frameCount == 1)
        {
            transform.SetPositionAndRotation(_initialPosition, _initialRotation);
        }

        //If I press right button on my mouse I can look around
        if (Input.GetMouseButton(1))
        {
            _rotationX += Input.GetAxis("Mouse X") * LookSpeed;
            _rotationY -= Input.GetAxis("Mouse Y") * LookSpeed;
            _rotationY = Mathf.Clamp(_rotationY, -90, 90); //Verticals is limited, Ha!

            transform.localRotation = Quaternion.Euler(_rotationY, _rotationX, 0);
        }

        //Speed speed lover!
        float speed = Input.GetKey(KeyCode.LeftShift) ? FastMoveSpeed : MoveSpeed;

        if (Input.GetKey(KeyCode.W))
        {
            transform.position += speed * Time.deltaTime * transform.forward;
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position -= speed * Time.deltaTime * transform.forward;
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.position -= speed * Time.deltaTime * transform.right;
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.position += speed * Time.deltaTime * transform.right;
        }
    }
}
