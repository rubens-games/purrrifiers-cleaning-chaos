using System;
using System.Collections;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class DishwasherNEO : MonoBehaviour
{
    // Lost in Sos
    public AudioSource[] AudioSos;

    // Twin
    public DOTweenAnimation[] Anim;
    public DOTweenAnimation[] ButtonAnim;

    // Booler gun! GO!
    private bool _isDoorOpen = false;
    private bool _isTopDishRackOpen = false;
    private bool _isLowerDishRackOpen = false;
    private bool _isSprayArmOn = false;
    private bool _isSoapDispenserOpen = false;
    private bool _isFilterDispenserOpen = false;

    //Text-time
    public TextMeshPro TimerText;

    //Button
    public GameObject[] Buttons;

    //Timer
    private readonly int[] _durations = { 15, 30, 60, 120 };
    private float _remainingTime;
    private bool _isTimerRunning = false;
    private bool _isTimerPaused = false;

    //SprayArms
    public float SprayArmsInterval = 1f;
    private float _timeSinceLastSprayArmsCall;

    void Start()
    {
        if (TimerText == null)
        {
            return;
        }

        foreach (var button in Buttons)
        {
            if (button == null)
            {
                continue;
            }

            var buttonHandler = button.AddComponent<DWButtons4Buttons>();
            buttonHandler.TimerIndex = System.Array.IndexOf(Buttons, button);
            buttonHandler.OnButtonClicked += HandleButtonClick;
        }
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                switch (hit.collider.tag)
                {
                    case "DWDoor":
                        ToggleDoor();
                        break;
                    case "DWUpperDishRack":
                        ToggleTopDishRack();
                        break;
                    case "DWLowerDishRack":
                        ToggleLowerDishRack();
                        break;
                    case "DWSoapDispenser":
                        ToggleSoapDispenser();
                        break;
                    case "DWFilterDispencer":
                        TogggleFilterDispenser();
                        break;
                    default:
                        break;
                }
            }
        }
        TimeUpdate();
    }

    void ToggleDoor()
    {
        if (!_isTopDishRackOpen && !_isLowerDishRackOpen && !_isSoapDispenserOpen && !_isFilterDispenserOpen)
        {
            _isDoorOpen = !_isDoorOpen;

            if (_isDoorOpen)
            {
                PlayAnim(0);
            }
            else
            {
                BackAnim(0);
            }

            PlaySound(0);
        }
    }

    void ToggleTopDishRack()
    {
        if (_isDoorOpen)
        {
            _isTopDishRackOpen = !_isTopDishRackOpen;

            if (_isTopDishRackOpen)
            {
                PlayAnim(1);
            }
            else
            {
                BackAnim(1);
            }

            PlaySound(1);
        }
    }

    void ToggleLowerDishRack()
    {
        if (_isDoorOpen)
        {
            _isLowerDishRackOpen = !_isLowerDishRackOpen;

            if (_isLowerDishRackOpen)
            {
                PlayAnim(2);
            }
            else
            {
                BackAnim(2);
            }

            PlaySound(2);
        }
    }

    public void ToggleSprayArms()
    {
        _isSprayArmOn = !_isSprayArmOn;

        if (_isSprayArmOn)
        {
            PlayAnim(3);
            PlayAnim(4);
        }
        else
        {
            BackAnim(3);
            BackAnim(4);
        }
    }

    void ToggleSoapDispenser()
    {
        if (_isDoorOpen)
        {
            _isSoapDispenserOpen = !_isSoapDispenserOpen;

            if (_isSoapDispenserOpen)
            {
                PlayAnim(5);
            }
            else
            {
                BackAnim(5);
            }

            PlaySound(3);
        }
    }

    void TogggleFilterDispenser()
    {
        if (_isDoorOpen)
        {
            _isFilterDispenserOpen = !_isFilterDispenserOpen;

            if (_isFilterDispenserOpen)
            {
                PlayAnim(6);
            }
            else
            {
                BackAnim(6);
            }
        }
    }

    void PlaySound(int index)
    {
        if (index >= 0 && index < AudioSos.Length)
        {
            AudioSos[index].Play();
        }
    }

    void PlayAnim(int index)
    {
        if (index >= 0 && index < Anim.Length)
        {
            Anim[index].DOPlayForward();
        }
    }

    void BackAnim(int index)
    {
        if (index >= 0 && index < Anim.Length)
        {
            Anim[index].DOPlayBackwards();
        }
    }
    void TimeUpdate()
    {
        if (_isTimerRunning && !_isTimerPaused)
        {
            if (_isDoorOpen)
            {
                PauseTimer();
                return;
            }

            _timeSinceLastSprayArmsCall += Time.deltaTime;

            if (_timeSinceLastSprayArmsCall >= SprayArmsInterval)
            {
                ToggleSprayArms();
                _timeSinceLastSprayArmsCall = 0f;
            }

            _remainingTime -= Time.deltaTime;
            UpdateTimerDisplay();

            if (_remainingTime <= 0)
            {
                _isTimerRunning = false;
                TimerText.fontSize = 0.2f;
                TimerText.text = "Done";
                _remainingTime = 0;
            }
        }
    }

    void HandleButtonClick(int buttonIndex)
    {
        if (_isDoorOpen)
        {
            TimerText.fontSize = 0.2f;
            TimerText.text = "Error";
            PlaySound(4);
            return;
        }

        if (buttonIndex < 0 || buttonIndex >= Buttons.Length)
        {
            return;
        }

        if (buttonIndex == 4)
        {
            SprayPlayAnim(buttonIndex);
            ResetTimer();
            PlaySound(4);
        }
        else if (buttonIndex == 5)
        {
            SprayPlayAnim(buttonIndex);
            PauseTimer();
            PlaySound(4);
        }
        else
        {
            StartTimer(buttonIndex);
            SprayPlayAnim(buttonIndex);
            PlaySound(4);
        }
    }

    void StartTimer(int buttonIndex)
    {
        if (buttonIndex < 0 || buttonIndex >= _durations.Length)
        {
            return;
        }

        _timeSinceLastSprayArmsCall = 0f;
        _remainingTime = _durations[buttonIndex];
        TimerText.fontSize = 0.2f;
        _isTimerRunning = true;
        _isTimerPaused = false;
        UpdateTimerDisplay();
    }

    void ResetTimer()
    {
        _timeSinceLastSprayArmsCall = 0f;
        _isTimerRunning = false;
        _remainingTime = 0;
        TimerText.fontSize = 0.2f;
        TimerText.text = "Done";
    }

    void PauseTimer()
    {
        _isTimerPaused = !_isTimerPaused;
        TimerText.fontSize = 0.2f;
        TimerText.text = _isTimerPaused ? "Paused" : string.Format("{0:00}:{1:00}", Mathf.FloorToInt(_remainingTime / 60), Mathf.FloorToInt(_remainingTime % 60));
    }

    void UpdateTimerDisplay()
    {
        int minutes = Mathf.FloorToInt(_remainingTime / 60);
        int seconds = Mathf.FloorToInt(_remainingTime % 60);
        TimerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    void SprayPlayAnim(int index)
    {
        if (index >= 0 && index < ButtonAnim.Length)
        {
            ButtonAnim[index].DOPlayForward();
            StartCoroutine(PlayBackwardAfterDelay(index, 0.3f));
        }
    }

    IEnumerator PlayBackwardAfterDelay(int index, float delay)
    {
        yield return new WaitForSeconds(delay);
        ButtonAnim[index].DOPlayBackwards();
    }
}
