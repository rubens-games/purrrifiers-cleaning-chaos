using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DWButtons4Buttons : MonoBehaviour
{
    public int TimerIndex;
    public delegate void ButtonClicked(int index);
    public event ButtonClicked OnButtonClicked;

    private void OnMouseDown()
    {
        OnButtonClicked?.Invoke(TimerIndex);
    }
}
