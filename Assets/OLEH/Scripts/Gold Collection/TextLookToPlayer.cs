using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextLookToPlayer : MonoBehaviour
{
    private Camera _mainCamera;

    void Start()
    {
        _mainCamera = Camera.main;
    }

    void Update()
    {
        if (_mainCamera != null)
        {
            Vector3 directionToCamera = _mainCamera.transform.position - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(directionToCamera);
            transform.rotation = Quaternion.Euler(0, lookRotation.eulerAngles.y - 180, 0);
        }
    }
}
