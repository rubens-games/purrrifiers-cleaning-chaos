using UnityEngine;
using TMPro;
using DG.Tweening;

public class MoneyCollect : MonoBehaviour
{
    public float FloatDistance = 0.3f;
    public float Duration = 0.4f;

    void LateUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                if (hit.collider.CompareTag("Gold"))
                {
                    ShowFloatingText(hit.collider.transform.position);
                    hit.collider.gameObject.SetActive(false);
                }
            }
        }
    }

    void ShowFloatingText(Vector3 position)
    {
        GameObject floatingText = ObjPool.Instance.GetPooledObject();
        floatingText.transform.position = position;
        floatingText.SetActive(true);

        TextMeshProUGUI textMesh = floatingText.GetComponentInChildren<TextMeshProUGUI>();
        textMesh.text = "200$";
        textMesh.alpha = 1;

        floatingText.transform.DOMoveY(position.y + FloatDistance, Duration)
            .SetEase(Ease.OutCubic);

        textMesh.DOFade(0, Duration).SetEase(Ease.InCubic)
            .OnComplete(() => floatingText.SetActive(false));
    }
}
