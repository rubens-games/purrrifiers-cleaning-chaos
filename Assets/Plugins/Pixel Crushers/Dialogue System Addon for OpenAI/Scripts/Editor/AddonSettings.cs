// Copyright (c) Pixel Crushers. All rights reserved.

using System;

namespace PixelCrushers.DialogueSystem.OpenAIAddon
{

    [Serializable]
    public class AddonSettings
    {

        public bool overrideBaseURL = false;
        public string baseURL;

    }

}