using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class UniwersalMovable : MonoBehaviour
{
    [SerializeField] private bool _doJump;
    private AudioSource _audioSource;
    [SerializeField] private List<AudioClip> _clips;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }
    public void MoveMyChild(Transform objectToMove)
    {
        PlayRandomSFX();
        objectToMove.DOLocalRotate(Vector3.zero, 0.5f, RotateMode.Fast);
        
        if (_doJump) objectToMove.DOLocalJump(Vector3.zero, 1f, 1, 0.5f);
        else objectToMove.DOLocalMove(Vector3.zero, 0.5f);
        
        QuestCustomManager.instance.MoveFurniture();
    }
    private void PlayRandomSFX()
    {
        int randomIndex = Random.Range(0, _clips.Count);
        AudioClip footstep = _clips[randomIndex];

        _audioSource.clip = footstep;
        _audioSource.Play();
    }
}
