using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserSFX : MonoBehaviour
{
    private AudioSource _audioSource;
    private void Awake()
    {
        _audioSource=GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        _audioSource.Play();
    }

    private void OnDisable()
    {
        _audioSource.Stop();
    }
}
