using System.Collections;
using System.Collections.Generic;
using TheraBytes.BetterUi;
using UnityEngine;

public class RadioMusic : MonoBehaviour
{
    private AudioSource _audioSource;
    private bool _isPlaying = true;
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }
    public void PlayStop()
    {
        _isPlaying = !_isPlaying;

        if (_isPlaying) _audioSource.Play();
        else _audioSource.Stop();
    }
}
