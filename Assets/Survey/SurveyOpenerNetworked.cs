﻿using FishNet.Object;
using PlayerSystem;
using UnityEngine.SceneManagement;
using Zenject;

namespace Survey
{
    public class SurveyOpenerNetworked : NetworkBehaviour, IPlayerLockHandler
    {
        [Inject] private PlayerManager _playerManager;
        
        public void OpenSurvey() => OpenSurveyServerRpc();
        
        [ServerRpc(RequireOwnership = false)] 
        private void OpenSurveyServerRpc() => OpenSurveyObserverRpc();

        [ObserversRpc]
        private void OpenSurveyObserverRpc()
        {
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("Survey", LoadSceneMode.Additive)!.completed += operation =>
            {
                _playerManager?.localPlayer.playerLock.Register(this);
            };
        }

        public void CloseSurvey()
        {
            UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("Survey")!.completed += operation =>
            {
                _playerManager?.localPlayer.playerLock.Unregister(this);
            };
        }
        
        #region IPlayerLockHandler

        public bool isLocked => true;

        #endregion
    }
}