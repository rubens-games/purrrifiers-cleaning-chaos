﻿using System.Collections.Generic;
using NoReleaseDate.Common.Runtime.Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace Survey
{
    public class CheckBoxQuestion : Question
    {
        public override string Answer
        {
            get
            {
                var answer = string.Empty;
                
                foreach (var toggle in _toggles)
                {
                    if (!toggle.Key.isOn) continue;
                    
                    answer += $"{toggle.Value.text},";
                }
                
                return answer.Length > 0 ? answer.Substring(0, answer.Length - 1) : null;
            }
        }

        [SerializeField] private bool _isMultipleChoice;
        
        private Dictionary<Toggle, Text> _toggles = new();

        private void Awake()
        {
            _toggles = new Dictionary<Toggle, Text>();
            foreach (var toggle in GetComponentsInChildren<Toggle>())
                _toggles.Add(toggle, toggle.GetComponentInChildren<Text>());
        }

        private void OnEnable()
        {
            foreach (var toggle in _toggles.Keys)
                toggle.onValueChanged.AddListener(isOn => OnUserToggle(toggle, isOn));
        }
        
        private void OnDisable()
        {
            foreach (var toggle in _toggles.Keys) toggle.onValueChanged.RemoveAllListeners();
        }
        
        private void OnUserToggle(Toggle toggle, bool isOn)
        {
            SubmitButton.OnTryEnableSubmitButton.Invoke();
            
            if (_isMultipleChoice) return;

            if (!isOn) return;
            
            foreach (var otherToggle in _toggles)
            {
                if (otherToggle.Key == toggle) continue;
                
                otherToggle.Key.isOn = false;
            }
        }
    }
}