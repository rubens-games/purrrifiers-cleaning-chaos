using TMPro;
using UnityEngine;

namespace Survey
{
    public class TextQuestion : Question
    {
        [SerializeField] private TMP_InputField _tmpInputField;

        private void Awake()
        {
            _tmpInputField.onValueChanged.AddListener(OnUserWriteAnswer);
        }

        private void OnUserWriteAnswer(string userAnswer)
        {
            SubmitButton.OnTryEnableSubmitButton.Invoke();
        }

        public override string Answer => _tmpInputField.text;
    }
}
