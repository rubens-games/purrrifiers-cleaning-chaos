using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Survey
{
    public class SubmitButton : MonoBehaviour
    {
        public static UnityEvent OnTryEnableSubmitButton = new();

        [SerializeField] private Survey _survey;
        [Space]
        [SerializeField] private Color _colorEnable = Color.green;
        [SerializeField] private Color _colorDisable = new Color(0.8f, 0.8f, 0.8f);
        [SerializeField] private Image _bcgImage;
        [SerializeField] private Image _mouseShadowImage;
        [SerializeField] private WindowBox _windowBox;
        [Space] 
        [SerializeField] private ScrollToTarget _scrollToTarget;

        private OnMouseEvents _onMouseEvents;
        private bool _isSurveyEmpty = true;

        [SerializeField] private int _sendLimitToFindEmptyQuestion = 1;

        private int _sendTryCount = 0;

        private void Awake()
        {
            _onMouseEvents = GetComponent<OnMouseEvents>();
            
            OnTryEnableSubmitButton.AddListener(TryEnableSubmitButton);
            
            _onMouseEvents.OnMouseClick.AddListener(OnUserClickButton);
            _onMouseEvents.OnMouseEnter.AddListener(OnUserHoverButton);
            _onMouseEvents.OnMouseExit.AddListener(OnUserDeHoverButton);
            _onMouseEvents.OnMouseMove.AddListener(OnUserMoveMouse);
        }

        private void TryEnableSubmitButton()
        {
            foreach (var question in _survey.Questions)
            {
                if (question.IsAnswerEmpty)
                {
                    _bcgImage.color = _colorDisable;
                    _isSurveyEmpty = true;
                    // Debug.Log($"IsEmpty:{question.IsAnswerEmpty} - {question.QuestionText} - ODP: {question.Answer}", question);
                }
                else
                {
                    _bcgImage.color = _colorEnable;
                    _isSurveyEmpty = false;
                    // Debug.Log($"IsEmpty:{question.IsAnswerEmpty} - {question.QuestionText} - ODP: {question.Answer}", question);
                    break;
                }
            }
        }

        private void OnUserClickButton(PointerEventData pointerEventData)
        {
            if (_isSurveyEmpty)
            {
                transform.DOShakePosition(0.1f, 5);
                _sendTryCount = 0;
            }
            else
            {
                // Find empty question to animation
                if (_sendTryCount < _sendLimitToFindEmptyQuestion)
                {
                    _sendTryCount++;
                    
                    var target = _survey.Questions.Where(question => question.IsAnswerEmpty).ToList();
                    if (target.Count > 0)
                    {
                        _scrollToTarget.ScrollTo(target[0].GetComponent<RectTransform>());
                        StartAnimation(target[0].transform);
                        return;
                    }
                }
                
                
                _survey.SendSurvey();
                _windowBox.Open();
            }
        }

        private void OnUserMoveMouse(PointerEventData pointerEventData)
        {
            _mouseShadowImage.transform.position = pointerEventData.position;
        }

        private void OnUserHoverButton(PointerEventData pointerEventData)
        {
            transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.1f);
        }
        
        private void OnUserDeHoverButton(PointerEventData pointerEventData)
        {
            transform.DOScale(Vector3.one, 0.1f);
        }

        #region EmptyQuestionAnimation

        private void StartAnimation(Transform target)
        {
            target.DOScale(new Vector3(1.05f, 1.05f, 1.05f), 0.3f)
                .SetLoops(4, LoopType.Yoyo);
        }

        #endregion
    }
}
