using System;
using System.Collections;
using System.Linq;
using CursorSystem;
using UnityEngine;
using UnityEngine.Networking;
using Zenject;

namespace Survey
{
    public class Survey : MonoBehaviour, ICursorHandler
    {
        public Question[] Questions { get; private set; }

        public CursorSettings CursorSettings => CursorSettings.ShowAndConfined;

        [Inject] private CursorManager _cursorManager;
        
        private const string URL =
            "https://docs.google.com/forms/d/e/1FAIpQLScLyDa6zh_fHcG6mxnm__v-6osJBrrtT_A1wcVSxRTKEnCWog/formResponse";

        private void Awake()
        {
            Questions = GetComponentsInChildren<Question>();
        }

        private void OnEnable() => _cursorManager.Register(this);

        private void OnDisable() => _cursorManager.Unregister(this);

        public void SendSurvey() => StartCoroutine(RequestToSendSurvey());

        private WWWForm GetFormWithAnswers()
        {
            var form = new WWWForm();
            foreach (var question in Questions.Where(question => !question.IsAnswerEmpty))
                form.AddField(question.EntryNr, question.Answer);

            return form;
        }

        private UnityWebRequest GetURLWithAnswers()
        {
            var urlWithAnswers = UnityWebRequest.Post(URL, GetFormWithAnswers());
            
            return urlWithAnswers;
        }

        private IEnumerator RequestToSendSurvey()
        {
            var urlWithAnswers = GetURLWithAnswers();

            Debug.Log($"Sending survey to {URL}");
            
            yield return urlWithAnswers.SendWebRequest();

            if (urlWithAnswers.result == UnityWebRequest.Result.Success) 
                Debug.Log("Form upload complete!");
            else Debug.LogError(urlWithAnswers.error);
        }
    }
}
