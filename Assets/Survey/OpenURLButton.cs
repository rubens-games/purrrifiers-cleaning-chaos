using UnityEngine;
using UnityEngine.UI;

namespace Survey
{
    [RequireComponent(typeof(Button))]
    public class OpenURLButton : MonoBehaviour
    {
        public string URL;

        private Button _button;
    
        private void Awake() => _button = GetComponent<Button>();

        private void OnEnable()
        {
            if (_button) _button.onClick.AddListener(OnButtonClicked);
        }
    
        private void OnDisable()
        {
            if (_button) _button.onClick.RemoveListener(OnButtonClicked);
        }
    
        private void OnButtonClicked() => OpenURL();

        public void OpenURL() => OpenURL(URL);
    
        public void OpenURL(string url) => Application.OpenURL(url);
    }
}
