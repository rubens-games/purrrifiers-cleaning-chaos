using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Survey
{
    public class SliderQuestion : Question
    {
        public override string Answer => _slider.value >= 1 ? $"{_slider.value}" : null;
        
        [SerializeField] private TextMeshProUGUI _tmpAnswerText;
        [SerializeField] private Slider _slider;

        private void Awake()
        {
            _slider.onValueChanged.AddListener(OnUserMoveSlider);
        }

        private void OnUserMoveSlider(float sliderValue)
        {
            _tmpAnswerText.text = $"{sliderValue}/10";
            
            SubmitButton.OnTryEnableSubmitButton.Invoke();
        }
    }
}
