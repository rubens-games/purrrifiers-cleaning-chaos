using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Survey
{
    public class FillEvaluationFormButton : MonoBehaviour
    {
        [SerializeField] private WindowBox _windowBox;
        [SerializeField] private Transform _mouseShadow;
        [SerializeField] private Image _targetImage;
        [SerializeField] private Color _normalColour;
        [SerializeField] private Color _hoverColour;
        
        private OnMouseEvents _onMouseEvents;
        

        private void Awake()
        {
            _onMouseEvents = GetComponent<OnMouseEvents>();
            
            _onMouseEvents.OnMouseClick.AddListener(OnMouseClick);
            _onMouseEvents.OnMouseEnter.AddListener(OnMouseEnter);
            _onMouseEvents.OnMouseExit.AddListener(OnMouseExit);
            _onMouseEvents.OnMouseMove.AddListener(OnMouseMove);
        }

        private void OnDestroy()
        {
            _onMouseEvents.OnMouseClick.RemoveListener(OnMouseClick);
            _onMouseEvents.OnMouseEnter.RemoveListener(OnMouseEnter);
            _onMouseEvents.OnMouseExit.RemoveListener(OnMouseExit);
            _onMouseEvents.OnMouseMove.RemoveListener(OnMouseMove);
            
            DOTween.Kill("FillEvaluationFormButton_DOScale0");
        }

        private void OnValidate()
        {
            _targetImage.color = _normalColour;
        }

        private void OnMouseClick(PointerEventData arg0)
        {
            _windowBox.Close();
        }

        private void OnMouseEnter(PointerEventData arg0)
        {
            _mouseShadow.gameObject.SetActive(true);
            _targetImage.DOColor(_hoverColour, 0.1f);
            _targetImage.DOFade(1, 0.1f);
            transform.DOScale(1.03f, 0.2f)
                .SetLoops(-1, LoopType.Yoyo)
                .SetId("FillEvaluationFormButton_DOScale0");
        }

        private void OnMouseExit(PointerEventData arg0)
        {
            _mouseShadow.gameObject.SetActive(false);
            DOTween.Kill("FillEvaluationFormButton_DOScale0");
            transform.DOScale(1, 0.1f);
            _targetImage.DOFade(0.3f, 0.1f);
            _targetImage.DOColor(_normalColour, 0.1f);
        }

        private void OnMouseMove(PointerEventData arg0)
        {
            _mouseShadow.position = arg0.position;
        }
    }
}
