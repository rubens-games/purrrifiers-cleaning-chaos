﻿using DG.Tweening;
using UnityEngine;

namespace Survey
{
    public class SimplePopUpAnimation : MonoBehaviour
    {
        [SerializeField] private Vector3 _scale = Vector3.one * 1.1f;
        [SerializeField] private float _duration = 0.5f;
        [SerializeField] private Ease _ease = Ease.OutBack;
        [SerializeField] private int _loops = -1;

        private RectTransform _rectTransform;
        private Tweener _tween;

        private void Awake() => _rectTransform = GetComponent<RectTransform>();

        private void OnEnable() => _tween = _rectTransform.DOScale(_scale, _duration).SetEase(_ease).SetLoops(_loops, LoopType.Yoyo);

        private void OnDisable() => _tween.Kill();
    }
}