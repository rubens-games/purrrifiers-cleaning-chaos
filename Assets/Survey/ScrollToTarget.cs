﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Survey
{
    public class ScrollToTarget : MonoBehaviour
    {
        [SerializeField] private ScrollRect _scrollRect;
        [SerializeField] private RectTransform _content;
        [SerializeField] private RectTransform _target;
        
        public void ScrollTo(RectTransform target)
        {
            _target = target;
            
            if (_target == null) return;
            
            RebuildUI();
            
            var viewportLocalPosition = _scrollRect.viewport.localPosition;
            var targetLocalPosition = _target.localPosition;

            var targetParent = target.parent;
            while (targetParent != null && targetParent != _content)
            {
                targetLocalPosition += targetParent.localPosition;
                targetParent = targetParent.parent;
            }

            var newContentLocalPosition = new Vector2(
                0 - (viewportLocalPosition.x + targetLocalPosition.x),
                0 - (viewportLocalPosition.y + targetLocalPosition.y)
            );
            
            _content.DOLocalMove(newContentLocalPosition, 0.2f).onComplete += RebuildUI;
        }
        
        
        // public void ScrollTo(RectTransform target)
        // {
        //     _target = target;
        //     
        //     if (_target == null) return;
        //     
        //     RebuildUI();
        //     
        //     var viewportLocalPosition = _scrollRect.viewport.localPosition;
        //     var targetLocalPosition = _target.localPosition;
        //
        //     var newContentLocalPosition = new Vector2(
        //         0 - (viewportLocalPosition.x + targetLocalPosition.x),
        //         0 - (viewportLocalPosition.y + targetLocalPosition.y)
        //     );
        //     
        //     _content.DOLocalMove(newContentLocalPosition, 0.2f).onComplete += RebuildUI;
        // }
        
        private void RebuildUI()
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(_target);
            LayoutRebuilder.ForceRebuildLayoutImmediate(_content);
            LayoutRebuilder.ForceRebuildLayoutImmediate(_scrollRect.viewport);
            LayoutRebuilder.ForceRebuildLayoutImmediate(_scrollRect.transform as RectTransform);
            Canvas.ForceUpdateCanvases();
        }
    }
}