using UnityEngine;

namespace Survey
{
    public abstract class Question : MonoBehaviour
    {
        public abstract string Answer { get; }
        
        public virtual bool IsAnswerEmpty => string.IsNullOrEmpty(Answer) || string.IsNullOrWhiteSpace(Answer);
        
        [field: SerializeField] public string EntryNr { get; private set; }
    }
}
