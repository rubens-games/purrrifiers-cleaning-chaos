using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Survey
{
    public class OpenCloseWindowButton : MonoBehaviour
    {
        [SerializeField] private WindowBox _windowBox;
        
        private OnMouseEvents _onMouseEvents;

        private void Awake()
        {
            _onMouseEvents = GetComponent<OnMouseEvents>();
            
            _onMouseEvents.OnMouseClick.AddListener(OnMouseClick);
            _onMouseEvents.OnMouseEnter.AddListener(OnMouseEnter);
            _onMouseEvents.OnMouseExit.AddListener(OnMouseExit);
        }

        private void OnMouseClick(PointerEventData arg0)
        {
            _windowBox.Open();
        }

        private void OnMouseEnter(PointerEventData arg0)
        {
            transform.DOScale(1.05f, 0.1f);
        }

        private void OnMouseExit(PointerEventData arg0)
        {
            transform.DOScale(1f, 0.1f);
        }
    }
}
