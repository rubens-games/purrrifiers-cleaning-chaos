using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Survey
{
    public class WindowBox : MonoBehaviour
    {
        [SerializeField] private Transform _content;
        [SerializeField] private Image _bcgImage;

        public void Open()
        {
            _content.localScale = Vector3.zero;
            var bcgImageColor = _bcgImage.color;
            bcgImageColor.a = 0;
            // _bcgImage.color = bcgImageColor;
            
            gameObject.SetActive(true);
            _content.DOScale(1, 0.1f).onComplete += () =>
                _bcgImage.DOFade(1, 0.1f);
        }

        public void Close()
        {
            _content.DOScale(0, 0.1f).onComplete += () =>
                _bcgImage.DOFade(0, 0.5f).onComplete += () => 
                    gameObject.SetActive(false);
        }
    }
}
