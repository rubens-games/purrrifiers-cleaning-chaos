﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Survey
{
    public class BackToGameButton : MonoBehaviour
    {
        [SerializeField] private Transform _mouseShadow;
        [SerializeField] private Image _targetImage;
        [SerializeField] private Color _normalColour;
        [SerializeField] private Color _hoverColour;
        [SerializeField] private Vector2 _randomAreaRangeFromCenter;
        [SerializeField] private int _tryClose;
        [SerializeField] private bool _positiveAnimation;

        private OnMouseEvents _onMouseEvents;
        private Vector3 _closeButtonStartPosition;
        private int _tryCloseCount;

        private void OnValidate()
        {
            _targetImage.color = _normalColour;
        }

        private void Awake()
        {
            _closeButtonStartPosition = transform.localPosition;
            
            _onMouseEvents = GetComponent<OnMouseEvents>();
            
            _onMouseEvents.OnMouseClick.AddListener(OnMouseClick);
            _onMouseEvents.OnMouseEnter.AddListener(OnMouseEnter);
            _onMouseEvents.OnMouseExit.AddListener(OnMouseExit);
            _onMouseEvents.OnMouseMove.AddListener(OnMouseMove);
        }

        private void OnEnable()
        {
            transform.localPosition = _closeButtonStartPosition;
            _tryCloseCount = 0;
        }

        private void OnDestroy()
        {
            _onMouseEvents.OnMouseClick.RemoveListener(OnMouseClick);
            _onMouseEvents.OnMouseEnter.RemoveListener(OnMouseEnter);
            _onMouseEvents.OnMouseExit.RemoveListener(OnMouseExit);
            _onMouseEvents.OnMouseMove.RemoveListener(OnMouseMove);
            
            DOTween.Kill("BackToMenuButton_DOShakePosition0");
        }

        private void OnMouseClick(PointerEventData arg0)
        {
            if (_tryCloseCount < _tryClose)
            {
                _tryCloseCount++;

                var x = Random.Range(_randomAreaRangeFromCenter.x, -_randomAreaRangeFromCenter.x);
                var y = Random.Range(_randomAreaRangeFromCenter.y, -_randomAreaRangeFromCenter.y);

                var newPosition = new Vector2(x, y);
                transform.DOLocalMove(newPosition, 0.2f, snapping: true);
            }
            else SurveyOpener.instance.CloseSurvey();
        }

        private void OnMouseMove(PointerEventData arg0)
        {
            _mouseShadow.position = arg0.position;
        }

        private void OnMouseEnter(PointerEventData arg0)
        {
            _mouseShadow.gameObject.SetActive(true);
            _targetImage.DOColor(_hoverColour, 0.1f);
            _targetImage.DOFade(1, 0.1f);

            if (_positiveAnimation) transform.DOScale(1.05f, 0.1f);
            else
            {
                transform.DOShakeRotation(0.2f, 3f, fadeOut: false)
                    .SetLoops(-1).SetId("BackToMenuButton_DOShakePosition0");
            }
            
        }

        private void OnMouseExit(PointerEventData arg0)
        {
            _mouseShadow.gameObject.SetActive(false);
            _targetImage.DOFade(0.3f, 0.1f);
            _targetImage.DOColor(_normalColour, 0.1f);

            if (_positiveAnimation) transform.DOScale(1, 0.1f);
            else
            {
                DOTween.Kill("BackToMenuButton_DOShakePosition0");
                transform.DORotate(Vector3.zero, 0.1f);
            }
        }
    }
}