using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Survey
{
    public class OnMouseEvents : MonoBehaviour, IPointerEnterHandler, IPointerMoveHandler, IPointerExitHandler,
        IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        public UnityEvent<PointerEventData> OnMouseEnter = new();
        public UnityEvent<PointerEventData> OnMouseMove = new();
        public UnityEvent<PointerEventData> OnMouseExit = new();
        public UnityEvent<PointerEventData> OnMouseClick = new();
        public UnityEvent<PointerEventData> OnMouseDown = new();
        public UnityEvent<PointerEventData> OnMouseUp = new();
        
        // OnPointerEnter
        public void OnPointerEnter(PointerEventData eventData) => OnMouseEnter.Invoke(eventData);

        // OnPointerMove
        public void OnPointerMove(PointerEventData eventData) => OnMouseMove.Invoke(eventData);

        // OnPointerExit
        public void OnPointerExit(PointerEventData eventData) => OnMouseExit.Invoke(eventData);

        // OnPointerClick
        public void OnPointerClick(PointerEventData eventData) => OnMouseClick.Invoke(eventData);

        // OnPointerDrag
        public void OnPointerDown(PointerEventData eventData) => OnMouseDown.Invoke(eventData);

        // OnPointerDrop
        public void OnPointerUp(PointerEventData eventData) => OnMouseUp.Invoke(eventData);
    }
}
