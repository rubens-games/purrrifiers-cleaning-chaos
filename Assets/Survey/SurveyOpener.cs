﻿using PixelCrushers.DialogueSystem;
using SingletonSystem.Runtime;
using UnityEngine.SceneManagement;

namespace Survey
{
    public class SurveyOpener : Singleton<SurveyOpener>
    {
        private SurveyOpenerNetworked _surveyOpenerNetworked;

        protected override void Awake()
        {
            base.Awake();
            
            _surveyOpenerNetworked = GetComponent<SurveyOpenerNetworked>();
        }

        private void OnEnable()
        {
            Lua.RegisterFunction("OpenSurvey", this, SymbolExtensions.GetMethodInfo(() => OpenSurvey()));
        }

        private void OnDisable()
        {
            Lua.UnregisterFunction("OpenSurvey");
        }
        
        public void OpenSurvey()
        {
            if (_surveyOpenerNetworked) _surveyOpenerNetworked.OpenSurvey();
            else OpenSurveyLocal();
        }

        public void CloseSurvey()
        {
            if (_surveyOpenerNetworked) _surveyOpenerNetworked.CloseSurvey();
            else CloseSurveyLocal();
        }
        
        private void OpenSurveyLocal()
        {
            SceneManager.LoadSceneAsync("Survey", LoadSceneMode.Additive);
        }

        public void CloseSurveyLocal()
        {
            SceneManager.UnloadSceneAsync("Survey");
        }
    }
}