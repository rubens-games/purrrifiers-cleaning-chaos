using System.Collections;
using UnityEngine;
using SingletonSystem.Runtime;
using UISystem.Monologue;

/// <summary>
/// This class is responsible for managing the monologue system.
/// Use MonologueManager.instance.PlayMonologue(MonologueSO) to play a monologue.
/// </summary>
public class MonologueManager : Singleton<MonologueManager>
{
    [Header("Debug")] [SerializeField] private MonologueSO _debugMonologueSo;
    
    private MonologueUI _monologueUI;
    private AudioSource _audioSource;
    private Coroutine _lastMonologueCoroutine;

    protected override void Awake()
    {
        base.Awake();
        
        _monologueUI = GetComponentInChildren<MonologueUI>();
        _audioSource = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Play a monologue.
    /// </summary>
    /// <param name="monologueSo">Monologue to play</param>
    public void PlayMonologue(MonologueSO monologueSo)
    {
        if (_lastMonologueCoroutine != null)
        {
            StopCoroutine(_lastMonologueCoroutine);
            _audioSource.Stop();
            _monologueUI.HideUI();
        }
        
        _lastMonologueCoroutine = StartCoroutine(PlayMonologueCoroutine(monologueSo));
    }
    
    private IEnumerator PlayMonologueCoroutine(MonologueSO monologueSo)
    {
        _audioSource.clip = monologueSo.audioClip;
        
        _audioSource.Play();
        
        _monologueUI.UpdateText(monologueSo.txt);
        
        // Wait for update Text
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        
        _monologueUI.ShowUI();

        yield return new WaitForSeconds(monologueSo.audioClip.length + 1f);
        
        _audioSource.Stop();
        
        _monologueUI.HideUI();

        _lastMonologueCoroutine = null;
    }
    
    [ContextMenu("Debug Play Monologue")]
    private void DebugPlayMonologue() => PlayMonologue(_debugMonologueSo);
}
