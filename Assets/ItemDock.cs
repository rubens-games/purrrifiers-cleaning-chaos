using System;
using DG.Tweening;
using InteractionSystem;
using PickableSystem;
using PlayerSystem;
using UnityEngine;
using Zenject;

public class ItemDock : Interactable
{
    [SerializeField] private ItemSO _myItemSlot;
    [Inject] private PlayerManager _playerManager;
    [SerializeField] private ItemState _requiredState;
    public ItemState RequiredState { 
        get { return _requiredState; } 
        set { _requiredState = value; }
    }
    [SerializeField] private bool _isAutoDisable = false;
    [SerializeField] private Vector3 _offsetForItem;
    [SerializeField] private Quaternion _offsetRotation;
    [SerializeField] private MeshRenderer _meshToDisable;

    [SerializeField] private bool _isSingle = true;
    [SerializeField] private bool _isMultiUseItem = false;
    public bool hasItem => transform.childCount > 0;

    public override bool IsSleeping => IsSleepingDock;
    public bool IsSleepingDock 
    {
        get; 
        set;  
    }

    private Collider _collider;
    
    public async void IncomingItem(Player p_player)
    {
        if (p_player.pickUpControllerRight.CurrentPickup.TryGetComponent(out ItemData _dataHoldable)) //weryfikacja
        {
            if (_dataHoldable.ItemSO != _myItemSlot) return; //czy dobry item
            if (_dataHoldable.CurrentState != _requiredState) return; //czy dobry state itemu
        }

        if (_dataHoldable.ItemSO is ClotheSO) //ciuchy
        {
            if (_dataHoldable.CurrentState == ItemState.wet)
            {
                //mokre do schni�cia
                _dataHoldable.ClotheToDry();
                QuestCustomManager.instance.HangClothe();

            }

            if (_dataHoldable.CurrentState == ItemState.dirty) //brudne do pralki
            {
                GetComponentInParent<WashingMachineControler>().PutClotheIn(_dataHoldable);
            }
        }

        await p_player.pickUpControllerRight.Drop();

        if (p_player.IsOwner)
        {
            _dataHoldable.GetComponent<Interactable>().CanInteract = false;
            _dataHoldable.GetComponent<Collider>().enabled = false;
        }
        
        _dataHoldable.GetComponent<Transform>().parent = transform;
        _dataHoldable.GetComponent<Transform>().localPosition = Vector3.zero + _offsetForItem;
        _dataHoldable.GetComponent<Transform>().localRotation = new Quaternion(0f + _offsetRotation.x, 0f + _offsetRotation.y, 0f + _offsetRotation.z, 0f + _offsetRotation.w);
        _dataHoldable.GetComponent<Rigidbody>().isKinematic = true;

        if (_isMultiUseItem) { _dataHoldable.GetComponent<Interactable>().CanInteract = true; _dataHoldable.GetComponent<Collider>().enabled = true; }
        if (_isAutoDisable) AutoDisable();
    }

    private void AutoDisable()
    {
        _meshToDisable.enabled = false;
        GetComponent<Collider>().enabled = false;
        enabled = false;
    }

    private void Awake()
    {
        _collider = GetComponent<Collider>();
    }

    private void OnEnable()
    {
        OnInteract.AddListener(IncomingItem);
        PickUpController.OnSomePickupAfter.AddListener(OnPickupAfter);
        PickUpController.OnSomeDropAfter.AddListener(OnDropAfter);
    }

    private void OnDisable()
    {
        OnInteract.RemoveListener(IncomingItem);
        PickUpController.OnSomePickupAfter.RemoveListener(OnPickupAfter);
        PickUpController.OnSomeDropAfter.RemoveListener(OnDropAfter);
    }

    private void Start()
    {
        HandleVisibility(null);
    }
    
    private void OnPickupAfter(PickUpController controller, Pickable pickable)
    {
        if (!controller.IsOwner) return;
        
        var player = pickable.CurrentPickUpController.Player;
        
        HandleVisibility(player);
    }

    private void OnDropAfter(PickUpController controller, Pickable pickable)
    {
        if (!controller.IsOwner) return;
        
        HandleVisibility(null);
    }

    public override bool CanInteractWithPlayer(Player player)
    {
        if (!player) return false;
        
        if (!CanInteract) return false;

        if (IsSleeping) return false;
        
        if (hasItem && _isSingle) return false;

        if (!player.pickUpControllerRight.CurrentPickup ||
            !player.pickUpControllerRight.CurrentPickup.TryGetComponent(out ItemData dataHoldable)) 
            return false;
        
        return dataHoldable.ItemSO == _myItemSlot;
    }
    
    private void HandleVisibility(Player player)
    {
        if (_meshToDisable) _meshToDisable.enabled = CanInteractWithPlayer(player);

        _collider.enabled = CanInteractWithPlayer(player);
    }
}



