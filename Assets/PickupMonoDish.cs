using PickableSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupMonoDish : MonoBehaviour
{
    [SerializeField] private MonologueSO _monologue;
    private Pickable _myPickable;
    private static bool _wasPlayed = false;
    private ItemData _myItemData;
    private void Awake()
    {
        _myPickable = GetComponent<Pickable>();
        _myItemData = GetComponent<ItemData>();
    }
    private void OnEnable()
    {
        _myPickable.OnPickup.AddListener(OnPickupMono);
    }
    private void OnDisable()
    {
        _myPickable.OnPickup.RemoveListener(OnPickupMono);
    }
    private void OnPickupMono(PickUpController p_controller)
    {
        if (_myItemData.CurrentState != ItemState.dirty) return;

        if (_myPickable.IsOwner == true)
            if (!_wasPlayed)
            {
                MonologueManager.instance.PlayMonologue(_monologue);
                _wasPlayed = true;
            }
    }
}
