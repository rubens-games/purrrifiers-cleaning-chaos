using UnityEngine;

namespace CC
{
    public class ScaleCharacter : MonoBehaviour
    {
        public Vector3 TargetScale = new Vector3(1f, 1f, 1f);
        public float Height = 1;
        public float Width = 1;
        public float CounterScaleLerp = 0.5f;

        public enum mode
        {
            Scale, CounterScale
        }

        public mode Mode;

        public void SetHeight(float _Height)
        {
            Height = _Height;
            TargetScale = new Vector3(Height * Width, Height * Width, Height);
        }

        public void SetWidth(float _Width)
        {
            Width = _Width;
            TargetScale = new Vector3(Height * Width, Height * Width, Height);
        }

        private void LateUpdate()
        {
            switch (Mode)
            {
                case mode.Scale:
                    {
                        transform.localScale = TargetScale;
                        break;
                    }
                case mode.CounterScale:
                    {
                        transform.localScale = new Vector3(1 / TargetScale.z * (Mathf.Lerp(TargetScale.z, 1, CounterScaleLerp)), 1 / TargetScale.y * (Mathf.Lerp(TargetScale.z, 1, CounterScaleLerp)), 1 / TargetScale.x * (Mathf.Lerp(TargetScale.z, 1, CounterScaleLerp)));
                        break;
                    }
            }
        }
    }
}