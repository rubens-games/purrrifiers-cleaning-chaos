using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace CC
{
    public class CopyPose : MonoBehaviour
    {
        private Transform sourceRoot;
        private Transform targetRoot;
        private List<Transform> sourceBones = new List<Transform>();
        private List<Transform> targetBones = new List<Transform>();

        private void Start()
        {
            sourceRoot = transform.parent.GetComponentInChildren<SkinnedMeshRenderer>().rootBone;
            targetRoot = getRootBone(transform.GetComponentInChildren<SkinnedMeshRenderer>().rootBone);

            Transform[] targetHierarchy = targetRoot.GetComponentsInChildren<Transform>();

            //Only copy bones that are found in both hierarchies, also ensures order is the same
            foreach (Transform sourceBone in sourceRoot.GetComponentsInChildren<Transform>())
            {
                Transform targetBone = targetHierarchy.FirstOrDefault(t => t.name == sourceBone.name);
                if (targetBone != null)
                {
                    sourceBones.Add(sourceBone);
                    targetBones.Add(targetBone);
                }
            }
        }

        private Transform getRootBone(Transform bone)
        {
            if (bone.parent == null || bone.parent == gameObject.transform) return bone;
            return getRootBone(bone.parent);
        }

        private void LateUpdate()
        {
            //Copy bone transform
            for (int i = 0; i < sourceBones.Count; i++)
            {
                targetBones[i].localPosition = sourceBones[i].localPosition;
                targetBones[i].localRotation = sourceBones[i].localRotation;
                targetBones[i].localScale = sourceBones[i].localScale;
            }
        }
    }
}