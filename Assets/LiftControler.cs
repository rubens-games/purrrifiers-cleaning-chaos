using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LiftControler : MonoBehaviour
{
    private Animator _myAnimator;
    private const string _liftUp = "LiftUp";
    private const string _liftDown = "LiftDown";
    private bool _isUp = false;
    private bool _isDuringAnim = false;
    [SerializeField] private GameObject _button;
    [SerializeField] private TMP_Text _timerOnButtonText;
    [SerializeField] private TMP_Text _timerOnLiftText;
    [SerializeField] private int _waitingTime = 10;

    private void Awake()
    {
        _myAnimator= GetComponent<Animator>();
    }
    public void MoveLift()
    {
        if (_isDuringAnim) return;
        StartCoroutine(AnimateLift());
        ButtonAnim();
    }
    IEnumerator AnimateLift()
    {
        _isDuringAnim = true;
        
        // Timer
        _timerOnButtonText.gameObject.SetActive(true);
        _timerOnLiftText.gameObject.SetActive(true);
        for (int i = _waitingTime; i > 0; i--)
        {
            _timerOnButtonText.text = i.ToString();
            _timerOnLiftText.text = i.ToString();
            yield return new WaitForSeconds(1f);
        }
        _timerOnButtonText.gameObject.SetActive(false);
        _timerOnLiftText.gameObject.SetActive(false);
        
        _myAnimator.SetTrigger(_isUp ? _liftDown : _liftUp);
        yield return new WaitForSeconds(4f);
        _isUp = !_isUp;
        _isDuringAnim = false;
    }
    private void ButtonAnim()
    {
        _button.transform.DOLocalMoveY(-0.55f, 0.4f).SetLoops(2, LoopType.Yoyo);
    }
}
