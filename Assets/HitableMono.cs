using FishNet.Object;
using HitpointsSystem;
using System.Collections.Generic;
using UnityEngine;

public class HitableMono : NetworkBehaviour
{
    [SerializeField] private List<MonologueSO> _hitMonos;
    [SerializeField] private MonologueSO _deathMono;
    [SerializeField] private MonologueSO _reviveMono;

    private Hitable _myHitable;
    private void Awake()
    {
        _myHitable=GetComponent<Hitable>();
    }

    private void OnEnable()
    {
        _myHitable.OnHitpointsDecrease.AddListener(Damage);
        _myHitable.OnDeath.AddListener(Death);
        _myHitable.OnRevive.AddListener(Revive);
    }
    private void OnDisable()
    {
        _myHitable.OnHitpointsDecrease.RemoveListener(Damage);
        _myHitable.OnDeath.RemoveListener(Death);
        _myHitable.OnRevive.RemoveListener(Revive);
    }

    private void Revive(int p_points)
    {
        if(IsOwner)
            MonologueManager.instance.PlayMonologue(_reviveMono);
    }
    private void Death(int p_points)
    {
        if (IsOwner)
            MonologueManager.instance.PlayMonologue(_deathMono);
    }
    private void Damage(int p_points)
    {
        if (IsOwner)
            MonologueManager.instance.PlayMonologue(_hitMonos[Random.Range(0,6)]);
    }
}
