using System.Collections;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class Stain : MonoBehaviour
{
    [SerializeField] private int _MaxHealth = 100;
    [SerializeField] private Durability _Durability = Durability.Low;
    [SerializeField] private float fadeDuration = 2.0f;

    private DecalProjector _DecalProjector;
    private int _CurrentHealth;
    private bool isFading = false;

    private static readonly int _transparent = Shader.PropertyToID("_Transparent");

    private void Awake()
    {
        _DecalProjector = GetComponent<DecalProjector>();
        InitializeMaterial();

        _CurrentHealth = _MaxHealth;
    }

    private void InitializeMaterial()
    {
        _DecalProjector.material = new Material(_DecalProjector.material);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && !isFading)
        {
            ReduceHealth();
        }
    }

    private IEnumerator FadeOutTransparency(float targetTransparency)
    {
        isFading = true;

        float startTransparency = _DecalProjector.material.GetFloat(_transparent);
        float elapsedTime = 0f;

        while (elapsedTime < fadeDuration)
        {
            elapsedTime += Time.deltaTime;
            float newTransparency = Mathf.Lerp(startTransparency, targetTransparency, elapsedTime / fadeDuration);
            _DecalProjector.material.SetFloat(_transparent, newTransparency);
            yield return null;
        }

        _DecalProjector.material.SetFloat(_transparent, targetTransparency);
        isFading = false;

        if (_CurrentHealth <= 0)
        {
            gameObject.SetActive(false);
        }
    }

    private void ReduceHealth()
    {
        int damage = Mathf.Max(1, 100 - (int)_Durability);

        _CurrentHealth -= damage;

        Debug.Log($"Current Health: {_CurrentHealth}, Durability: {_Durability}");

        float targetTransparency = (float)_CurrentHealth / _MaxHealth;

        StartCoroutine(FadeOutTransparency(targetTransparency));
    }

    private enum Durability
    {
        None = 0,
        Low = 80, // 5 razy
        Medium = 75, // 4 razy
        High = 65, // 3 razy
        VeryHigh = 50 // 2 razy
    }
}

