using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
[RequireComponent(typeof(LineRenderer))]
public class LaserController : MonoBehaviour
{
    private LineRenderer _lineRenderer;

    [SerializeField] private Transform _positionEnd;

    private void Awake()
    {
        _lineRenderer = GetComponent<LineRenderer>();
    }

    private void Start()
    {
        UpdateLineRenderer();
    }

    private void UpdateLineRenderer()
    {
        if (_lineRenderer != null && _positionEnd != null)
        {
            _lineRenderer.positionCount = 2;
            _lineRenderer.SetPosition(0, transform.localPosition);
            _lineRenderer.SetPosition(1, _positionEnd.localPosition);
        }
    }

#if UNITY_EDITOR
    private void Update()
    {
        UpdateLineRenderer();
    }
#endif

    private void OnDrawGizmos()
    {
        if (_positionEnd != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, _positionEnd.position);
        }
    }
}
