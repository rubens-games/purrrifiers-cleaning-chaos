using UnityEngine;

public class ParticleController : MonoBehaviour
{
    [SerializeField] private ParticleSystem[] _particleSystems;

    [SerializeField] private ParticleSystem[] _toDisable;

    private bool _isHorn = true;

    private void Start()
    {
        ParticleStop();
    }
    public void RemoveUnicorn() 
    {
        _isHorn = false;
        foreach (var particle in _toDisable) 
        {
            particle.Stop();
        }
        
        QuestCustomManager.instance.WipeBigStain(true); //zaliczy� plame?
    }

    private void ParticlePlay()
    {
        foreach (var particleSystem in _particleSystems)
        {
            particleSystem.Play();
        }
    }

    private void ParticleStop()
    {
        foreach(var particleSystem in _particleSystems)
        {
            particleSystem.Stop();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && _isHorn)
        {
            ParticlePlay();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && _isHorn)
        {
            ParticleStop();
        }
    }
}
