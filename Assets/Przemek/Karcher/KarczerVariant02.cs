using UnityEngine;
using UnityEngine.VFX;

public class KarczerVariant02 : MonoBehaviour
{
    private VisualEffect effect;

    // Maksymalny zasi�g raycasta
    public float maxRayDistance = 5.0f;

    [SerializeField] private float margines = 2f;

    private Ray _ray;
    private RaycastHit _hit;

    private void Start()
    {
        effect = GetComponent<VisualEffect>();
    }

    private void CreateRay()
    {
        _ray = new Ray(transform.position, transform.forward * -1);
    }

    private void Update()
    {
        Debug.Log("Karczer");

        CreateRay();

        if (Physics.Raycast(_ray, out _hit, maxRayDistance, -5 , QueryTriggerInteraction.Ignore))
        {
            float distance = _hit.distance / margines;

            Debug.Log(distance);

            effect.SetFloat("Distance", distance);

            Debug.DrawRay(transform.position, (transform.forward * -1) * distance, Color.yellow);
        }
    }
}

