using UnityEngine;

public class KarcherRayDistanceEffects : MonoBehaviour
{
    [SerializeField] private ParticleSystem[] _LaserCore;
    [SerializeField] private ParticleSystem _FogSystem;
    [SerializeField] private GameObject _BackHead;

    private float _CurrentDistance;

    public void OnKarcherDistanceScale(Ray ray, float lenght)
    {
        LayerMask layerMask = ~LayerMask.GetMask("Dirt");

        if (Physics.Raycast(ray, out RaycastHit hit, lenght, layerMask, QueryTriggerInteraction.Ignore))
        {
            _CurrentDistance = hit.distance * 4;
            _BackHead.transform.position = hit.point;
            _FogSystem.Play();
        }
        else
        {
            _CurrentDistance = lenght * 4;
            _BackHead.transform.position = ray.origin + ray.direction * lenght;
            _FogSystem.Stop();
        }

        UpdateParticleSystems();
    }

    private void UpdateParticleSystems()
    {
        foreach (var lc in _LaserCore)
        {
            var mainModule = lc.main;
            mainModule.startSize3D = true;
            mainModule.startSizeX = 1f;
            mainModule.startSizeY = 1f;
            mainModule.startSizeZ = _CurrentDistance;
        }
    }
}
