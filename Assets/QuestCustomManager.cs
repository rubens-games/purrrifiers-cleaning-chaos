using System;
using Other;
using PixelCrushers;
using PixelCrushers.DialogueSystem;
using PixelCrushers.QuestMachine;
using SingletonSystem.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestCustomManager : Singleton<QuestCustomManager>
{
    [SerializeField] private BlackHole _blackHole;
    public bool isBlackHoleComing = false;


    [SerializeField] private MonologueSO _firstCleanDish;
    [SerializeField] private MonologueSO _firstTrash;
    [SerializeField] private MonologueSO _electroOff;

    [SerializeField] private QuestCustom _bigStains;
    [SerializeField] private QuestCustom _furnitureStains;

    [SerializeField] private QuestCustom _dirtyClothes;
    [SerializeField] private QuestCustom _wetClothes;

    [SerializeField] private QuestCustom _dirtyDishes;
    [SerializeField] private QuestCustom _cleanDishes;

    [SerializeField] private QuestCustom _trashes;
    [SerializeField] private QuestCustom _toys;

    [SerializeField] private QuestCustom _moveFurniture;

    [SerializeField] private MonologueSO _poisonedMono;

    [SerializeField] private QuestCustomManagerNetworked _questNetworked;
    public QuestCustomManagerNetworked QuestNetworked
    {
        get => _questNetworked;
    }

    [SerializeField] private List<AudioClip> _clips;
    private AudioSource _audioSource;
    private void OnEnable()
    {
        Lua.RegisterFunction("BringCat", instance, SymbolExtensions.GetMethodInfo(() => instance.BringCat()));
        Lua.RegisterFunction("SetBlackHoleDialogueDone", instance,
            SymbolExtensions.GetMethodInfo(() => instance.SetBlackHoleDialogueDone()));
        Lua.RegisterFunction("SetDoorsTalk", instance, SymbolExtensions.GetMethodInfo(() => instance.SetDoorsTalk()));
        Lua.RegisterFunction("ChangeVarD", instance, SymbolExtensions.GetMethodInfo(() => instance.ChangeVarD(string.Empty, false)));
        Lua.RegisterFunction("UnlockToys", instance, SymbolExtensions.GetMethodInfo(() => instance.UnlockToys()));
        Lua.RegisterFunction("ChangeQuestNodeStateN", instance, SymbolExtensions.GetMethodInfo(() => instance.ChangeQuestNodeStateN(string.Empty, string.Empty)));
        Lua.RegisterFunction("IncreaseMaxCounter", instance, SymbolExtensions.GetMethodInfo(() => instance.IncreaseMaxCounter(string.Empty, 0)));
        _audioSource = GetComponent<AudioSource>();

    }

    private void OnDisable()
    {
        Lua.UnregisterFunction("BringCat");
        Lua.UnregisterFunction("SetBlackHoleDialogueDone");
        Lua.UnregisterFunction("SetDoorsTalk");
        Lua.UnregisterFunction("ChangeVarD");
        Lua.UnregisterFunction("UnlockToys");
        Lua.UnregisterFunction("ChangeQuestNodeStateN");
        Lua.UnregisterFunction("IncreaseMaxCounter");
    }
    private void PlayRandomSFX()
    {
        int randomIndex = UnityEngine.Random.Range(0, _clips.Count);
        AudioClip clip = _clips[randomIndex];

        _audioSource.clip = clip;
        _audioSource.Play();
    }

    public void UnlockToys()
    {
        _questNetworked.UnlockToysServerRPC();
    }
    public void ChangeQuestNodeStateN(string p_message, string p_parameter)
    {
        _questNetworked.SendMessageNetworkedServerRPC(p_message, p_parameter);
    }
    public void IncreaseMaxCounter(string p_name, int p_value)
    {
        _questNetworked.IncreaseMaxCounterServerRPC(p_name, p_value);
    }
    public void ChangeVarD(string p_name, bool p_state)
    {
        _questNetworked.ChangeVarDServerRPC(p_name, p_state);
    }
    public void BlackHoleComing()
    {
        if (isBlackHoleComing) return;

        isBlackHoleComing = true;
        _blackHole.AscendToGround();
    }

    public void BringCat()
    {
        _questNetworked.BringCatServerRPC();
    }

    public void SetBlackHoleDialogueDone()
    {
        _questNetworked.SetBlackHoleDialogueDoneServerRPC();
    }

    public void SetDoorsTalk()
    {
        _questNetworked.SetDoorsTalkServerRPC();
    }

    public void TakeTrash(bool isOwner) //�mieci do kub�a
    {
        MessageSystem.SendMessage(this, "Find", "Trash");
        _trashes._counterBackup++;

        var counter = QuestMachine.GetQuestCounter("Q1", "Trash");
        counter.currentValue = _trashes._counterBackup;

        if (!isOwner) return;
        
        if (counter.currentValue == 1)
        {
            MonologueManager.instance.PlayMonologue(_firstTrash);
        }

        if (counter.currentValue != counter.maxValue) return;
        
        if (_trashes._wasPlayed) return;
        
        MonologueManager.instance.PlayMonologue(_trashes._monologue);
        _trashes._wasPlayed = true;
    }

    public void ThrowFullTrashbag() //worki do czarnej dziury
    {
        _questNetworked.ThrowFullTrashBagServerRPC();

        //if (counter.currentValue == counter.maxValue)
        //{
        //    if (!_trashes._wasPlayed)
        //    {
        //        MonologueManager.instance.PlayMonologue(_trashes._monologue);
        //        _trashes._wasPlayed = true;
        //    }
        //}
    }

    public void WipeBigStain(bool isOwner) //du�e plamy �ciany i pod�ogi
    {
        if (isOwner)
            PlayRandomSFX();
        
        MessageSystem.SendMessage(this, "Clean", "Stain");
        _bigStains._counterBackup++;

        var counter = QuestMachine.GetQuestCounter("Q1", "Stains");
        counter.currentValue = _bigStains._counterBackup;

        if (counter.currentValue == counter.maxValue)
        {
            if (!_bigStains._wasPlayed)
            {
                MonologueManager.instance.PlayMonologue(_bigStains._monologue);
                _bigStains._wasPlayed = true;
            }
        }
    }

    public void WipeSmallStain(bool isOwner) //ma�e plamy na meblach //to trzeba zrobić i podpiąc jak @jasio rozdzieli logikę plam
    {
        if (isOwner)
            PlayRandomSFX();
        
        MessageSystem.SendMessage(this, "Clean", "Furniture");

        _furnitureStains._counterBackup++;

        var counter = QuestMachine.GetQuestCounter("Q1", "StainsFurniture");
        counter.currentValue = _furnitureStains._counterBackup;

        if (counter.currentValue == counter.maxValue)
        {
            if (!_furnitureStains._wasPlayed)
            {
                MonologueManager.instance.PlayMonologue(_furnitureStains._monologue);
                _furnitureStains._wasPlayed = true;
            }
        }
    }

    public void WashDish() //umycie naczynia
    {
        MessageSystem.SendMessage(this, "Clean", "Dish");
        _dirtyDishes._counterBackup++;

        var counter = QuestMachine.GetQuestCounter("Q1", "Dishes");
        counter.currentValue = _dirtyDishes._counterBackup;

        if (counter.currentValue == 1)
        {
            MonologueManager.instance.PlayMonologue(_firstCleanDish);
        }

        if (counter.currentValue == counter.maxValue)
        {
            if (!_dirtyDishes._wasPlayed)
            {
                MonologueManager.instance.PlayMonologue(_dirtyDishes._monologue);
                _dirtyDishes._wasPlayed = true;
            }
        }
    }

    public void PutDish(bool isOwner) //od�o�enie czystego naczynia
    {
        MessageSystem.SendMessage(this, "Put", "Dish");
        _cleanDishes._counterBackup++;

        var counter = QuestMachine.GetQuestCounter("Q1", "DishesDone");
        counter.currentValue = _cleanDishes._counterBackup;

        if (counter.currentValue != counter.maxValue) return;

        if (_cleanDishes._wasPlayed) return;
      
        if (isOwner)
            MonologueManager.instance.PlayMonologue(_cleanDishes._monologue);
        
        _cleanDishes._wasPlayed = true;
    }

    public void WashClothe() //umycie koszulki
    {
        MessageSystem.SendMessage(this, "Clean", "Clothes");
        _dirtyClothes._counterBackup++;

        var counter = QuestMachine.GetQuestCounter("Q1", "Clothes");
        counter.currentValue = _dirtyClothes._counterBackup;

        if (counter.currentValue == counter.maxValue)
        {
            if (!_dirtyClothes._wasPlayed)
            {
                MonologueManager.instance.PlayMonologue(_dirtyClothes._monologue);
                _dirtyClothes._wasPlayed = true;
            }
        }
    }

    public void HangClothe() //rozwieszenie mokrej koszulki
    {
        MessageSystem.SendMessage(this, "Put", "Clothe");
        _wetClothes._counterBackup++;

        var counter = QuestMachine.GetQuestCounter("Q1", "ClothesDone");
        counter.currentValue = _wetClothes._counterBackup;

        if (counter.currentValue == counter.maxValue)
        {
            if (!_wetClothes._wasPlayed)
            {
                MonologueManager.instance.PlayMonologue(_wetClothes._monologue);
                _wetClothes._wasPlayed = true;
            }
        }
    }

    public void MoveFurniture() //ustaw meble na swoje miejsce
    {
        MessageSystem.SendMessage(this, "Move", "Furniture");
        _moveFurniture._counterBackup++;

        var counter = QuestMachine.GetQuestCounter("Q1", "MoveFurniture");
        counter.currentValue = _moveFurniture._counterBackup;

        if (counter.currentValue == counter.maxValue)
        {
            if (!_moveFurniture._wasPlayed)
            {
                MonologueManager.instance.PlayMonologue(_moveFurniture._monologue);
                _moveFurniture._wasPlayed = true;
            }
        }
    }

    public void FindKeyToAttic() //znalezienie klucza na strych
    {
        MessageSystem.SendMessage(this, "Find", "Key1");
    }

    public void ReceiveMissionFromAnabelle() //przyj�cie misji od Anabelle
    {
        MessageSystem.SendMessage(this, "Talk", "Anabelle");
    }

    public void HelpAnabelle() //po�o�enie 4 maskotek w rytuale
    {
        MessageSystem.SendMessage(this, "Put", "Toy");

        _toys._counterBackup++;

        var counter = QuestMachine.GetQuestCounter("Q1", "Toy");
        counter.currentValue = _toys._counterBackup;

        if (counter.currentValue == counter.maxValue)
        {
            if (!_toys._wasPlayed)
            {
                MonologueManager.instance.PlayMonologue(_toys._monologue);
                _toys._wasPlayed = true;
            }
        }
    }

    public void MinimumMet() //jak ogarn� sobie 80%
    {
        MessageSystem.SendMessage(this, "Done", "80"); //uszykowanie taska jak minimum 80%
    }

    public void ClaimMission() //oddanie misji Taylor
    {
        MessageSystem.SendMessage(this, "Claim", "Mission"); //oddanie taska w dialogu
    }

    public void GoToToilet() //konieczno�� skorzystania z kibelka
    {
        MonologueManager.instance.PlayMonologue(_poisonedMono);
        ChangeQuestNodeStateN("WC", "Need");
    }

    public void NoToilet() //ju� po kibelku
    {
        ChangeQuestNodeStateN("WC", "Done");
    }

    public void EnableFuseBox() //przywr�ci� pr�d
    {
        MessageSystem.SendMessage(this, "FuseBox", "Enable");
    }

    public void DisableFuseBox() //wy�aczenie pr�du
    {
        MessageSystem.SendMessage(this, "FuseBox", "Disable");
    }
}

[System.Serializable]
public class QuestCustom
{
    public MonologueSO _monologue;
    public bool _wasPlayed = false;
    public int _counterBackup = 0;
}

