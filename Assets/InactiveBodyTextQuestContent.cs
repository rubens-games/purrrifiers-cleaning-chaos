using PixelCrushers.QuestMachine.Wrappers;

public class InactiveBodyTextQuestContent : BodyTextQuestContent
{
    public override string runtimeText => $"<color=gray>{base.runtimeText}</color>";

}
