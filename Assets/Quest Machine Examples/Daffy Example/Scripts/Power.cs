using UnityEngine;
using UnityEngine.UI;

namespace PixelCrushers.QuestMachine.Examples
{

    public class Power : MonoBehaviour, IMessageHandler
    {
        public bool isPowerOn = true;
        public Button getClothesButton;

        private void OnEnable()
        {
            MessageSystem.AddListener(this, "Power", "");
        }

        private void OnDisable()
        {
            MessageSystem.RemoveListener(this);
        }

        public void OnMessage(MessageArgs messageArgs)
        {
            isPowerOn = string.Equals(messageArgs.parameter, "on", System.StringComparison.OrdinalIgnoreCase);
            QuestMachine.defaultQuestAlertUI.ShowAlert($"Power: {isPowerOn.ToString().ToUpper()}");
            getClothesButton.interactable = isPowerOn;
        }
    }
}
