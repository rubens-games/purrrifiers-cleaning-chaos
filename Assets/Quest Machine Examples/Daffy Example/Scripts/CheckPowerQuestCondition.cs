namespace PixelCrushers.QuestMachine.Examples
{

    /// <summary>
    /// Condition becomes true if Power is on.
    /// </summary>
    public class CheckPowerQuestCondition : QuestCondition, IMessageHandler
    {

        private Power power => FindObjectOfType<Power>();

        public override string GetEditorName() => "Is Power On?";

        public override void StartChecking(System.Action trueAction)
        {
            base.StartChecking(trueAction);
            if (power.isPowerOn)
            {
                SetTrue();
            }
            else
            {
                MessageSystem.AddListener(this, "Power", "");
            }
        }

        public override void StopChecking()
        {
            base.StopChecking();
            MessageSystem.RemoveListener(this);
        }

        public void OnMessage(MessageArgs messageArgs)
        {
            if (power.isPowerOn)
            {
                SetTrue();
            }
        }

    }

}
