using CC;
using ECM2;
using PickableSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXManager : MonoBehaviour
{
    public AudioSource audioSourceStepFoley;
    public AudioSource audioSourceSteps;
    public AudioSource audioSourceJump;
    public AudioSource audioSourceGrab;

    [SerializeField] private List<AudioClip> _footStepsFoley;
    [SerializeField] private List<AudioClip> _footSteps;
    [SerializeField] private AudioClip _jumpSound;
    [SerializeField] private AudioClip _landingSound;
    [SerializeField] private AudioClip _crouchSound;
    [SerializeField] private List<AudioClip> _grabs;

    [SerializeField] private PickUpController _rightHand;
    [SerializeField] private PickUpController _leftHand;

    
    private CharacterMovement _characterMovement;
    private Character _character;
    private float _intervalTime = 0.4f;

    private void Awake()
    {
        _characterMovement = GetComponent<CharacterMovement>();
        _character = GetComponent<Character>();
    }

    private void Start()
    {
        StartCoroutine(FootStepRoutine());
        StartCoroutine(FootStepGrass());
    }

    private void OnEnable()
    {
        _character.Jumped += JumpSFX;
        _character.Landed += LandingSFX;
        _character.Crouched += CrouchSFX;
        _rightHand.OnPickup.AddListener(GrabSFX);
        _leftHand.OnPickup.AddListener(GrabSFX);
    }

    private void OnDisable()
    {
        _character.Jumped -= JumpSFX;
        _character.Landed -= LandingSFX;
        _character.Crouched -= CrouchSFX;
        _rightHand.OnPickup.RemoveListener(GrabSFX);
        _leftHand.OnPickup.RemoveListener(GrabSFX);
    }

    private IEnumerator FootStepRoutine()
    {
        while (true)
        {
            _intervalTime = _characterMovement.velocity.magnitude >= 3.1f ? 0.3f : 0.4f;
            audioSourceStepFoley.pitch = _characterMovement.velocity.magnitude >= 3.1f ? 0.5f : 1f;
            if (_characterMovement.velocity.magnitude > 0.1f && _characterMovement.isGrounded)
            {
                //if (!audioSourceSteps.isPlaying)
                // {
                audioSourceStepFoley.Stop();
                PlayFootstep();
                //}
            }

            yield return new WaitForSeconds(_intervalTime);
        }
    }

    IEnumerator FootStepGrass()
    {
        while (true)
        {
            _intervalTime = _characterMovement.velocity.magnitude >= 3.1f ? 0.3f : 0.4f;
            audioSourceSteps.pitch = _characterMovement.velocity.magnitude >= 3.1f ? 0.5f : 1f;
            if (_characterMovement.velocity.magnitude > 0.1f && _characterMovement.isGrounded)
            {
                //if (!audioSourceSteps.isPlaying)
                // {
                audioSourceSteps.Stop();
                PlayFootstepGrass();
                //}
            }

            yield return new WaitForSeconds(_intervalTime);
        }
    }

    private void JumpSFX()
    {
        audioSourceJump.clip = _jumpSound;
        audioSourceJump.Play();
    }

    private void LandingSFX(Vector3 p_vector)
    {
        audioSourceJump.clip = _landingSound;
        audioSourceJump.Play();
    }

    private void CrouchSFX()
    {
        audioSourceJump.clip = _crouchSound;
        audioSourceJump.Play();
    }

    private void PlayFootstep()
    {
        int randomIndex = Random.Range(0, _footStepsFoley.Count);
        AudioClip footstep = _footStepsFoley[randomIndex];

        audioSourceStepFoley.clip = footstep;
        audioSourceStepFoley.Play();
    }

    private void PlayFootstepGrass()
    {
        int randomIndex = Random.Range(0, _footSteps.Count);
        AudioClip footstep = _footSteps[randomIndex];

        audioSourceSteps.clip = footstep;
        audioSourceSteps.Play();
    }
    private void GrabSFX(Pickable p)
    {
        if (_rightHand.IsOwner)
        {
            int randomIndex = Random.Range(0, _grabs.Count);
            AudioClip grab = _grabs[randomIndex];

            audioSourceGrab.clip = grab;
            audioSourceGrab.Play();
        }
    }
}
