using DG.Tweening;
using FishNet.Object;
using PixelCrushers.DialogueSystem;
using PixelCrushers.QuestMachine;
using PlayerSystem;
using System.Collections;
using System.Collections.Generic;
using PixelCrushers;
using UnityEngine;
using Zenject;

public class EgzorcyzmManager : MonoBehaviour
{
    private int _toysInCircle = 0;
    [SerializeField] private GameObject _goodUnicorn;
    [SerializeField] private GameObject _badUnicorn;
    [SerializeField] private ItemDock _unicornGoodDock;
    [SerializeField] private ItemDock _unicornBadDock;

    [SerializeField] private GameObject _ritualEffect;
    [SerializeField] private GameObject _cloneGoodSister;
    [SerializeField] private GameObject _cloneBadSister;

    [Inject] private PlayerManager _playerManager;
    [SerializeField] private Collider _roomArea;
    [SerializeField] private UniwersalDrawerManager _door;
    [SerializeField] private GameObject _anabelle;
    [SerializeField] private GameObject _anabelleBag;

    [SerializeField] private Collider _pentagramCollider;

    [SerializeField] private List<ItemDock> _toysDock;
    private bool _amandaIsAlive = true;

    private AudioSource _audioSource;
    [SerializeField] private AudioSource _audioSource2;
    [SerializeField] private AudioClip _scream;
    [SerializeField] private AudioClip _ritual;

    private void Awake()
    {
        _audioSource=GetComponent<AudioSource>();
        _unicornBadDock.IsSleepingDock = true;
        _unicornGoodDock.IsSleepingDock = true;
        foreach (ItemDock dock in _toysDock)
        {
            dock.IsSleepingDock = true;
        }
    }
    public void AddToy()
    {
        _toysInCircle++;
        QuestCustomManager.instance.HelpAnabelle();

        if( _toysInCircle == 3 )
        {
            _unicornBadDock.IsSleepingDock = false;
            _unicornGoodDock.IsSleepingDock = false;
        }
    }
    public void AddGoodUnicorn()
    {
        QuestCustomManager.instance.HelpAnabelle();

        _unicornBadDock.IsSleepingDock = true;
        StartCoroutine(RitualBegin(_goodUnicorn, _cloneGoodSister, true));
    }
    public void AddBadUnicorn()
    {
        QuestCustomManager.instance.HelpAnabelle();

        _unicornGoodDock.IsSleepingDock = true;
        StartCoroutine(RitualBegin(_badUnicorn, _cloneBadSister, false));
    }

    IEnumerator RitualBegin(GameObject p_unicorn, GameObject p_girl, bool p_result)
    {
        _audioSource.clip = _ritual;
        _audioSource.Play();
        _ritualEffect.SetActive(true);
        yield return new WaitForSeconds(8f);
        p_girl.SetActive(true);
        _audioSource2.Play();
        p_unicorn.transform.DOScale(Vector3.zero, 1f).OnComplete(() =>
        {
            p_girl.transform.DOScale(Vector3.one, 1f);
        });

        _pentagramCollider.enabled = true;

        if (!p_result)
        {
            StartCoroutine(ReadyToKill());
        }
       
    }
    private IEnumerator ReadyToKill()
    {
        while (_amandaIsAlive)
        {
            yield return new WaitForSeconds(.5f);

            if (DialogueLua.GetVariable("ReadyToKill").AsBool == true && !IsPlayerInside() && _amandaIsAlive)
            {
                KillAnabelle();
                yield return null;
            }
        }
    }
    private bool IsPlayerInside()
    {
        foreach(var player in _playerManager.players)
        {
            if(_roomArea.bounds.Contains(player.transform.position))
            {
                return true;
            }
        }
        return false;
    }
    private void KillAnabelle() //sfx slam drzwi
    {
        _amandaIsAlive = false;
        _door.InteractDrawer(0);
        _anabelle.SetActive(false);
        _anabelleBag.transform.DOMoveY(6.992f, 0.1f).OnComplete(() => { _anabelleBag.GetComponent<Collider>().enabled = true; });
        _audioSource.clip = _scream;
        _audioSource.Play();
        // QuestCustomManager.instance.IncreaseMaxCounter("Trash", 1);

        var counter = QuestMachine.GetQuestCounter("Q1", "Trash");
        counter.maxValue += 1;
        MessageSystem.SendMessage(this, QuestMachineMessages.QuestCounterChangedMessage, string.Empty);
        
        //Gdyby kod wy�ej na zwi�kszanie countera nie dzia�a� to wdro�y� ten:
        /*var playerQuestJournal = QuestMachine.GetQuestJournal();
        var quest = playerQuestJournal.FindQuest("Q1");
        QuestCounter counter = quest.GetCounter("Trash");
        counter.maxValue++;*/

        //DialogueLua.SetVariable("AmandaKill", true);
        QuestCustomManager.instance.ChangeVarD("AmandaKill", true);
    }
}
