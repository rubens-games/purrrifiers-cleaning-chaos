﻿using UnityEngine;

namespace CleanableSystem
{
    public struct CleaningData
    {
        public RenderTexture alphaMask;
        public Vector2 mousePosition;
        public CleanableDecal cleanableDecal;
        public float cleaningRadius;

        public static CleaningData empty => new(null, Vector2.zero, null, 0.0f);
        
        public CleaningData(RenderTexture alphaMask, Vector2 mousePosition, CleanableDecal cleanableDecal, float cleaningRadius)
        {
            this.alphaMask = alphaMask;
            this.mousePosition = mousePosition;
            this.cleanableDecal = cleanableDecal;
            this.cleaningRadius = cleaningRadius;
        }
    }
}