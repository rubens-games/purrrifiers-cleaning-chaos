using FishNet.Object;
using UnityEngine;

namespace CleanableSystem
{
    public abstract class CleaningTool : NetworkBehaviour
    {
        public Cleanable CurrentCleaningTarget { get; private set; }
        
        [Tooltip("Cleaning percentage per second")]
        [field: SerializeField] public float CleaningSpeed { get; private set; }
        
        private bool _localCanCallStartCleaning = true;
        private bool _localCanCallStopCleanig = true;
        
        public void StartCleaning(Cleanable cleanable)
        {
            if (!_localCanCallStartCleaning) return;
            _localCanCallStartCleaning = false;
            
            StartCleaningServerRpc(cleanable);
        }

        [ServerRpc(RequireOwnership = false)]
        private void StartCleaningServerRpc(Cleanable cleanable)
        {
            StartCleaningObserverRpc(cleanable);
        }

        [ObserversRpc]
        private void StartCleaningObserverRpc(Cleanable cleanable)
        {
            CurrentCleaningTarget = cleanable;
            
            CurrentCleaningTarget.CleaningStart();
            
            _localCanCallStartCleaning = true;
        }
        
        public void StopCleaning()
        {
            if (!_localCanCallStopCleanig) return;
            _localCanCallStopCleanig = false;
            
            StopCleaningServerRpc();
        }

        [ServerRpc(RequireOwnership = false)]
        private void StopCleaningServerRpc()
        {
            _localCanCallStopCleanig = true;
            StopCleaningObserverRpc();
        }

        [ObserversRpc]
        private void StopCleaningObserverRpc()
        {
            if (!CurrentCleaningTarget) return;
            
            CurrentCleaningTarget.CleaningStop();
            
            CurrentCleaningTarget = null;
        }
        
        public virtual void Update()
        {
            if (!IsOwner) return;
            
            if (!CurrentCleaningTarget) return;
            
            if (CurrentCleaningTarget.IsCleaned) return;

            CurrentCleaningTarget.DirtAmount -= CleaningSpeed * (float)TimeManager.TickDelta;
        }
    }
}
