using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace CleanableSystem
{
    public class Cleaner : ILateDisposable
    {
        private ComputeShader _computeShader;
        private ComputeBuffer _cleanlinessBuffer;
        private int _kernelDraw = 0;
        private int _kernelPercentage = 1;
        private Vector2Int _dispatchCount;

        // Dodanie s�ownik�w do przechowywania stanu dla ka�dego gracza
        private Dictionary<int, Vector2?> playerLastMousePositions = new Dictionary<int, Vector2?>();
        private Dictionary<int, CleanableDecal> playerLastDecals = new Dictionary<int, CleanableDecal>();

        private struct PixelPercentageData
        {
            public uint filledCount;
            public uint processedCount;
        };

        private PixelPercentageData[] _data;

        private static readonly int _result = Shader.PropertyToID("Result");
        private static readonly int _resultForPercentage = Shader.PropertyToID("ResultForPercentage");
        private static readonly int _computeShaderPropertyNameId = Shader.PropertyToID("data");
        private static readonly int _computeShaderVariableStartPos = Shader.PropertyToID("_StartPos");
        private static readonly int _computeShaderVariableEndPos = Shader.PropertyToID("_EndPos");
        private static readonly int _computeShaderVariableSteps = Shader.PropertyToID("_Steps");
        private static readonly int _mouseRadius = Shader.PropertyToID("_MouseRadius");

        public Cleaner(ComputeShader computeShader) => _computeShader = computeShader;

        public void Clean(RaycastHit hit, float radius, int playerId)
        {
            if (!hit.collider.TryGetComponent(out CleanableDecal cleanableDecal)) return;

            var alphaMask = cleanableDecal.alphaMask;
            var textureCord = hit.textureCoord;

            Clean(new CleaningData(alphaMask, textureCord, cleanableDecal, radius), playerId);
        }

        public void Clean(CleaningData cleaningData, int playerId)
        {
            // Reset pozycji myszki jeśli nowy decal
            if (!playerLastDecals.ContainsKey(playerId) || playerLastDecals[playerId] != cleaningData.cleanableDecal)
            {
                playerLastMousePositions[playerId] = null;
                playerLastDecals[playerId] = cleaningData.cleanableDecal;
            }

            InitializeShader(cleaningData.alphaMask, cleaningData.cleaningRadius);
            UpdateMouse(cleaningData.alphaMask, cleaningData.mousePosition, playerId);
            CalculateAndApplyAlphaPercentage(cleaningData.cleanableDecal);
        }


        public void StopCleaning(int playerId)
        {
            if (playerLastMousePositions.ContainsKey(playerId))
            {
                playerLastMousePositions[playerId] = null;
            }
        }


        private void InitializeShader(RenderTexture pAlphaMask, float radius)
        {
            _computeShader.SetFloat(_mouseRadius, radius);
            _computeShader.SetTexture(_kernelDraw, _result, pAlphaMask);
            _computeShader.SetTexture(_kernelPercentage, _resultForPercentage, pAlphaMask);

            _computeShader.GetKernelThreadGroupSizes(_kernelDraw, out var threadX, out var threadY, out _);
            _dispatchCount.x = Mathf.CeilToInt((float)pAlphaMask.width / threadX);
            _dispatchCount.y = Mathf.CeilToInt((float)pAlphaMask.height / threadY);

            InitializeBuffer();
        }

        private void InitializeBuffer()
        {
            if (_cleanlinessBuffer == null)
            {
                _data = new PixelPercentageData[1];
                _cleanlinessBuffer = new ComputeBuffer(_data.Length, sizeof(uint) * 2); // 2 * 4 bytes (uint)
            }

            _data[0].filledCount = 0;
            _data[0].processedCount = 0;
            _cleanlinessBuffer.SetData(_data);
            _computeShader.SetBuffer(_kernelPercentage, _computeShaderPropertyNameId, _cleanlinessBuffer);
        }

        private void UpdateMouse(RenderTexture pAlphaMask, Vector2 pMousePos, int playerId)
        {
            var scaledMousePos = new Vector2(pMousePos.x * pAlphaMask.width, pMousePos.y * pAlphaMask.height);
            Vector2 previousScaledMousePos;
            int steps = 1;

            if (playerLastMousePositions.TryGetValue(playerId, out var lastMousePos) && lastMousePos.HasValue)
            {
                previousScaledMousePos = new Vector2(lastMousePos.Value.x * pAlphaMask.width, lastMousePos.Value.y * pAlphaMask.height);
                steps = Mathf.CeilToInt(Vector2.Distance(previousScaledMousePos, scaledMousePos));
            }
            else
            {
                previousScaledMousePos = scaledMousePos;
            }

            _computeShader.SetVector(_computeShaderVariableStartPos, previousScaledMousePos);
            _computeShader.SetVector(_computeShaderVariableEndPos, scaledMousePos);
            _computeShader.SetFloat(_computeShaderVariableSteps, steps);

            // Dispatch do rysowania
            _computeShader.Dispatch(_kernelDraw, _dispatchCount.x, _dispatchCount.y, 1);
            _computeShader.Dispatch(_kernelPercentage, _dispatchCount.x, _dispatchCount.y, 1);

            playerLastMousePositions[playerId] = pMousePos;

            // Pobieranie danych z GPU
            _cleanlinessBuffer.GetData(_data);
        }

        private void CalculateAndApplyAlphaPercentage(CleanableDecal cleanableDecal)
        {
            if (!cleanableDecal) return;

            cleanableDecal.alphaPercentage = (float)_data[0].filledCount / _data[0].processedCount;
        }

        public void LateDispose()
        {
            _cleanlinessBuffer?.Release();
        }
    }
}

