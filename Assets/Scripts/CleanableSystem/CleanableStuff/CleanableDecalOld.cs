using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace CleanableSystem.CleanableStuff
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(DecalProjector), typeof(BoxCollider))]
    public class CleanableDecalOld : Cleanable
    {
        public DecalProjector Decal { get; private set; }
        public BoxCollider BoxCollider { get; private set; }

        [SerializeField] private LayerMask _dirtLayer;
        [SerializeField] private float _colliderZSize = 0.1f;
        
        private void Awake()
        {
            Decal = GetComponent<DecalProjector>();
            BoxCollider = GetComponent<BoxCollider>();
        }

        #if UNITY_EDITOR

        private void Update()
        {
            if (Application.isPlaying) return;
            
            // Lock the transform scale to one
            transform.localScale = Vector3.one;
            
            // Set the decal size to the box collider size
            var boxColliderSize = BoxCollider.size;
            boxColliderSize.z = _colliderZSize;
            BoxCollider.size = boxColliderSize;
            
            // Calculate the box collider center from raycast distance
            if (Physics.Raycast(transform.position, transform.forward, out var hit, Decal.size.z, ~_dirtLayer))
                BoxCollider.center = new Vector3(0, 0, hit.distance);
        }
        
        #endif

        public override void CleaningStart()
        {
            base.CleaningStart();

            Debug.Log($"{name} is being cleaned!");
        }
        
        public override void CleaningStop()
        {
            base.CleaningStop();

            Debug.Log($"{name} is stopped cleaning!");
        }

        public override void CleaningFinish()
        {
            BoxCollider.enabled = false;
            
            base.CleaningFinish();

            Debug.Log($"{name} is cleaned!");
        }

        public override void CleaningPercentageChange(float dirtPercentage) => 
            Decal.fadeFactor = dirtPercentage;
    }
}