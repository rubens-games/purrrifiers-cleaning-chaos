using System;
using System.Collections;
using FishNet.Connection;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using Other;
using PlayerSystem;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.Universal;
using Zenject;

namespace CleanableSystem
{
    [Flags]
    public enum CleaningAvailableTo
    {
        None = 0,
        Karcher = 1 << 0,
        Sprayer = 1 << 1,
        Everything = ~0
    }

    public enum DirtPlace
    {
        Dummy,
        Furniture,
    }
    
    public class CleanableDecal : NetworkBehaviour
    {
        public static UnityEvent<CleanableDecal> onBeforeSomeDecalCleaned = new();
        public static UnityEvent<CleanableDecal> onSomeDecalCleaned = new();
        public static UnityEvent<CleanableDecal> onAfterSomeDecalCleaned = new();

        public UnityEvent onBeforeClean = new();
        public UnityEvent onClean = new();
        public UnityEvent onAfterClean = new();
        
        public DecalProjector decalProjector => _decalProjector;
        
        public RenderTexture alphaMask { get; private set; }
        private bool _isDone = false;

        public float alphaPercentage
        {
            get => _alphaPercentage;
            set
            {
                _alphaPercentage = value;

                if (_isDone) return;

                if (_alphaPercentage >= _AlphaPercentClear)
                {
                    _isDone = true;
                    StartCoroutine(DisableDecal());
                }
            }
        }

        [field: SerializeField] public CleaningAvailableTo cleaningAvailableTo { get; private set; } = CleaningAvailableTo.Everything;
        [field: SerializeField] public DirtPlace dirtPlace { get; private set; } = DirtPlace.Dummy;
        
        [SerializeField] private float _AlphaPercentClear = 0.9f;
        [SerializeField] private ComputeShader _computeShader;
        [SerializeField] private ParticleSystem _shinyEffect;
        
        [Inject] private Cleaner _cleaner;
        [Inject] private PlayerManager _playerManager;
        
        private int _extractAlpha = 0;
        private Vector2Int _dispatchCount;
        private Vector2Int _sizeTexture;
        private DecalProjector _decalProjector;
        private float _alphaPercentage;
        private SendCounter _sendCounter;

        private readonly SyncVar<byte[]> _alphaMask = new();
        private static readonly int _alpha = Shader.PropertyToID("_Alpha");
        private static readonly int _baseMap = Shader.PropertyToID("_Base_Map");
        private static readonly int _scale = Shader.PropertyToID("_Scale");
        private static readonly int _inputTexture = Shader.PropertyToID("_InputTexture");
        private static readonly int _result = Shader.PropertyToID("_Result");

        private void Awake()
        {
            _decalProjector = GetComponent<DecalProjector>();
            _sendCounter = GetComponent<SendCounter>();
            Vector2Int text = CreateTextureSize();
            InitializeMaterial(text.x, text.y);
            CreateAlphaMask(text.x, text.y);
        }

        private void Start()
        {
            _shinyEffect.Stop();
        }

        private Vector2Int CreateTextureSize()
        {
            _sizeTexture = new Vector2Int(_decalProjector.material.GetTexture(_baseMap).width, _decalProjector.material.GetTexture(_baseMap).height);

            Vector2 size = new Vector2(_decalProjector.size.x, _decalProjector.size.y);

            float scaleFactorX = size.x / 10;
            float scaleFactorY = size.y / 10;

            int width = Mathf.CeilToInt(_sizeTexture.x * scaleFactorX);
            int height = Mathf.CeilToInt(_sizeTexture.y * scaleFactorY);

            float a = 10.0f / _decalProjector.gameObject.transform.localScale.x;
            float b = 10.0f / _decalProjector.gameObject.transform.localScale.y;

            _computeShader.SetFloats(_scale, new float[] { a, b });

            return new Vector2Int(width, height);
        }

        private void InitializeMaterial(int width, int height)
        {
            _decalProjector.material = new Material(_decalProjector.material);

            Vector2 baseMapScale = new Vector2(width / (float)_sizeTexture.x, height / (float)_sizeTexture.y);
            _decalProjector.material.SetTextureScale(_baseMap, baseMapScale);
        }

        private void CreateAlphaMask(int width, int height)
        {
            Texture2D pTexture = _decalProjector.material.GetTexture(_baseMap) as Texture2D;

            alphaMask = new RenderTexture(width, height, 0)
            {
                wrapMode = TextureWrapMode.Clamp,
                filterMode = FilterMode.Bilinear,
                enableRandomWrite = true,
            };

            alphaMask.Create();

            _computeShader.SetTexture(_extractAlpha, _inputTexture, pTexture);
            _computeShader.SetTexture(_extractAlpha, _result, alphaMask);

            _computeShader.GetKernelThreadGroupSizes(_extractAlpha, out var threadX, out var threadY, out _);
            _dispatchCount.x = Mathf.CeilToInt((float)width / threadX);
            _dispatchCount.y = Mathf.CeilToInt((float)height / threadY);

            _computeShader.Dispatch(_extractAlpha, _dispatchCount.x, _dispatchCount.y, 1);

            ApplyAlphaMaskToMaterial();
        }

        private void ApplyAlphaMaskToMaterial()
        {
            _decalProjector.material.SetTexture(_alpha, alphaMask);
        }

        internal IEnumerator DisableDecal()
        {
            if(dirtPlace==DirtPlace.Furniture)
            {
                _sendCounter.SendWipeSmallStain();
            }
            else
            {
                _sendCounter.SendWipeBigStain();
            }

            onBeforeClean.Invoke();
            onBeforeSomeDecalCleaned.Invoke(this);
            onClean.Invoke();
            onSomeDecalCleaned.Invoke(this);
            onAfterClean.Invoke();
            onAfterSomeDecalCleaned.Invoke(this);

            _shinyEffect.Play();
            _decalProjector.enabled = false;

            yield return new WaitForSeconds(3.0f);

            _shinyEffect.Stop();

            yield return new WaitForSeconds(4.0f);

            gameObject.SetActive(false);
        }

        #region Networking

        public void CleanMe(Vector2 textureCords, float radius, NetworkConnection cleaningGuy)
        {
            var playerId = cleaningGuy.GetHashCode();
            
            if (_playerManager.localPlayer.Owner == cleaningGuy) 
                _cleaner.Clean(new CleaningData(alphaMask, textureCords, this, radius), playerId);

            CleanMeServerRpc(textureCords, radius, cleaningGuy);
        }

        [ServerRpc(RequireOwnership = false)]
        private void CleanMeServerRpc(Vector2 textureCords, float radius, NetworkConnection cleaningGuy) =>
            CleanMeObserversRpc(textureCords, radius, cleaningGuy);

        [ObserversRpc]
        private void CleanMeObserversRpc(Vector2 textureCords, float radius, NetworkConnection cleaningGuy)
        {
            int playerId = cleaningGuy.GetHashCode();
            if (_playerManager.localPlayer.Owner == cleaningGuy) return;

            _cleaner.Clean(new CleaningData(alphaMask, textureCords, this, radius), playerId);
        }

        #endregion
    }

}