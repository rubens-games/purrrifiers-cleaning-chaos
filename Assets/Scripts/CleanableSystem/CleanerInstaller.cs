﻿using UnityEngine;
using Zenject;

namespace CleanableSystem
{
    public class CleanerInstaller : MonoInstaller
    {
        [SerializeField] private ComputeShader _computeShader;
        
        public override void InstallBindings()
        {
            var cleaner = new Cleaner(_computeShader);
            
            Container.BindInterfacesAndSelfTo<Cleaner>().FromInstance(cleaner).AsSingle().NonLazy();
        }
    }
}