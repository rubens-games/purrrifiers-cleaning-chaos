﻿using System.Collections.Generic;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using UnityEngine;
using UnityEngine.Events;

namespace CleanableSystem
{
    public abstract class Cleanable : NetworkBehaviour
    {
        [field: SerializeField] public UnityEvent OnCleaningStart { get; private set; } = new();
        [field: SerializeField] public UnityEvent OnCleaningStop { get; private set; } = new();
        [field: SerializeField] public UnityEvent OnCleaningFinish { get; private set; } = new();
        
        public bool IsCleaned => DirtPercentage <= 0 || _finishCleaningCalled;
        public float DirtPercentage => _dirtAmount.Value / MaxDirtAmount;

        private bool _finishCleaningCalled;
        
        public float DirtAmount
        {
            get => _dirtAmount.Value;
            set => SetDirtAmountServerRpc(value);
        }

        public float MaxDirtAmount = 1;
        public float MinDirtAmount = 0;
        
        private readonly SyncVar<float> _dirtAmount = new();

        public override void OnStartServer()
        {
            base.OnStartServer();
            
            _dirtAmount.Value = MaxDirtAmount;
        }

        public override void OnStartClient()
        {
            base.OnStartClient();
            
            _dirtAmount.OnChange += OnDirtAmountChange;
        }
        
        public override void OnStopClient()
        {
            base.OnStopClient();
            
            _dirtAmount.OnChange -= OnDirtAmountChange;
        }

        public virtual void CleaningStart() => OnCleaningStart?.Invoke();

        public virtual void CleaningStop() => OnCleaningStop?.Invoke();

        public virtual void CleaningFinish()
        {
            if (_finishCleaningCalled)
                return;
            
            _finishCleaningCalled = true;
            
            OnCleaningFinish?.Invoke();
        }

        private void OnDirtAmountChange(float oldValue, float newValue, bool asServer)
        {
            CleaningPercentageChange(DirtPercentage);
            
            if (_finishCleaningCalled)
                return;
            
            if (newValue <= MinDirtAmount)
                CleaningFinish();
        }
        
        public abstract void CleaningPercentageChange(float dirtPercentage);
        
        [ServerRpc(RequireOwnership = false)]
        private void SetDirtAmountServerRpc(float dirtAmount)
        {
            if (dirtAmount < MinDirtAmount)
                dirtAmount = MinDirtAmount;
            else if (dirtAmount > MaxDirtAmount)
                dirtAmount = MaxDirtAmount;
            
            _dirtAmount.Value = dirtAmount;
        }
    }
}