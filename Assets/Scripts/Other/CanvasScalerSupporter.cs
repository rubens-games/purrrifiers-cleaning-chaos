using System;
using UnityEngine;
using UnityEngine.UI;

public class CanvasScalerSupporter : MonoBehaviour
{
    // Reference to the CanvasScaler component
    private CanvasScaler canvasScaler;

    // Screen aspect ratio (width / height)
    private float aspectRatio;

    private void Awake()
    {
        canvasScaler = GetComponent<CanvasScaler>();
    }

    void Start()
    {
        // If CanvasScaler is not assigned in the inspector, try to get it from the current GameObject
        if (canvasScaler == null)
        {
            canvasScaler = GetComponent<CanvasScaler>();
        }

        if (canvasScaler != null)
        {
            UpdateMatchBasedOnAspectRatio();
        }
        else
        {
            Debug.LogError("No CanvasScaler component found. Please assign or attach one to the GameObject.");
        }
    }

    // Calculate the current screen aspect ratio
    private float GetScreenAspectRatio()
    {
        return (float)Screen.width / (float)Screen.height;
    }

    // Update the CanvasScaler Match property based on the current aspect ratio
    private void UpdateMatchBasedOnAspectRatio()
    {
        aspectRatio = GetScreenAspectRatio();

        // If the screen is wider than it is tall, prioritize width
        // Otherwise, prioritize height
        if (aspectRatio >= 1.5f) // Adjust the threshold as needed
        {
            canvasScaler.matchWidthOrHeight = 1f; // Prioritize width
        }
        else
        {
            canvasScaler.matchWidthOrHeight = 0f; // Prioritize height
        }
    }
}