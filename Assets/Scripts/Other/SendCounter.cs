﻿using PlayerSystem;
using UnityEngine;
using Zenject;

namespace Other
{
    public class SendCounter : MonoBehaviour
    {
        [Inject] private PlayerManager _playerManager;

        private const int MAX_DISTANCE = 3;

        public void SendWipeBigStain()
        {
            var playerPosition = _playerManager.localPlayer.transform.position;
            var distance = Vector3.Distance(playerPosition, transform.position);
            var canPlayAudio = distance < MAX_DISTANCE;
            
            QuestCustomManager.instance.WipeBigStain(canPlayAudio);
        }

        public void SendWipeSmallStain()
        {
            var playerPosition = _playerManager.localPlayer.transform.position;
            var distance = Vector3.Distance(playerPosition, transform.position);
            
            var canPlayAudio = distance < MAX_DISTANCE;
            
            QuestCustomManager.instance.WipeSmallStain(canPlayAudio);
        }
    }
}