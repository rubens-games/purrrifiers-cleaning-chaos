﻿using FishNet.Connection;
using FishNet.Object;
using UnityEngine;

namespace Other
{
    public static class GameObjectExtension
    {
        public static NetworkConnection GetConnection(this GameObject gameObject)
        {
            if (!gameObject) return null;
            
            return gameObject.TryGetComponent(out NetworkObject networkObject) 
                ? networkObject.Owner 
                : null;
        }
        
        public static T GetOrAddComponent<T>(this GameObject go) where T : Component
        {
            return go.GetComponent<T>() ?? go.AddComponent<T>();
        }
    }
}