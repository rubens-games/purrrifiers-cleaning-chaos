﻿using UnityEngine;

namespace Other
{
    public class SystemRequirementsChecker : MonoBehaviour
    {
        public GameObject warningPanel;
        
        [Header("Minimum System Requirements")]
        public int minimumRAM = 8;
        public int minimumCPUCores = 4;
        public int minimumGPUMemory = 2048;
        
        private void Start() => CheckSystemRequirements();

        private void CheckSystemRequirements()
        {
            var systemRam = SystemInfo.systemMemorySize; 
            if (systemRam < minimumRAM * 1024) 
                DisplayWarning();

            var cpuCores = SystemInfo.processorCount;
            if (cpuCores < minimumCPUCores) 
                DisplayWarning();

            var gpuMemory = SystemInfo.graphicsMemorySize; 
            if (gpuMemory < minimumGPUMemory) 
                DisplayWarning();
        }

        private void DisplayWarning() => 
            warningPanel.SetActive(true);
    }

}