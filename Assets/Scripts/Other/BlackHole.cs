﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

namespace Other
{
    public class BlackHole : MonoBehaviour
    {
        public UnityEvent<GameObject> onEnter = new();
        public UnityEvent<TrashBag> onTrashEnter = new();

        private void OnTriggerEnter(Collider other)
        {
            OnEnter(other.gameObject);

            if (!other.TryGetComponent(out TrashBag trashBag)) return;
            
            other.enabled = false;

            Debug.Log($"TrashBag entered black hole: {trashBag}");
            OnTrashEnter(trashBag);
        }
        
        private void OnEnter(GameObject go) => onEnter.Invoke(go);
        
        private void OnTrashEnter(TrashBag trashBag)
        {
            trashBag.moveTarget = transform;

            if (trashBag.IsOwner) 
                QuestCustomManager.instance.ThrowFullTrashbag();

            onTrashEnter.Invoke(trashBag);
        }

        public void AscendToGround()
        {
            transform.DOMoveY(3f, 10f).SetEase(Ease.OutQuad);
        }
    }
}