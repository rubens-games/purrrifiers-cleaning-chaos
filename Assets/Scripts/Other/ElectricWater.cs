using HitpointsSystem;
using UnityEngine;

namespace Other
{
    public class ElectricWater : MonoBehaviour
    {
        private Collider _hittingArea;

        private void Awake() => _hittingArea = GetComponent<Collider>();

        private void OnEnable() => ElectroManager.OnElectroStateChanged += OnElectroStateChanged;

        private void OnDisable() => ElectroManager.OnElectroStateChanged -= OnElectroStateChanged;

        private void OnElectroStateChanged(bool state) => _hittingArea.enabled = state;
    }
}
