﻿using FishNet.Connection;
using FishNet.Object;
using UnityEngine;

namespace Other
{
    public static class ComponentExtension
    {
        public static NetworkConnection GetConnection(this Component component)
        {
            if (!component) return null;
            
            return component.TryGetComponent(out NetworkObject networkObject) 
                ? networkObject.Owner 
                : null;
        }
    }
}