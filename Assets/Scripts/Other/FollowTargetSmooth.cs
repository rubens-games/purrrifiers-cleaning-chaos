using UnityEngine;

namespace Other
{
    public class FollowTargetSmooth : MonoBehaviour
    {
        private enum ExecuteFollowPlace
        {
            fixedUpdate,
            update,
            lateUpdate,
        }
        
        public Transform target;
        public float moveSpeed = 5f;
        public float rotationSpeed = 5f;
        public float distanceMultiplier = 1f;

        [SerializeField] private ExecuteFollowPlace _executeFollowPlace;

        private void FixedUpdate() => HandleExecuteFollowPlace(ExecuteFollowPlace.fixedUpdate);

        private void Update() => HandleExecuteFollowPlace(ExecuteFollowPlace.update);

        private void LateUpdate() => HandleExecuteFollowPlace(ExecuteFollowPlace.lateUpdate);

        private void HandleExecuteFollowPlace(ExecuteFollowPlace place)
        {
            if(_executeFollowPlace == place)
                Follow();
        }
        
        private void Follow()
        {
            if (!target) return;
            
            var distance = Vector3.Distance(transform.position, target.position);
            var adjustedMoveSpeed = moveSpeed + (distance * distanceMultiplier);
            var adjustedRotationSpeed = rotationSpeed + (distance * distanceMultiplier);

            transform.position = Vector3.Lerp(transform.position, target.position, adjustedMoveSpeed * Time.deltaTime);
            transform.rotation = Quaternion.Slerp(transform.rotation, target.rotation, adjustedRotationSpeed * Time.deltaTime);
        }
    }
}

