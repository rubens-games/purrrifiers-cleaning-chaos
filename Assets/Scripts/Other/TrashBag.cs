﻿using System;
using System.Collections;
using DG.Tweening;
using FishNet.Object;
using HitpointsSystem;
using NoReleaseDate.TickerSystem.Runtime;
using UnityEngine;

namespace Other
{
    public class TrashBag : NetworkBehaviour
    {
        public Transform moveTarget
        {
            get => _moveTarget;
            set
            {
                _moveTarget = value;
                
                if (_moveTarget)
                    Ticker.instance.Register(MoveToTarget, 0.2f);
                else Ticker.instance.Unregister(MoveToTarget);
            }
        }

        [SerializeField] private float _destroyTime = 5f;
        [SerializeField] private int _hitDamage = 10;
        [SerializeField] private float _minVelocityToHit = 3;
        
        [Header("Animation Settings")]
        [SerializeField] private float _animationTime = 0.5f;
        [SerializeField] private Ease _animationEase = Ease.Linear;

        private Rigidbody _rigidbody;
        private Transform _moveTarget;
        
        private void Awake() => _rigidbody = GetComponent<Rigidbody>();

        private IEnumerator Start()
        {
            yield return new WaitForSeconds(_destroyTime);
            StartDestroyingAnim();
        }

        private void OnDestroy() => moveTarget = null;

        private void StartDestroyingAnim()
        {
            transform.DOScale(Vector3.zero, _animationTime).SetEase(_animationEase)
                .OnComplete(() => Destroy(gameObject));
        }

        private void MoveToTarget(float time)
        {
            if (!_moveTarget) return;
            
            var direction = (_moveTarget.position - transform.position).normalized;
            _rigidbody.velocity = direction * 10;
        }

        private void OnCollisionEnter(Collision other)
        {
            // if (_rigidbody.velocity.magnitude < _minVelocityToHit) return;
            //
            // if (other.gameObject.TryGetComponent(out Hitable hitable))
            //     hitable.Hit((int)(_hitDamage * _rigidbody.velocity.magnitude));
        }
    }
}