﻿using UnityEngine;

namespace Other
{
    public static class Vector3Extensions
    {
        public static Vector3 With(this Vector3 vector, Vector3 other) => new(other.x, other.y, other.z);
        
        public static Vector3 With(this Vector3 vector, float? x = null, float? y = null, float? z = null) =>
            new(x ?? vector.x, y ?? vector.y, z ?? vector.z);

        public static Vector3 WithX(this Vector3 vector, float x) => new(x, vector.y, vector.z);

        public static Vector3 WithY(this Vector3 vector, float y) => new(vector.x, y, vector.z);

        public static Vector3 WithZ(this Vector3 vector, float z) => new(vector.x, vector.y, z);

        public static Vector3 Add(this Vector3 vector, Vector3 other) =>
            new(vector.x + other.x, vector.y + other.y, vector.z + other.z);
        
        public static Vector3 Add(this Vector3 vector, float? x = null, float? y = null, float? z = null) =>
            new(vector.x + (x ?? 0), vector.y + (y ?? 0), vector.z + (z ?? 0));

        public static Vector3 AddX(this Vector3 vector, float x) =>
            new(vector.x + x, vector.y, vector.z);

        public static Vector3 AddY(this Vector3 vector, float y) => new(vector.x, vector.y + y, vector.z);
        
        public static Vector3 AddZ(this Vector3 vector, float z) => new(vector.x, vector.y, vector.z + z);
    }
}