using System;
using System.Collections;
using CursorSystem;
using DG.Tweening;
using PlayerSystem;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Other
{
    public class RespawnButton : MonoBehaviour, ICursorHandler
    {
        public CursorSettings CursorSettings => CursorSettings.ShowAndConfined;

        [Header("Settings")] 
        [SerializeField] private float _disableButtonFade;
        [SerializeField] private float _unlockButtonTime = 5f;
        [SerializeField] private Ease _ease = Ease.Linear;
        
        [Header("References")]
        [SerializeField] private Slider _slider;
        [SerializeField] private Button _button;
        [SerializeField] private CanvasGroup _canvasGroup;

        [Inject] private PlayerManager _playerManager;
        [Inject] private CursorManager _cursorManager;
        
        private const string ANIM_ID_BUTTON = "RespawnButtonAnim";
        private const string ANIM_ID_FADE = "RespawnButtonFade";

        private void OnEnable()
        {
            PlayerHitable.OnPlayerHitableDeath += OnPlayerDeath;
            PlayerHitable.OnPlayerHitableRevive += OnPlayerRevive;
            _button.onClick.AddListener(OnRespawnButtonClicked);
        }

        private void OnDisable()
        {
            PlayerHitable.OnPlayerHitableDeath -= OnPlayerDeath;
            PlayerHitable.OnPlayerHitableRevive -= OnPlayerRevive;
            _button.onClick.RemoveListener(OnRespawnButtonClicked);
        }
        
        private void OnPlayerDeath(PlayerHitable playerHitable)
        {
            if (playerHitable != _playerManager.localPlayer.playerHitable)
                return;
            
            EnableButton();
        }
        
        private void OnPlayerRevive(PlayerHitable playerHitable)
        {
            if (playerHitable != _playerManager.localPlayer.playerHitable)
                return;
            
            DisableButton();
        }

        private void OnRespawnButtonClicked() => Respawn();

        private void Respawn()
        {
            var hitable = _playerManager.localPlayer.playerHitable;
            var healPoints = hitable.missingHitpoints;
            
            hitable.Heal(healPoints);
        }

        private void EnableButton()
        {
            _cursorManager.Register(this);
            
            _button.gameObject.SetActive(true);
            
            _button.interactable = false;
            _slider.value = 0;
            _canvasGroup.alpha = _disableButtonFade;
            
            _slider.DOValue(1, _unlockButtonTime).SetEase(_ease).SetId(ANIM_ID_BUTTON)
                    .onComplete += () =>
            {
                _button.interactable = true;
                _button.transform.DOPunchScale(Vector3.one * 0.1f, 0.5f);
            };
            
            _canvasGroup.DOFade(1, _unlockButtonTime).SetEase(_ease).SetId(ANIM_ID_FADE);
        }

        private void DisableButton()
        {
            DOTween.Kill(ANIM_ID_BUTTON);
            DOTween.Kill(ANIM_ID_FADE);
            
            _button.interactable = false;
            _slider.value = 0;
            _canvasGroup.alpha = _disableButtonFade;
            
            _button.gameObject.SetActive(false);
            
            _cursorManager.Unregister(this);
        }
    }
}
