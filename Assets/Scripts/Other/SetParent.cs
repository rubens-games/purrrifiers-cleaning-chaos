using System;
using UnityEngine;

namespace Other
{
    public class SetParent : MonoBehaviour
    {
        private enum SetParentPlace
        {
            Start,
            Awake,
            OnEnable,
            OnDisable,
        }
    
        public Transform target;
    
        [SerializeField] private SetParentPlace _setParentPlace = SetParentPlace.Start;

        private void Awake() => HandleSetParentPlace(SetParentPlace.Awake);
    
        private void Start() => HandleSetParentPlace(SetParentPlace.Start);

        private void OnEnable() => HandleSetParentPlace(SetParentPlace.OnEnable);

        private void OnDisable() => HandleSetParentPlace(SetParentPlace.OnDisable);

        private void HandleSetParentPlace(SetParentPlace place)
        {
            if (_setParentPlace == place)
                SetParentToTarget();
        }
    
        private void SetParentToTarget() => transform.SetParent(target);
    }
}
