using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace URPRenderFeatures
{
    public class CustomRenderFeature : ScriptableRendererFeature
    {
        [System.Serializable]
        public class CustomRenderSettings
        {
            public RenderPassEvent renderPassEvent = RenderPassEvent.AfterRenderingOpaques;
            public Material customMaterial = null;
        }

        public CustomRenderSettings settings = new();

        CustomRenderPass customRenderPass;

        public override void Create()
        {
            customRenderPass = new CustomRenderPass(settings);
        }

        public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
        {
            if (settings.customMaterial != null)
            {
                renderer.EnqueuePass(customRenderPass);
            }
        }

        private class CustomRenderPass : ScriptableRenderPass
        {
            private CustomRenderSettings settings;

            public CustomRenderPass(CustomRenderSettings settings)
            {
                this.settings = settings;
                this.renderPassEvent = settings.renderPassEvent;
            }

            public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
            {
                CommandBuffer cmd = CommandBufferPool.Get("CustomRenderPass");
            
                // Custom rendering logic goes here, for example:
                var camera = renderingData.cameraData.camera;
                cmd.Blit(null, BuiltinRenderTextureType.CameraTarget, settings.customMaterial);

                context.ExecuteCommandBuffer(cmd);
                CommandBufferPool.Release(cmd);
            }
        }
    }
}
