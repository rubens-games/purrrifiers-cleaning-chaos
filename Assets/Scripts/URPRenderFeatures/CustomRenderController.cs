﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using System.Linq;
using System.Reflection;
using URPRenderFeatures;

public class CustomRenderController : MonoBehaviour
{
    public RenderPassEvent renderPassEvent;

    private CustomRenderFeature customRenderFeature;

    void OnEnable()
    {
        FindAndAssignCustomRenderFeature();

        if (customRenderFeature != null)
        {
            customRenderFeature.settings.renderPassEvent = renderPassEvent;
        }
        else
        {
            Debug.LogWarning("CustomRenderFeature not found in the active renderer features.");
        }
    }

    void OnValidate()
    {
        if (customRenderFeature != null)
        {
            customRenderFeature.settings.renderPassEvent = renderPassEvent;
        }
    }

    private void FindAndAssignCustomRenderFeature()
    {
        // Get the current Universal Render Pipeline asset
        var pipelineAsset = GraphicsSettings.renderPipelineAsset as UniversalRenderPipelineAsset;
        if (pipelineAsset == null)
        {
            Debug.LogError("No Universal Render Pipeline Asset found.");
            return;
        }

        // Get the ScriptableRenderer from the pipeline asset
        var renderer = pipelineAsset.scriptableRenderer as ScriptableRenderer;
        if (renderer == null)
        {
            Debug.LogError("No ScriptableRenderer found in the active Universal Render Pipeline Asset.");
            return;
        }

        // Use reflection to access the protected 'rendererFeatures' property
        var rendererFeaturesField = typeof(ScriptableRenderer).GetProperty("rendererFeatures", BindingFlags.Instance | BindingFlags.NonPublic);

        if (rendererFeaturesField != null)
        {
            var rendererFeatures = rendererFeaturesField.GetValue(renderer) as System.Collections.Generic.List<ScriptableRendererFeature>;
            if (rendererFeatures != null)
            {
                // Find the CustomRenderFeature in the renderer features
                customRenderFeature = rendererFeatures.OfType<CustomRenderFeature>().FirstOrDefault();
            }
        }

        if (customRenderFeature == null)
        {
            Debug.LogWarning("CustomRenderFeature not found in the renderer features.");
        }
    }
}
