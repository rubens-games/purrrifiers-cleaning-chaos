using DetectorSystem;
using UnityEngine;
using Zenject;

namespace PlayerSystem
{
    public class LookingAt : Detector<GameObject>
    {
        protected override Ray Ray => _player.playerInput.rtsRotationMode
            ? _camera.ScreenPointToRay(Input.mousePosition)
            : new Ray(_camera.transform.position, _camera.transform.forward);

        protected override float RayMaxDistance => _rayMaxDistance;

        protected override LayerMask IgnoredLayerMask => _ignoredLayerMask;

        [SerializeField] private LayerMask _ignoredLayerMask;
        [SerializeField] private float _rayMaxDistance = 10;
        [SerializeField] private Player _player;
        [SerializeField] private Transform _headTrackingTarget;
        
        [Inject] private Camera _camera;
        
        public override void Update()
        {
            if (!_player.IsOwner) return;
            
            base.Update();
        }

        private void LateUpdate()
        {
            if (!_player.IsOwner) return;
            
            _headTrackingTarget.position = GetLookingAtPoint();
        }

        public Vector3 GetLookingAtPoint()
        {
            if (Hit.point != Vector3.zero) return Hit.point;
            
            if (Hit.collider) return Hit.point;
            
            return Ray.origin + Ray.direction * RayMaxDistance;
        }

        private void OnDrawGizmosSelected()
        {
            if (!_camera) return;
            
            Gizmos.color = Color.red;
            Gizmos.DrawRay(Ray);
            
            // Draw the Hit point
            Gizmos.DrawWireSphere(GetLookingAtPoint(), 0.1f);
        }
    }
}
