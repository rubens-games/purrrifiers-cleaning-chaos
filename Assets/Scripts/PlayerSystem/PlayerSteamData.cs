﻿using FishNet.Connection;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using HeathenEngineering.SteamworksIntegration;
using Steamworks;
using UnityEngine.Events;

namespace PlayerSystem
{
    public class PlayerSteamData : NetworkBehaviour
    {
        public UnityEvent<PlayerSteamData> OnSomethingChange = new();
        
        public UserData userData => GetUserData();

        public CSteamID cSteamId => userData.id;
        
        public ulong steamId
        {
            get => syncSteamId.Value;
            [ServerRpc] set => syncSteamId.Value = value;
        }
        
        public string steamName
        {
            get => syncSteamName.Value;
            [ServerRpc] set => syncSteamName.Value = value;
        }
        
        public string playerAvatarUrl
        {
            get => syncPlayerAvatarUrl.Value;
            [ServerRpc] set => syncPlayerAvatarUrl.Value = value;
        }
        
        public readonly SyncVar<ulong> syncSteamId = new();
        public readonly SyncVar<string> syncSteamName = new();
        public readonly SyncVar<string> syncPlayerAvatarUrl = new();

        private void OnEnable()
        {
            syncSteamId.OnChange += OnSyncSteamIdChange;
            syncSteamName.OnChange += OnSyncSteamNameChange;
            syncPlayerAvatarUrl.OnChange += OnSyncPlayerAvatarUrlChange;
        }

        private void OnDisable()
        {
            syncSteamId.OnChange -= OnSyncSteamIdChange;
            syncSteamName.OnChange -= OnSyncSteamNameChange;
            syncPlayerAvatarUrl.OnChange -= OnSyncPlayerAvatarUrlChange;
        }

        public override void OnOwnershipClient(NetworkConnection prevOwner)
        {
            base.OnOwnershipClient(prevOwner);
            
            if (!IsOwner) return;
            
            UpdatePlayerSteamData();
        }

        private void OnSyncSteamIdChange(ulong oldValue, ulong newValue, bool asServer) => OnSomethingChange.Invoke(this);

        private void OnSyncSteamNameChange(string oldValue, string newValue, bool asServer) => OnSomethingChange.Invoke(this);

        private void OnSyncPlayerAvatarUrlChange(string oldValue, string newValue, bool asServer) => OnSomethingChange.Invoke(this);

        private void UpdatePlayerSteamData()
        {
            if (!IsOwner) return;
            
            steamId = SteamUser.GetSteamID().m_SteamID;
            steamName = SteamFriends.GetPersonaName();
        }
        
        public UserData GetUserData() => UserData.Get(steamId);
    }
}