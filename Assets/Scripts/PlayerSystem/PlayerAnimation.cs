using ECM2.Examples.FirstPerson;
using FishNet.Component.Animating;
using FishNet.Connection;
using FishNet.Object;
using UnityEngine;

namespace PlayerSystem
{
    public class PlayerAnimation : NetworkBehaviour
    {
        public bool IsSitOnToilet { get; set; }
        
        private bool IsRotationRight => _playerInput.InputLook.x > 0;
        private bool IsRotationLeft => _playerInput.InputLook.x < 0;
        
        [SerializeField] private Animator _animator;
        [SerializeField] private RuntimeAnimatorController _defaultAnimatorController;
        [SerializeField] private AnimatorOverrideController _overrideAnimatorController;
        [SerializeField] private NetworkAnimator _networkAnimator;
        [SerializeField] private FirstPersonCharacter _character;
        [SerializeField] private PlayerInput _playerInput;
        [SerializeField] private float _transitionTime = 0.2f;
        [SerializeField] private float _blendTreeBlendingSpeed = 10f;
        [SerializeField] private float _animMoveSpeedMultiplierInStanding = 1.5f;
        [SerializeField] private float _animMoveSpeedMultiplierInStandingSprinting = 1.5f;
        [SerializeField] private float _animMoveSpeedMultiplierInCrouch = 1f;
        [SerializeField] private float _animMoveSpeedMultiplierInCrouchSprinting = 1f;
        
        private static readonly int _animIdSpeed = Animator.StringToHash("MoveSpeed");
        private static readonly int _animIdMoveDirectionX = Animator.StringToHash("MoveDirectionX");
        private static readonly int _animIdMoveDirectionY = Animator.StringToHash("MoveDirectionY");
        
        private const string ANIM_STATE_IDLE = "Idle01";
        private const string ANIM_STATE_LOCOMOTION_STANDING = "LocomotionStanding";
        private const string ANIM_STATE_LOCOMOTION_CROUCHING = "LocomotionCrouching";
        private const string ANIM_STATE_LOCOMOTION_SWIMMING = "LocomotionSwimming";
        private const string ANIM_STATE_JUMP = "Jump";
        private const string ANIM_STATE_JUMP_LANDING = "JumpLand";
        private const string ANIM_STATE_FALLING = "Falling";
        private const string ANIM_STATE_SHUFFLE_LEFT = "ShuffleLeft";
        private const string ANIM_STATE_SHUFFLE_RIGHT = "ShuffleRight";
        private const string ANIM_STATE_SITTING = "SittingOnToilet";
        
        private string _currentState;
        private float _blendTreeMoveSpeed;
        private Vector2 _blendTreeMoveDirection;

        private void Update()
        {
            if (!IsOwner) return;

            ChangeState(GetState());

            float currentMoveSpeedMultiplayer;
            if (_character.IsCrouched())
                currentMoveSpeedMultiplayer = _character.IsSprinting()
                    ? _animMoveSpeedMultiplierInCrouchSprinting
                    : _animMoveSpeedMultiplierInCrouch;
            else
                currentMoveSpeedMultiplayer = _character.IsSprinting()
                    ? _animMoveSpeedMultiplierInStandingSprinting
                    : _animMoveSpeedMultiplierInStanding;

            var currentMoveSpeed = _character.GetMaxSpeed() / _character.GetMaxStateSpeed() * currentMoveSpeedMultiplayer;
            _blendTreeMoveSpeed = Mathf.Lerp(_blendTreeMoveSpeed, currentMoveSpeed, _blendTreeBlendingSpeed * Time.deltaTime);
            
            _animator.SetFloat(_animIdSpeed, _blendTreeMoveSpeed);
            
            var currentMoveDirection = transform.InverseTransformDirection(_character.GetMovementDirection());
            _blendTreeMoveDirection = Vector2.Lerp(_blendTreeMoveDirection,
                new Vector2(currentMoveDirection.x, currentMoveDirection.z), _blendTreeBlendingSpeed * Time.deltaTime);

            _animator.SetFloat(_animIdMoveDirectionX, _blendTreeMoveDirection.x);
            _animator.SetFloat(_animIdMoveDirectionY, _blendTreeMoveDirection.y);
        }

        public override void OnOwnershipClient(NetworkConnection prevOwner)
        {
            base.OnOwnershipClient(prevOwner);

            _animator.runtimeAnimatorController = IsOwner ? _overrideAnimatorController : _defaultAnimatorController;
        }

        private string GetState()
        {
            if (IsSitOnToilet)
                return ANIM_STATE_SITTING;
            
            if (_character.isPaused)
                return ANIM_STATE_IDLE;
            
            if (_character.IsCrouched())
                return ANIM_STATE_LOCOMOTION_CROUCHING;
            
            if (_character.IsWalking())
            {
                // if (_character.GetSpeed() == 0)
                // {
                //     if (IsRotationLeft)
                //         return ANIM_STATE_SHUFFLE_LEFT;
                //
                //     if (IsRotationRight)
                //         return ANIM_STATE_SHUFFLE_RIGHT;
                // }
                
                if (_character.GetSpeed() > 0)
                    return ANIM_STATE_LOCOMOTION_STANDING;
            }

            if (_character.IsSwimming())
                return ANIM_STATE_LOCOMOTION_SWIMMING;

            if (_character.IsFalling()) 
                return _character.IsJumping() ? ANIM_STATE_JUMP : ANIM_STATE_FALLING;
            
            return ANIM_STATE_IDLE;
        }
        
        private void ChangeState(string state)
        {
            if (_currentState == state) return;
            _currentState = state;
            
            // _networkAnimator.Play(state);
            _networkAnimator.CrossFade(state, _transitionTime, 0, 0, 0);
        }
    }
}
