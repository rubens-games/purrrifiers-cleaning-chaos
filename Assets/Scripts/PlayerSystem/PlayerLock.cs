﻿using System;
using System.Collections.Generic;
using FishNet.Object;
using ToolSystem;
using UnityEngine;

namespace PlayerSystem
{
    public class PlayerLock : NetworkBehaviour, IPlayerLockHandler
    {
        public event Action<IPlayerLockHandler> onRegister;
        public event Action<IPlayerLockHandler> onUnregister;

        public IPlayerLockHandler currentHandler => handlers.Count > 0 ? handlers[0] : this;
        public List<IPlayerLockHandler> handlers { get; } = new();

        [SerializeField] private Player _player;
        
        public bool isLocked => false;

        public override void OnStartClient()
        {
            base.OnStartClient();
            
            if (!IsOwner) return;
            
            Register(this);
        }

        public override void OnStopClient()
        {
            base.OnStopClient();
            
            if (!IsOwner) return;
            
            Unregister(this);
            UnregisterAll();
        }

        public void Register(IPlayerLockHandler handler)
        {
            handlers.Insert(0, handler);
            
            ActiveHandler(handler);
            
            onRegister?.Invoke(handler);
        }
        
        public void Unregister(IPlayerLockHandler handler)
        {
            handlers.Remove(handler);
            
            ActiveHandler(currentHandler);
            
            onUnregister?.Invoke(handler);
        }
        
        private void UnregisterAll()
        {
            for (var i = 0; i < handlers.Count; i++) 
                Unregister(handlers[i]);
        }
        
        private void ActiveHandler(IPlayerLockHandler handler)
        {
            if (handler == null) return;
            
            if (currentHandler != handler)
            {
                handlers.Remove(handler);
                handlers.Insert(0, handler);
            }
            
            SetSettings(handler);
        }
        
        private void SetSettings(IPlayerLockHandler handler)
        {
            if (handler.isLocked) LockPlayer();
            else UnlockPlayer();
        }
        
        private void LockPlayer() => LockPlayerServerRpc();

        [ServerRpc]
        private void LockPlayerServerRpc() => LockPlayerObserverRpc();

        [ObserversRpc]
        private void LockPlayerObserverRpc()
        {
            _player.character.Pause(true);
            _player.playerInput.isInputLocked = true;
            _player.interactionController.CanInteract = false;

            if (!IsOwner) return;
            
            var rightHandPickable = _player.pickUpControllerRight.CurrentPickup;
            var leftHandPickable = _player.pickUpControllerLeft.CurrentPickup;

            if (rightHandPickable is Tool rightHandTool) 
                rightHandTool.DisableInputs(); 
                
            if (leftHandPickable is Tool leftHandTool) 
                leftHandTool.DisableInputs();
        }

        private void UnlockPlayer() => UnlockPlayerServerRpc();

        [ServerRpc]
        private void UnlockPlayerServerRpc() => UnlockPlayerObserverRpc();
        
        [ObserversRpc]
        private void UnlockPlayerObserverRpc()
        {
            _player.character.Pause(false);
            _player.playerInput.isInputLocked = false;
            _player.interactionController.CanInteract = true;
            
            if (!IsOwner) return;
            
            var rightHandPickable = _player.pickUpControllerRight.CurrentPickup;
            var leftHandPickable = _player.pickUpControllerLeft.CurrentPickup;

            if (rightHandPickable is Tool rightHandTool) 
                rightHandTool.EnableInputs();
                
            if (leftHandPickable is Tool leftHandTool) 
                leftHandTool.EnableInputs();
        }
    }
}