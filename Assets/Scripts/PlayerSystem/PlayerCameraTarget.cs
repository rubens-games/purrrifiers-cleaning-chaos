using CameraSystem;
using ECM2.Examples.FirstPerson;
using FishNet.Object;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace PlayerSystem
{
    public class PlayerCameraTarget : NetworkBehaviour, ICameraTarget
    {
        [SerializeField] private FirstPersonCharacter _firstPersonCharacter;
        [SerializeField] private Transform _playerModel;
        
        [Inject] private CameraManager _cameraManager;

        public override void OnStartClient()
        {
            base.OnStartClient();
            
            if (IsOwner)
                _cameraManager.RegisterTarget(this);
        }
        
        public override void OnStopClient()
        {
            base.OnStopClient();
            
            if (IsOwner)
                _cameraManager.UnregisterTarget(this);
        }

        #region ICameraTarget

        public UnityEvent OnRegistered { get; } = new();
        public UnityEvent OnUnregistered { get; } = new();

        public Transform CameraFollowTarget => _firstPersonCharacter.cameraParent.transform;
        
        public void OnRegister()
        {
            _firstPersonCharacter.camera = transform.GetChild(0);
            _cameraManager.Camera.transform.SetParent(_playerModel);
        }

        public void OnUnregister()
        {
            _firstPersonCharacter.camera = null;
        }

        #endregion
    }
}
