﻿using ECM2;
using FishNet.Object;
using FishNet.Object.Prediction;
using FishNet.Transporting;
using UnityEngine;

namespace PlayerSystem
{
    public class PlayerMovementNetwork : NetworkBehaviour
    {
        private CharacterMovement characterMovement { get; set; }
        private Character character { get; set; }
        
        private void Awake()
        {
            characterMovement = GetComponent<CharacterMovement>();
            character = GetComponent<Character>();
        }
        
        #region STRUCTS

        /// <summary>
        /// Input movement data.
        /// </summary>
        
        private struct MoveData : IReplicateData
        {
            public readonly float horizontal;
            public readonly float vertical;
            public readonly bool jump;
        
            private uint _tick;

            public MoveData(float horizontal, float vertical, bool jump)
            {
                this.horizontal = horizontal;
                this.vertical = vertical;
                this.jump = jump;
                _tick = 0;
            }

            public void Dispose() { }
            public uint GetTick() => _tick;
            public void SetTick(uint value) => _tick = value;
        }
        
        /// <summary>
        /// Reconciliation data.
        /// </summary>
        
        private struct ReconcileData : IReconcileData
        {
            public readonly Vector3 position;
            public readonly Quaternion rotation;
        
            public readonly Vector3 velocity;
        
            public readonly bool constrainedToGround;
            public readonly float unconstrainedTime;
        
            public readonly bool hitGround;
            public readonly bool isWalkable;
        
            private uint _tick;

            public ReconcileData(Vector3 position, Quaternion rotation, Vector3 velocity, bool constrainedToGround,
                float unconstrainedTime, bool hitGround, bool isWalkable)
            {
                this.position = position;
                this.rotation = rotation;
                
                this.velocity = velocity;
                
                this.constrainedToGround = constrainedToGround;
                this.unconstrainedTime = unconstrainedTime;
                
                this.hitGround = hitGround;
                this.isWalkable = isWalkable;

                _tick = 0;
            }

            public void Dispose() { }
            public uint GetTick() => _tick;
            public void SetTick(uint value) => _tick = value;
        }

        #endregion
        
        #region METHODS

        private MoveData BuildMoveData()
        {
            if (!IsOwner)
                return default;
            
            MoveData moveData;

            float horizontal = character.GetMovementDirection().x;
            float vertical = character.GetMovementDirection().y;
            
            if (horizontal != 0 || vertical != 0)
                moveData = new MoveData(horizontal, vertical, character.IsJumping());
            else
                moveData = new MoveData(horizontal, vertical, character.IsJumping());

            character.StopJumping();

            return moveData;
        }

        [Replicate]
        private void Simulate(MoveData md, ReplicateState state = ReplicateState.Invalid,
            Channel channel = Channel.Unreliable)
        {
            // Jump
            
            if (md.jump && characterMovement.isGrounded)
            {
                characterMovement.PauseGroundConstraint();
                characterMovement.velocity.y = Mathf.Max(characterMovement.velocity.y, character.jumpImpulse);
            }
            
            
            // Movement

            Vector3 moveDirection = Vector3.right * md.horizontal + Vector3.forward * md.vertical;
            moveDirection = Vector3.ClampMagnitude(moveDirection, 1.0f);
            
            Vector3 desiredVelocity = moveDirection * character.maxWalkSpeed;

            float actualAcceleration = characterMovement.isGrounded ? character.maxAcceleration : character.maxAcceleration * character.airControl;
            float actualDeceleration = characterMovement.isGrounded ? character.brakingDeceleration : 0.0f;

            float actualFriction = characterMovement.isGrounded ? character.groundFriction : character.flyingFriction;

            float deltaTime = (float)TimeManager.TickDelta;
            
            characterMovement.RotateTowards(moveDirection, character.rotationRate * deltaTime);
            // characterMovement.RotateTowards(md.lookingDirection, rotationRate * deltaTime);
            characterMovement.SimpleMove(desiredVelocity, character.maxWalkSpeed, actualAcceleration, actualDeceleration,
                actualFriction, actualFriction, character.gravity, true, deltaTime);
            
            // Rotate the camera parent based on looking direction
            // CameraParent.rotation = Quaternion.LookRotation(md.lookingDirection, Vector3.up);
        }

        [Reconcile]
        private void Reconciliation(ReconcileData rd, Channel channel = Channel.Unreliable)
        {
            characterMovement.SetState(
                rd.position,
                rd.rotation,
                rd.velocity,
                rd.constrainedToGround,
                rd.unconstrainedTime,
                rd.hitGround,
                rd.isWalkable);
        }
        
        private void OnTick()
        {
            if (!IsOwner) Simulate(BuildMoveData());

            if (IsServerStarted)
            {
                ReconcileData reconcileData = new ReconcileData(
                    characterMovement.position,
                    characterMovement.rotation,
                    characterMovement.velocity,
                    characterMovement.constrainToGround,
                    characterMovement.unconstrainedTimer,
                    characterMovement.currentGround.hitGround,
                    characterMovement.currentGround.isWalkable
                );

                Reconciliation(reconcileData);
            }
        }
        
        public override void OnStartNetwork()
        {
            TimeManager.OnTick += OnTick;
        }
        
        public override void OnStopNetwork()
        {
            if (TimeManager != null)
                TimeManager.OnTick -= OnTick;
        }

        #endregion
        
        #region NETWORK CALLBACKS
        
        public override void CreateReconcile()
        {
            Reconciliation(new ReconcileData(characterMovement.position, characterMovement.rotation,
                characterMovement.velocity, characterMovement.constrainToGround, characterMovement.unconstrainedTimer,
                characterMovement.currentGround.hitGround, characterMovement.currentGround.isWalkable));
        }
        
        #endregion
    }
}