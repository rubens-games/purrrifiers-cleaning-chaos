﻿using System;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using UnityEngine;

namespace PlayerSystem
{
    public class PlayerFollowingQuestNode : NetworkBehaviour
    {
        public static event Action<Player, string, string> evtQuestNodeChanged; 
        
        public string questNodeId
        {
            get => _questNodeId.Value;
            [ServerRpc] set => _questNodeId.Value = value;
        }

        [SerializeField] private Player _player;
        
        private readonly SyncVar<string> _questNodeId = new();

        private void OnEnable()
        {
            _questNodeId.OnChange += OnQuestNodeIdChanged;
        }
        
        private void OnDisable()
        {
            _questNodeId.OnChange -= OnQuestNodeIdChanged;
        }
        
        private void OnQuestNodeIdChanged(string oldValue, string newValue, bool asServer)
        {
            evtQuestNodeChanged?.Invoke(_player, oldValue, newValue);
        }
    }
}