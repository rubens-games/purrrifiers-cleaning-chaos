using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace PlayerSystem.RigSystem
{
    public enum RigType
    {
        Default,
        SittingOnToilet,
    }
    
    [Serializable]
    public class RigCombination
    {
        public RigType RigType;
        public List<Rig> Rigs;
    }
    
    public class PlayerRigController : MonoBehaviour
    {
        [SerializeField] private RigBuilder _rigBuilder;
        [SerializeField] private List<RigCombination> _rigCombinations;
        
        public void ActiveRigCombination(RigType rigType)
        {
            var rigCombination = _rigCombinations.Find(x => x.RigType == rigType);
            
            if (rigCombination == null) return;
            
            ActiveRigCombination(rigCombination);
        }
        
        private void ActiveRigCombination(RigCombination rigCombination)
        {
            DeActiveRigs();
            
            foreach (var rig in rigCombination.Rigs) 
                rig.weight = 1;
        }
        
        private void DeActiveRigs()
        {
            foreach (var layer in _rigBuilder.layers) 
                layer.rig.weight = 0;
        }
    }
}
