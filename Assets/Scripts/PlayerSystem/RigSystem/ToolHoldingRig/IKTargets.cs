﻿using System;
using UnityEngine;

namespace PlayerSystem.RigSystem.ToolHoldingRig
{
    [Serializable]
    public class PositionAndRotationTarget
    {
        public bool showPositionHandle;
        public bool showRotationHandle;
        public Vector3 position;
        public Vector3 rotation;
    }
    
    [Serializable]
    public class IKTarget
    {
        [Range(0f, 1f)] public float weight;
        public PositionAndRotationTarget target;
        public PositionAndRotationTarget hint;
    }
    
    [Serializable]
    public class HandIKTarget : IKTarget
    {
        public IKTarget ThumbIK;
        public IKTarget IndexIK;
        public IKTarget MiddleIK;
        public IKTarget RingIK;
        public IKTarget PinkyIK;
    }
    
    [Serializable]
    public class HandsIKTarget
    {
        public HandIKTarget RightHandIK;
        public HandIKTarget LeftHandIK;
    }
}