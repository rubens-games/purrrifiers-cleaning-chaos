using System;
using PickableSystem;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace PlayerSystem.RigSystem.ToolHoldingRig
{
    [Serializable]
    public class ToolHoldingFingerRig
    {
        public TwoBoneIKConstraint ThumbIK;
        public TwoBoneIKConstraint IndexIK;
        public TwoBoneIKConstraint MiddleIK;
        public TwoBoneIKConstraint RingIK;
        public TwoBoneIKConstraint PinkyIK;
    }

    [Serializable]
    public class ToolHoldingHandRig
    {
        public TwoBoneIKConstraint IK;
        public ToolHoldingFingerRig FingerRig;
    }

    public class ToolHoldingRig : MonoBehaviour
    {
        [SerializeField] private PickUpController _pickUpControllerRight;
        [SerializeField] private PickUpController _pickUpControllerLeft;
        [SerializeField] private LookingAt _lookingAt;
        
        public ToolHoldingHandRig RightHandIK;
        public ToolHoldingHandRig LeftHandIK;
        public MultiAimConstraint PrimaryWeaponAim;
        public MultiAimConstraint LeftHandWeaponAim;
        public MultiAimConstraint BothHandWeaponAim;

        // private void OnAnimatorIK(int layerIndex) => UpdateRigs();

        private void LateUpdate() => UpdateRigs();

        private void UpdateRigs()
        {
            
            // if (_pickUpController.CurrentPickup)
            // {
            //     var currentPickup = _pickUpController.CurrentPickup;
            //     var source = currentPickup.Origin;
            //     var handsIKTarget = currentPickup.HandsIKTarget;
            //     var rightHandIKTarget = handsIKTarget.RightHandIK;
            //     var leftHandIKTarget = handsIKTarget.LeftHandIK;
            //     
            //     currentPickup.OnAnimateBefore?.Invoke(_pickUpController);
            //     ApplyIK(RightHandIK, rightHandIKTarget, source);
            //     ApplyIK(LeftHandIK, leftHandIKTarget, source);
            //     
            //     if (currentPickup.AniamteLookAt)
            //     {
            //         PrimaryWeaponAim.weight = 1f;
            //         PrimaryWeaponAim.data.sourceObjects[0].transform.position = _lookingAt.GetLookingAtPoint();
            //     }
            //     else PrimaryWeaponAim.weight = 0f;
            //     
            //     currentPickup.OnAnimateAfter?.Invoke(_pickUpController);
            // }
            // else
            // {
            //     SetZeroWeight(RightHandIK);
            //     SetZeroWeight(LeftHandIK);
            //     PrimaryWeaponAim.weight = 0f;
            // }

            var rightHandPickup = _pickUpControllerRight.CurrentPickup;
            var leftHandPickup = _pickUpControllerLeft.CurrentPickup;
            
            if (!rightHandPickup) rightHandPickup = leftHandPickup;
            if (!leftHandPickup) leftHandPickup = rightHandPickup;
            
            if (rightHandPickup)
            {
                var source = rightHandPickup.Origin;
                var handsIKTarget = rightHandPickup.HandsIKTarget;
                var rightHandIKTarget = handsIKTarget.RightHandIK;
                
                rightHandPickup.OnAnimateBefore?.Invoke(_pickUpControllerRight);
                ApplyIK(RightHandIK, rightHandIKTarget, source);
                rightHandPickup.OnAnimateAfter?.Invoke(_pickUpControllerRight);
            }
            else SetZeroWeight(RightHandIK);
            
            if (leftHandPickup)
            {
                var source = leftHandPickup.Origin;
                var handsIKTarget = leftHandPickup.HandsIKTarget;
                var leftHandIKTarget = handsIKTarget.LeftHandIK;
                
                leftHandPickup.OnAnimateBefore?.Invoke(_pickUpControllerRight);
                ApplyIK(LeftHandIK, leftHandIKTarget, source);
                leftHandPickup.OnAnimateAfter?.Invoke(_pickUpControllerRight);
            }
            else SetZeroWeight(LeftHandIK);

            if (rightHandPickup && rightHandPickup.AniamteLookAt)
            {
                PrimaryWeaponAim.weight = 1f;
                PrimaryWeaponAim.data.sourceObjects[0].transform.position = _lookingAt.GetLookingAtPoint();
            }
            else PrimaryWeaponAim.weight = 0f;

            if (leftHandPickup && leftHandPickup.AniamteLookAt)
            {
                LeftHandWeaponAim.weight = 1f;
                LeftHandWeaponAim.data.sourceObjects[0].transform.position = _lookingAt.GetLookingAtPoint();
            }
            else LeftHandWeaponAim.weight = 0f;
        }

        private void SetZeroWeight(ToolHoldingHandRig handRig)
        {
            handRig.IK.weight = 0f;
            handRig.FingerRig.ThumbIK.weight = 0f;
            handRig.FingerRig.IndexIK.weight = 0f;
            handRig.FingerRig.MiddleIK.weight = 0f;
            handRig.FingerRig.RingIK.weight = 0f;
            handRig.FingerRig.PinkyIK.weight = 0f;
        }

        private void ApplyIK(ToolHoldingHandRig handRig, HandIKTarget handIKTarget, Transform source)
        {
            ApplyIK(handRig.IK, handIKTarget, source);
            ApplyIK(handRig.FingerRig.ThumbIK, handIKTarget.ThumbIK, source);
            ApplyIK(handRig.FingerRig.IndexIK, handIKTarget.IndexIK, source);
            ApplyIK(handRig.FingerRig.MiddleIK, handIKTarget.MiddleIK, source);
            ApplyIK(handRig.FingerRig.RingIK, handIKTarget.RingIK, source);
            ApplyIK(handRig.FingerRig.PinkyIK, handIKTarget.PinkyIK, source);
        }
        
        private void ApplyIK(TwoBoneIKConstraint ik, IKTarget ikTarget, Transform source)
        {
            ik.weight = ikTarget.weight;
            
            var newTargetPosition = source.TransformPoint(ikTarget.target.position);
            var newTargetRotation = source.rotation * Quaternion.Euler(ikTarget.target.rotation);
            
            var newHintPosition = source.TransformPoint(ikTarget.hint.position);
            var newHintRotation = source.rotation * Quaternion.Euler(ikTarget.hint.rotation);
            
            ik.data.target.position = newTargetPosition;
            ik.data.target.rotation = newTargetRotation;
            
            ik.data.hint.position = newHintPosition;
            ik.data.hint.rotation = newHintRotation;
        }
    }
}
