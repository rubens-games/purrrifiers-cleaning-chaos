﻿using Zenject;

namespace PlayerSystem
{
    public class PlayerManagerInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<PlayerManager>().AsSingle().NonLazy();
        }
    }
}