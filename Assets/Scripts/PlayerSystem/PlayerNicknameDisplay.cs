using System;
using DG.Tweening;
using FishNet.Connection;
using HeathenEngineering.SteamworksIntegration.UI;
using RebuildUISystem;
using TMPro;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.UI;
using Zenject;

namespace PlayerSystem
{
    public class PlayerNicknameDisplay : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private float _displayAreaRadius = 5f;
        [SerializeField] private float _startFadeDistance = 4f;
        [SerializeField] private Vector3 _scale = new(0.01f, 0.01f, 0.01f);
        
        [Header("References")]
        [SerializeField] private RectTransform _canvasRect;
        [SerializeField] private Player _player;
        [SerializeField] private PlayerSteamData _playerSteamData;
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private SetUserName _setUserName;
        [SerializeField] private SetUserAvatar _setUserAvatar;
        [SerializeField] private Slider _hpSlider;

        [Inject] private Camera _camera;
        [Inject] private PlayerManager _playerManager;
        
        private void Awake()
        {
            _playerSteamData.syncSteamId.OnChange += OnSyncSteamIdChange;
            _player.OnOwnershipChange.AddListener(OnOwnershipChange);
            _player.playerHitable.OnHitpointsChange.AddListener(OnHitpointsChange);
        }

        private void OnDestroy()
        {
            _playerSteamData.syncSteamId.OnChange -= OnSyncSteamIdChange;
            _player.OnOwnershipChange.RemoveListener(OnOwnershipChange);
            _player.playerHitable.OnHitpointsChange.RemoveListener(OnHitpointsChange);
        }

        private void Update()
        {
            HandleDisplayArea();
            HandleScale();

            if (_camera) transform.LookAt(_camera.transform);
        }

        private void OnSyncSteamIdChange(ulong prev, ulong next, bool asServer)
        {
            UpdateNickname(next);
            UpdateAvatar(next);
        }

        private void OnOwnershipChange(NetworkConnection prevOwner, NetworkConnection newOwner)
        {
            if (newOwner == null)
            {
                DisableNickname();
                return;
            }

            if (newOwner != _playerManager.localPlayer.Owner) EnableNickname();
            else DisableNickname();
        }

        private void UpdateNickname(ulong steamId)
        {
            _setUserName.SetName(_playerSteamData.userData);
            RebuildUIManager.instance.Register(_canvasRect);
        }

        private void UpdateAvatar(ulong steamId)
        {
            _setUserAvatar.LoadAvatar(_playerSteamData.userData);
            RebuildUIManager.instance.Register(_canvasRect);
        }

        private void HandleDisplayArea()
        {
            // Fade out the nickname when the player is far away
            var distance = Vector3.Distance(_camera.transform.position, transform.position);
            var newAlpha = Mathf.Clamp01((distance - _startFadeDistance) / (_displayAreaRadius - _startFadeDistance));
            _canvasGroup.alpha = newAlpha;
        }

        private void HandleScale()
        {
            // Scale the nickname based on the distance from the camera that will make it always the same size on screen
            var distance = Vector3.Distance(_camera.transform.position, transform.position);
            var scale = _scale * distance;
            transform.localScale = scale;
        }

        public void EnableNickname()
        {
            UpdateNickname(_playerSteamData.steamId);
            UpdateAvatar(_playerSteamData.steamId);
            // UpdateLookAtTarget(_camera.transform);
            
            gameObject.SetActive(true);
        }
        
        public void DisableNickname()
        {
            gameObject.SetActive(false);
        }
        
        private void OnHitpointsChange(int hitpoints)
        {
            var maxHitpoints = _player.playerHitable.maxHitpoints;
            var hitpointsPercentage = (float)hitpoints / maxHitpoints;
            
            UpdateHealthSlider(hitpointsPercentage);
        }
        
        private void UpdateHealthSlider(float hitpointsPercentage)
        {
            _hpSlider.DOValue(hitpointsPercentage, 0.2f);
        }
    }
}
