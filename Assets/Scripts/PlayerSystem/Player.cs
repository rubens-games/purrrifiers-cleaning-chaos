using CursorSystem;
using ECM2.Examples.FirstPerson;
using FishNet.Connection;
using FishNet.Object;
using InteractionSystem;
using PickableSystem;
using PlayerSystem.RigSystem;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace PlayerSystem
{
    public class Player : NetworkBehaviour
    {
        [field: SerializeField] public UnityEvent<NetworkConnection, NetworkConnection> OnOwnershipChange { get; private set; } = new();
        
        [Inject] public Camera playerCamera { get; private set; }

        public PlayerFollowingQuestNode playerFollowingQuestNode;
        public PlayerSteamData playerSteamData;
        public PlayerModel playerModel;
        public FirstPersonCharacter character;
        public PlayerInput playerInput;
        public PickUpController pickUpControllerRight;
        public PickUpController pickUpControllerLeft;
        public PlayerRagdollController ragdollController;
        public PlayerHitable playerHitable;
        public InteractionController interactionController;
        public LookingAt lookingAt;
        public PlayerLock playerLock;
        public PlayerRigController playerRigController;
        public PlayerAnimation playerAnimation;

        [Inject] private PlayerManager _playerManager;
        [Inject] private CursorManager _cursorManager;

        private void OnEnable()
        {
            _playerManager.Register(this);

            playerSteamData.OnSomethingChange.AddListener(OnPlayerSteamDataChanged);
        }

        private void OnDisable()
        {
            _playerManager.Unregister(this);

            playerSteamData.OnSomethingChange.RemoveListener(OnPlayerSteamDataChanged);
        }

        public override void OnStartClient()
        {
            base.OnStartClient();

            UpdateRigidbodySettings();
        }

        public override void OnOwnershipClient(NetworkConnection prevOwner)
        {
            base.OnOwnershipClient(prevOwner);
            
            OnOwnershipChange?.Invoke(prevOwner, Owner);
            
            if (Owner == _playerManager.localConnection)
                _playerManager.onLocalPlayerChange?.Invoke(this);
        }

        private void OnPlayerSteamDataChanged(PlayerSteamData arg0)
        {
            gameObject.name = $"Player: {playerSteamData.steamName} ({playerSteamData.steamId})";
        }
        
        private void UpdateRigidbodySettings()
        {
            var rb = GetComponent<Rigidbody>();
            rb.isKinematic = true;
        }
    }
}
