﻿namespace PlayerSystem
{
    public interface IPlayerLockHandler
    {
        bool isLocked { get; }
    }
}