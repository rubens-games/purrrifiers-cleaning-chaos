using FishNet.Object;
using UnityEngine;

namespace PlayerSystem
{
    public class PlayerRagdollController : NetworkBehaviour, IPlayerLockHandler
    {
        public bool isLocked => true;
        
        [SerializeField] private Player _player;
        [SerializeField] private Animator _animator;
        [SerializeField] private Transform _hips;
        [SerializeField] private Rigidbody[] _ragdollRigidbodies;

        [ContextMenu("Find Ragdoll References")]
        private void FindRagdollReferences()
        {
            _ragdollRigidbodies = _hips.GetComponentsInChildren<Rigidbody>();
        }

        [ContextMenu("Enable Ragdoll Local")]
        private void EnableRagdollLocal() => EnableRigidbodies();

        [ContextMenu("Disable Ragdoll Local")]
        private void DisableRagdollLocal() => DisableRigidbodies();

        [ContextMenu("Enable Ragdoll")]
        public void EnableRagdoll() => EnableRagdollServerRpc();

        [ServerRpc]
        private void EnableRagdollServerRpc() => EnableRagdollObserversRpc();

        [ObserversRpc]
        private void EnableRagdollObserversRpc()
        {
            DisablePlayerController();
            EnableRigidbodies();
            ApplyZeroForceToRigidbodies();
        }
        
        [ContextMenu("Disable Ragdoll")]
        public void DisableRagdoll() => DisableRagdollServerRpc();

        [ServerRpc]
        private void DisableRagdollServerRpc() => DisableRagdollObserversRpc();
        
        [ObserversRpc]
        private void DisableRagdollObserversRpc()
        {
            EnablePlayerController();
            DisableRigidbodies();
        }

        private void EnableRigidbodies() => SetRigidbodiesIsKinematicState(false);
        private void DisableRigidbodies() => SetRigidbodiesIsKinematicState(true);
        
        private void SetRigidbodiesIsKinematicState(bool state)
        {
            foreach (var rb in _ragdollRigidbodies) 
                rb.isKinematic = state;
        }
        
        private void ApplyZeroForceToRigidbodies()
        {
            foreach (var rb in _ragdollRigidbodies) 
                rb.velocity = Vector3.zero;
        }
        
        private void EnablePlayerController()
        {
            _animator.enabled = true;
            // _player.UnlockPlayer();
            _player.playerLock.Unregister(this);
        }

        private void DisablePlayerController()
        {
            _animator.enabled = false;
            // _player.LockPlayer();
            _player.playerLock.Register(this);
        }
    }
}