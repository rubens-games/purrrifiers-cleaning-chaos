using System;
using CursorSystem;
using ECM2.Examples.FirstPerson;
using FishNet.Connection;
using FishNet.Object;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace PlayerSystem
{
    public class PlayerInput : NetworkBehaviour, ICursorHandler
    {
        #region ICursorHandler
        
        public CursorSettings CursorSettings => CursorSettings.HideAndLock;

        #endregion
        
        public Vector2 InputMove => new(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        public Vector2 InputLook
        {
            get
            {
                if (!rtsRotationMode)
                    return new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
                
                var mousePosition = (Vector2)Input.mousePosition;
                var screenSize = new Vector2(Screen.width, Screen.height);
                var deadZone = screenSize * border;
                
                var screenCenter = screenSize * 0.5f;
                
                // Calculate the dead zone bounds (left, right, top, bottom)
                var deadZoneMin = screenCenter - deadZone * 0.5f;
                var deadZoneMax = screenCenter + deadZone * 0.5f;

                // Check if the mouse is within the dead zone
                var isMouseInDeadZone = mousePosition.x >= deadZoneMin.x && mousePosition.x <= deadZoneMax.x &&
                                        mousePosition.y >= deadZoneMin.y && mousePosition.y <= deadZoneMax.y;

                
                if (isMouseInDeadZone) return Vector2.zero;
                
                var tmpDirection = mousePosition / screenSize;
                var remappedMousePosition = Remap(mousePosition, Vector2.zero, screenSize, screenSize * -.5f, screenSize * .5f).normalized;
                
                return remappedMousePosition;


                // var mousePosition = (Vector2)Input.mousePosition;
                // var tmpSize = new Vector2(Screen.width, Screen.height);
                // var tmpDirection = mousePosition / tmpSize;
                //
                // // Sprawdzanie czy myszka jest blisko krawędzi
                // if (tmpDirection.x < border || tmpDirection.y < border || 1f - tmpDirection.x < border || 1f - tmpDirection.y < border)
                // {
                //     // Remapowanie pozycji myszy
                //     mousePosition = Remap(mousePosition, Vector2.zero, tmpSize, tmpSize * -.5f, tmpSize * .5f).normalized;
                //     Debug.Log($"{mousePosition}");
                //     return mousePosition;
                // }
                //
                // // Jeśli myszka nie jest blisko krawędzi, nie zwracamy ruchu kamery
                // return Vector2.zero;
            }
        }

        public bool isInputLocked;
        
        public bool rtsRotationMode;
        public float border = 0.1f;
        
        [Space(15.0f)]
        public bool invertLook = true;
        [Tooltip("Mouse look sensitivity")]
        public Vector2 mouseSensitivity = new(1.0f, 1.0f);
        
        [Space(15.0f)]
        [Tooltip("How far in degrees can you move the camera down.")]
        public float minPitch = -80.0f;
        [Tooltip("How far in degrees can you move the camera up.")]
        public float maxPitch = 80.0f;
        [Tooltip("Time taken to smooth camera rotation.")]
        public float SmoothCameraRotationTime = 0.01f;

        [SerializeField] private InputActionReference _sprintAction;
        
        [Inject] private CursorManager _cursorManager;
        
        private FirstPersonCharacter _character;
        private float _oldYaw;
        private float _oldPitch;
        private float _currentYawVelocity;
        private float _currentPitchVelocity;
        
        private void Awake()
        {
            _character = GetComponent<FirstPersonCharacter>();
        }

        public override void OnOwnershipClient(NetworkConnection prevOwner)
        {
            base.OnOwnershipClient(prevOwner);
            
            if(IsOwner)
            {
                _cursorManager.Register(this);
                
                _sprintAction.action.performed += OnSprintActionPerformed;
                _sprintAction.action.canceled += OnSprintActionCanceled;
                _sprintAction.action.Enable();
            }
            else
            {
                if (prevOwner.ClientId != LocalConnection.ClientId) return;
                
                _cursorManager.Unregister(this);
                
                _sprintAction.action.performed -= OnSprintActionPerformed;
                _sprintAction.action.canceled -= OnSprintActionCanceled;
                _sprintAction.action.Disable();
            }
        }

        private void Update()
        {
            if (!IsOwner) return;
            if (isInputLocked) return;

            // Movement input, relative to character's view direction

            var targetMovementDirection = Vector3.zero;

            targetMovementDirection += _character.GetRightVector() * InputMove.x;
            targetMovementDirection += _character.GetForwardVector() * InputMove.y;

            _character.SetMovementDirection(targetMovementDirection);

            // Look input

            var lookInput = InputLook;

            lookInput *= mouseSensitivity;

            var targetYaw = lookInput.x;
            var targetPitch = invertLook ? -lookInput.y : lookInput.y;

            var smoothYaw = Mathf.SmoothDampAngle(_oldYaw, targetYaw, ref _currentYawVelocity,
                SmoothCameraRotationTime);
            var smoothPitch = Mathf.SmoothDampAngle(_oldPitch, targetPitch, ref _currentPitchVelocity,
                SmoothCameraRotationTime);

            _oldYaw = smoothYaw;
            _oldPitch = smoothPitch;

            _character.AddControlYawInput(smoothYaw);
            _character.AddControlPitchInput(smoothPitch, minPitch, maxPitch);

            // Crouch input

            if (Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.C))
                _character.Crouch();
            else if (Input.GetKeyUp(KeyCode.LeftControl) || Input.GetKeyUp(KeyCode.C))
                _character.UnCrouch();

            // Jump input

            if (Input.GetButtonDown("Jump"))
                _character.Jump();
            else if (Input.GetButtonUp("Jump"))
                _character.StopJumping();
        }

        private void OnSprintActionPerformed(InputAction.CallbackContext context) => _character.Sprint();
        
        private void OnSprintActionCanceled(InputAction.CallbackContext context) => _character.StopSprinting();
        
        private Vector2 Remap(Vector2 a, Vector2 fromMin, Vector2 fromMax, Vector2 toMin, Vector2 toMax) => 
            toMin + (a - fromMin) / (fromMax - fromMin) * (toMax - toMin);
        
        
    }
}
