using System.Collections.Generic;
using System.Linq;
using FishNet;
using FishNet.Connection;
using UnityEngine.Events;

namespace PlayerSystem
{
    public class PlayerManager
    {
        public UnityEvent<Player> onLocalPlayerChange { get; } = new();
        public UnityEvent<Player> onPlayerRegistered { get; } = new();
        public UnityEvent<Player> onPlayerUnregistered { get; } = new();
        
        public NetworkConnection localConnection => InstanceFinder.ClientManager.Connection;
        public Player localPlayer => players.FirstOrDefault(player => player.Owner == localConnection);
        public List<Player> players { get; } = new();

        public void Register(Player player)
        {
            players.Add(player);
            onPlayerRegistered?.Invoke(player);
        }

        public void Unregister(Player player)
        {
            players.Remove(player);
            onPlayerUnregistered?.Invoke(player);
        }
    }
}
