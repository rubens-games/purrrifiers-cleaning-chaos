using Other;
using UnityEngine;

namespace PlayerSystem
{
    public class PlayerModel : MonoBehaviour
    {
        public Player player;
        public PlayerFollowTargetSmooth followTargetSmooth;
    }
}
