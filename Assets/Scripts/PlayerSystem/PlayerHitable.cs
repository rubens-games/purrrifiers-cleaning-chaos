﻿using System;
using System.Collections;
using DG.Tweening;
using HitpointsSystem;
using SpawnPointSystem;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.VFX;

namespace PlayerSystem
{
    public class PlayerHitable : Hitable
    {
        public static Action<PlayerHitable> OnPlayerHitableHit;
        public static Action<PlayerHitable> OnPlayerHitableHeal;
        public static Action<PlayerHitable> OnPlayerHitableDeath;
        public static Action<PlayerHitable> OnPlayerHitableRevive;
        
        [SerializeField] private Player _player;
        [SerializeField] private PlayerRagdollController _ragdollController;
        [SerializeField] private VisualEffect _healthVfx;
        [SerializeField] private float _healthVfxDuration = 3f;
        [SerializeField] private VisualEffect _kissVfx;
        [SerializeField] private float _kissDuration = 3f;
        
        [Header("Fall Damage")]
        [SerializeField] private float _minFallVelocityToTakeDamage = 10f;
        [SerializeField] private float _fallDamageMultiplier = 0.1f;
        
        [Header("Camera Shake")]
        [SerializeField] private float _cameraShakeDuration = 0.5f;
        [SerializeField] private float _cameraShakeStrength = 1f;
        [SerializeField] private int _cameraShakeVibrato = 10;
        [SerializeField] private float _cameraShakeRandomness = 90f;
        
        [SerializeField] private InputAction _killAction;
        [SerializeField] private InputAction _reviveAction;
        
        private bool _isHealthVfxPlaying;
        private bool _isKissVfxPlaying;
        
        protected override void OnEnable()
        {
            base.OnEnable();
#if UNITY_EDITOR
            OnHitpointsIncrease.AddListener(OnHitpointsIncreaseHandler);
            OnHitpointsDecrease.AddListener(OnHitpointsDecreaseHandler);
            _killAction.performed += OnKillActionPerformed;
            _reviveAction.performed += OnReviveActionPerformed;
#endif
            OnDeath.AddListener(OnDeathHandler);
            OnRevive.AddListener(OnReviveHandler);
            
            _killAction.Enable();
            _reviveAction.Enable();
            
            _player.character.Landed += OnLanded;
        }
        
        protected override void OnDisable()
        {
            base.OnDisable();
            
#if UNITY_EDITOR
            OnHitpointsIncrease.RemoveListener(OnHitpointsIncreaseHandler);
            OnHitpointsDecrease.RemoveListener(OnHitpointsDecreaseHandler);
            _killAction.performed -= OnKillActionPerformed;
            _reviveAction.performed -= OnReviveActionPerformed;
#endif
            OnDeath.RemoveListener(OnDeathHandler);
            OnRevive.RemoveListener(OnReviveHandler);
            
            _killAction.Disable();
            _reviveAction.Disable();
            
            _player.character.Landed -= OnLanded;
        }
        
        private void OnHitpointsIncreaseHandler(int pHitpoints)
        {
            if (Owner != null)
                StartCoroutine(AnimatedHealthVfx());
            
            OnPlayerHitableHeal?.Invoke(this);
        }
        
        private void OnHitpointsDecreaseHandler(int pHitpoints)
        {
            if (Owner != null)
                DoCameraShake();
            
            OnPlayerHitableHit?.Invoke(this);
        }
        
        private void OnDeathHandler(int pHitpoints)
        {
            _player.pickUpControllerLeft.Drop();
            _player.pickUpControllerRight.Drop();
            
            _ragdollController.EnableRagdoll();
            
            OnPlayerHitableDeath?.Invoke(this);
        }

        private void OnReviveHandler(int pHitpoints)
        {
            _ragdollController.DisableRagdoll();

            Respawn();
            
            OnPlayerHitableRevive?.Invoke(this);
        }

#if UNITY_EDITOR
        private void OnKillActionPerformed(InputAction.CallbackContext pContext)
        {
            if (IsOwner) Hit1000();
        }
        
        private void OnReviveActionPerformed(InputAction.CallbackContext pContext)
        {
            if (IsOwner) Heal1000();
        }
#endif

        private void Respawn()
        {
            var respawnPosition = SpawnPointManager.SpawnPoints[0].position;
            
            transform.position = respawnPosition;
        }

        private IEnumerator AnimatedHealthVfx()
        {
            if (_isHealthVfxPlaying) yield break;
            
            _isHealthVfxPlaying = true;
            
            _healthVfx.Play();
            yield return new WaitForSeconds(_healthVfxDuration);
            _healthVfx.Stop();
            
            _isHealthVfxPlaying = false;
        }
        
        public IEnumerator PlayKissVfxOnce()
        {
            if (_isKissVfxPlaying) yield break;
            
            _isKissVfxPlaying = true;
            
            _kissVfx.Play();
            yield return new WaitForSeconds(0.2f);
            _kissVfx.Stop();
            
            _isKissVfxPlaying = false;
        }

        #region FallDamage
        
        private void OnLanded(Vector3 landingVelocity)
        {
            if (!IsOwner) return;
            
            var yVelocity = Mathf.Abs(landingVelocity.y);

            if (yVelocity < _minFallVelocityToTakeDamage) return;
            
            var fallDamage = yVelocity * _fallDamageMultiplier;
            
            Hit((int)fallDamage);
        }

        #endregion

        #region CameraShakeOnHit
        
        private void DoCameraShake()
        {
            if (!IsOwner) return;
            
            _player.playerCamera.DOShakeRotation(_cameraShakeDuration, _cameraShakeStrength, _cameraShakeVibrato,
                _cameraShakeRandomness);
        }

        #endregion
    }
}