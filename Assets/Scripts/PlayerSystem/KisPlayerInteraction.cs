using FishNet.Connection;
using FishNet.Object;
using InteractionSystem;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerSystem
{
    public class KisPlayerInteraction : Interactable
    {
        [SerializeField] private Player _kissedPlayer;
        [SerializeField] private List<AudioClip> _clips;
        private AudioSource _audioSource;


        public override bool IsSleeping => _kissedPlayer.IsOwner;
        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }
        private void OnEnable()
        {
            OnInteract.AddListener(KisPlayer);
        }
        private void PlayRandomSFX()
        {
            int randomIndex = Random.Range(0, _clips.Count);
            AudioClip clip = _clips[randomIndex];

            _audioSource.clip = clip;
            _audioSource.Play();
        }
        private void OnDisable()
        {
            OnInteract.RemoveListener(KisPlayer);
        }

        public override bool CanInteractWithPlayer(Player player)
        {
            if (!_canInteract.Value) 
                return _canInteract.Value;
                
            return !_kissedPlayer.IsOwner && _canInteract.Value;
        }

        private void KisPlayer(Player kissingPlayer)
        {
            Debug.Log($"{kissingPlayer.gameObject.name} kissed {_kissedPlayer.gameObject.name}");

            if (kissingPlayer.IsOwner) 
                HealPlayerServerRpc(_kissedPlayer.Owner, 5);
            
            StartCoroutine(kissingPlayer.playerHitable.PlayKissVfxOnce());
            PlayRandomSFX();

        }

        [ServerRpc(RequireOwnership = false)]
        private void HealPlayerServerRpc(NetworkConnection target, int amount) => 
            HealPlayerTargetRpc(target, amount);

        [ObserversRpc][TargetRpc]
        private void HealPlayerTargetRpc(NetworkConnection target, int amount) => 
            _kissedPlayer.playerHitable.Heal(amount);
    }
}
