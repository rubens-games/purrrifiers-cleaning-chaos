using UnityEngine;

namespace RebuildUISystem
{
    public class RebuildUI : MonoBehaviour
    {
        private enum UnityLifecycle
        {
            Awake,
            OnEnable,
            Start
        }

        [SerializeField] private UnityLifecycle _rebuiltOn = UnityLifecycle.Start;
    
        private void Awake()
        {
            if (_rebuiltOn == UnityLifecycle.Awake)
                Rebuild();
        }
    
        private void OnEnable()
        {
            if (_rebuiltOn == UnityLifecycle.OnEnable)
                Rebuild();
        }
    
        private void Start()
        {
            if (_rebuiltOn == UnityLifecycle.Start)
                Rebuild();
        }
    
        private void Rebuild()
        {
            RebuildUIManager.instance.Register(GetComponent<RectTransform>());
        }
    }
}
