using System.Collections;
using System.Collections.Generic;
using SingletonSystem.Runtime;
using UnityEngine;
using UnityEngine.UI;

namespace RebuildUISystem
{
    public class RebuildUIManager : Singleton<RebuildUIManager>
    {
        private bool _isRebuilding;
        
        private readonly List<RectTransform> _rectTransforms = new();
        
        public void Register(RectTransform rectTransform)
        {
            if (!rectTransform) return;
            
            if (_rectTransforms.Contains(rectTransform)) return;
            
            _rectTransforms.Add(rectTransform);

            StartRebuilding();
        }
        
        public void Unregister(RectTransform rectTransform)
        {
            if (!rectTransform) return;
            
            if (!_rectTransforms.Contains(rectTransform)) return;
            
            _rectTransforms.Remove(rectTransform);
        }
        
        private void StartRebuilding()
        {
            if (_isRebuilding) return;
            StartCoroutine(Rebuild());
        }

        private IEnumerator Rebuild()
        {
            if (_isRebuilding) yield break;
            
            _isRebuilding = true;

            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();

            foreach (var rectTransform in _rectTransforms) 
                LayoutRebuilder.ForceRebuildLayoutImmediate(rectTransform);
            
            _rectTransforms.Clear();
            
            _isRebuilding = false;
        }
    }
}
