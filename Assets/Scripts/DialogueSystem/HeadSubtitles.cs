using PixelCrushers.DialogueSystem;
using TMPro;
using UnityEngine;

namespace DialogueSystem
{
    public class HeadSubtitles : MonoBehaviour
    {
        [SerializeField] private TMP_Text _subtitleText;
        
        private void OnConversationLine(Subtitle subtitle)
        {
            _subtitleText.text = subtitle.formattedText.text;
        }
    }
}
