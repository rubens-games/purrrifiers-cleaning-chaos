using PlayerSystem;
using UnityEngine;
using Zenject;

namespace DialogueSystem
{
    public class LockRespondingPlayer : MonoBehaviour, IPlayerLockHandler
    {
        public bool isLocked => true;
        
        [Inject] private PlayerManager _playerManager;
        
        private void OnEnable()
        {
            if (_playerManager?.localPlayer) 
                _playerManager.localPlayer.playerLock.Register(this);
        }

        private void OnDisable()
        {
            if (_playerManager?.localPlayer) 
                _playerManager.localPlayer.playerLock.Unregister(this);
        }
    }
}
