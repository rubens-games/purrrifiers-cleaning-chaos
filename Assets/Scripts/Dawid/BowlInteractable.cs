using DG.Tweening;
using InteractionSystem;
using PlayerSystem;
using System.Collections;
using System.Collections.Generic;
using PickableSystem;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

public class BowlInteractable : Interactable
{
    [Inject] private PlayerManager _playerManager;

    private HashSet<ItemSO> _playerCraftRecipe = new HashSet<ItemSO>();
    [SerializeField] private RecipeSO _correctRecipe;

    [SerializeField] private GameObject _cake;
    private Vector3 _cakePosition;
    private Quaternion _cakeRotation;

    private void Awake()
    {
        _cakePosition = _cake.transform.localPosition;
        _cakeRotation = _cake.transform.localRotation;
    }
    public void IncomingItem(Player p_player)
    {
        if (p_player.pickUpControllerRight.CurrentPickup.TryGetComponent(out ItemData _dataHoldable)) //weryfikacja
        {
            if (!_dataHoldable.ItemSO) return; //czy dobry item
        }


        if (_dataHoldable.ItemSO is FoodSO) //�arcie z r�ki do miski
        {
            Vector3 origin = _dataHoldable.transform.localPosition;

            _dataHoldable.transform.DOJump(transform.position, 1f, 1, 0.5f).OnComplete(() =>
                {
                    _playerCraftRecipe.Add(_dataHoldable.ItemSO);
                    if (_playerCraftRecipe.Count >= 1) { _cake.SetActive(true); }
                    _dataHoldable.transform.DOLocalJump(origin, 1f, 1, 0.5f);
                    CheckCraft();                    
                });
        }
    }
    public async void ResetBowl()
    {
        await _cake.GetComponent<Pickable>().CurrentPickUpController.Drop();
        
        _cake.SetActive(false);
        _cake.transform.parent = transform;
        _cake.transform.localPosition = _cakePosition;
        _cake.transform.localRotation = _cakeRotation;
        _cake.GetComponent<Rigidbody>().isKinematic = true;
        _cake.GetComponent<ItemData>().CurrentState = ItemState.poisoned;
        _playerCraftRecipe.Clear();
    }
    private void CheckCraft()
    {
        _cake.GetComponent<ItemData>().CurrentState = _correctRecipe.IsMatch(new List<ItemSO>(_playerCraftRecipe)) ? ItemState.good : ItemState.poisoned;
        Debug.Log(_cake.GetComponent<ItemData>().CurrentState);
    }
    private void OnEnable()
    {
        OnInteract.AddListener(IncomingItem);
    }
    private void OnDisable()
    {
        OnInteract.RemoveListener(IncomingItem);
    }

    public override bool CanInteractWithPlayer(Player player)
    {
        if (!player) return false;

        if (!player.pickUpControllerRight.CurrentPickup ||
            !player.pickUpControllerRight.CurrentPickup.TryGetComponent(out ItemData dataHoldable)) return false;
        
        return dataHoldable.ItemSO is FoodSO;
    }
}
