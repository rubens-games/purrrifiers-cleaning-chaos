using System;
using System.Collections;
using System.Collections.Generic;
using InteractionSystem;
using UnityEngine;
using PixelCrushers.QuestMachine;
using PlayerSystem;
using ToolSystem;
using ToolSystem.Tools.HitableByTools;

public class ElectroManager : QuestControl
{
    public static event Action<bool> OnElectroStateChanged; 
    
    public static bool ElectroState { get; private set; } = true;
    
    [SerializeField] private List<ElectroSocket> _electroSockets;
    [SerializeField] private List<Light> _allLights;
    [SerializeField] private List<GameObject> _allObjects;
    [SerializeField] private WashingMachineControler _washingMachine;
    [SerializeField] private OvenControler _ovenMachine;

    private DamagePlayerOnKarcherHit _damagePlayerOnKarcherHit;
    
    private bool ElectroStateSwitch
    {
        get => _electroState;
        set
        {
            if (_electroState == value) return;
            
            _electroState = value;
            ElectroState = value;

            for (var i = 0; i < _allLights.Count; i++)
            {
                var lightSource = _allLights[i];
                lightSource.enabled = value;
            }

            for (var i = 0; i < _allObjects.Count; i++)
            {
                var obj = _allObjects[i];
                obj.SetActive(value);
            }

            for (var i = 0; i < _electroSockets.Count; i++)
            {
                var socket = _electroSockets[i];
                socket.enabled = value;
            }

            _washingMachine.ElectroChangeState();
            _ovenMachine.ElectroChangeState();

            if (_electroState) QuestCustomManager.instance.EnableFuseBox();
            else QuestCustomManager.instance.DisableFuseBox();
            
            OnElectroStateChanged?.Invoke(value);
        }        
    }
    
    [SerializeField] private bool _electroState;
    [SerializeField] private MonologueSO _electroOff;
    
    private bool _electroMono;
    private AudioSource _audioSource;
    private Interactable _interactable;

    private void Awake()
    {
        _interactable = GetComponent<Interactable>();
        _audioSource= GetComponent<AudioSource>();
        _damagePlayerOnKarcherHit = GetComponent<DamagePlayerOnKarcherHit>();


        foreach (var socket in _electroSockets) 
            socket.ElectroFuseBox = this;
    }

    private void OnEnable()
    {
        _interactable.OnInteract.AddListener(OnInteract);
        _damagePlayerOnKarcherHit.onHitAfter.AddListener(OnHitAfter);
    }
    
    private void OnDisable()
    {
        _interactable.OnInteract.RemoveListener(OnInteract);
        _damagePlayerOnKarcherHit.onHitAfter.RemoveListener(OnHitAfter);
    }

    private void OnInteract(Player player) => SetElectroState(!ElectroStateSwitch, player.IsOwner);
    
    private void OnHitAfter(Tool tool) => SetElectroState(false, tool.IsOwner);
    
    public void SetElectroState(bool state, bool isOwner)
    {
        ElectroStateSwitch = state;
        
        _audioSource.Play();
        
        if (!isOwner) return;

        if (_electroState) return;
        
        if (_electroMono) return;
        _electroMono = true;
        
        StartCoroutine(DelayMonoElectro());
    }
    
    private IEnumerator DelayMonoElectro()
    {
        yield return new WaitForSeconds(2f);
        MonologueManager.instance.PlayMonologue(_electroOff);
        yield return new WaitForSeconds(1f);
        _electroMono = false;
    }
}
