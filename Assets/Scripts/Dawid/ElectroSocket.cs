using System;
using System.Collections;
using System.Collections.Generic;
using ToolSystem;
using ToolSystem.Tools.HitableByTools;
using UnityEngine;

public class ElectroSocket : MonoBehaviour
{
    public ElectroManager ElectroFuseBox
    {
        get => _electroFuseBox;
        set => _electroFuseBox = value;
    }

    [SerializeField] private GameObject _vfx;
    [SerializeField] private float _electroVFXDuration = 1f;
    
    private ElectroManager _electroFuseBox;
    private Collider _collider;
    private AudioSource _audioSource;
    private DamagePlayerOnKarcherHit _damagePlayerOnKarcherHit;

    private void Awake()
    {
        _collider = GetComponent<Collider>();
        _audioSource = GetComponent<AudioSource>();
        _damagePlayerOnKarcherHit = GetComponent<DamagePlayerOnKarcherHit>();
    }

    private void OnEnable()
    {
        _collider.enabled = true;
        _damagePlayerOnKarcherHit.onHitAfter.AddListener(OnHitAfter);
    }
    
    private void OnDisable()
    {
        _collider.enabled = false;
        _damagePlayerOnKarcherHit.onHitAfter.RemoveListener(OnHitAfter);
    }
    
    private void OnHitAfter(Tool tool) => ElectroFailure(tool.IsOwner);

    public void ElectroFailure(bool isOwner)
    {
        ElectroFuseBox.SetElectroState(false, isOwner);
        
        StartCoroutine(ElectroSocketVFX());
    }
    
    private IEnumerator ElectroSocketVFX()
    {
        _vfx.SetActive(true);
        _audioSource.Play();
        yield return new WaitForSeconds(_electroVFXDuration);
        _vfx.SetActive(false);
    }
}
