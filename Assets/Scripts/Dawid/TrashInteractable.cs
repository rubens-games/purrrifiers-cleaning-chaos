using DG.Tweening;
using InteractionSystem;
using PlayerSystem;
using FishNet.Object;
using ToolSystem.Tools;
using UnityEngine;
using Zenject;

public class TrashInteractable : Interactable
{
    [SerializeField] private ItemState _myBin;
    [SerializeField] private bool _isFullTrashBag;

    [Inject] private PlayerManager _playerManager;

    private ItemData _myItemData;
    private Vector3 _binOffset = new(0f, 0.606f, 0f);

    private void Awake()
    {
        _myItemData = GetComponent<ItemData>();
    }

    public override bool CanInteractWithPlayer(Player player)
    {
        var bin = GetBin(player);

        return bin;
    }

    private void OnEnable()
    {
        OnInteract.AddListener(JumpIntoBin);
    }

    private void OnDisable()
    {
        OnInteract.RemoveListener(JumpIntoBin);
    }
    
    private void JumpIntoBin(Player player)
    {
        var bin = GetBin(player);
        
        if (!bin) return;
        
        transform.parent = bin.transform;
        transform.DOLocalJump(Vector3.zero + _binOffset, 1, 1, .5f).OnComplete(() => gameObject.SetActive(false));
        transform.DOScale(transform.localScale * 0.5f, 0.5f).SetDelay(.0f);
        
        QuestCustomManager.instance.TakeTrash(player.IsOwner);

        var trashesToAdd = 1;
        
        if (_isFullTrashBag)
        {
            var currentTrashCount = bin.trashesInBin;
            var maxTrashCount = bin.MaxTrashesInBin;
            var requiredTrashCount = maxTrashCount - currentTrashCount;
            
            trashesToAdd = requiredTrashCount;
            QuestCustomManager.instance.BlackHoleComing();
        }
        
        bin.AddTrash(trashesToAdd);
    }
    
    private TrashBin GetBin(Player player)
    {
        var currentPickUp = player.pickUpControllerLeft.CurrentPickup;

        if (!currentPickUp) return null;

        var itemData = currentPickUp.GetComponent<ItemData>();
        
        if (!itemData) return null;

        var trashBin = itemData.CurrentState == _myBin ? currentPickUp.GetComponent<TrashBin>() : null;
        
        if (!trashBin) return null;
        
        return trashBin.IsTrashBinFull ? null : trashBin;
    }
}
