﻿using System;
using DG.Tweening;
using InteractionSystem;
using PlayerSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class CleanSink : Interactable
{
    [Inject] private PlayerManager _playerManager;
    [SerializeField] private ItemState _requiredState;
    [SerializeField] private Transform _sinkPoint;

    private SinkManager _sinkManager;

    private void Awake()
    {
        _sinkManager = GetComponentInParent<SinkManager>();
    }

    private void OnEnable()
    {
        OnInteract.AddListener(IncomingItem);
    }

    private void OnDisable()
    {
        OnInteract.RemoveListener(IncomingItem);
    }

    public void IncomingItem(Player p_player)
    {
        if (p_player.pickUpControllerRight.CurrentPickup.TryGetComponent(out ItemData _dataHoldable)) //weryfikacja
        {
            if (!_dataHoldable.ItemSO) return; //czy dobry item
            if (_dataHoldable.CurrentState != _requiredState) return; //czy dobry state itemu
        }

        if (_dataHoldable.ItemSO is DishSO) //naczynia
        {
            if (_dataHoldable.CurrentState == ItemState.dirty) //brudne do zlewu
            {
                QuestCustomManager.instance.WashDish();
                Vector3 origin = _dataHoldable.transform.localPosition;
                _dataHoldable.transform.DOJump(_sinkPoint.position, 1f, 1, 0.5f).OnComplete(() =>
                {
                    if (GetComponentInParent<SinkManager>().IsRunningWater)
                    {
                        _dataHoldable.CurrentState = ItemState.clean;
                        _dataHoldable.GetComponent<ItemData>().DeleteDirt();
                    }
                    _dataHoldable.transform.DOLocalJump(origin, 1f, 1, 0.5f);
                });
                return;
            }
        }
    }

    public override bool CanInteractWithPlayer(Player player)
    {
        if (!_sinkManager.IsRunningWater) return false;
        
        if (player == null) return false;

        if (player.pickUpControllerRight.CurrentPickup && player.pickUpControllerRight.CurrentPickup.TryGetComponent(out ItemData dataHoldable))
        {
            if (dataHoldable.ItemSO is DishSO) return true;
        }
        return false;
    }
}
