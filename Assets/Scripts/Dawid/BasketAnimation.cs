using FishNet.Object;
using PickableSystem;
using PlayerSystem;
using UnityEngine;

public class BasketAnimation : NetworkBehaviour
{
    [SerializeField] private Pickable _pickable;
    [SerializeField] private Vector3 _animationPositionOffset;
    [SerializeField] private Vector3 _animationRotation;
    private Player _player;


    void Update()
    {
        Animate();
    }

    private void Animate()
    {
        if (!IsOwner) return;

        if (!_pickable.ToolHoldingRig) return;

        var rhIK = _pickable.ToolHoldingRig.RightHandIK.IK;
        var rhIKTarget = rhIK.data.target;
        var rhIKHint = rhIK.data.hint;
        rhIK.weight = 1;

        var newPos = rhIK.data.root.parent.localPosition + _animationPositionOffset;
        newPos = rhIK.data.root.parent.TransformPoint(newPos);
        rhIKTarget.position = newPos;

        // Todo : looking at point network sync
        if (IsOwner) rhIKTarget.LookAt(_player.lookingAt.GetLookingAtPoint());
        rhIKTarget.rotation *= Quaternion.Euler(_animationRotation);
    }

    private void ResetAnimation()
    {
        if (!_pickable.ToolHoldingRig) return;

        _pickable.ToolHoldingRig.RightHandIK.IK.weight = 0;
    }

    private void OnPickup(PickUpController controller)
    {
        _player = controller.GetComponent<Player>();
    }

    private void OnDropBefore(PickUpController controller)
    {
        ResetAnimation();
    }

    private void OnDrop(PickUpController controller)
    {
        _player = null;
    }

    private void OnEnable()
    {
        _pickable.OnPickup.AddListener(OnPickup);
        _pickable.OnDropBefore.AddListener(OnDropBefore);
        _pickable.OnDrop.AddListener(OnDrop);
    }

    private void OnDisable()
    {
        _pickable.OnPickup.RemoveListener(OnPickup);
        _pickable.OnDropBefore.RemoveListener(OnDropBefore);
        _pickable.OnDrop.RemoveListener(OnDrop);
    }

}
