using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Recipe", menuName = "Recipe")]

public class RecipeSO : ScriptableObject
{
    public string recipeName;
    //public List<FoodType> ingredients;
    public List<ItemSO> ingredients;
    public float timeInSecondsBake;
    public int tempBake;

    //public bool IsMatch(List<FoodType> playerIngredients) //por�wnanie czy wrzucone sk�adniki zgadzaj� si� z przepisem
    //{
    //    if (ingredients.Count != playerIngredients.Count) return false;

    //    HashSet<FoodType> recipeSet = new HashSet<FoodType>(ingredients);
    //    HashSet<FoodType> playerSet = new HashSet<FoodType>(playerIngredients);

    //    return recipeSet.SetEquals(playerSet);
    //}

    public bool IsMatch(List<ItemSO> playerIngredients) //por�wnanie czy wrzucone sk�adniki zgadzaj� si� z przepisem
    {
        if (ingredients.Count != playerIngredients.Count) return false;

        HashSet<ItemSO> recipeSet = new HashSet<ItemSO>(ingredients);
        HashSet<ItemSO> playerSet = new HashSet<ItemSO>(playerIngredients);

        return recipeSet.SetEquals(playerSet);
    }
}
