﻿using DG.Tweening;
using System.Collections;
using FishNet.Object;
using TMPro;
using UnityEngine;
using UnityEngine.VFX;

public class OvenControler : NetworkBehaviour
{
    public static OvenControler Instance { get; private set; }
    
    [SerializeField] private Transform _door;
    private bool isDoorOpen = false;

    [SerializeField] private Transform _drawer;
    private bool isDrawerOpen = false;

    [SerializeField] private Transform _rotatorTemp;
    private int _currentTempIndex = 0;
    public int CurrentTemp {  
        get { return _currentTemp; }
        set { _currentTemp = value; _temperatureTxt.text = _currentTemp.ToString(); }
    }
    private int _currentTemp = 0;

    [SerializeField] private Transform _rotatorTimer;
    private int _currentTimerIndex = 0;
    public int CurrentTimer
    {
        get { return _currentTimer; }
        set { _currentTimer = value; _timerTxt.text = string.Format("{0:00}:{1:00}:{2:00}", Mathf.FloorToInt(_currentTimer / 3600), Mathf.FloorToInt((_currentTimer % 3600) / 60), Mathf.FloorToInt(_currentTimer % 60)); }
    }
    private int _currentTimer = 0;

    [SerializeField] private Transform _buttonStart;
    private bool _isRunning = false;

    [SerializeField] private TextMeshPro _timerTxt;
    [SerializeField] private TextMeshPro _temperatureTxt;

    private float _remainingTime = 10f;

    [SerializeField] private VisualEffect _smoke;

    private bool _isMuffinInside = false;
    [SerializeField] private RecipeSO _correctRecipe;

    private Coroutine coroutine = null;
    private Coroutine corotuineOver = null;

    private bool _isElectro = true;
    [SerializeField] private GameObject _screen;

    [SerializeField] private MuffinFormInteractable _formInteractable;
    [SerializeField] private GameObject _burnStain;
    [SerializeField] private GameObject _light;

    public int PoisonedPlayerCount { get; set; }

    private bool _isAnimation = false;

    private AudioSource _audioSource;
    [SerializeField] private AudioClip _openDoorSFX;
    [SerializeField] private AudioClip _openDrawerSFX;
    [SerializeField] private AudioClip _runningSFX;
    [SerializeField] private AudioClip _rotatorSFX;
    [SerializeField] private AudioClip _startSFX;
    private void Awake()
    {
        Instance = this;
        _audioSource = GetComponent<AudioSource>();

    }

    private void Start()
    {
        SetTemperature(0);
        SetTimer(0);
    }

    public void DoorInteract() //drzwiczki
    {
        if (_isAnimation) return;

        if (_isRunning) return;

        if (isDrawerOpen) DrawerInteract();
        _isAnimation = true;
        _audioSource.clip = _openDoorSFX;
        _audioSource.Play();
        _door.DOLocalRotate(new Vector3(isDoorOpen ? 90f : 180f, 90f, 0f), 0.5f).SetEase(Ease.Linear).OnComplete(() =>
        {
            if (!isDoorOpen && corotuineOver!=null)
            {
                StopCoroutine(corotuineOver);
                _smoke.Stop();
            }
            isDoorOpen = !isDoorOpen;
            _isAnimation = false;


            if (_light.activeSelf) _light.SetActive(false);
        });
    }
    public void DrawerInteract() //raszki w środku
    {
        if (_isAnimation) return;

        if (_isRunning || !isDoorOpen) return;
        _isAnimation = true;
        _audioSource.clip = _openDrawerSFX;
        _audioSource.Play();
        _drawer.DOLocalMoveX(isDrawerOpen ? 0.3231f : 0.7153f, 0.5f).SetEase(Ease.OutBack).OnComplete(() => { isDrawerOpen = !isDrawerOpen; _isAnimation = false; });
    }
    public void RotatorTempInteract() //pokrętło od temperatury
    {
        if (_isAnimation) return;

        if (_isRunning) return;
        _isAnimation = true;
        _audioSource.clip = _rotatorSFX;
        _audioSource.Play();
        _rotatorTemp.DOLocalRotate(new Vector3(0f, 45f, 0f), 0.2f, RotateMode.LocalAxisAdd).SetEase(Ease.Linear).OnComplete(() => { SetTemperature(_currentTempIndex == 7 ? _currentTempIndex = 0 : ++_currentTempIndex); _isAnimation = false; });
    }
    public void RotatorTimerInteract() //pokrętło od czasu
    {
        if (_isAnimation) return;

        if (_isRunning) return;
        _isAnimation = true;
        _audioSource.clip = _rotatorSFX;
        _audioSource.Play();
        _rotatorTimer.DOLocalRotate(new Vector3(0f, 45f, 0f), 0.2f, RotateMode.LocalAxisAdd).SetEase(Ease.Linear).OnComplete(() => { SetTimer(_currentTimerIndex == 7 ? _currentTimerIndex = 0 : ++_currentTimerIndex); _isAnimation = false; });
    }
    public void MachineStartPause() //start lub stop piekarnika
    {
        _buttonStart.DOLocalMove(new Vector3(0.562f, 0.8238f, 0.4354f), 0.3f).SetEase(Ease.OutBack).SetLoops(2, LoopType.Yoyo); //animacja przycisku start/stop

        if (!isDoorOpen && !isDrawerOpen && !_isRunning && _isElectro && _formInteractable.ReadyMuffins<=0) //start
        {
            _isRunning = true;
            _audioSource.clip = _startSFX;
            _audioSource.Play();
            coroutine = StartCoroutine(MachineIsWorking());
        }
        //else if (!isDoorOpen && !isDrawerOpen && _isRunning)//pause
        //{
        //    _isRunning = false;
        //    StopCoroutine(coroutine);
        //}
    }
    public IEnumerator MachineIsWorking() //piekarnik pracuje
    {
        _light.SetActive(true);
        _remainingTime = CurrentTimer;
        yield return new WaitForSeconds(.5f);
        _audioSource.clip = _runningSFX;
        _audioSource.loop = true;
        _audioSource.Play();
        while (_remainingTime >= 0)
        {
            _timerTxt.text = string.Format("{0:00}:{1:00}:{2:00}", Mathf.FloorToInt(_remainingTime / 3600), Mathf.FloorToInt((_remainingTime % 3600) / 60), Mathf.FloorToInt(_remainingTime % 60));
            yield return new WaitForSecondsRealtime(1f);
            _remainingTime--;            
        }
        MachineFinishedNormal();
    }
    public void MachineFinishedNormal() //piekarnik skończył piec
    {
        StopCoroutine(coroutine);
        _isRunning = false;
        _audioSource.Stop();
        _audioSource.loop = false;
        corotuineOver = StartCoroutine(OverBaked());
        if (_isMuffinInside)
        {
            if (_currentTemp == _correctRecipe.tempBake && _currentTimer == _correctRecipe.timeInSecondsBake) _formInteractable.BakedMuffins(true); //sprawdzenie czy dobrze wykonany przepis
            else _formInteractable.BakedMuffins(false);
        }
    }
    private IEnumerator OverBaked() //przepalenie po 30 sekundach od skończenia, jak nie otworzą drzwi
    {
        
        int overBakedTime = 0;
        while (overBakedTime < 30)
        {
            yield return new WaitForSeconds(1f);
            overBakedTime++;            
        }
        _smoke.Play();
        _formInteractable.BakedMuffins(false); //nawet jak były ok to spalili je
        yield return new WaitForSecondsRealtime(1f);
        //_burnStain.SetActive(true);
    }
    public void SetTemperature(int p_index) //ustawianie temperatury
    {
        //170 190 210 230 250 270 290
        switch (p_index)
        {
            case 0: //testy
                CurrentTemp = 170;
                return;
            case 1:
                CurrentTemp = 190;
                return;
            case 2:
                CurrentTemp = 210;
                return;
            case 3:
                CurrentTemp = 220;
                return;
            case 4:
                CurrentTemp = 240;
                return;
            case 5:
                CurrentTemp = 250;
                return;
            case 6:
                CurrentTemp = 270;
                return;
            case 7:
                CurrentTemp = 666;
                return;
            default:
                return;

        }

    }
    public void SetTimer(int p_index) //ustawianie czasu
    {
        switch (p_index)
        {
            case 0: //testy
                CurrentTimer = 10;
                return;
            case 1:
                CurrentTimer = 60;
                return;
            case 2:
                CurrentTimer = 120;
                return;
            case 3:
                CurrentTimer = 180;
                return;
            case 4:
                CurrentTimer = 300;
                return;
            case 5:
                CurrentTimer = 600;
                return;
            case 6:
                CurrentTimer = 1200;
                return;
            case 7:
                CurrentTimer = 1800;
                return;
            default:
                return;

        }

    }   
    public void ElectroChangeState()
    {
        _isElectro = !_isElectro;

        if (_isElectro)
        {
            _screen.SetActive(true);
        }
        else
        {
            _screen.SetActive(false);
            _isRunning = false;
           if(coroutine!=null) StopCoroutine(coroutine);
            SetTemperature(_currentTempIndex);
            SetTimer(_currentTimerIndex);
            _light.SetActive(false);
        }

    }
    public void PutMuffinsInside()
    {
        _isMuffinInside = true;
    }
}
