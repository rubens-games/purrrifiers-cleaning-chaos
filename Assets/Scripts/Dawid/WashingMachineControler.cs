using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using InteractionSystem;

public class WashingMachineControler : MonoBehaviour
{
    [SerializeField] private Transform _door;
    private bool isDoorOpen = false;

    [SerializeField] private Transform _drawer;
    private bool isDrawerOpen = false;

    [SerializeField] private Transform _rotator;

    [SerializeField] private Transform _buttonStart;
    private bool _isRunning = false;

    [SerializeField] private TextMeshPro _timerTxt;
    [SerializeField] private TextMeshPro _temperatureTxt;
    [SerializeField] private TextMeshPro _capacityMaxTxt;
    [SerializeField] private TextMeshPro _clothesInTxt;

    [SerializeField] private Transform _inside;

    [SerializeField] private ParticleSystem _foamInside;
    [SerializeField] private ParticleSystem _foamFailure;
    [SerializeField] private GameObject _foamDecal;

    private List<ItemData> _clothesIn = new List<ItemData>();

    [SerializeField] private GameObject _powder;
    [SerializeField] private MeshCollider _clotheOut;

    private Vector3 _wetClothesPos = new Vector3(0.1f, 0.9f, 0f);
    private Vector3 _offsetWetClothesY = new Vector3(0f, 0.04f, 0f);

    //wy�wietlacz pralki
    private Color32 _defaultColor = new Color32(0x00, 0x9C, 0xFF, 0xFF);
    private Color32 _redColor = new Color32(0xFF, 0x00, 0x16, 0xFF);
    private float _remainingTime = 10f;
    private int _currentCapacity = 0;
    public int CurrentClothesIn
    {
        get => _currentClothesIn;
        set
        {
            _currentClothesIn = value;
            if (_currentClothesIn > _currentCapacity)
            {
                _clothesInTxt.color = _redColor;
                _willBeFailed = true;
            }
            else
            {
                _clothesInTxt.color = _defaultColor;
                _willBeFailed = false;
            }
        }        
    }
    private int _currentClothesIn = 0;
    private int _currentTemp = 0;
    private int _currentProgram = 0;

    //event
    private bool _willBeFailed = false;

    private Coroutine coroutine = null;

    [SerializeField] private GameObject _screen;
    private bool _isElectro = true;

    [SerializeField] private MonologueSO _myMonologue;

    private bool _isAnimation = false;

    [SerializeField] private AudioSource _loop;
    [SerializeField] private AudioSource _nonLoop;
    [SerializeField] private AudioClip _finishedSFX;
    [SerializeField] private AudioClip _goodLoopSFX;
    [SerializeField] private AudioClip _overloadSFX;
    [SerializeField] private AudioClip _rotatorSFX;
    [SerializeField] private AudioClip _startSFX;
    [SerializeField] private AudioClip _doorSFX;
    [SerializeField] private AudioClip _drawerSFX;
    [SerializeField] private AudioClip _powderSFX;
    private void Start()
    {
        SetProgram(_currentProgram);
    }
    public void MachineStartPause() //start lub stop pralki
    {
        _buttonStart.DOLocalMove(new Vector3(-0.1616f, 0.7809f, 0.3174f), 0.3f).SetEase(Ease.OutBack).SetLoops(2, LoopType.Yoyo); //animacja przycisku start/stop

        if (!isDoorOpen && !isDrawerOpen && !_isRunning && _isElectro) //start
        {
            _isRunning = true;
            _nonLoop.clip = _startSFX;
            _nonLoop.Play();
            coroutine =StartCoroutine(MachineIsWorking());
            _foamInside.Play();
        }
        //else if(!isDoorOpen && !isDrawerOpen && _isRunning)//pause
        //{
        //    _isRunning = false;
        //    StopCoroutine(coroutine);
        //    _foamInside.Stop();
        //}
    }
    public void DoorInteract() //drzwiczki
    {
        if (_isRunning) return;
        if (_isAnimation) return;

        _isAnimation = true;
        _nonLoop.clip = _doorSFX;
        _nonLoop.Play();
        _door.DOLocalRotate(new Vector3(90f, 0f, isDoorOpen ? 0f : -90f), 0.5f).SetEase(Ease.OutBack).OnComplete(()=> { isDoorOpen = !isDoorOpen; _isAnimation = false; });
    }
    public void DrawerInteract() //szufladka
    {
        if (_isRunning) return;
        if (_isAnimation) return;

        _isAnimation = true;
        _nonLoop.clip = _drawerSFX;
        _nonLoop.Play();
        _drawer.DOLocalMoveZ(isDrawerOpen ? 0.1665f : 0.4358f, 0.5f).SetEase(Ease.OutBack).OnComplete(() => { isDrawerOpen = !isDrawerOpen; _isAnimation = false; });
    }
    public void RotatorInteract() //pokr�t�o
    {
        if (_isRunning) return;
        if (_isAnimation) return;
        
        _isAnimation = true;
        _nonLoop.clip = _rotatorSFX;
        _nonLoop.Play();
        _rotator.DOLocalRotate(new Vector3(0f, 45f, 0f), 0.2f, RotateMode.LocalAxisAdd).SetEase(Ease.Linear).OnComplete(()=> { SetProgram(_currentProgram == 7 ? _currentProgram = 0 : ++_currentProgram); _isAnimation = false; });
    }
    public IEnumerator MachineIsWorking() //pralka pracuje
    {
        int pastTime = 0; 
        _loop.clip = _willBeFailed ? _overloadSFX : _goodLoopSFX;
        _loop.loop = true;
        _loop.Play();
        while (_remainingTime >= 0)
        {            
            _timerTxt.text = string.Format("{0:00}:{1:00}", Mathf.FloorToInt(_remainingTime / 60), Mathf.FloorToInt(_remainingTime % 60));
            _inside.DOLocalRotate(new Vector3(0f, 360f, 0f), 1f, RotateMode.LocalAxisAdd).SetEase(Ease.Linear);
            yield return new WaitForSecondsRealtime(1f);
            _remainingTime--;
            pastTime++;
            if (_willBeFailed && pastTime>10f)
            {
                FailureWashing(); 
                _loop.Stop();
                StopCoroutine(coroutine);
            }
        }
        MachineFinishedNormal();
    }
    public void FailureWashing() //pralka si� zepsu�a
    {
        transform.DOShakeRotation(2f, new Vector3(1f,1f,1f), 32, 47.3f).SetEase(Ease.Linear).OnComplete(() => 
        {
            _isRunning = false;
            DoorInteract();
            DrawerInteract();
            _foamFailure.Play();
            //_foamDecal.SetActive(true);
            _foamInside.Stop();
            SetProgram(_currentProgram);
        });
        
    }
    public void MachineFinishedNormal() //pralka sko�czy�a normalne pranie
    {
        StopCoroutine(coroutine);
        _isRunning = false;
        _foamInside.Stop();
        foreach (ItemData clothe in _clothesIn) 
        {
            clothe.CurrentState = ItemState.wet;
            clothe.DeleteDirt();
        }
        _clotheOut.enabled = true;
        _inside.GetComponent<MeshCollider>().enabled = false;
        PlayMonologue();
        _loop.loop = false;
        _loop.clip = _finishedSFX;
        _loop.Play();
    } 
    private void UpdateProgramsData(float p_time, int p_capacity, int p_temp) //aktualizacja UI i danych
    {
        _remainingTime = p_time;
        _currentCapacity = p_capacity;
        _currentTemp = p_temp;
        _temperatureTxt.text = _currentTemp.ToString();
        _timerTxt.text = string.Format("{0:00}:{1:00}", Mathf.FloorToInt(_remainingTime / 60), Mathf.FloorToInt(_remainingTime % 60));

        //_clothesInTxt.text = CurrentClothesIn.ToString(); //ile w �rodku
        _capacityMaxTxt.text = _currentCapacity.ToString(); //ile jest �adowno�ci
        CurrentClothesIn = CurrentClothesIn; //od�wie�y ewentualne warningi
    }
    public void SetProgram(int p_id)
    {
        switch (p_id)
        {
            case 0: //testy
                UpdateProgramsData(60f, 1, 30);
                return;
            case 1:
                UpdateProgramsData(180f, 5, 30);
                return;
            case 2:
                UpdateProgramsData(300f, 8, 30);
                return;
            case 3:
                UpdateProgramsData(420f, 10, 35);
                return;
            case 4:
                UpdateProgramsData(600f, 12, 35);
                return;
            case 5:
                UpdateProgramsData(10f, 15, 40);
                return;
            case 6: //pluszaki program
                UpdateProgramsData(900f, 10, 45);
                return;
            case 7: //pluszaki program
                UpdateProgramsData(1020f, 20, 60);
                return;
            default:
                return;
        }
    } //ustawianie program�w odpowiada rotator
    public void PutClotheIn(ItemData p_clothe) //wk�adanie ciucha do pralki
    {
        int howMuchSave = _currentCapacity - CurrentClothesIn; //tyle do zape�nienia na maksa �adowno�� pralki/ Z kosza zniknie tyle prania, ile brakuje w pralce do maksa
        if (howMuchSave <= 0) //MORE! dojdzie do awarii je�eli wrzuc�
        {
            //podmieni� tekst na b�bnie
        }
        CurrentClothesIn++;
        QuestCustomManager.instance.WashClothe();
        _clothesInTxt.text = CurrentClothesIn.ToString();
        _clothesIn.Add(p_clothe);
    }
    public void TakeClotheOut() //zabieranie ciucha z pralki
    {
        _clotheOut.enabled = false;
        _inside.GetComponent<MeshCollider>().enabled = true;
        for (int i=0; i< _clothesIn.Count; i++)
        {
            _clothesIn[i].transform.parent = transform;
            _clothesIn[i].transform.localPosition = _wetClothesPos + (_offsetWetClothesY*(i+1));            
            _clothesIn[i].transform.localRotation = new Quaternion(0f,0f,0f,0f);
            _clothesIn[i].CurrentState = ItemState.wet;
            _clothesIn[i].GetComponent<Rigidbody>().isKinematic = true;
            _clothesIn[i].GetComponent<Interactable>().CanInteract = true;
        }
        _clothesIn.Clear();
        CurrentClothesIn = 0;
        _clothesInTxt.text = CurrentClothesIn.ToString();
        SetProgram(_currentProgram);
    }
    public void PutPowderIntoDrawer() //wsypywanie proszku do szufladki
    {
        if (!isDrawerOpen) return;

        _nonLoop.clip = _powderSFX;
        _nonLoop.Play();
        _powder.transform.DOLocalJump(new Vector3(0.06f, 1.053f, 0.378f), .2f, 1, .7f).SetEase(Ease.OutQuad).SetLoops(2, LoopType.Yoyo);
        _powder.transform.DORotate(new Vector3(0f, 0f, -113f), .7f).SetEase(Ease.OutQuad).SetLoops(2, LoopType.Yoyo);
    } 
    public void ElectroChangeState()
    {
        _isElectro = !_isElectro;

        if(_isElectro)
        {
            _screen.SetActive(true);
        }
        else
        {
            _screen.SetActive(false);
            _isRunning = false;
            if (coroutine != null) StopCoroutine(coroutine);
            //DoorInteract();
            //DrawerInteract();
            //_foamFailure.Play();
            //_foamDecal.SetActive(true);
            _foamInside.Stop();
            SetProgram(_currentProgram);
        }
       
    }
    private void PlayMonologue()
    {
        MonologueManager.instance.PlayMonologue(_myMonologue);
    }
}

