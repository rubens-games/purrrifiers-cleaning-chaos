using DG.Tweening;
using InteractionSystem;
using PlayerSystem;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UniwersalDrawerManager : MonoBehaviour
{
    [SerializeField] private List<Drawer> _elements;
    [SerializeField] private MonologueSO _monologue;
    private bool _isMono = false;
    private AudioSource _audioSource;
    [SerializeField] private float _time = .5f;
    public Player player;
    private List<Interactable> _myInteractable = new List<Interactable>();

    public void SetPlayer(Player p_player)
    {
        player = p_player;
    }
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        _myInteractable = GetComponentsInChildren<Interactable>().ToList();
    }
    private void OnEnable()
    {
        foreach (var element in _myInteractable)
        {
            element.OnInteractBefore.AddListener(SetPlayer);
        }
    }
    private void OnDisable()
    {
        foreach (var element in _myInteractable)
        {
            element.OnInteractBefore.RemoveListener(SetPlayer);
        }
    }
    private IEnumerator Delay()
    {
        _isMono = true;
        MonologueManager.instance.PlayMonologue(_monologue);
        yield return new WaitForSeconds(2f);
        _isMono = false;
    }
    public void InteractDrawer(int p_index)
    {
        if (_elements[p_index].isLocked) 
        {
            if (!_isMono)
            {
                if (player != null && player.IsOwner)
                StartCoroutine(Delay());
            }
            return; 
        }
        if (_elements[p_index].isAnimation) return;

        _audioSource.clip = _elements[p_index].IsOpen ? _elements[p_index].audioClipClose : _elements[p_index].audioClipOpen;
        _audioSource.Play();
        _elements[p_index].isAnimation = true;
        Vector3 movement = _elements[p_index].IsOpen ? _elements[p_index].origin : _elements[p_index].destination;

        if (!_elements[p_index].isDrawer) //drzwiczki
        {
            _elements[p_index].visual.DOLocalRotate(movement, _time).SetEase(Ease.Linear).OnComplete(() =>
                {
                    _elements[p_index].IsOpen = !_elements[p_index].IsOpen;
                    _elements[p_index].isAnimation = false;
                });    
        }
        else //szuflada
        {
            _elements[p_index].visual.DOLocalMove(movement, _time).SetEase(Ease.Linear).OnComplete(() =>
                {
                    _elements[p_index].IsOpen = !_elements[p_index].IsOpen;
                    _elements[p_index].isAnimation = false;
                });
        }
    }
    public void Unlock(int p_index)
    {
        _elements[p_index].isLocked = false;
    }
}

[System.Serializable]
public class Drawer
{
    public bool isLocked = false;
    public Transform visual;
    public bool isOpen;
    public bool IsOpen { 
        get { return isOpen; } 
        set { isOpen = value; }
    }
    public bool isDrawer;
    public Vector3 origin;
    public Vector3 destination;
    public AudioClip audioClipOpen;
    public AudioClip audioClipClose;
    public bool isAnimation = false;
}
