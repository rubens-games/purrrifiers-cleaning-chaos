using UnityEngine;

[CreateAssetMenu(fileName = "New Monologue", menuName = "Monologue")]
public class MonologueSO : ScriptableObject
{
    public AudioClip audioClip;
    [TextArea] public string txt;
}
