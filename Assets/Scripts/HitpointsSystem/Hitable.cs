using System;
using FishNet.CodeGenerating;
using FishNet.Connection;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using FishNet.Transporting;
using UnityEngine;
using UnityEngine.Events;

namespace HitpointsSystem
{
    public class Hitable : NetworkBehaviour
    {
        public UnityEvent<int> OnHitpointsChange = new();
        public UnityEvent<int> OnHitpointsIncrease = new();
        public UnityEvent<int> OnHitpointsDecrease = new();
        public UnityEvent<int> OnDeath = new();
        public UnityEvent<int> OnRevive = new();

        public int hitpoints
        {
            get => _syncHitpoints.Value;
            [ServerRpc] private set => _syncHitpoints.Value = Mathf.Clamp(value, 0, _maxHitpoints);
        }
        public int maxHitpoints => _maxHitpoints;
        public int hitpointsPercentage => Mathf.RoundToInt((float) hitpoints / _maxHitpoints * 100);
        public int missingHitpoints => _maxHitpoints - hitpoints;
        public Collider hitbox { get; private set; }
        
        public bool isAlive => hitpoints > 0;
        public bool isDead => !isAlive;
        
        [SerializeField] private int _maxHitpoints = 100;
        
        // [AllowMutableSyncType, SerializeField]
        private readonly SyncVar<int> _syncHitpoints = new(100);

        private void Awake()
        {
            hitbox = GetComponent<Collider>();
        }

        protected virtual void OnEnable()
        {
            _syncHitpoints.OnChange += OnSyncHitpointsChange;
        }
        
        protected virtual void OnDisable()
        {
            _syncHitpoints.OnChange -= OnSyncHitpointsChange;
        }

        public override void OnOwnershipClient(NetworkConnection prevOwner)
        {
            base.OnOwnershipClient(prevOwner);

            if (IsOwner) hitpoints = _maxHitpoints;
        }

        private void OnSyncHitpointsChange(int oldValue, int newValue, bool asServer)
        {
            OnHitpointsChange?.Invoke(newValue);

            if (newValue > oldValue) OnHitpointsIncrease?.Invoke(newValue - oldValue);
            else if (newValue < oldValue) OnHitpointsDecrease?.Invoke(oldValue - newValue);
            
            var isOldAlive = oldValue > 0;
            var isNewAlive = newValue > 0;
            var isOldDead = !isOldAlive;
            var isNewDead = !isNewAlive;
            
            if (isOldAlive && isNewDead) OnDeath?.Invoke(newValue);
            else if (isOldDead && isNewAlive) OnRevive?.Invoke(newValue);
        }

        public void Hit(int damage) => hitpoints -= damage;
        
        public void Heal(int health) => hitpoints += health;
        
        [ContextMenu("Hit 1000")]
        public void Hit1000() => Hit(1000);
        
        [ContextMenu("Heal 1000")]
        public void Heal1000() => Heal(1000);
    }
}