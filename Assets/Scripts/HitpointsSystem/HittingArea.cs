using System.Collections;
using UnityEngine;

namespace HitpointsSystem
{
    public class HittingArea : MonoBehaviour
    {
        [SerializeField] private int _damage = 1;
        [SerializeField] private float _hitFrequency = 0.5f;

        private Collider _collider;

        private void Awake() => _collider = GetComponent<Collider>();

        private void OnTriggerEnter(Collider other)
        {
            if (!other.TryGetComponent(out Hitable hitable)) return;

            if (hitable.IsOwner)
            {
                Debug.Log($"Started hitting {hitable.name}");
                StartHitting(hitable);
            }
        }
        
        private void OnTriggerExit(Collider other)
        {
            if (!other.TryGetComponent(out Hitable hitable)) return;

            if (hitable.IsOwner)
            {
                Debug.Log($"Stopped hitting {hitable.name}");
                StopHitting(hitable);
            }
        }
        
        private void StartHitting(Hitable hitable) => StartCoroutine(Hitting(hitable));

        private IEnumerator Hitting(Hitable hitable)
        {            
            while (CanHit(hitable))
            {
                if (hitable.IsOwner)
                {
                    hitable.Hit(_damage);
                    Debug.Log($"Hitting {hitable.name} for {_damage} damage. Remaining hitpoints: {hitable.hitpoints}");
                }
            
                yield return new WaitForSeconds(_hitFrequency);
            }
        }
        
        private void StopHitting(Hitable hitable) => StopCoroutine(Hitting(hitable));
        
        private bool CanHit(Hitable hitable) => hitable && hitable.isAlive && IsHittableInCollider(hitable);

        private bool IsHittableInCollider(Hitable hitable) => _collider.bounds.Intersects(hitable.hitbox.bounds);
    }
}
