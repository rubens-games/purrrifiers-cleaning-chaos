using System;
using DG.Tweening;
using RebuildUISystem;
using TMPro;
using UnityEngine;

namespace UISystem.Monologue
{
    public class MonologueUI : MonoBehaviour
    {
        [Header("UI Elements")] 
        [SerializeField] private RectTransform _canvasRectTransform;
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private TMP_Text _text;
        
        [Header("UI Animation")]
        [SerializeField] private float _fadeInDuration;
        [SerializeField] private Ease _fadeInEase;
        [SerializeField] private float _fadeOutDuration;
        [SerializeField] private Ease _fadeOutEase;
        
        private const string ANIMATION_ID = "MonologueUIAnimation";

        private void Awake() => _canvasGroup.alpha = 0;

        public void UpdateText(string text)
        {
            _text.text = text;
            
            RebuildUIManager.instance.Register(_canvasRectTransform);
        }

        public void ShowUI()
        {
            DOTween.Kill(ANIMATION_ID);

            _canvasGroup.DOFade(1, _fadeInDuration).SetUpdate(true).SetEase(_fadeInEase)
                .SetId(ANIMATION_ID);
        }

        public void HideUI()
        {
            DOTween.Kill(ANIMATION_ID);

            _canvasGroup.DOFade(0, _fadeOutDuration).SetUpdate(true).SetEase(_fadeOutEase)
                .SetId(ANIMATION_ID);
        }
    }
}
