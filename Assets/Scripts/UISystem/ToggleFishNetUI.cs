﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace UISystem
{
    public class ToggleFishNetUI : MonoBehaviour
    {
        [SerializeField] private InputAction _toggleFishNetUIAction;

        private void Awake()
        {
            _toggleFishNetUIAction.performed += OnToggleFishNetUI;
            _toggleFishNetUIAction.Enable();
        }

        private void OnDestroy()
        {
            _toggleFishNetUIAction.performed -= OnToggleFishNetUI;
            _toggleFishNetUIAction.Disable();
        }

        private void OnToggleFishNetUI(InputAction.CallbackContext context) => 
            gameObject.SetActive(!gameObject.activeSelf);
    }
}