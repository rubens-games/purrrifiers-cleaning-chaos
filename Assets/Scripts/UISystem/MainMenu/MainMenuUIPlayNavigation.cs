﻿using FishNet.Managing;
using FishNet.Transporting.Multipass;
using FishNet.Transporting.Tugboat;
using HeathenEngineering.SteamworksIntegration;
using SceneLoaderSystem;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UISystem.MainMenu
{
    public class MainMenuUIPlayNavigation : MonoBehaviour
    {
        [Header("Buttons")]
        [SerializeField] private Button _playSoloButton;
        [SerializeField] private Button _joinLobbyButton;
        [SerializeField] private Button _createLobbyButton;
        [SerializeField] private Button _backButton;
        
        [Header("Panels")]
        [SerializeField] private GameObject _mainMenuPanel;
        [SerializeField] private GameObject _joinLobbyPanel;
        [SerializeField] private GameObject _lobbyPanel;

        [Inject] private LobbyManager _lobbyManager;
        [Inject] private NetworkManager _networkManager;
        
        private static MainMenuUI mainMenuUI => MainMenuUI.instance;
        
        private void OnEnable()
        {
            _playSoloButton.onClick.AddListener(OnPlaySoloButtonClicked);
            _joinLobbyButton.onClick.AddListener(OnJoinLobbyButtonClicked);
            _createLobbyButton.onClick.AddListener(OnCreateLobbyButtonClicked);
            _backButton.onClick.AddListener(OnBackButtonClicked);
        }
        
        private void OnDisable()
        {
            _playSoloButton.onClick.RemoveListener(OnPlaySoloButtonClicked);
            _joinLobbyButton.onClick.RemoveListener(OnJoinLobbyButtonClicked);
            _createLobbyButton.onClick.RemoveListener(OnCreateLobbyButtonClicked);
            _backButton.onClick.RemoveListener(OnBackButtonClicked);
        }

        private void OnPlaySoloButtonClicked()
        {
            SceneLoader.instance.LoadScene("Demo_Town", 
                onBeforeNextSceneLoaded: OnBeforeNextSceneLoaded,
                onAfterNextSceneLoaded: OnAfterNextSceneLoaded);

            return;
            
            void OnBeforeNextSceneLoaded()
            {
                _networkManager.TransportManager.GetTransport<Multipass>().SetClientTransport<Tugboat>();
                _networkManager.ServerManager.StartConnection();
            }
            
            void OnAfterNextSceneLoaded() => _networkManager.ClientManager.StartConnection();
        }

        private void OnJoinLobbyButtonClicked()
        {
            _mainMenuPanel.SetActive(false);
            _joinLobbyPanel.SetActive(true);
        }
        
        private void OnCreateLobbyButtonClicked()
        {
            _mainMenuPanel.SetActive(false);
            _lobbyManager.Create();
        }

        private void OnBackButtonClicked()
        {
            gameObject.SetActive(false);
            mainMenuUI.mainNavigation.gameObject.SetActive(true);
        }
    }
}