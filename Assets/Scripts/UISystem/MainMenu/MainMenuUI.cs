using HeathenEngineering.SteamworksIntegration;
using SingletonSystem.Runtime;
using Zenject;

namespace UISystem.MainMenu
{
    public class MainMenuUI : Singleton<MainMenuUI>
    {
        [Inject] public LobbyManager lobbyManager { get; private set; }

        public MainMenuUIMainNavigation mainNavigation;
        public MainMenuUIPlayNavigation playNavigation;
    }
}