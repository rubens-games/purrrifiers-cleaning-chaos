﻿using System.Linq;
using FishNet.Managing;
using FishNet.Transporting.Multipass;
using FishNet.Transporting.Tugboat;
using HeathenEngineering.SteamworksIntegration;
using HeathenEngineering.SteamworksIntegration.API;
using NoReleaseDate.Common.Runtime.Extensions;
using SceneLoaderSystem;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;
using SceneManager = UnityEngine.SceneManagement.SceneManager;

namespace UISystem.MainMenu
{
    public class MainMenuUILobby : MonoBehaviour
    {
        [Header("Buttons")]
        [SerializeField] private Button _readyButton;
        [SerializeField] private Button _leaveLobbyButton;
        [SerializeField] private Button _startGameButton;
        
        [Header("Panels")]
        [SerializeField] private GameObject _mainMenuPanel;
        [SerializeField] private GameObject _lobbyContentPanel;
        [SerializeField] private GameObject _joinLobbyPanel;
        
        [Header("UI Elements")]
        [SerializeField] private TMP_InputField _lobbyCodeInputField;
        [SerializeField] private TMP_Text _playersCountText;
        [SerializeField] private TMP_Text _readyButtonText;
        
        [Header("Lobby Members")]
        [SerializeField] private Transform _lobbyMembersContainer;
        [SerializeField] private MainMenuUILobbyMember _lobbyMemberPrefab;
        
        [Inject] private NetworkManager _networkManager;
        [Inject] private LobbyManager _lobbyManager;
        [Inject] private FishySteamworks.FishySteamworks _fishySteamworks;

        [ContextMenu("Debug Lobby")]
        private void DebugLobby()
        {
            Debug.Log($"Has Lobby: {_lobbyManager.HasLobby}");
            Debug.Log($"Lobby Name: {_lobbyManager.Lobby.Name}");
            Debug.Log($"Lobby Code: {_lobbyManager.Lobby.ToString()}");
            Debug.Log($"Lobby Owner: {_lobbyManager.Lobby.Owner.user.Name}");
            _lobbyManager.Lobby.Members.ForEach(member => Debug.Log($"Member: {member.user.Name}"));
        }
        
        private void OnEnable()
        {
            _lobbyManager.evtCreated.AddListener(OnLobbyCreated);
            _lobbyManager.evtEnterSuccess.AddListener(OnLobbyEnterSuccess);
            _lobbyManager.evtUserJoined.AddListener(OnUserJoined);
            _lobbyManager.evtUserLeft.AddListener(OnUserLeft);
            _lobbyManager.evtLeave.AddListener(OnLeaveLobby);
            _lobbyManager.evtDataUpdated.AddListener(OnLobbyDataUpdated);
            _lobbyManager.evtGameCreated.AddListener(OnGameCreated);
            
            _readyButton.onClick.AddListener(OnReadyButtonClicked);
            _leaveLobbyButton.onClick.AddListener(OnLeaveLobbyButtonClicked);
            _startGameButton.onClick.AddListener(OnStartGameButtonClicked);
        }
        
        private void OnDisable()
        {
            _lobbyManager.evtCreated.RemoveListener(OnLobbyCreated);
            _lobbyManager.evtEnterSuccess.RemoveListener(OnLobbyEnterSuccess);
            _lobbyManager.evtUserJoined.RemoveListener(OnUserJoined);
            _lobbyManager.evtUserLeft.RemoveListener(OnUserLeft);
            _lobbyManager.evtLeave.RemoveListener(OnLeaveLobby);
            _lobbyManager.evtDataUpdated.RemoveListener(OnLobbyDataUpdated);
            _lobbyManager.evtGameCreated.RemoveListener(OnGameCreated);
            
            _readyButton.onClick.RemoveListener(OnReadyButtonClicked);
            _leaveLobbyButton.onClick.RemoveListener(OnLeaveLobbyButtonClicked);
            _startGameButton.onClick.RemoveListener(OnStartGameButtonClicked);
        }

        #region Lobby Events

        private void OnLobbyCreated(LobbyData lobbyData)
        {
            EnableLobbyContentPanel();
            UpdateUI();
        }
        
        private void OnLobbyEnterSuccess(LobbyData lobbyData)
        {
            EnableLobbyContentPanel();
            
            UpdateUI();
        }
        
        private void OnUserJoined(UserData userData) => UpdateUI();

        private void OnUserLeft(UserLobbyLeaveData arg0) => UpdateUI();
        
        private void OnLeaveLobby()
        {
            StopServer();
            DisableLobbyContentPanel();
        }

        private void OnLobbyDataUpdated(LobbyDataUpdateEventData arg0) => UpdateUI();
        
        private void OnGameCreated(LobbyGameServer gameServer)
        {
            // Return if i am the host
            if (gameServer.id == User.Client.Id.id)
                return;
            
            _fishySteamworks.SetClientAddress(gameServer.id.m_SteamID.ToString());
            
            SceneLoader.instance.LoadScene("Demo_Town", onAfterNextSceneLoaded: OnGameplaySceneLoaded);
            
            return;
            
            void OnGameplaySceneLoaded()
            {
                _networkManager.TransportManager.GetTransport<Multipass>()
                    .SetClientTransport<FishySteamworks.FishySteamworks>();
                
                _networkManager.ClientManager.StartConnection();
            }
        }

        #endregion

        #region Button Events

        private void OnReadyButtonClicked() => _lobbyManager.IsPlayerReady = !_lobbyManager.IsPlayerReady;

        private void OnLeaveLobbyButtonClicked() => _lobbyManager.Leave();

        private void OnStartGameButtonClicked()
        {
            _startGameButton.interactable = false;
            
            _lobbyManager.Lobby.SetJoinable(false);
            
            SceneLoader.instance.LoadScene("Demo_Town", onAfterNextSceneLoaded: OnGameplaySceneLoaded);
            
            return;
            
            void OnGameplaySceneLoaded()
            {
                StartServer();
                
                _networkManager.ClientManager.StartConnection();
                
                var lobbyHost = _lobbyManager.Lobby.Owner.user;
                _lobbyManager.Lobby.SetGameServer(lobbyHost);
            }
        }

        #endregion

        private void UpdateUI()
        {
            _lobbyCodeInputField.text = _lobbyManager.Lobby.ToString();
            _playersCountText.text = $"Players: {_lobbyManager.Lobby.Members.Length}/{_lobbyManager.Lobby.MaxMembers}";
            _readyButtonText.text = _lobbyManager.IsPlayerReady ? "Mark as not Ready" : "Mark as Ready";
            
            RefreshLobbyMembers();
            
            HandleStartGameButtonVisibility();
        }
        
        public void RefreshLobbyMembers()
        {
            _lobbyMembersContainer.DestroyChildren();
            
            var prevEnabled = _lobbyMemberPrefab.gameObject.activeSelf;
            _lobbyMemberPrefab.gameObject.SetActive(false);
            
            foreach (var member in _lobbyManager.Members)
            {
                var lobbyMemberUIClone = Instantiate(_lobbyMemberPrefab, _lobbyMembersContainer);
                
                lobbyMemberUIClone.lobbyMemberData = member;
                
                lobbyMemberUIClone.gameObject.SetActive(true);
            }
            
            _lobbyMemberPrefab.gameObject.SetActive(prevEnabled);
        }
        
        private void EnableLobbyContentPanel()
        {
            _mainMenuPanel.SetActive(false);
            _joinLobbyPanel.SetActive(false);
            _lobbyContentPanel.SetActive(true);
        }

        private void DisableLobbyContentPanel()
        {
            _mainMenuPanel.SetActive(true);
            _lobbyContentPanel.SetActive(false);
        }
        
        private void HandleStartGameButtonVisibility()
        {
            if (!_lobbyManager.Lobby.IsOwner)
            {
                _startGameButton.gameObject.SetActive(false);
                return;
            }

            var isAllMembersReady = _lobbyManager.Lobby.Members.All(member => member.IsReady);
            _startGameButton.gameObject.SetActive(isAllMembersReady);
        }

        private void StartServer()
        {
            if (!_lobbyManager.Lobby.IsOwner) return;

            _fishySteamworks.SetClientAddress(_lobbyManager.Lobby.Owner.user.SteamId.ToString());
            
            _networkManager.TransportManager.GetTransport<Multipass>()
                .SetClientTransport<FishySteamworks.FishySteamworks>();
            
            _networkManager.ServerManager.StartConnection();
        }

        private void StopServer() => _networkManager.ServerManager.StopConnection(true);
    }
}