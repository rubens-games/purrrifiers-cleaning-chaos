﻿using System;
using HeathenEngineering.SteamworksIntegration;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UISystem.MainMenu
{
    public class MainMenuUIJoinLobby : MonoBehaviour
    {
        [Header("Buttons")]
        [SerializeField] private Button _joinLobbyButton;
        [SerializeField] private Button _backButton;
        
        [Header("UI Elements")]
        [SerializeField] private TMP_InputField _lobbyCodeInputField;
        
        [Header("UI Panels")]
        [SerializeField] private GameObject _mainMenuPanel;

        [Inject] private LobbyManager _lobbyManager;
        
        private void OnEnable()
        {
            _joinLobbyButton.onClick.AddListener(OnJoinLobbyButtonClicked);
            _backButton.onClick.AddListener(OnBackButtonClicked);
            _lobbyCodeInputField.onValueChanged.AddListener(OnLobbyCodeInputFieldChanged);
        }
        
        private void OnDisable()
        {
            _joinLobbyButton.onClick.RemoveListener(OnJoinLobbyButtonClicked);
            _backButton.onClick.RemoveListener(OnBackButtonClicked);
            _lobbyCodeInputField.onValueChanged.RemoveListener(OnLobbyCodeInputFieldChanged);
        }

        private void OnJoinLobbyButtonClicked()
        {
            var lobby = LobbyData.Get(_lobbyCodeInputField.text);
            if (lobby.IsValid) _lobbyManager.Join(lobby);
            else Debug.LogWarning("Invalid Lobby ID was provided, no action taken.");
        }

        private void OnBackButtonClicked()
        {
            gameObject.SetActive(false);
            _mainMenuPanel.SetActive(true);
        }
        
        private void OnLobbyCodeInputFieldChanged(string inputValue)
        {
            HandleJoinLobbyButtonInteractable();
        }

        private void HandleJoinLobbyButtonInteractable()
        {
            _joinLobbyButton.interactable = IsLobbyCodeValid(_lobbyCodeInputField.text);
        }
        
        private static bool IsLobbyCodeValid(string lobbyCode)
        {
            var lobbyId = Convert.ToUInt32(lobbyCode, 16);
            return lobbyId > 0;
        }
    }
}