﻿using FishNet.Object;
using UnityEngine;
using UnityEngine.UI;

namespace UISystem.MainMenu
{
    public class MainMenuUIMainNavigation : MonoBehaviour
    {
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _settingsButton;
        [SerializeField] private Button _quitGameButton;
        
        private static MainMenuUI mainMenuUI => MainMenuUI.instance;
        
        private void OnEnable()
        {
            _playButton.onClick.AddListener(OnPlayButtonClicked);
            _settingsButton.onClick.AddListener(OnSettingsButtonClicked);
            _quitGameButton.onClick.AddListener(OnQuitGameButtonClicked);
        }
        
        private void OnDisable()
        {
            _playButton.onClick.RemoveListener(OnPlayButtonClicked);
            _settingsButton.onClick.RemoveListener(OnSettingsButtonClicked);
            _quitGameButton.onClick.RemoveListener(OnQuitGameButtonClicked);
        }
        
        private void OnPlayButtonClicked() => Play();
        
        private void OnSettingsButtonClicked() => Debug.Log("Settings Button Clicked");
        
        private void OnQuitGameButtonClicked() => QuitGame();
        
        private void Play()
        {
            gameObject.SetActive(false);
            mainMenuUI.playNavigation.gameObject.SetActive(true);
        }
        
        private static void QuitGame()
        {
#if UNITY_EDITOR
            
            UnityEditor.EditorApplication.isPlaying = false;
            
#endif
            
            Application.Quit();
        }
    }
}