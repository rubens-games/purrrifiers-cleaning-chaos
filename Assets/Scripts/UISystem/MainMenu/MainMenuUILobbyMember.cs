﻿using HeathenEngineering.SteamworksIntegration;
using HeathenEngineering.SteamworksIntegration.UI;
using TMPro;
using UnityEngine;

namespace UISystem.MainMenu
{
    public class MainMenuUILobbyMember : MonoBehaviour
    {
        public LobbyMemberData lobbyMemberData
        {
            get => _lobbyMemberData;
            set
            {
                _lobbyMemberData = value;
                
                UpdateUI();
            }
        }


        [Header("UI Elements")]
        [SerializeField] private SetUserAvatar _playerAvatar;
        [SerializeField] private TMP_Text _playerNameText;
        [SerializeField] private TMP_Text _playerStatusText;
        
        private LobbyMemberData _lobbyMemberData;
        
        public void UpdateUI()
        {
            var userData = _lobbyMemberData.user;

            _playerAvatar.UserData = userData;
            _playerNameText.text = userData.Name;
            _playerStatusText.text = _lobbyMemberData.IsReady ? "Ready" : "Not Ready";

            gameObject.name = $"Lobby Member - {userData.Name}";
        }
    }
}