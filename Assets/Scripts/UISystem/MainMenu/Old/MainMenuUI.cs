using UnityEngine;
using UnityEngine.UI;

namespace UISystem.MainMenu.Old
{
    public class MainMenuUI : MonoBehaviour
    {
        [Header("UI References")]
        [SerializeField] private RectTransform _window;
        
        [Header("Panels")]
        [SerializeField] private MainMenuUIMultiplayer _multiplayerPanel;
        
        [Header("Buttons")]
        [SerializeField] private Button _selectProfileButton;
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _multiplayerButton;
        [SerializeField] private Button _settingsButton;
        [SerializeField] private Button _exitButton;

        private void OnEnable()
        {
            _selectProfileButton.onClick.AddListener(OnSelectProfileButtonClicked);
            _playButton.onClick.AddListener(OnPlayButtonClicked);
            _multiplayerButton.onClick.AddListener(OnMultiplayerButtonClicked);
            _settingsButton.onClick.AddListener(OnSettingsButtonClicked);
            _exitButton.onClick.AddListener(OnExitButtonClicked);
        }
        
        private void OnDisable()
        {
            _selectProfileButton.onClick.RemoveListener(OnSelectProfileButtonClicked);
            _playButton.onClick.RemoveListener(OnPlayButtonClicked);
            _multiplayerButton.onClick.RemoveListener(OnMultiplayerButtonClicked);
            _settingsButton.onClick.RemoveListener(OnSettingsButtonClicked);
            _exitButton.onClick.RemoveListener(OnExitButtonClicked);
        }
        
        private void OnSelectProfileButtonClicked()
        {
            Debug.Log("Select Profile Button Clicked");
        }
        
        private void OnPlayButtonClicked()
        {
            Debug.Log("Play Button Clicked");
        }
        
        private void OnMultiplayerButtonClicked()
        {
            Disable();
            _multiplayerPanel.Enable();
        }
        
        private void OnSettingsButtonClicked()
        {
            Debug.Log("Settings Button Clicked");
        }
        
        private void OnExitButtonClicked()
        {
            Debug.Log("Exit Button Clicked");
        }
        
        public void Enable() => _window.gameObject.SetActive(true);

        public void Disable() => _window.gameObject.SetActive(false);
    }
}
