﻿using HeathenEngineering.SteamworksIntegration;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UISystem.MainMenu.Old
{
    public class MainMenuUILobbyClient : MonoBehaviour
    {
        public UserData UserData;
        
        [Header("UI References")]
        [SerializeField] private Image _avatarImage;
        [SerializeField] private TMP_Text _playerNameText;
        
        [Header("Buttons")]
        [SerializeField] private Button _kickButton;
        [SerializeField] private Button _leaveButton;

        [Inject] private LobbyManager _lobbyManager;
        
        private void OnEnable()
        {
            _kickButton.onClick.AddListener(OnKickButtonClicked);
            _leaveButton.onClick.AddListener(OnLeaveButtonClicked);
        }
        
        private void OnDisable()
        {
            _kickButton.onClick.RemoveListener(OnKickButtonClicked);
            _leaveButton.onClick.RemoveListener(OnLeaveButtonClicked);
        }
        
        private void OnKickButtonClicked()
        {
            _lobbyManager.Lobby.KickMember(UserData.SteamId);
            Destroy(gameObject);
        }
        
        private void OnLeaveButtonClicked()
        {
            _lobbyManager.Lobby.Leave();
            Destroy(gameObject);
        }

        public void SetUserData(UserData userData)
        {
            UserData = userData;
            UpdateUI();
        }
        
        private void UpdateUI()
        {
            UserData.LoadAvatar(OnAvatarLoaded);

            if (UserData.IsMe)
            {
                _kickButton.gameObject.SetActive(false);
                _leaveButton.gameObject.SetActive(true);
            }
            else
            {
                _kickButton.gameObject.SetActive(true);
                _leaveButton.gameObject.SetActive(false);
            }
            
            _playerNameText.text = UserData.Name;
        }
        
        private void OnAvatarLoaded(Texture2D texture)
        {
            _avatarImage.sprite = Sprite.Create(texture,
                new Rect(0, 0, texture.width, texture.height),
                new Vector2(0.5f, 0.5f));
        }
        
        public void Enable() => gameObject.SetActive(true);

        public void Disable() => gameObject.SetActive(false);
    }
}