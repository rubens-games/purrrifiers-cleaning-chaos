using HeathenEngineering.SteamworksIntegration;
using Steamworks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UISystem.MainMenu.Old
{
    public class MainMenuUILobby : MonoBehaviour
    {
        [Header("Panels")]
        [SerializeField] private MainMenuUIMultiplayer _multiplayerPanel;
        
        [Header("UI References")]
        [SerializeField] private RectTransform _window;
        [SerializeField] private RectTransform _lobbyClientContainer;
        [SerializeField] private TMP_Text _lobbyNameText;
        
        [Header("Prefabs")]
        [SerializeField] private MainMenuUILobbyClient _lobbyClientPrefab;
        
        [Header("Buttons")]
        [SerializeField] private Button _changeProfileButton;
        [SerializeField] private Button _lobbySettingsButton;
        [SerializeField] private Button _readyButton;
        [SerializeField] private Button _leaveLobbyButton;
        [SerializeField] private Button _startGameButton;
        [SerializeField] private Button _backButton;

        [Inject] private LobbyManager _lobbyManager;
        [Inject] private HostGame _hostGame;
        
        private LobbyData _selectedLobby;
        
        private void OnEnable()
        {
            _lobbyManager.evtCreated.AddListener(OnLobbyCreated);
            _lobbyManager.evtUserJoined.AddListener(OnSomeOneJoinedTheLobby);
            _lobbyManager.evtUserLeft.AddListener(OnSomeOneLeftTheLobby);
            _lobbyManager.evtEnterSuccess.AddListener(OnEnterLobbySuccess);
            _lobbyManager.evtDataUpdated.AddListener(OnLobbyDataUpdated);
            _lobbyManager.evtAuthenticationSessionResult.AddListener(OnAuthenticationSessionResult);
            _lobbyManager.evtLeave.AddListener(OnLeaveLobby);
            
            _changeProfileButton.onClick.AddListener(OnSelectProfileButtonClicked);
            _lobbySettingsButton.onClick.AddListener(OnLobbySettingsButtonClicked);
            _readyButton.onClick.AddListener(OnReadyButtonClicked);
            _leaveLobbyButton.onClick.AddListener(OnLeaveLobbyButtonClicked);
            _startGameButton.onClick.AddListener(OnStartGameButtonClicked);
            _backButton.onClick.AddListener(OnBackButtonClicked);
            
            UpdateUI();
        }
        
        private void OnDisable()
        {
            _lobbyManager.evtCreated.RemoveListener(OnLobbyCreated);
            _lobbyManager.evtUserJoined.RemoveListener(OnSomeOneJoinedTheLobby);
            _lobbyManager.evtUserLeft.RemoveListener(OnSomeOneLeftTheLobby);
            _lobbyManager.evtEnterSuccess.RemoveListener(OnEnterLobbySuccess);
            _lobbyManager.evtDataUpdated.RemoveListener(OnLobbyDataUpdated);
            _lobbyManager.evtAuthenticationSessionResult.RemoveListener(OnAuthenticationSessionResult);
            _lobbyManager.evtLeave.RemoveListener(OnLeaveLobby);
            
            _changeProfileButton.onClick.RemoveListener(OnSelectProfileButtonClicked);
            _lobbySettingsButton.onClick.RemoveListener(OnLobbySettingsButtonClicked);
            _readyButton.onClick.RemoveListener(OnReadyButtonClicked);
            _leaveLobbyButton.onClick.RemoveListener(OnLeaveLobbyButtonClicked);
            _startGameButton.onClick.RemoveListener(OnStartGameButtonClicked);
            _backButton.onClick.RemoveListener(OnBackButtonClicked);
        }

        #region Event Handlers

        #region Lobby Events

        private void OnLobbyCreated(LobbyData lobbyData)
        {
            _multiplayerPanel.Disable();
            Enable();
            
            var id = lobbyData.SteamId;
            Debug.Log("On Handle Lobby Created: a new lobby has been created with CSteamID = " + lobbyData
                + "\nThe CSteamID can be broken down into its parts such as :"
                + "\nAccount Type = " + id.GetEAccountType()
                + "\nAccount Instance = " + id.GetUnAccountInstance()
                + "\nUniverse = " + id.GetEUniverse()
                + "\nAccount Id = " + id.GetAccountID());

            // When the lobby is created the owner also starts with a IsReady = false status:
            if (!lobbyData.IsOwner) return;
            
            lobbyData.IsReady = false;
            _selectedLobby = lobbyData;
        }
        
        private void OnLeaveLobby()
        {
            Disable();
            _multiplayerPanel.Enable();
            _selectedLobby = null;
        }
        
        private void OnSomeOneJoinedTheLobby(UserData userData)
        {
            // The argument contains the user who joined the lobby:
            Debug.Log($"{userData.Name} joined the lobby.");

            DisplayLobbyMembers();
            
            // Now iterate through all lobby members:
            var lobbyReference = _lobbyManager.Lobby;
            foreach (var member in lobbyReference.Members)
            {
                Debug.Log(member.user.Name);
            }
        
            // Now check if all players are ready so we can enable the startGameButton (linked to in this component)
            // All players are ready and I am the Lobby owner so I can now start the game
            _startGameButton.interactable = _selectedLobby is { AllPlayersReady: true, IsOwner: true };
        }
        
        private void OnSomeOneLeftTheLobby(UserLobbyLeaveData userLobbyLeaveData)
        {
            // The argument contains the user who left the lobby:
            Debug.Log($"{userLobbyLeaveData.user.Name} left the lobby.");

            // Destroy the LobbyClient prefab
            DisplayLobbyMembers();
            
            // Now iterate through all lobby members:
            Debug.Log("The lobby members are:");
            var lobbyReference = _lobbyManager.Lobby;
            foreach(var member in lobbyReference.Members) Debug.Log(member.user.Name);
        
            // Now check if all players are ready so we can enable the startGameButton (linked to in this component)
            // All players are ready and I am the Lobby owner so I can now start the game
            _startGameButton.interactable = _selectedLobby is { AllPlayersReady: true, IsOwner: true };
        }
        
        public void OnEnterLobbySuccess(LobbyData lobby)
        {
            Debug.Log("You entered Lobby: "+lobby.Name);
            _selectedLobby = lobby;
        
            // Show the lobby details
            ReportLobbyDetails(lobby);
            
            DisplayLobbyMembers();
        }
        
        public void OnLobbyDataUpdated(LobbyDataUpdateEventData lobbyDataUpdateEventData)
        {
            Debug.Log("Lobby Meta Data has been updated");
            if (_selectedLobby == null) return;

            if (!_selectedLobby.IsOwner) return;
            
            // Only if you are the owner...
            if (_startGameButton)
                // ... enable the startGameButton if all players have reported to be ready
                _startGameButton.interactable = _selectedLobby.AllPlayersReady;
            
            DisplayLobbyMembers();
        }
        
        public void OnAuthenticationSessionResult(AuthenticationSession authenticationSession, byte[] bytes)
        {
            Debug.Log("Authentication Session Result: " + authenticationSession);
        }

        #endregion

        #region Button Clicks
        
        private void OnSelectProfileButtonClicked()
        {
            Debug.Log("Select Profile Button Clicked");
        }
        
        private void OnLobbySettingsButtonClicked()
        {
            Debug.Log("Lobby Settings Button Clicked");
        }
        
        private void OnReadyButtonClicked()
        {
            // First check if we have created or joined a lobby
            if (_selectedLobby == null) return;
            
            // OK, we are ready to indicate this to the lobby:
            _selectedLobby.IsReady = !_selectedLobby.IsReady;

            if (!_selectedLobby.AllPlayersReady || !_selectedLobby.IsOwner) return;
            
            // All players are ready and I am the Lobby owner so I can now start the game
            if (_startGameButton) 
                _startGameButton.interactable = true;
        }
        
        private void OnLeaveLobbyButtonClicked()
        {
            Debug.Log("Leave Lobby Button Clicked");
            
            _lobbyManager.Leave();
        }
        
        private void OnStartGameButtonClicked()
        {
            if (_selectedLobby == null) return;
            // We have an active lobby

            if (_selectedLobby.IsOwner)
            {
                // I am the owner so I can start the game

                if(_hostGame)
                    _hostGame.StartHostingGame(_selectedLobby);
                 
            }
            else Debug.LogWarning("StartGame not by the owner.");
        }
        
        private void OnBackButtonClicked()
        {
            gameObject.SetActive(false);
            _multiplayerPanel.gameObject.SetActive(true);
        }
        
        #endregion

        #endregion
        
        public void ReportSearchResults(LobbyData[] results)
        {
            // Do we have anyh results?
            if (results.Length > 0)
            {
                // Just pick teh first lobby
                LobbyData firstLobby = results[0];
            
                Debug.Log("Joining Lobby: "+firstLobby.Name);
            
                // Join the lobby
                _lobbyManager.Join(firstLobby);

                // if (browseButton)
                //     browseButton.interactable = false;
                //
                // if (readyButton)
                //     readyButton.interactable = true;
            }
            else Debug.Log("No lobbies found!");

        }
        
        private void ReportLobbyDetails(LobbyData lobby)
        {
            if (lobby == null) return;
            
            // retrieve the Lobby owner from the Lobby:
            var owner = HeathenEngineering.SteamworksIntegration.API.Matchmaking.Client.GetLobbyOwner(lobby);
            // Then get the owner name:
            UserData ownerData = new CSteamID(owner.m_SteamID);

            Debug.Log("Lobby name: " + lobby.Name);
            Debug.Log("Lobby owner: " + ownerData.Name);

            // Get all lobby members and show some of their details:
            var members = lobby.Members;
            foreach (var lobbyMember in members)
                Debug.Log($"Lobby member: {lobbyMember.user.Name}, isReady = {lobbyMember.IsReady}");
        }
        
        public void Enable() => _window.gameObject.SetActive(true);
        
        public void Disable() => _window.gameObject.SetActive(false);
        
        private void DisplayLobbyMembers()
        {
            Debug.Log($"Displaying Lobby Members: {_lobbyManager.Lobby.Members.Length} members in the lobby.");
            
            var lobby = _lobbyManager.Lobby;

            var currentLobbyClients = _lobbyClientContainer.GetComponentsInChildren<MainMenuUILobbyClient>();
            foreach (var lobbyClient in currentLobbyClients) 
                Destroy(lobbyClient.gameObject);
            
            foreach (var member in lobby.Members)
            {
                var lobbyClient = Instantiate(_lobbyClientPrefab, _lobbyClientContainer);
                lobbyClient.SetUserData(member.user);
                lobbyClient.Enable();
            }
        }

        private void UpdateUI()
        {
            _lobbyNameText.text = _selectedLobby.Name;
        }
    }
}
