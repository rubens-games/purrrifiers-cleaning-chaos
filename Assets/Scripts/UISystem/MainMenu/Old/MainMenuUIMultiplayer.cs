﻿using HeathenEngineering.SteamworksIntegration;
using Steamworks;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UserAPI = HeathenEngineering.SteamworksIntegration.API.User.Client;

namespace UISystem.MainMenu.Old
{
    public class MainMenuUIMultiplayer : MonoBehaviour
    {
        [Header("UI References")]
        [SerializeField] private RectTransform _window;
        
        [Header("Panels")]
        [SerializeField] private MainMenuUI _mainMenuPanel;
        [SerializeField] private MainMenuUILobby _lobbyPanel;
        [SerializeField] private MainMenuUILobbies _lobbiesPanel;
        
        [Header("Buttons")]
        [SerializeField] private Button _createLobbyButton;
        [SerializeField] private Button _browseLobbyButton;
        [SerializeField] private Button _joinLobbyButton;
        [SerializeField] private Button _backButton;
        
        [Inject] private LobbyManager _lobbyManager;
        
        private void OnEnable()
        {
            _createLobbyButton.onClick.AddListener(OnCreateLobbyButtonClicked);
            _browseLobbyButton.onClick.AddListener(OnBrowseLobbyButtonClicked);
            _joinLobbyButton.onClick.AddListener(OnJoinLobbyButtonClicked);
            _backButton.onClick.AddListener(OnBackButtonClicked);
        }
        
        private void OnDisable()
        {
            _createLobbyButton.onClick.RemoveListener(OnCreateLobbyButtonClicked);
            _browseLobbyButton.onClick.RemoveListener(OnBrowseLobbyButtonClicked);
            _joinLobbyButton.onClick.RemoveListener(OnJoinLobbyButtonClicked);
            _backButton.onClick.RemoveListener(OnBackButtonClicked);
        }
        
        private void OnCreateLobbyButtonClicked()
        {
            // Having your own Lobby creation methods add some flexibility, for example adding custom meta data to the lobby
        
            var user = UserAPI.Id;

            string lobbyName = user.Name + "'s lobby";                          // Use a more dynamic lobby name
            _lobbyManager.createArguments.name = lobbyName;                      
            _lobbyManager.createArguments.type = ELobbyType.k_ELobbyTypePublic;  // Example: add a switch to the UI which toggles between public and friends only 

            var scene = new MetadataTempalate
            {
                // Now we add a meta data field for the game scene to be loaded
                // Each meta data entry is using a key/value pair:
                key = "gameplay",
                value = "_Testing" // Example: read the scene from a dropdown box instead
            };

            _lobbyManager.createArguments.metadata.Add(scene);
        
            _lobbyManager.Create();
        }
        
        private void OnBrowseLobbyButtonClicked()
        {
            Disable();
            _lobbiesPanel.Enable();
        }
        
        private void OnJoinLobbyButtonClicked()
        {
            Debug.Log("Join Lobby Button Clicked");
        }
        
        private void OnBackButtonClicked()
        {
            Disable();
            _mainMenuPanel.Enable();
        }

        public void Enable() => _window.gameObject.SetActive(true);
        
        public void Disable() => _window.gameObject.SetActive(false);
    }
}