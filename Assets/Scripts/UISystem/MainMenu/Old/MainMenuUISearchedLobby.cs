﻿using HeathenEngineering.SteamworksIntegration;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UISystem.MainMenu.Old
{
    public class MainMenuUISearchedLobby : MonoBehaviour
    {
        public LobbyData lobbyData { get; private set; }
        
        [Header("Buttons")]
        public Button joinButton;
        
        [Header("UI References")]
        public TMP_Text lobbyNameText;

        [Inject] private LobbyManager _lobbyManager;

        private void OnEnable()
        {
            joinButton.onClick.AddListener(JoinLobby);
        }
        
        private void OnDisable()
        {
            joinButton.onClick.RemoveListener(JoinLobby);
        }

        public void JoinLobby()
        {
            _lobbyManager.Join(lobbyData);
        }
        
        public void SetLobbyData(LobbyData data)
        {
            lobbyData = data;
            UpdateUI();
        }
        
        private void UpdateUI()
        {
            lobbyNameText.text = lobbyData.Name;
        }
    }
}