﻿using HeathenEngineering.SteamworksIntegration;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UISystem.MainMenu.Old
{
    public class MainMenuUILobbies : MonoBehaviour
    {
        [SerializeField] private Transform _lobbyContainer;
        [SerializeField] private MainMenuUISearchedLobby _lobbyPrefab;
        [SerializeField] private GameObject _window;
        [SerializeField] private Button _backButton;
        [SerializeField] private Button _searchButton;
        [SerializeField] private MainMenuUIMultiplayer _multiplayerPanel;

        [Inject] private LobbyManager _lobbyManager;

        private void OnEnable()
        {
            _lobbyManager.evtFound.AddListener(OnLobbiesFound);
            _lobbyManager.evtEnterSuccess.AddListener(OnEnterLobbySuccess);
            _backButton.onClick.AddListener(OnBackButtonClicked);
            _searchButton.onClick.AddListener(SearchLobbies);
        }
        
        private void OnDisable()
        {
            _lobbyManager.evtFound.RemoveListener(OnLobbiesFound);
            _lobbyManager.evtEnterSuccess.RemoveListener(OnEnterLobbySuccess);
            _backButton.onClick.RemoveListener(OnBackButtonClicked);
            _searchButton.onClick.RemoveListener(SearchLobbies);
        }

        public void SearchLobbies()
        {
            _lobbyManager.Search(10);
        }
        
        private void OnLobbiesFound(LobbyData[] lobbies)
        {
            Debug.Log("Lobbies found: " + lobbies.Length);
            
            foreach (Transform t in _lobbyContainer) 
                Destroy(t.gameObject);

            foreach (var lobby in lobbies)
            {
                var lobbyUI = Instantiate(_lobbyPrefab, _lobbyContainer);
                lobbyUI.SetLobbyData(lobby);
            }
        }
        
        private void OnEnterLobbySuccess(LobbyData lobby) => Disable();

        private void OnBackButtonClicked()
        {
            Disable();
            _multiplayerPanel.Enable();
        }

        public void Enable() => _window.SetActive(true);

        public void Disable() => _window.SetActive(false);
    }
}