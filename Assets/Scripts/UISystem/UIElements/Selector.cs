using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UISystem.UIElements
{
    public class Selector : MonoBehaviour
    {
        public event Action<int, string> onChanged;
        
        public int CurrentIndex
        {
            get => _currentIndex;
            set
            {
                if (value == _currentIndex) return;
                
                var newValue = value;
                if (newValue < 0) newValue = Options.Length - 1;
                else if (newValue >= Options.Length) newValue = 0;
                
                _currentIndex = newValue;
                
                UpdateUI();
                
                onChanged?.Invoke(_currentIndex, Options[_currentIndex]);
            }
        }
        
        [field: SerializeField] public string[] Options { get; set; }
        
        [Header("UI Elements")]
        [SerializeField] private Button _leftButton;
        [SerializeField] private Button _rightButton;
        [SerializeField] private TMP_Text _text;
        
        private int _currentIndex;

        private void OnValidate() => UpdateUI();

        private void OnEnable()
        {
            _leftButton.onClick.AddListener(MoveLeft);
            _rightButton.onClick.AddListener(MoveRight);
        }

        private void Start() => UpdateUI();

        private void OnDisable()
        {
            _leftButton.onClick.RemoveListener(MoveLeft);
            _rightButton.onClick.RemoveListener(MoveRight);
        }

        private void MoveLeft() => CurrentIndex--;
        
        private void MoveRight() => CurrentIndex++;
        
        private void UpdateUI() => _text.text = Options[_currentIndex];
    }
}
