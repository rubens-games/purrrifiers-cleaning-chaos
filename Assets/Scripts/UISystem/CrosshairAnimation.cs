using System;
using DG.Tweening;
using InteractionSystem;
using PlayerSystem;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace UISystem
{
    public class CrosshairAnimation : MonoBehaviour
    {
        private InteractionController interactionController
        {
            get => _interactionController;
            set
            {
                if (_interactionController == value) return;
                
                if (_interactionController) 
                    _interactionController.OnCurrentInteractableChange.RemoveListener(OnCurrentInteractableChange);

                _interactionController = value;

                if (_interactionController) 
                    _interactionController.OnCurrentInteractableChange.AddListener(OnCurrentInteractableChange);
            }
        }

        [SerializeField] private Transform _frame;
        [SerializeField] private Transform _dot;

        [Header("Selected Animation Settings")] 
        [SerializeField] private Vector3 _selectedRootScale;
        [SerializeField] private Vector3 _selectedDotScale;
        [SerializeField] private float _selectedAnimationDuration = 0.2f;
        [SerializeField] private Ease _selectedEase = Ease.OutBack;
        
        [Header("Deselected Animation Settings")]
        [SerializeField] private Vector3 _deselectedRootScale;
        [SerializeField] private Vector3 _deselectedDotScale;
        [SerializeField] private float _deselectedAnimationDuration = 0.2f;
        [SerializeField] private Ease _deselectedEase = Ease.OutBack;
        
        [Inject] private PlayerManager _playerManager;
        
        private InteractionController _interactionController;

        private void Awake()
        {
            _playerManager.onLocalPlayerChange.AddListener(OnLocalPlayerChange);
        }

        private void OnDestroy()
        {
            _playerManager.onLocalPlayerChange.RemoveListener(OnLocalPlayerChange);
        }
        
        private void OnLocalPlayerChange(Player player)
        {
            if (player == null) return;

            interactionController = player.interactionController;
        }
        
        private void OnCurrentInteractableChange(Interactable interactable)
        {
            var canCrosshairAnimate = interactable && interactable.CanInteract && !interactable.IsSleeping &&
                                      interactionController && interactionController.CanInteract;

            if (canCrosshairAnimate) ToSelectedAnimation();
            else ToDeselectedAnimation();
        }
        
        private void ToSelectedAnimation()
        {
            DOTween.Kill("Deselected");
            
            DOTween.Sequence().SetId("Selected")
                .Append(_frame.DOScale(_selectedRootScale, _selectedAnimationDuration).SetEase(_selectedEase).SetUpdate(true))
                .Append(_dot.DOScale(_selectedDotScale, _selectedAnimationDuration).SetEase(_selectedEase).SetUpdate(true));
        }
        
        private void ToDeselectedAnimation()
        {
            DOTween.Kill("Selected");
            
            DOTween.Sequence().SetId("Deselected").SetEase(Ease.OutBack).SetUpdate(true)
                .Append(_dot.DOScale(_deselectedDotScale, _deselectedAnimationDuration).SetEase(_deselectedEase).SetUpdate(true))
                .Append(_frame.DOScale(_deselectedRootScale, _deselectedAnimationDuration).SetEase(_deselectedEase).SetUpdate(true));
        }
    }
}
