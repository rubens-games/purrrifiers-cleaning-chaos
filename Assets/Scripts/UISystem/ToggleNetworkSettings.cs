using FishNet.Managing;
using FishNet.Managing.Transporting;
using FishNet.Transporting.Tugboat;
using Steamworks;
using UnityEngine;
using UnityEngine.UI;

namespace UISystem
{
    public class ToggleNetworkSettings : MonoBehaviour
    {
        [SerializeField] private Color _enabledColor;
        [SerializeField] private Color _disabledColor;
        [SerializeField] private Button _useTugboatButton;
        [SerializeField] private Image _useTugboatButtonIndicator;
        [SerializeField] private Button _useFishySteamworksButton;
        [SerializeField] private Image _useFishySteamworksButtonIndicator;

        [SerializeField] private TransportManager _transportManager;
        [SerializeField] private Tugboat _tugboat;
        [SerializeField] private FishySteamworks.FishySteamworks _fishySteamworks;

        private void OnEnable()
        {
            _useTugboatButton.onClick.AddListener(UseTugboat);
            _useFishySteamworksButton.onClick.AddListener(UseFishySteamworks);
        }
    
        private void OnDisable()
        {
            _useTugboatButton.onClick.RemoveListener(UseTugboat);
            _useFishySteamworksButton.onClick.RemoveListener(UseFishySteamworks);
        }
    
        private void UseTugboat()
        {
            _fishySteamworks.enabled = false;
        
            // _transportManager.enabled = false;
            _transportManager.Transport = _tugboat;
            // _transportManager.enabled = true;
        
            _useTugboatButtonIndicator.color = _enabledColor;
            _useFishySteamworksButtonIndicator.color = _disabledColor;
        }
    
        private void UseFishySteamworks()
        {
            // _transportManager.enabled = false;
            // _fishySteamworks.enabled = false;
            
            _transportManager.Transport = _fishySteamworks;

            var mySteamID = SteamUser.GetSteamID().m_SteamID;
            _fishySteamworks.LocalUserSteamID = mySteamID;
            
            _fishySteamworks.enabled = true;
            // _transportManager.enabled = true;
            
            _useTugboatButtonIndicator.color = _disabledColor;
            _useFishySteamworksButtonIndicator.color = _enabledColor;
        }
    }
}
