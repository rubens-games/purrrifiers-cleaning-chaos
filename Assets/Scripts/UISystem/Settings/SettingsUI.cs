using System;
using IntuitiveBackSystem.Runtime;
using UISystem.UIElements;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UISystem.Settings
{
    public class SettingsUI : MonoBehaviour, IIntuitiveBackHandler
    {
        public Selector resolutionSelector;
        public Selector qualitySelector;
        public Slider volumeSlider;
        public Toggle fullScreenToggle;

        private float currentVolume = 1.0f;
        private Resolution[] resolutions;
        
        [Inject]

        private void OnEnable()
        {
            resolutions = Screen.resolutions;
            resolutionSelector.Options = new string[resolutions.Length];

            var currentResolutionIndex = 0;
            for (var i = 0; i < resolutions.Length; i++)
            {
                var resolutionOption = resolutions[i].width + " x " + resolutions[i].height;
                resolutionSelector.Options[i] = resolutionOption;

                if (resolutions[i].width == Screen.currentResolution.width &&
                    resolutions[i].height == Screen.currentResolution.height) 
                    currentResolutionIndex = i;
            }

            resolutionSelector.CurrentIndex = currentResolutionIndex;
            resolutionSelector.onChanged += SetResolution;

            qualitySelector.Options = new string[QualitySettings.names.Length];
            var qualityLevels = QualitySettings.names;
            for (var i = 0; i < qualityLevels.Length; i++)
                qualitySelector.Options[i] = qualityLevels[i];

            qualitySelector.CurrentIndex = QualitySettings.GetQualityLevel();
            qualitySelector.onChanged += SetQuality;

            // Set up volume slider
            volumeSlider.value = AudioListener.volume;
            volumeSlider.onValueChanged.AddListener(SetVolume);
            
            // Set up full screen toggle
            fullScreenToggle.isOn = Screen.fullScreen;
            fullScreenToggle.onValueChanged.AddListener(SetFullScreen);
            
            // Register to back system
            IntuitiveBackManager.instance.Register(this);
        }

        private void OnDisable()
        {
            resolutionSelector.onChanged -= SetResolution;
            qualitySelector.onChanged -= SetQuality;
            volumeSlider.onValueChanged.RemoveListener(SetVolume);
            fullScreenToggle.onValueChanged.RemoveListener(SetFullScreen);
            
            // Unregister from back system
            IntuitiveBackManager.instance.Unregister(this);
        }

        public void SetResolution(int resolutionIndex, string resolutionName)
        {
            var selectedResolution = resolutions[resolutionIndex];
            Screen.SetResolution(selectedResolution.width, selectedResolution.height, Screen.fullScreen);
        }

        public void SetQuality(int qualityIndex, string qualityName) => 
            QualitySettings.SetQualityLevel(qualityIndex);

        public void SetVolume(float volume)
        {
            currentVolume = volume;
            AudioListener.volume = currentVolume;
        }
        
        public void SetFullScreen(bool isFullScreen) => Screen.fullScreen = isFullScreen;

        #region IIntuitiveBackHandler

        public string toolTip => "Back";
        
        public void OnBack()
        {
            gameObject.SetActive(false);
            
            // Unregister from back system
            IntuitiveBackManager.instance.Unregister(this);
        }

        #endregion
    }
}
