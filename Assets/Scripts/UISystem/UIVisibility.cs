﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UISystem
{
    public class UIVisibility : MonoBehaviour, IUIVisibilityHandler
    {
        [SerializeField] private List<CanvasGroup> _canvasGroups;
        
        public event Action<IUIVisibilityHandler> onRegister;
        public event Action<IUIVisibilityHandler> onUnregister;

        public IUIVisibilityHandler currentHandler => handlers.Count > 0 ? handlers[0] : this;
        public List<IUIVisibilityHandler> handlers { get; } = new();
        public bool isVisible => _isVisible;

        [SerializeField] private bool _isVisible = true;

        private void Awake() => Register(this);

        public void OnDestroy()
        {
            Unregister(this);
            UnregisterAll();
        }

        public void Register(IUIVisibilityHandler handler)
        {
            // Add the mouse handler to be the first in the list
            handlers.Insert(0, handler);
            
            // Set the mouse settings
            ActiveHandler(handler);
            
            onRegister?.Invoke(handler);
        }
        
        public void Unregister(IUIVisibilityHandler handler)
        {
            handlers.Remove(handler);
            
            ActiveHandler(currentHandler);
            
            onUnregister?.Invoke(handler);
        }
        
        private void UnregisterAll()
        {
            for (var i = 0; i < handlers.Count; i++) 
                Unregister(handlers[i]);
        }
        
        private void ActiveHandler(IUIVisibilityHandler handler)
        {
            if (handler == null) return;
            
            // If the handler is not the Currenthandler, set the handler to be the first in the list
            if (currentHandler != handler)
            {
                handlers.Remove(handler);
                handlers.Insert(0, handler);
            }
            
            SetSettings(handler);
        }
        
        private void SetSettings(IUIVisibilityHandler cursorSettings)
        {
            foreach (var canvasGroup in _canvasGroups) 
                canvasGroup.alpha = cursorSettings.isVisible ? 1.0f : 0.0f;
        }
    }
    
    public interface IUIVisibilityHandler
    {
        public bool isVisible { get; }
    }
}