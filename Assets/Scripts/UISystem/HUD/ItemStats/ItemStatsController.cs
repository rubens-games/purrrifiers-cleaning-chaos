using System.Collections.Generic;
using System.Linq;
using PickableSystem;
using UnityEngine;
using Zenject;

namespace UISystem.HUD.ItemStats
{
    [RequireComponent(typeof(Pickable))]
    public class ItemStatsController : MonoBehaviour
    {
        public string itemName;
        [TextArea] public string itemDescription;
        public Sprite itemIcon;
        
        public virtual List<ItemStat> itemStats => _itemStats;

        [Inject] private ItemStatsUI _itemStatsUI;
        
        private List<ItemStat> _itemStats = new();
        private Pickable _pickable;

        private void Awake()
        {
            _pickable = GetComponent<Pickable>();
            _itemStats = GetComponents<ItemStat>().ToList();
        }

        private void OnEnable()
        {
            _pickable.OnPickup.AddListener(OnPickup);
            _pickable.OnDrop.AddListener(OnDrop);
        }
        
        private void OnDisable()
        {
            _pickable.OnPickup.RemoveListener(OnPickup);
            _pickable.OnDrop.RemoveListener(OnDrop);
        }
        
        private void OnPickup(PickUpController pickUpController)
        {
            if (!pickUpController.IsOwner) return;
            
            Register();
        }

        private void OnDrop(PickUpController pickUpController)
        {
            if (!pickUpController.IsOwner) return;
            
            Unregister();
        }

        public void Register() => _itemStatsUI.RegisterController(this);

        public void Unregister() => _itemStatsUI.UnregisterController();
    }
}