﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UISystem.HUD.ItemStats
{
    public class ItemStatsUIItem : MonoBehaviour
    {
        [Header("UI Elements")]
        [SerializeField] private TMP_Text itemName;
        [SerializeField] private TMP_Text itemDescription;
        [SerializeField] private Image itemIcon;
        
        public void UpdateItem(string newItemName, string newItemDescription, Sprite newItemIcon)
        {
            itemName.text = newItemName;
            itemDescription.text = newItemDescription;
            itemIcon.sprite = newItemIcon;
        }
    }
}