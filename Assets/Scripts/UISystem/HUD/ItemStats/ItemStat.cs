﻿using System;
using FishNet.Object;
using UnityEngine;

namespace UISystem.HUD.ItemStats
{
    public class ItemStat : NetworkBehaviour
    {
        public event Action<ItemStat> onStatChange;

        public virtual string statName { get; }
        public virtual string statDescription { get; }
        public virtual Sprite statIcon { get; }
        public virtual float minValue { get; }
        public virtual float value { get; }
        public virtual float maxValue { get; }
        public virtual string statValueString { get; }

        protected virtual void InvokeOnStatChange(ItemStat obj) => onStatChange?.Invoke(obj);

        private void OnDestroy() => onStatChange = null;
    }
}