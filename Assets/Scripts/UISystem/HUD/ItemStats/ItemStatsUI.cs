using System;
using System.Collections.Generic;
using RebuildUISystem;
using UnityEngine;

namespace UISystem.HUD.ItemStats
{
    public class ItemStatsUI : MonoBehaviour
    {
        public ItemStatsController currentController;
        
        public RectTransform canvasRect;
        public ItemStatsUIItem item;
        public RectTransform statsContent;
        public ItemStatsUIItemStatPrefab itemStatPrefab;

        private void Start() => gameObject.SetActive(false);

        public void RegisterController(ItemStatsController  controller)
        {
            currentController = controller;
            
            UpdateItem(currentController.itemName, currentController.itemDescription, currentController.itemIcon);
            
            UpdateStats(currentController.itemStats);

            gameObject.SetActive(true);
            
            RebuildUIManager.instance.Register(canvasRect);
        }
        
        public void UnregisterController()
        {
            currentController = null;
            
            gameObject.SetActive(false);
            
            RebuildUIManager.instance.Unregister(canvasRect);
        }
        
        private void UpdateItem(string newItemName, string newItemDescription, Sprite newItemIcon) => 
            item.UpdateItem(newItemName, newItemDescription, newItemIcon);

        private void UpdateStats(List<ItemStat> newStats)
        {
            foreach (Transform child in statsContent) 
                Destroy(child.gameObject);
            
            var prevEnabled = itemStatPrefab.enabled;
            itemStatPrefab.enabled = false;
            
            foreach (var stat in newStats)
            {
                var statPrefabClone = Instantiate(itemStatPrefab, statsContent);
                statPrefabClone.SetStat(stat);

                statPrefabClone.enabled = prevEnabled;
            }
            
            itemStatPrefab.enabled = prevEnabled;
        }
    }
}
