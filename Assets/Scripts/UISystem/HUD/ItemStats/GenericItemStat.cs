﻿using UnityEngine;

namespace UISystem.HUD.ItemStats
{
    public class GenericItemStat : ItemStat
    {
        public override string statName => _statName;
        public override string statDescription => _statDescription;
        public override float minValue => _minValue;
        public override float value => _value;
        public override float maxValue => _maxValue;
        public override string statValueString => _statValueString;

        [SerializeField] private string _statName;
        [SerializeField] private string _statDescription;
        [SerializeField] private float _minValue;
        [SerializeField] private float _value;
        [SerializeField] private float _maxValue;
        [SerializeField] private string _statValueString;
    }
}