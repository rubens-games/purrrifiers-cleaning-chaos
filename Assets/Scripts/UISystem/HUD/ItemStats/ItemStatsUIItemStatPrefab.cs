﻿using System;
using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UISystem.HUD.ItemStats
{
    public class ItemStatsUIItemStatPrefab : MonoBehaviour
    {
        public ItemStat itemStat;
        
        [Header("UI Elements")]
        [SerializeField] private TMP_Text statName;
        [SerializeField] private Slider statSlider;
        [SerializeField] private Transform statSliderTransform;
        [SerializeField] private TMP_Text statSliderString;
        [SerializeField] private Image statIcon;
        [SerializeField] private Image _statSliderFillImage;
        [SerializeField] private Image _shineImage;
        
        [Header("Color Settings")] 
        [SerializeField] private Color _sliderFullColor = Color.cyan;
        [SerializeField] private Color _sliderEmptyColor = Color.red;

        private static readonly int _canShine = Shader.PropertyToID("_CanShine");
        private static readonly int _cycleTime = Shader.PropertyToID("_CycleTime");

        private bool _isScaling;
        
        public void SetStat(ItemStat stat)
        {
            if (itemStat) itemStat.onStatChange -= OnStatChange;
            
            itemStat = stat;

            if (!itemStat) return;
            
            itemStat.onStatChange += OnStatChange;
        }

        private void Start()
        {
            if (itemStat) UpdateStat(itemStat);
        }

        private void OnDestroy()
        {
            if (itemStat) itemStat.onStatChange -= OnStatChange;
            
            DOTween.Kill(statSlider);
            DOTween.Kill(_statSliderFillImage);
            DOTween.Kill(statSliderTransform);
        }

        private void OnStatChange(ItemStat stat) => UpdateStat(stat);

        private void UpdateStat(ItemStat stat)
        {
            var a = stat.minValue;
            var b = stat.maxValue;
            var c = stat.value;
            var percentage = (c - a) / (b - a);
            
            statName.text = stat.statName;
            UpdateSlider(percentage);
            statSliderString.text = stat.statValueString;
            
            statIcon.sprite = stat.statIcon;
            
            var canShine = Mathf.Approximately(stat.value, stat.maxValue);
            if (canShine) StartCoroutine(PlayShineEffect());
        }
        
        private void UpdateSlider(float percentage)
        {
            statSlider.DOValue(percentage, 0.2f);
            
            var newColor = Color.Lerp(_sliderEmptyColor, _sliderFullColor, percentage);
            _statSliderFillImage.DOColor(newColor, 0.2f);
            
            // PopUp Animation
            if (_isScaling) return;
            _isScaling = true;
            statSliderTransform.DOPunchScale(Vector3.one * 0.1f, 0.2f).SetUpdate(true).onComplete +=
                () => statSliderTransform.DOScale(Vector3.one, 0.1f).SetUpdate(true).onComplete +=
                    () => _isScaling = false;
        }

        private IEnumerator PlayShineEffect()
        {
            var shineMaterial = _shineImage.material;
            shineMaterial.SetInt(_canShine, 1);
            var delay = shineMaterial.GetFloat(_cycleTime);
            yield return new WaitForSeconds(delay);
            shineMaterial.SetInt(_canShine, 0);
        }
    }
}