﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PixelCrushers;
using PixelCrushers.QuestMachine;
using UnityEngine;

namespace UISystem.HUD.MyStats
{
    public class MyStatsUIFollowedTask : MonoBehaviour, IMessageHandler
    {
        public QuestNode followedQuestNode
        {
            get => _followedQuestNode;
            internal set
            {
                if (_followedQuestNode == value) return;
                
                _followedQuestNode = value;
                
                var localPlayer = _myStatsUI.playerManager.localPlayer;
                
                if (!localPlayer) return;
                
                var playerFollowingQuestNode = localPlayer.playerFollowingQuestNode;
                
                if (!playerFollowingQuestNode) return;
                
                if (_followedQuestNode == null) return;
                
                playerFollowingQuestNode.questNodeId = StringField.GetStringValue(_followedQuestNode.id);
            
                _taskPrefab.questNode = _followedQuestNode;
            }
        }

        [SerializeField] private MyStatsUI _myStatsUI;
        [SerializeField] private MyStatsUITasksList _tasksList;
        [SerializeField] private MyStatsUITaskPrefab _taskPrefab;
        
        private static QuestNode _followedQuestNode;

        private void OnEnable()
        {
            MessageSystem.AddListener(this, QuestMachineMessages.QuestStateChangedMessage, string.Empty);
            MessageSystem.AddListener(this, QuestMachineMessages.QuestCounterChangedMessage, string.Empty);
            MessageSystem.AddListener(this, QuestMachineMessages.RefreshUIsMessage, string.Empty);
            MessageSystem.AddListener(this, QuestMachineMessages.QuestTrackToggleChangedMessage, string.Empty);
        }

        private void OnDisable()
        {
            MessageSystem.RemoveListener(this);
        }

        private void Start() => _taskPrefab.questNode = followedQuestNode;

        public void OnMessage(MessageArgs messageArgs)
        {
            _taskPrefab.UpdateText();
            StartCoroutine(ValidateFollowedQuestNode());
        }

        public IEnumerator ValidateFollowedQuestNode()
        {
            yield return new WaitForEndOfFrame();
            
            if (IsQuestNodeAvailableToDo(followedQuestNode)) yield break;
                
            followedQuestNode = GetFirstAvailableToDoQuestNode(_tasksList.questNodesInUI);

            if (followedQuestNode != null)
                Debug.Log($"Followed quest node is not available to do. New followed quest node: {followedQuestNode.id}");
        }

        private bool IsQuestNodeAvailableToDo(QuestNode questNode) =>
            questNode != null && _tasksList.questNodesInUI.Contains(questNode) &&
            questNode.GetState() == QuestNodeState.Active;

        private static bool IsQuestNodeAvailableToDisplay(QuestNode questNode)
        {
            var journalContents = questNode?.GetContentList(QuestContentCategory.HUD);
            return journalContents is { Count: > 0 };
        }

        private QuestNode GetFirstAvailableToDoQuestNode(List<QuestNode> questNodes)
        {
            if (questNodes == null || questNodes.Count == 0) return null;

            return questNodes.Where(IsQuestNodeAvailableToDo).FirstOrDefault(IsQuestNodeAvailableToDisplay);
        }
    }
}