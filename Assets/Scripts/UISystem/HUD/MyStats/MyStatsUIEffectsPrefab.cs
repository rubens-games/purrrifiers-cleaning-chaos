﻿using System;
using DG.Tweening;
using NoReleaseDate.TickerSystem.Runtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UISystem.HUD.MyStats
{
    public class MyStatsUIEffectsPrefab : MonoBehaviour
    {
        public class EffectData
        {
            public event Action onDispose;
            
            public string title;
            public Sprite sprite;
            public DateTime startTime;
            public DateTime endTime;
            
            public void Dispose()
            {
                onDispose?.Invoke();
                
                title = null;
                sprite = null;
                startTime = DateTime.MinValue;
                endTime = DateTime.MinValue;
            }
        }

        public EffectData effectData { get; set; }

        [SerializeField] private TMP_Text _title;
        [SerializeField] private Image _icon;
        [SerializeField] private Slider _slider;
        [SerializeField] private TMP_Text _timeLeft;

        private void Start()
        {
            if (effectData == null)
            {
                Destroy(gameObject);
                return;
            }
            
            effectData.onDispose += Dispose;
            Ticker.instance.Register(UpdateUI, 1);
            
            UpdateUI();
        }

        private void OnDestroy()
        {
            if (effectData == null) return;
            
            effectData.onDispose -= Dispose;
            Ticker.instance.Unregister(UpdateUI);
        }
        
        private void Dispose() => Destroy(gameObject);

        private void UpdateUI(float time) => UpdateUI();

        private void UpdateUI()
        {
            _title.text = effectData.title;
            if (effectData.sprite) _icon.sprite = effectData.sprite;
            UpdateSlider();
            UpdateTime();
        }
        
        private void UpdateSlider()
        {
            // Calculate the time left
            // Slider display the time left. 0 is the end of the effect, 1 is the start of the effect
            
            var timeLeft = effectData.endTime - DateTime.Now;
            var totalTime = effectData.endTime - effectData.startTime;
            var timeLeftNormalized = timeLeft / totalTime;
            _slider.DOValue((float)timeLeftNormalized, 0.5f);
        }
        
        private void UpdateTime()
        {
            // Calculate the time left
            // Display the time left
            
            var timeLeft = effectData.endTime - DateTime.Now;
            _timeLeft.text = timeLeft.ToString(timeLeft.TotalHours >= 1 ? @"h\:mm\:ss" : @"m\:ss");
        }
    }
}