﻿using UnityEngine;

namespace UISystem.HUD.MyStats
{
    public class MyStatsUIEffects : MonoBehaviour
    {
        [SerializeField] private Transform _container;
        [SerializeField] private MyStatsUIEffectsPrefab _effectPrefab;
        
        public void SpawnEffect(MyStatsUIEffectsPrefab.EffectData effectData)
        {
            var prevEnabled = _effectPrefab.gameObject.activeSelf;
            _effectPrefab.gameObject.SetActive(false);
            var effectClone = Instantiate(_effectPrefab, _container);
            effectClone.effectData = effectData;
            effectClone.gameObject.SetActive(true);
            _effectPrefab.gameObject.SetActive(prevEnabled);
        }
    }
}