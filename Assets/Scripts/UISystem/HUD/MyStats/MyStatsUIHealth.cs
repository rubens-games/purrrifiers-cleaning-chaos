﻿using DG.Tweening;
using PlayerSystem;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UISystem.HUD.MyStats
{
    public class MyStatsUIHealth : MonoBehaviour
    {
        public Player currentPlayer
        {
            get => _currentPlayer;
            set
            {
                if (_currentPlayer == value) return;
                
                var previousPlayer = _currentPlayer;
                
                _currentPlayer = value;
                
                OnCurrentPlayerChanged(previousPlayer, _currentPlayer);
            }
        }
        
        [Header("UI Components")]
        [SerializeField] private Slider _healthSlider;
        [SerializeField] private Image _healthFillImage;


        [Header("Color Settings")] 
        [SerializeField] private Color _highHealthColor = Color.cyan;
        [SerializeField] private Color _lowHealthColor = Color.red;

        [Inject] private PlayerManager _playerManager;

        private Player _currentPlayer;
        
        private void OnEnable()
        {
            _playerManager.onLocalPlayerChange.AddListener(OnLocalPlayerChange);
        }
        
        private void OnDisable()
        {
            _playerManager.onLocalPlayerChange.RemoveListener(OnLocalPlayerChange);
        }
        
        private void OnLocalPlayerChange(Player player) => currentPlayer = player;
        
        private void OnCurrentPlayerChanged(Player previousPlayer, Player nextPlayer)
        {
            if (previousPlayer) 
                previousPlayer.playerHitable.OnHitpointsChange.RemoveListener(OnHitpointsChange);

            if (!nextPlayer) return;
            
            nextPlayer.playerHitable.OnHitpointsChange.AddListener(OnHitpointsChange);
            
            OnHitpointsChange(nextPlayer.playerHitable.hitpoints);
        }
        
        private void OnHitpointsChange(int currentHitpoints)
        {
            var maxHitpoints = _currentPlayer.playerHitable.maxHitpoints;
            var hitpointsPercentage = (float)currentHitpoints / maxHitpoints;
            
            UpdateHealthSlider(hitpointsPercentage);
        }
        
        private void UpdateHealthSlider(float hitpointsPercentage)
        {
            _healthSlider.DOValue(hitpointsPercentage, 0.2f);
            
            var newColor = Color.Lerp(_lowHealthColor, _highHealthColor, hitpointsPercentage);
            _healthFillImage.DOColor(newColor, 0.2f);
            
            // PopUp Animation
            transform.DOPunchScale(Vector3.one * 0.1f, 0.2f).SetUpdate(true).onComplete +=
                () => transform.DOScale(Vector3.one, 0.1f).SetUpdate(true);
        }
    }
}