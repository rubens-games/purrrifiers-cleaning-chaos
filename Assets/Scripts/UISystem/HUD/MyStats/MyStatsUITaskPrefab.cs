﻿using System.Collections.Generic;
using System.Linq;
using HeathenEngineering.SteamworksIntegration;
using PixelCrushers;
using PixelCrushers.QuestMachine;
using PlayerSystem;
using RebuildUISystem;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UISystem.HUD.MyStats
{
    public class MyStatsUITaskPrefab : MonoBehaviour
    {
        public QuestNode questNode
        {
            get => _questNode;
            set
            {
                _questNode = value;

                if (_questNode == null)
                {
                    UpdateVisibility();
                    return;
                }

                UpdateText();
                
                UpdateFollowingPlayers();

                UpdateIndicator();
                
                UpdateVisibility();
            }
        }
        
        public HeadingTextQuestContent headingTextQuestContent
        {
            get => _headingTextQuestContent;
            set
            {
                _headingTextQuestContent = value;
                _headingText.text = _headingTextQuestContent.runtimeText;
            }
        }
        
        public BodyTextQuestContent bodyTextQuestContent
        {
            get => _bodyTextQuestContent;
            set
            {
                _bodyTextQuestContent = value;
                _bodyText.text = _bodyTextQuestContent.runtimeText;
            }
        }

        public bool isFollowed => _myStatsUI.followedTask.followedQuestNode == _questNode;
        
        [Header("References")] 
        [SerializeField] private GameObject _indicator;
        [SerializeField] private MyStatsUIFollowingPlayerAvatarPrefab _followingPlayerAvatarPrefab;
        [SerializeField] private RectTransform _followingPlayersContent;
        [SerializeField] private RectTransform _rectTransform;
        [SerializeField] private Button _selectTaskButton;
        [SerializeField] private TMP_Text _headingText;
        [SerializeField] private TMP_Text _bodyText;
        [SerializeField] private Image _iconImage;
        [SerializeField] private Button _button;
        [SerializeField] private AudioSource _audioSource;

        [Header("Runtime")]
        [SerializeField] private MyStatsUI _myStatsUI;
        
        private QuestNode _questNode;
        private HeadingTextQuestContent _headingTextQuestContent;
        private BodyTextQuestContent _bodyTextQuestContent;
        private IconQuestContent _iconQuestContent;
        private ButtonQuestContent _buttonQuestContent;
        private AudioClipQuestContent _audioClipQuestContent;

        private void OnEnable()
        {
            _selectTaskButton.onClick.AddListener(OnSelectTaskButtonClicked);
            
            PlayerFollowingQuestNode.evtQuestNodeChanged += OnPlayerChangeFollowingQuestNode;
            
            UpdateFollowingPlayers();
            UpdateIndicator();
        }

        private void OnDisable()
        {
            _selectTaskButton.onClick.RemoveListener(OnSelectTaskButtonClicked);
            
            PlayerFollowingQuestNode.evtQuestNodeChanged -= OnPlayerChangeFollowingQuestNode;
        }

        public void SetData(MyStatsUI pMyStatsUI, QuestNode pQuestNode)
        {
            _myStatsUI = pMyStatsUI;
            questNode = pQuestNode;
        }
        
        private void OnSelectTaskButtonClicked()
        {
            if (_questNode == null) return;
            
            _myStatsUI.followedTask.followedQuestNode = _questNode;
            
            // foreach (var taskPrefab in _myStatsUI.tasksList.taskPrefabsInUI) 
            //     taskPrefab.UpdateFollowingPlayers();
        }
        
        private void OnPlayerChangeFollowingQuestNode(Player player, string oldQuestNodeId, string newQuestNodeId)
        {
            UpdateIndicator();
            UpdateFollowingPlayers();
        }

        public void UpdateText()
        {
            if (_questNode == null) return;
            
            var contents = _questNode.GetContentList(QuestContentCategory.HUD);

            foreach (var content in contents.Where(content => content))
                switch (content)
                {
                    case HeadingTextQuestContent headingContent:
                        headingTextQuestContent = headingContent;
                        break;
                    case BodyTextQuestContent bodyContent:
                        bodyTextQuestContent = bodyContent;
                        break;
                    case ButtonQuestContent:
                        break;
                    case IconQuestContent:
                        break;
                    case AudioClipQuestContent:
                        break;
                }
        }
        
        private void UpdateVisibility()
        {
            if (_questNode == null)
            {
                gameObject.SetActive(false);
                return;
            }

            gameObject.SetActive(true);

            UpdateVisibility(_headingTextQuestContent, _headingText.gameObject);
            UpdateVisibility(_bodyTextQuestContent, _bodyText.gameObject);
            UpdateVisibility(_iconQuestContent, _iconImage.gameObject);
            UpdateVisibility(_buttonQuestContent, _button.gameObject);
            UpdateVisibility(_audioClipQuestContent, _audioSource.gameObject);

            if (!_headingText.gameObject.activeSelf && !_bodyText.gameObject.activeSelf &&
                !_iconImage.gameObject.activeSelf && !_button.gameObject.activeSelf &&
                !_audioSource.gameObject.activeSelf)
                gameObject.SetActive(false);
            
            RebuildUIManager.instance.Register(_rectTransform.parent.GetComponent<RectTransform>());
        }
        
        private static void UpdateVisibility(QuestContent content, GameObject go) => 
            go.SetActive(content);

        private void UpdateFollowingPlayers()
        {
            var followingPlayers = new List<UserData>();
            
            foreach (var player in _myStatsUI.playerManager.players)
            {
                var playerFollowingQuestNodeId = player.playerFollowingQuestNode.questNodeId;
                if (player.IsOwner && _myStatsUI.followedTask.followedQuestNode is not null) 
                    playerFollowingQuestNodeId = _myStatsUI.followedTask.followedQuestNode.id.value;
                
                if (playerFollowingQuestNodeId == null) continue;
                
                if (playerFollowingQuestNodeId == StringField.GetStringValue(_questNode.id))
                    followingPlayers.Add(player.playerSteamData.userData);
            }
            
            UpdateFollowingPlayers(followingPlayers);
        }

        private void UpdateIndicator() => _indicator.SetActive(isFollowed);

        private void UpdateFollowingPlayers(List<UserData> userDataList)
        {
            foreach (Transform child in _followingPlayersContent) 
                Destroy(child.gameObject);
        
            foreach (var userData in userDataList)
            {
                var userAvatar = Instantiate(_followingPlayerAvatarPrefab, _followingPlayersContent);
                userAvatar.setUserAvatar.LoadAvatar(userData);
            }
        
            _followingPlayersContent.gameObject.SetActive(userDataList.Count != 0);
        
            RebuildUIManager.instance.Register(_rectTransform.parent.GetComponent<RectTransform>());
        }
    }
}