﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CursorSystem;
using PixelCrushers;
using PixelCrushers.QuestMachine;
using PlayerSystem;
using QuestSystem;
using RebuildUISystem;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Zenject;

namespace UISystem.HUD.MyStats
{
    public sealed class MyStatsUITasksList : MonoBehaviour, IMessageHandler, ICursorHandler, IPlayerLockHandler
    {
        public List<MyStatsUITaskPrefab> taskPrefabsInUI => _tasksListContent.GetComponentsInChildren<MyStatsUITaskPrefab>(true).ToList();
        public List<MyStatsUITaskPrefab> activeTaskPrefabsInUI => taskPrefabsInUI.FindAll(task => task.gameObject.activeSelf);
        public List<QuestNode> questNodesInUI => taskPrefabsInUI.ConvertAll(task => task.questNode);
        
        public CursorSettings CursorSettings => CursorSettings.ShowAndConfined;

        public bool isLocked => true;
        
        [Header("References")]
        [SerializeField] private MyStatsUI _myStatsUI;
        [SerializeField] private InputActionReference _toggleListAction;
        [SerializeField] private MyStatsUITaskPrefab _taskPrefab;
        [SerializeField] private RectTransform _tasksListContent;
        [SerializeField] private TMP_Text _noTasksText;
        [SerializeField] private TMP_Text _openCloseText;
        [SerializeField] private Image _openCloseImage;

        [Header("Settings")]
        [SerializeField] private string _openText;
        [SerializeField] private string _closeText;
        [SerializeField] private Sprite _openSprite;
        [SerializeField] private Sprite _closeSprite;
        
        [Inject] private QuestJournalNetworked _questJournal;
        [Inject] private CursorManager _cursorManager;
        [Inject] private PlayerManager _playerManager;
        [Inject] private Camera _camera;

        private bool isActivated => gameObject.activeSelf;
        
        private bool _isRefreshing;
        
        private void Awake()
        {
            _toggleListAction.action.performed += OnToggleListActionPerformed;
            _toggleListAction.action.Enable();
        }

        private void OnEnable()
        {
            UpdateNoTasksText();
            UpdateOpenCloseButton();
        }

        private void Start()
        {
            MessageSystem.AddListener(this, QuestMachineMessages.QuestStateChangedMessage, string.Empty);
            MessageSystem.AddListener(this, QuestMachineMessages.QuestCounterChangedMessage, string.Empty);
            MessageSystem.AddListener(this, QuestMachineMessages.RefreshUIsMessage, string.Empty);
            MessageSystem.AddListener(this, QuestMachineMessages.QuestTrackToggleChangedMessage, string.Empty);

            gameObject.SetActive(false);
        }

        private void OnDisable()
        {
            UpdateNoTasksText();
            UpdateOpenCloseButton();
        }

        private void OnDestroy()
        {
            MessageSystem.RemoveListener(this);
            
            _toggleListAction.action.performed -= OnToggleListActionPerformed;
            _toggleListAction.action.Disable();
        }

        public void OnMessage(MessageArgs messageArgs)
        {
            if (_isRefreshing) return;
            if (_questJournal && _questJournal.isActiveAndEnabled) _questJournal.StartCoroutine(Refresh());
        }

        private void OnToggleListActionPerformed(InputAction.CallbackContext context)
        {
            if (isActivated) Deactivate();
            else Activate();
        }

        private IEnumerator Refresh()
        {
            if (_isRefreshing) yield break;
            
            _isRefreshing = true;
            
            yield return new WaitForEndOfFrame();
            
            foreach (Transform child in _tasksListContent) Destroy(child.gameObject);
            
            var quests = new List<Quest>(_questJournal.questList);
            var questWithGroup = quests.FindAll(quest => quest && !StringField.IsNullOrEmpty(quest.group));
            var questWithoutGroup = quests.FindAll(quest => quest && StringField.IsNullOrEmpty(quest.group));

            questWithGroup.Sort((x, y) =>
                string.CompareOrdinal(StringField.GetStringValue(x.group), StringField.GetStringValue(y.group)));
            
            var questsOrdered = new List<Quest>();
            questsOrdered.AddRange(questWithGroup);
            questsOrdered.AddRange(questWithoutGroup);

            foreach (var quest in questsOrdered)
            {
                if (!quest) continue;
                if (!quest.showInTrackHUD || quest.GetState() != QuestState.Active) continue;
                
                foreach (var questNode in quest.nodeList)
                {
                    if (questNode == null) continue;
                    if (questNode.nodeType != QuestNodeType.Condition) continue;
                    if (questNode.GetState() != QuestNodeState.Active) continue;

                    _taskPrefab.enabled = false;
                    var taskClone = Instantiate(_taskPrefab, _tasksListContent);
                    taskClone.SetData(_myStatsUI, questNode);
                    taskClone.enabled = true;
                }
            }

            _questJournal.StartCoroutine(_myStatsUI.followedTask.ValidateFollowedQuestNode());
            
            UpdateNoTasksText();
            
            RebuildUIManager.instance.Register(_myStatsUI.canvasRectTransform);
            
            _isRefreshing = false;
        }
        
        private void Activate()
        {
            gameObject.SetActive(true);
            _cursorManager.Register(this);
            _playerManager.localPlayer.playerLock.Register(this);

            RebuildUIManager.instance.Register(transform.GetComponent<RectTransform>());
            RebuildUIManager.instance.Register(_myStatsUI.canvasRectTransform);
        }
        
        private void Deactivate()
        {
            gameObject.SetActive(false);
            _cursorManager.Unregister(this);
            _playerManager.localPlayer.playerLock.Unregister(this);
        }
        
        private void UpdateNoTasksText() => _noTasksText.gameObject.SetActive(activeTaskPrefabsInUI.Count == 0);
        
        private void UpdateOpenCloseButton()
        {
            _openCloseText.text = isActivated ? _closeText : _openText;
            _openCloseImage.sprite = isActivated ? _closeSprite : _openSprite;
        }
    }
}