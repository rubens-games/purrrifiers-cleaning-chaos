using PlayerSystem;
using UnityEngine;
using Zenject;

namespace UISystem.HUD.MyStats
{
    public class MyStatsUI : MonoBehaviour
    {
        [field: SerializeField] public RectTransform canvasRectTransform { get; private set; }
        [field: SerializeField] public MyStatsUIFollowedTask followedTask { get; private set; }
        [field: SerializeField] public MyStatsUITasksList tasksList { get; private set; }
        [field: SerializeField] public MyStatsUIAvatar avatar { get; private set; }
        [field: SerializeField] public MyStatsUIHealth health { get; private set; }
        [field: SerializeField] public MyStatsUIEffects myStatsUIEffects { get; private set; }

        [Inject] public PlayerManager playerManager { get; private set; }
    }
}
