﻿using FishNet.Managing;
using SceneLoaderSystem;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UISystem.PauseMenuSystem.UI
{
    public class PauseMenuUI : MonoBehaviour
    {
        [Header("UI References")]
        [SerializeField] private GameObject _pauseMenuPanel;
        [SerializeField] private Button _resumeButton;
        [SerializeField] private Button _settingsButton;
        [SerializeField] private Button _exitButton;
        
        [Header("Warning UI References")]
        [SerializeField] private GameObject _warningPanel;
        [SerializeField] private GameObject _settingsPanel;
        [SerializeField] private Button _yesButton;
        [SerializeField] private Button _noButton;

        [Inject] private PauseMenu _pauseMenu;
        [Inject] private NetworkManager _networkManager;

        #region UnityMethods

        private void OnEnable()
        {
            _pauseMenu.OnPauseStateChanged.AddListener(OnPauseStateChanged);
            _resumeButton.onClick.AddListener(OnResumeButtonClicked);
            _settingsButton.onClick.AddListener(OnSettingsButtonClicked);
            _exitButton.onClick.AddListener(OnExitButtonClicked);
            _yesButton.onClick.AddListener(OnYesButtonClicked);
            _noButton.onClick.AddListener(OnNoButtonClicked);
        }
        
        private void OnDisable()
        {
            _pauseMenu.OnPauseStateChanged.RemoveListener(OnPauseStateChanged);
            _resumeButton.onClick.RemoveListener(OnResumeButtonClicked);
            _settingsButton.onClick.RemoveListener(OnSettingsButtonClicked);
            _exitButton.onClick.RemoveListener(OnExitButtonClicked);
            _yesButton.onClick.RemoveListener(OnYesButtonClicked);
            _noButton.onClick.RemoveListener(OnNoButtonClicked);
        }

        #endregion
        
        private void OnPauseStateChanged(bool isPaused) => _pauseMenuPanel.SetActive(isPaused);

        #region PauseMenuButtons
        
        private void OnResumeButtonClicked()
        {
            _pauseMenu.Resume();
        }
        
        private void OnSettingsButtonClicked()
        {
            Debug.Log($"Settings button clicked!");
            _settingsPanel.SetActive(true);
        }
        
        private void OnExitButtonClicked() => _warningPanel.SetActive(true);
        
        private void OnYesButtonClicked()
        {
            
            SceneLoader.instance.LoadScene("Menu", onBeforeCurrentSceneRemoved: OnBeforeCurrentSceneRemoved);
            
            return;

            void OnBeforeCurrentSceneRemoved()
            {
                _networkManager.ClientManager.StopConnection();
                
                if (_networkManager.ServerManager.Started)
                    _networkManager.ServerManager.StopConnection(true);
            }
        }
        
        private void OnNoButtonClicked() => _warningPanel.SetActive(false);

        #endregion
    }
}