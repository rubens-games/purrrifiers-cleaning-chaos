﻿using IntuitiveBackSystem.Runtime;
using UnityEngine;
using Zenject;

namespace UISystem.PauseMenuSystem
{
    public class PauseMenuToggleController : MonoBehaviour, IIntuitiveBackHandler
    {
        [Inject] private PauseMenu _pauseMenu;

        private static IntuitiveBackManager BackManager => IntuitiveBackManager.instance;
        
        public void OnEnable()
        {
            BackManager.Register(this);
        }

        public void OnDisable()
        {
            BackManager.Unregister(this);
        }
        
        #region IBackHandler

        public string toolTip => _pauseMenu.IsPaused ? "Resume" : "Pause";
        
        public void OnBack()
        {
            if (!_pauseMenu.IsPaused) _pauseMenu.Pause();
            else _pauseMenu.Resume();
        }

        #endregion
    }
}