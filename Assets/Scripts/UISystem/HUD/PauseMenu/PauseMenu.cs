using CursorSystem;
using PlayerSystem;
using UISystem.HUD;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using Zenject;

namespace UISystem.PauseMenuSystem
{
    public class PauseMenu : MonoBehaviour, ICursorHandler, IPlayerLockHandler
    {
        public UnityEvent<bool> OnPauseStateChanged {get; private set;} = new();

        public bool IsPaused
        {
            get => _isPaused;
            private set
            {
                if (_isPaused == value)
                    return;
                
                _isPaused = value;
                
                OnPauseStateChanged?.Invoke(_isPaused);
            }
        }
        
        [Inject] private CursorManager _cursorManager;
        [Inject] private PlayerManager _playerManager;
        
        private bool _isPaused;

        public void Pause()
        {
            if (IsPaused)
                return;
            
            _cursorManager.Register(this);
            if (_playerManager.localPlayer) _playerManager.localPlayer.playerLock.Register(this);

            IsPaused = true;
        }

        public void Resume()
        {
            if (!IsPaused)
                return;
            
            _cursorManager.Unregister(this);
            if (_playerManager.localPlayer != null) _playerManager.localPlayer.playerLock.Unregister(this);

            IsPaused = false;
        }

        #region ICursorHandler
        
        public CursorSettings CursorSettings => CursorSettings.ShowAndConfined;
        
        #endregion
        
        #region IPlayerLockHandler
        
        public bool isLocked => true;
        
        #endregion
    }
}
