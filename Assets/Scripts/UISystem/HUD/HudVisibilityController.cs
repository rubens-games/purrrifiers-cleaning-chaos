﻿using UnityEngine;

namespace UISystem.HUD
{
    public class HudVisibilityController : MonoBehaviour, IUIVisibilityHandler
    {
        private enum UnityEvent
        {
            Start,
            OnEnable,
            OnDisable,
            OnDestroy
        }

        public bool isVisible => _isVisible;
        
        [SerializeField] private UnityEvent _registerAt;
        [SerializeField] private UnityEvent _unregisterAt;
        [SerializeField] private bool _isVisible = true;

        private static UIVisibility uiVisibility => HUD.instance ? HUD.instance.uiVisibility : null;

        private void Start() => ChoiceAction(UnityEvent.Start);
        
        private void OnEnable() => ChoiceAction(UnityEvent.OnEnable);
        
        private void OnDisable() => ChoiceAction(UnityEvent.OnDisable);
        
        private void OnDestroy()
        {
            if (!Application.isPlaying) return;
            
            ChoiceAction(UnityEvent.OnDestroy);
        }

        private void ChoiceAction(UnityEvent unityEvent)
        {
            if (!uiVisibility) return;
            
            if (_registerAt == unityEvent)
                uiVisibility.Register(this);
            
            if (_unregisterAt == unityEvent)
                uiVisibility.Unregister(this);
        }
    }
}