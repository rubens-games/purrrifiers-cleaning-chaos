using SingletonSystem.Runtime;
using UnityEngine;

namespace UISystem.HUD
{
    public class HUD : Singleton<HUD>
    {
        [field: SerializeField] public UIVisibility uiVisibility {get; private set;}
    }
}
