using System.Collections;
using CursorSystem;
using FishNet.Managing;
using FishNet.Transporting;
using PlayerSystem;
using SceneLoaderSystem;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UISystem
{
    public class HostLeftWarningUI : MonoBehaviour, ICursorHandler, IPlayerLockHandler
    {
        public CursorSettings CursorSettings => CursorSettings.ShowAndConfined;
        
        public bool isLocked => true;
        
        [Header("Warning UI References")]
        [SerializeField] private GameObject _warningPanel;
        [SerializeField] private Button _exitToMainMenuButton;
        
        [Inject] private NetworkManager _networkManager;
        [Inject] private CursorManager _cursorManager;
        [Inject] private PlayerManager _playerManager;

        private void OnEnable()
        {
            _networkManager.ClientManager.OnClientConnectionState += OnClientConnectionState;
            _exitToMainMenuButton.onClick.AddListener(OnExitToMainMenuButtonClicked);
        }
        
        private void OnDisable()
        {
            _networkManager.ClientManager.OnClientConnectionState -= OnClientConnectionState;
            _exitToMainMenuButton.onClick.RemoveListener(OnExitToMainMenuButtonClicked);
        }
        
        private void OnClientConnectionState(ClientConnectionStateArgs clientConnectionStateArgs)
        { 
            if (_networkManager.IsServerStarted) return;
            
            if (_networkManager.IsClientStarted) return;
            
            if (clientConnectionStateArgs.ConnectionState != LocalConnectionState.Stopped) return;
            
            StartCoroutine(OpenWarningPanel());
        }
        
        private IEnumerator OpenWarningPanel()
        {
            yield return new WaitForSeconds(5f);
            
            _cursorManager.Register(this);
                
            _warningPanel.SetActive(true);
        }
        
        private void OnExitToMainMenuButtonClicked()
        {
            SceneLoader.instance.LoadScene("Menu", onBeforeCurrentSceneRemoved: OnBeforeCurrentSceneRemoved);
            
            return;
            
            void OnBeforeCurrentSceneRemoved()
            {
                _cursorManager.Unregister(this);
                
                if (_networkManager.IsServerStarted) 
                    _networkManager.ServerManager.StopConnection(true);
                
                _networkManager.ClientManager.StopConnection();
            }
        }
    }
}
