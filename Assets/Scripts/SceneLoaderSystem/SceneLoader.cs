using System;
using System.Threading.Tasks;
using DG.Tweening;
using SingletonSystem.Runtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace SceneLoaderSystem
{
    public class SceneLoader : Singleton<SceneLoader>
    {
        [SerializeField] private string _loadingScreenSceneName = "LoadingScreen";
        [SerializeField] private Image _coverImage;

        private const int PROGRESS_CHECK_DELAY = 100;
        private const string ANIM_SHOW_ID = "sceneLoaderShowImage";
        private const string ANIM_HIDE_ID = "sceneLoaderHideImage";

        public async void LoadScene(string sceneName, 
            Action onBeforeCurrentSceneRemoved = null, Action onAfterCurrentSceneRemoved = null, 
            Action onBeforeNextSceneLoaded = null, Action onAfterNextSceneLoaded = null)
        {
            var currentSceneName = SceneManager.GetActiveScene().name;
            await ShowImage(_coverImage);
            await AddScene(_loadingScreenSceneName);
            await HideImage(_coverImage);
            
            // TODO: Good place to load save file

            await WaitForAction(onBeforeCurrentSceneRemoved);
			await Task.Delay(1);
			await RemoveScene(currentSceneName);
            await Task.Delay(1);
            await WaitForAction(onAfterCurrentSceneRemoved);

            await WaitForAction(onBeforeNextSceneLoaded);
			await Task.Delay(1);
			await AddScene(sceneName);
            await Task.Delay(1);
            await WaitForAction(onAfterNextSceneLoaded);

            LightProbes.TetrahedralizeAsync();

            await ShowImage(_coverImage);
            await RemoveScene(_loadingScreenSceneName);
            await HideImage(_coverImage);
        }

        private static async Task AddScene(string sceneName)
        {
            var scene = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            
            if (scene != null)
            {
                scene.allowSceneActivation = false;

                while (scene.progress < 0.9f)
                    await Task.Delay(PROGRESS_CHECK_DELAY);

                scene.allowSceneActivation = true;

                while (!scene.isDone)
                    await Task.Delay(PROGRESS_CHECK_DELAY);

                Debug.Log($"Scene {sceneName} loaded successfully");

            }
            else Debug.LogError($"Failed to load scene {sceneName}");
            
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
		}    

        private static async Task RemoveScene(string sceneName)
        {
            Debug.Log($"Attempting to unload scene: {sceneName}");

            var sceneToUnload = SceneManager.GetSceneByName(sceneName);

            if (!sceneToUnload.isLoaded)
            {
                Debug.Log($"Cannot unload scene {sceneName} because it is not loaded.");
                return;
            }

            var scene = SceneManager.UnloadSceneAsync(sceneName);

            if (scene != null)
            {
                Debug.Log($"Unloading scene {sceneName} started.");

                while (scene.progress < 0.9f)
                {
                    Debug.Log($"Scene {sceneName} unload progress: {scene.progress}");
                    await Task.Delay(PROGRESS_CHECK_DELAY);
                }

                while (!scene.isDone)
                {
                    Debug.Log($"Scene {sceneName} is not done unloading.");
                    await Task.Delay(PROGRESS_CHECK_DELAY);
                }
                Debug.Log($"Scene {sceneName} unloaded successfully.");

            }
            else
            {
                Debug.LogError($"Failed to start unloading scene: {sceneName}");
            }
        }

        private static Task WaitForAction(Action action)
        {
            if (action == null) return Task.CompletedTask;
            
            var task = new Task(action);
            task.Start(TaskScheduler.FromCurrentSynchronizationContext());
            task.Wait();
            
            return task;
        }
        
        private static async Task ShowImage(Image image) => 
            await image.DOFade(1, 1).SetUpdate(true).SetId(ANIM_SHOW_ID).AsyncWaitForCompletion();

        private static async Task HideImage(Image image) => 
            await image.DOFade(0, 1).SetUpdate(true).SetId(ANIM_HIDE_ID).AsyncWaitForCompletion();

    }
}
