using UnityEngine;

namespace SceneLoaderSystem
{
    public class LoadScene : MonoBehaviour
    {
        [SerializeField] private string _sceneName;

        [ContextMenu("LoadSceneByName")]
        public void LoadSceneByName() => LoadSceneByName(_sceneName);

        public void LoadSceneByName(string sceneName)
        {
            SceneLoader.instance.LoadScene(sceneName);
        }
    }
}
