﻿using PickableSystem;
using PlayerSystem.RigSystem.ToolHoldingRig;
using UnityEditor;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace GrabbableSystem.Editor
{
    [CustomEditor(typeof(Pickable))]
    public class PickableEditor : UnityEditor.Editor
    {
        private void OnSceneGUI()
        {
            var pickable = (Pickable)target;
            var handsIKTarget = pickable.HandsIKTarget;
            var toolHoldingRig = pickable.ToolHoldingRig;

            if (!pickable.Origin || !toolHoldingRig || handsIKTarget == null) return;

            DrawIKTargetGizmos(pickable.Origin, toolHoldingRig.RightHandIK, handsIKTarget.RightHandIK, "Right Hand");
            DrawIKTargetGizmos(pickable.Origin, toolHoldingRig.LeftHandIK, handsIKTarget.LeftHandIK, "Left Hand");
        }

        private void DrawIKTargetGizmos(Transform origin, ToolHoldingHandRig toolHoldingHandRig,
            HandIKTarget handIKTarget, string handName)
        {
            if (handIKTarget == null) return;

            var handIK = toolHoldingHandRig.IK.data;
            var handRig = toolHoldingHandRig.FingerRig;
            var handThumbIK = handRig.ThumbIK.data;
            var handIndexIK = handRig.IndexIK.data;
            var handMiddleIK = handRig.MiddleIK.data;
            var handRingIK = handRig.RingIK.data;
            var handPinkyIK = handRig.PinkyIK.data;

            if (handIKTarget.weight > 0)
            {
                DrawPositionAndRotationGizmo(origin, handIK.target, handIKTarget.target, handName + " Target");
                DrawPositionAndRotationGizmo(origin, handIK.hint, handIKTarget.hint, handName + " Hint");
            }

            if (handIKTarget.ThumbIK.weight > 0)
            {
                DrawPositionAndRotationGizmo(origin, handThumbIK.target, handIKTarget.ThumbIK.target,
                    handName + " Thumb Target");
                DrawPositionAndRotationGizmo(origin, handThumbIK.hint, handIKTarget.ThumbIK.hint,
                    handName + " Thumb Hint");
            }

            if (handIKTarget.IndexIK.weight > 0)
            {
                DrawPositionAndRotationGizmo(origin, handIndexIK.target, handIKTarget.IndexIK.target,
                    handName + " Index Target");
                DrawPositionAndRotationGizmo(origin, handIndexIK.hint, handIKTarget.IndexIK.hint,
                    handName + " Index Hint");
            }

            if (handIKTarget.MiddleIK.weight > 0)
            {
                DrawPositionAndRotationGizmo(origin, handMiddleIK.target, handIKTarget.MiddleIK.target,
                    handName + " Middle Target");
                DrawPositionAndRotationGizmo(origin, handMiddleIK.hint, handIKTarget.MiddleIK.hint,
                    handName + " Middle Hint");
            }

            if (handIKTarget.RingIK.weight > 0)
            {
                DrawPositionAndRotationGizmo(origin, handRingIK.target, handIKTarget.RingIK.hint,
                    handName + " Ring Hint");
                DrawPositionAndRotationGizmo(origin, handRingIK.hint, handIKTarget.RingIK.target,
                    handName + " Ring Target");
            }

            if (handIKTarget.PinkyIK.weight > 0)
            {
                DrawPositionAndRotationGizmo(origin, handPinkyIK.target, handIKTarget.PinkyIK.target,
                    handName + " Pinky Target");
                DrawPositionAndRotationGizmo(origin, handPinkyIK.hint, handIKTarget.PinkyIK.hint,
                    handName + " Pinky Hint");
            }

        }

        private void DrawPositionAndRotationGizmo(Transform parentTransform, Transform drawGizmoPoint,
            PositionAndRotationTarget targetData, string label)
        {
            if (!target) return;
            
            var globalPosition = parentTransform.TransformPoint(targetData.position);
            var globalRotation = parentTransform.rotation * Quaternion.Euler(targetData.rotation);

            if (targetData.showPositionHandle)
            {
                var newGlobalPosition = Handles.PositionHandle(globalPosition, globalRotation);
                if (Vector3.Distance(newGlobalPosition, globalPosition) > 0.0001f)
                    targetData.position = parentTransform.InverseTransformPoint(newGlobalPosition);
            }

            if (targetData.showRotationHandle)
            {
                var newGlobalRotation = Handles.RotationHandle(globalRotation, globalPosition);
                if (Quaternion.Angle(newGlobalRotation, globalRotation) > 0.01f)
                    targetData.rotation = (Quaternion.Inverse(parentTransform.rotation) * newGlobalRotation).eulerAngles;
            }

            Handles.Label(drawGizmoPoint.position, label);
        }
    }
}
