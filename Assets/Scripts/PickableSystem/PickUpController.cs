﻿using System.Threading.Tasks;
using FishNet.Connection;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using PlayerSystem;
using PlayerSystem.RigSystem.ToolHoldingRig;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace PickableSystem
{
    public class PickUpController : NetworkBehaviour
    {
        public static UnityEvent<PickUpController, Pickable> OnSomePickupBefore = new();
        public static UnityEvent<PickUpController, Pickable> OnSomePickup = new();
        public static UnityEvent<PickUpController, Pickable> OnSomePickupAfter = new();
        public static UnityEvent<PickUpController, Pickable> OnSomeDropBefore = new();
        public static UnityEvent<PickUpController, Pickable> OnSomeDrop = new();
        public static UnityEvent<PickUpController, Pickable> OnSomeDropAfter = new();
        
        public UnityEvent<Pickable> OnPickupBefore = new();
        public UnityEvent<Pickable> OnPickup = new();
        public UnityEvent<Pickable> OnPickupAfter = new();
        public UnityEvent<Pickable> OnDropBefore = new();
        public UnityEvent<Pickable> OnDrop = new();
        public UnityEvent<Pickable> OnDropAfter = new();

        public bool CanPickup => enabled && !CurrentPickup;
        public bool CanDrop => CurrentPickup;
        public Pickable CurrentPickup => HoldingPoint.GetComponentInChildren<Pickable>();

        public Player Player;
        public Transform HoldingPoint;
        public ToolHoldingRig ToolHoldingRig;
        
        [SerializeField] private InputActionReference _dropAction;
        
        private bool _isPickUpProcessing;
        private bool _isDropProcessing;
        
        private readonly SyncVar<string> _currentPickupID = new();

        private void OnEnable()
        {
            _currentPickupID.OnChange += OnCurrentPickupIDChanged;
        }
        
        private void OnDisable()
        {
            _currentPickupID.OnChange -= OnCurrentPickupIDChanged;
        }

        public override void OnOwnershipClient(NetworkConnection prevOwner)
        {
            base.OnOwnershipClient(prevOwner);

            if (IsOwner)
            {
                _dropAction.action.performed += OnDropActionPerformed;
                _dropAction.action.Enable();
            }
            else
            {
                if (prevOwner != LocalConnection) return;
                
                _dropAction.action.performed -= OnDropActionPerformed;
                _dropAction.action.Disable();
            }
        }

        public override void OnStopClient()
        {
            base.OnStopClient();
            
            if (IsOwner) Drop();
        }

        private void OnDropActionPerformed(InputAction.CallbackContext context) => Drop();

        public async Task PickUp(Pickable pickable)
        {
            if (!IsOwner) return;
            
            _isPickUpProcessing = true;
            
            PickUpServerRpc(pickable);

            while (_isPickUpProcessing) await Task.Yield();
        }

        [ServerRpc]
        private void PickUpServerRpc(Pickable pickable)
        {
            pickable.GiveOwnership(Owner);
            
            _currentPickupID.Value = pickable.ID;
            
            _isPickUpProcessing = false;
        }

        public async Task Drop()
        {
            _isDropProcessing = true;
            
            if (IsOwner) DropServerRpc();

            while (_isDropProcessing) 
                await Task.Yield();
        }

        [ServerRpc]
        private void DropServerRpc()
        {
            var pickable = CurrentPickup;

            if (!pickable)
            {
                _isDropProcessing = false;
                return;
            }
            
            pickable.GiveOwnership(null);
            
            _currentPickupID.Value = string.Empty;
            
            _isDropProcessing = false;
        }
        
        private void OnCurrentPickupIDChanged(string oldID, string newID, bool asServer)
        {
            var hasNewID = !string.IsNullOrEmpty(newID) && !string.IsNullOrWhiteSpace(newID);

            if (hasNewID)
            {
                OnCurrentPickupIDChanged_Drop();
                
                OnCurrentPickupIDChanged_Pickup(newID);
                
                return;
            }

            OnCurrentPickupIDChanged_Drop();
        }

        private void OnCurrentPickupIDChanged_Pickup(string pickableID)
        {
            var pickable = Pickable.GetPickableByID(pickableID);

            if (!pickable || !pickable.CanBePickUp || !CanPickup)
            {
                _isPickUpProcessing = false;
                return;
            }
            
            // Invoke events
            OnPickupBefore?.Invoke(pickable);
            pickable.OnPickupBefore?.Invoke(this);
            OnSomePickupBefore?.Invoke(this,pickable);
            
            // Do pick up
            pickable.UpdateCollidersLayer();
            
            pickable.CurrentPickUpController = this;
            
            if (pickable.Rigidbody)
            {
                pickable.Rigidbody.isKinematic = true;
                pickable.Rigidbody.useGravity = false;
            }

            pickable.Origin.parent = HoldingPoint;
            pickable.Origin.localPosition = pickable.PositionOffset;
            pickable.Origin.localRotation = Quaternion.Euler(pickable.RotationOffset);
            
            // Invoke events
            pickable.OnPickup?.Invoke(this);
            OnPickup?.Invoke(pickable);
            pickable.OnPickupAfter?.Invoke(this);
            OnPickupAfter?.Invoke(pickable);
            OnSomePickup?.Invoke(this, pickable);
            OnSomePickupAfter?.Invoke(this, pickable);
            
            _isPickUpProcessing = false;
        }
        
        private void OnCurrentPickupIDChanged_Drop()
        {
            var pickable = CurrentPickup;

            if (!pickable)
            {
                _isDropProcessing = false;
                return;
            }
            
            // Invoke events
            OnDropBefore?.Invoke(pickable);
            pickable.OnDropBefore?.Invoke(this);
            OnSomeDropBefore?.Invoke(this, pickable);
            
            // Do drop
            pickable.CurrentPickUpController = null;
            
            if (pickable.Rigidbody)
            {
                pickable.Rigidbody.isKinematic = false;
                pickable.Rigidbody.useGravity = true;
            }

            pickable.Origin.parent = null;
            
            // Invoke events
            pickable.OnDrop?.Invoke(this);
            OnDrop?.Invoke(pickable);
            pickable.OnDropAfter?.Invoke(this);
            OnDropAfter?.Invoke(pickable);
            OnSomeDrop?.Invoke(this, pickable);
            OnSomeDropAfter?.Invoke(this, pickable);

            _isDropProcessing = false;
        }
    }
}