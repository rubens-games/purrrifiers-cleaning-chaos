using System;
using System.Collections.Generic;
using InteractionSystem;
using Other;
using PlayerSystem;
using PlayerSystem.RigSystem.ToolHoldingRig;
using UISystem.MainMenu;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace PickableSystem
{
    public enum Hand
    {
        Right,
        Left,
        Both
    }
    
    [RequireComponent(typeof(Rigidbody))]
    public class Pickable : Interactable
    {
        #region Interactable

        public override bool IsSleeping => IsPickUp;

        #endregion
        
        public static List<Pickable> Pickables = new();
        
        public string ID = Guid.NewGuid().ToString();

        public bool CanBePickUp => CanInteract;
        public bool IsPickUp => CurrentPickUpController;

        public PickUpController CurrentPickUpController { get; set; }
        public ToolHoldingRig ToolHoldingRig => CurrentPickUpController?.ToolHoldingRig;

        [field: SerializeField] public UnityEvent<PickUpController> OnPickupBefore { get; private set; } = new();
        [field: SerializeField] public UnityEvent<PickUpController> OnPickup { get; private set; } = new();
        [field: SerializeField] public UnityEvent<PickUpController> OnPickupAfter { get; private set; } = new();
        [field: SerializeField] public UnityEvent<PickUpController> OnDropBefore { get; private set; } = new();
        [field: SerializeField] public UnityEvent<PickUpController> OnDrop { get; private set; } = new();
        [field: SerializeField] public UnityEvent<PickUpController> OnDropAfter { get; private set; } = new();
        [field: SerializeField] public UnityEvent<PickUpController> OnAnimateBefore { get; private set; } = new();
        [field: SerializeField] public UnityEvent<PickUpController> OnAnimateAfter { get; private set; } = new();
        
        [field: SerializeField] public Hand Hand { get; private set; }
        [field: SerializeField] public bool CanBeDrop { get; private set; }
        [field: SerializeField] public Rigidbody Rigidbody { get; private set; }
        [field: SerializeField] public Transform Origin { get; private set; }
        [field: SerializeField] public Vector3 PositionOffset { get; private set; }
        [field: SerializeField] public Vector3 RotationOffset { get; private set; }
        [field: SerializeField] public HandsIKTarget HandsIKTarget { get; private set; }
        [field: SerializeField] public bool AniamteLookAt { get; private set; }

        [Inject] private PlayerManager _playerManager;
        
        protected override void OnValidate()
        {
            base.OnValidate();
            
            UpdateCollidersLayer();
            
            if (string.IsNullOrEmpty(ID) || string.IsNullOrWhiteSpace(ID)) ID = Guid.NewGuid().ToString(); 
            if (!Origin) Origin = transform;
            if (!Rigidbody) Rigidbody = gameObject.GetOrAddComponent<Rigidbody>();
        }

        protected virtual void Awake() => Pickables.Add(this);

        protected virtual void OnEnable()
        {
            OnInteract.AddListener(OnInteractInvoke);
        }
        
        protected virtual void OnDisable()
        {
            OnInteract.RemoveListener(OnInteractInvoke);
        }

        private void OnDestroy() => Pickables.Remove(this);

#if UNITY_EDITOR
        
        [SerializeField] private bool _debug;
        [SerializeField] private bool _debugSlowMotion;
        
        private void Update()
        {
            if (!_debug) return;

            if (!IsPickUp) return;
            
            var inRightHandHoldingPoint = CurrentPickUpController.HoldingPoint;
            Origin.localPosition = PositionOffset;
            Origin.localRotation = Quaternion.Euler(RotationOffset);

            Time.timeScale = _debugSlowMotion ? 0.01f : 1f;
        }
        
#endif

        private void OnInteractInvoke(Player player)
        {
            switch (Hand)
            {
                case Hand.Right:
                    player.pickUpControllerRight.PickUp(this);
                    break;
                case Hand.Left:
                    player.pickUpControllerLeft.PickUp(this);
                    break;
                case Hand.Both:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void UpdateCollidersLayer()
        {
            if (!Origin) return;
            
            var pickableColliders = Origin.GetComponentsInChildren<Collider>();
            
            foreach (var pickableCollider in pickableColliders)
            {
                var currentLayer = pickableCollider.gameObject.layer;
                
                if (currentLayer == LayerMask.NameToLayer("Pickable")) continue;
                
                pickableCollider.gameObject.layer = LayerMask.NameToLayer("Pickable");
            }
        }
        
        public static Pickable GetPickableByID(string id) => Pickables.Find(pickable => pickable.ID == id);
    }
}
