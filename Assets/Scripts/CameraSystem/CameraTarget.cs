using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace CameraSystem
{
    public class CameraTarget : MonoBehaviour, ICameraTarget
    {
        [Inject] private CameraManager _cameraManager;
    
        private void OnEnable() => 
            _cameraManager.RegisterTarget(this);

        private void OnDisable() => 
            _cameraManager.UnregisterTarget(this);

        #region ICameraTarget

        public UnityEvent OnRegistered { get; } = new();
        public UnityEvent OnUnregistered { get; } = new();
        
        [field: SerializeField] public Transform CameraFollowTarget { get; private set; }
        
        public void OnRegister()
        {
            
        }

        public void OnUnregister()
        {
            
        }

        #endregion
    }
}