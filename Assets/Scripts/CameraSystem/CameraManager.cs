﻿using System;
using System.Collections.Generic;
using FishNet;
using FishNet.Managing.Timing;
using Other;
using UnityEngine;
using UnityEngine.Events;

namespace CameraSystem
{
    public class CameraManager : MonoBehaviour
    {
        public UnityEvent<ICameraTarget> OnCameraTargetRegistered = new();
        public UnityEvent<ICameraTarget> OnCameraTargetUnregistered = new();
        public UnityEvent<ICameraTarget> OnCurrentCameraTargetChanged = new();
    
        [field: SerializeField] public Camera Camera { get; private set; }
        [field: SerializeField] public FollowTargetSmooth FollowTargetSmooth { get; private set; }
        
        public ICameraTarget CurrentCameraTarget => CameraTargets is { Count: > 0 } ? CameraTargets[0] : null;
        public List<ICameraTarget> CameraTargets { get; private set; } = new();
        
        public void RegisterTarget(ICameraTarget target)
        {
            CameraTargets.Insert(0, target);
            
            UpdateTarget();
            
            OnCameraTargetRegistered.Invoke(target);
            target.OnRegister();
            target.OnRegistered?.Invoke();
        }
    
        public void UnregisterTarget(ICameraTarget target)
        {
            CameraTargets.Remove(target);
            
            UpdateTarget();
            
            OnCameraTargetUnregistered.Invoke(target);
            target.OnUnregister();
            target.OnUnregistered?.Invoke();
        }
        
        public void UpdateTarget()
        {
            FollowTargetSmooth.target = CurrentCameraTarget?.CameraFollowTarget;

            OnCurrentCameraTargetChanged?.Invoke(CurrentCameraTarget);
        }
    }
}