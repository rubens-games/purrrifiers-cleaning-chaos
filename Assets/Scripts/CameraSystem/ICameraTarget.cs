﻿using UnityEngine;
using UnityEngine.Events;

namespace CameraSystem
{
    public interface ICameraTarget
    {
        public UnityEvent OnRegistered { get; }
        public UnityEvent OnUnregistered { get; }
        
        Transform CameraFollowTarget { get; }

        public void OnRegister();
        public void OnUnregister();
    }
}