﻿using CursorSystem;
using FishNet.Connection;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using PickableSystem;
using PlayerSystem;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using Zenject;

namespace ToolSystem
{
    public abstract class Tool : Pickable, ICursorHandler
    {
        #region ICursorHandler
        
        public CursorSettings CursorSettings => CursorSettings.ShowAndConfined;

        #endregion
        
        public UnityEvent<Tool> onUse = new();
        public UnityEvent<Tool> onStopUse = new();
        
        public UnityEvent<Tool> onFreeze = new();
        public UnityEvent<Tool> onStopFreeze = new();

        public bool isUsing
        {
            get => _isUsing.Value;
            [ServerRpc(RequireOwnership = false)] set
            {
                if (isUsing == value) return;
                _isUsing.Value = value;
            }
        }
        
        public bool isFreezing
        {
            get => _isFreezing.Value;
            [ServerRpc(RequireOwnership = false)] private set
            {
                if (isFreezing == value) return;
                _isFreezing.Value = value;
            }
        }

        public abstract InputAction useAction { get; }
        public abstract InputAction freezeAction { get; }

        [Inject] private PlayerManager _playerManager;
        [Inject] private CursorManager _cursorManager;
        
        private readonly SyncVar<bool> _isUsing = new(false);
        private readonly SyncVar<bool> _isFreezing = new(false);

        protected override void OnEnable()
        {
            base.OnEnable();
            
            _isUsing.OnChange += OnIsUsingChange;
            _isFreezing.OnChange += OnIsFreezingChange;
            
            OnPickup.AddListener(OnPickupExecute);
            OnDrop.AddListener(OnDropExecute);
        }
    
        protected override void OnDisable()
        {
            base.OnDisable();
            
            _isUsing.OnChange -= OnIsUsingChange;
            _isFreezing.OnChange -= OnIsFreezingChange;
            
            OnPickup.RemoveListener(OnPickupExecute);
            OnDrop.RemoveListener(OnDropExecute);
        }

        private void OnPickupExecute(PickUpController controller)
        {
            if (!controller.IsOwner) return;
            
            EnableInputs();
        }
        
        private void OnDropExecute(PickUpController controller)
        {
            Debug.Log($"OnDropExecute: {controller.IsOwner}");
            
            if (!controller.IsOwner) return;
            
            isUsing = false;
            isFreezing = false;
            
            _playerManager.localPlayer.playerInput.rtsRotationMode = false;
            _cursorManager.Unregister(this);
            
            DisableInputs();
        }

        #region Inputs
        
        public void EnableInputs()
        {
            useAction.Enable();
            freezeAction.Enable();
            
            useAction.performed += OnUseActionPerformed;
            useAction.canceled += OnUseActionCanceled;
            freezeAction.performed += OnFreezeActionPerformed;
        }
        
        public void DisableInputs()
        {
            useAction.Disable();
            freezeAction.Disable();
            
            useAction.performed -= OnUseActionPerformed;
            useAction.canceled -= OnUseActionCanceled;
            freezeAction.performed -= OnFreezeActionPerformed;
            
            if (isUsing) isUsing = false;
        }

        #endregion
        
        #region Use

        private void OnUseActionPerformed(InputAction.CallbackContext context) => isUsing = true;

        private void OnUseActionCanceled(InputAction.CallbackContext context) => isUsing = false;

        private void OnIsUsingChange(bool prev, bool next, bool asServer)
        {
            if (asServer) return;
            if (!IsOwner) return;
            
            if (next) Use();
            else StopUse();
        }
        
        private void Use() => UseServerRPC();

        [ServerRpc]
        private void UseServerRPC() => UseObserverRpc();

        [ObserversRpc]
        private void UseObserverRpc() => OnUse();

        protected virtual void OnUse() => onUse?.Invoke(this);

        private void StopUse() => StopUseServerRPC();

        [ServerRpc]
        private void StopUseServerRPC() => StopUseObserverRpc();
        
        [ObserversRpc]
        private void StopUseObserverRpc() => OnStopUse();
        
        protected virtual void OnStopUse() => onStopUse?.Invoke(this);

        #endregion

        #region Freeze
        
        private void OnFreezeActionPerformed(InputAction.CallbackContext context)
        {
            if (IsOwner)
                isFreezing = !isFreezing;
        }

        private void OnIsFreezingChange(bool prev, bool next, bool asServer)
        {
            if (!IsOwner) return;
            
            if (next) Freeze();
            else StopFreeze();
        }
        
        private void Freeze() => FreezeServerRPC();
        
        [ServerRpc]
        private void FreezeServerRPC() => FreezeObserverRPC();
        
        [ObserversRpc]
        private void FreezeObserverRPC() => OnFreeze();
        
        protected virtual void OnFreeze()
        {
            if (IsOwner)
            {
                _playerManager.localPlayer.playerInput.rtsRotationMode = true;
                _cursorManager.Register(this);
            }
            
            onFreeze?.Invoke(this);
        }

        private void StopFreeze() => StopFreezeServerRPC();
        
        [ServerRpc]
        private void StopFreezeServerRPC() => StopFreezeObserverRPC();
        
        [ObserversRpc]
        private void StopFreezeObserverRPC() => OnStopFreeze();
        
        protected virtual void OnStopFreeze()
        {
            if (IsOwner)
            {
                _playerManager.localPlayer.playerInput.rtsRotationMode = false;
                _cursorManager.Unregister(this);
            }
            
            onStopFreeze?.Invoke(this);
        }

        #endregion

    }
}