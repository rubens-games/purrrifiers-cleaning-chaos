﻿using FishNet.Object;
using UnityEngine;
using UnityEngine.Events;

namespace ToolSystem
{
    public class HitableByTool : NetworkBehaviour
    {
        [field: SerializeField] public UnityEvent<Tool> onHitBefore { get; private set; } = new();
        [field: SerializeField] public UnityEvent<Tool> onHit { get; private set; } = new();
        [field: SerializeField] public UnityEvent<Tool> onHitAfter { get; private set; } = new();

        internal virtual void Hit(Tool tool)
        {
            HitServerRpc(tool);
        }

        [ServerRpc(RequireOwnership = false)]
        private void HitServerRpc(Tool tool) => HitObserversRpc(tool);
        
        [ObserversRpc]
        private void HitObserversRpc(Tool tool)
        {
            OnHitBefore(tool);
            OnHit(tool);
            OnHitAfter(tool);
        }
        
        public virtual void OnHitBefore(Tool tool)
        {
            onHitBefore?.Invoke(tool);
        }

        public virtual void OnHit(Tool tool)
        {
            onHit?.Invoke(tool);
        }

        public virtual void OnHitAfter(Tool tool)
        {
            onHitAfter?.Invoke(tool);
        }
    }
}