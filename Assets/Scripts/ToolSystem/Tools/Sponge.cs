﻿using DG.Tweening;
using FishNet.Connection;
using PickableSystem;
using System.Collections.Generic;
using ToolSystem.Tools.SprayerTool;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace ToolSystem.Tools
{
    public class Sponge : Tool
    {
        public override InputAction useAction => _useAction.action;
        public override InputAction freezeAction => _freezeAction.action;
        
        [SerializeField] private InputActionReference _useAction;
        [SerializeField] private InputActionReference _freezeAction;
        [SerializeField] private float _spongeDistance = 5f;

        [Header("Animation")]
        [SerializeField] private Vector3 _startingPosition;
        [SerializeField] private Vector3 _animPosOut;
        [SerializeField] private float _animDuration = 0.2f;
        [SerializeField] private Ease _animEase = Ease.OutQuad;
        
        [Inject] private Camera _camera;

        [SerializeField] private List<AudioClip> _clips;
        private AudioSource _audioSource;

        protected override void OnEnable()
        {
            base.OnEnable();
            
            OnPickupAfter.AddListener(OnPickupAfterExecute);
            _audioSource = GetComponent<AudioSource>();

        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            OnPickupAfter.RemoveListener(OnPickupAfterExecute);
        }
        
        protected override void OnUse()
        {
            base.OnUse();
            
            Animate();
            PlayRandomSFX();

            if (!IsOwner) return;
            
            var ray = _camera.ScreenPointToRay(Mouse.current.position.ReadValue());

            if (!Physics.Raycast(ray, out var hit, _spongeDistance)) return;
            
            var cleaningFoam = hit.collider.GetComponent<CleaningFoam>();
            
            if (!cleaningFoam) cleaningFoam = hit.collider.GetComponentInParent<CleaningFoam>();

            if (!cleaningFoam) return;
            
            cleaningFoam.Clean(Owner);
        }
        
        private void OnPickupAfterExecute(PickUpController controller) => _startingPosition = transform.localPosition;

        private void Animate()
        {
            transform.DOLocalMove(_animPosOut, _animDuration / 2).SetEase(_animEase)
                .OnComplete(() => transform.DOLocalMove(_startingPosition, _animDuration / 2).SetEase(_animEase));
        }

        private void PlayRandomSFX()
        {
            int randomIndex = Random.Range(0, _clips.Count);
            AudioClip footstep = _clips[randomIndex];

            _audioSource.clip = footstep;
            _audioSource.Play();
        }
    }
}