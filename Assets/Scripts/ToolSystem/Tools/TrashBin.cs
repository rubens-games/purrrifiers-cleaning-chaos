﻿using System.Collections;
using System.Collections.Generic;
using FishNet;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using Other;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace ToolSystem.Tools
{
    public class TrashBin : Tool
    {
        [field: SerializeField] public ItemState binType { get; private set; }

        public int trashesInBin
        {
            get => _trashesInBinItemStat.TrashesInBin;
            set => _trashesInBinItemStat.TrashesInBin = value;
        }

        public int MaxTrashesInBin => (int)_trashesInBinItemStat.maxValue;

        public bool IsTrashBinFull => trashesInBin >= MaxTrashesInBin;
        
        public override InputAction useAction => _useActionReference.action;
        public override InputAction freezeAction => _freezeActionReference.action;
        
        [SerializeField] private InputActionReference _useActionReference;
        [SerializeField] private InputActionReference _freezeActionReference;
        
        [Header("Bin Settings")]
        [SerializeField] private Transform _trashBagSpawnPoint;
        [SerializeField] private GameObject _trashBagOpenVisual;
        [SerializeField] private GameObject _trashBagClosedVisual;
        [SerializeField] private TrashBag _trashBagPrefab;
        [SerializeField] private float _shootForce = 10;

        [Inject] private Camera _camera;
        
        private TrashBinTrashesInBinItemStat _trashesInBinItemStat;
        private TrashBinShootLoadingItemStat _shootLoadingItemStat;
        private Coroutine _shootLoadingCoroutine;
        private bool _wasPlayedMono = false;
        [SerializeField] private MonologueSO _myMonologue;
        [SerializeField] private List<AudioClip> _clips;
        private AudioSource _audioSource;


        protected override void Awake()
        {
            base.Awake();

            _trashesInBinItemStat = GetComponent<TrashBinTrashesInBinItemStat>();
            _shootLoadingItemStat = GetComponent<TrashBinShootLoadingItemStat>();
            _audioSource = GetComponent<AudioSource>();

        }

        protected override void OnEnable()
        {
            base.OnEnable();
            
            _trashesInBinItemStat.SyncTrashesInBin.OnChange += OnTrashesInBinChange;
        }
        
        protected override void OnDisable()
        {
            base.OnDisable();
            
            _trashesInBinItemStat.SyncTrashesInBin.OnChange -= OnTrashesInBinChange;
        }

        #region Use Behaviour

        protected override void OnUse()
        {
            base.OnUse();
            
            if (!IsOwner) return;
            
            if (!IsTrashBinFull) return;

            StartShootLoading();
        }

        protected override void OnStopUse()
        {
            base.OnStopUse();
            
            if (!IsOwner) return;

            if (_shootLoadingCoroutine != null)
                StopShooting();
        }

        private void StartShootLoading()
        {
            if (_shootLoadingCoroutine != null) 
                StopShooting();
            
            _shootLoadingCoroutine = StartCoroutine(ShootLoading());
        }
        
        private IEnumerator ShootLoading()
        {
            while (_shootLoadingItemStat.shootLoading < _shootLoadingItemStat.maxShootLoading)
            {
                _shootLoadingItemStat.shootLoading += Time.deltaTime;
                yield return null;
            }
        }

        private void StopShooting()
        {
            if (_shootLoadingCoroutine != null)
            {
                StopCoroutine(_shootLoadingCoroutine);
                
                var shootForce = _shootLoadingItemStat.shootLoading * _shootForce;
                
                Shoot(shootForce);
                
                _shootLoadingCoroutine = null;
            }
            
            _shootLoadingItemStat.shootLoading = 0;
        }

        [ServerRpc]
        private void Shoot(float shootForce)
        {
            var trashBagClone = Instantiate(_trashBagPrefab, _trashBagSpawnPoint.position, _trashBagSpawnPoint.rotation);
            
            Spawn(trashBagClone.gameObject, Owner);
            
            ShootObserversRpc(trashBagClone.gameObject, shootForce);
            
            trashesInBin = 0;
        }

        [ObserversRpc]
        private void ShootObserversRpc(GameObject trashBag, float shootForce)
        {
            if (!IsOwner) return;
            
            var trashBagRigidbody = trashBag.GetComponent<Rigidbody>();
            
            trashBagRigidbody.isKinematic = false;
            trashBagRigidbody.AddForce(_camera.transform.forward * shootForce, ForceMode.Impulse);
        }

        #endregion
        
        #region Adding Trashes

        public void AddTrash(int amount)
        {
            if (!IsOwner) return;

            PlayRandomSFX();
            trashesInBin += amount;
        }
        private void PlayRandomSFX()
        {
            int randomIndex = Random.Range(0, _clips.Count);
            AudioClip clip = _clips[randomIndex];

            _audioSource.clip = clip;
            _audioSource.Play();
        }
        private void OnTrashesInBinChange(int prev, int next, bool asServer)
        {
            if (!IsOwner) return;

            var isFull = next >= _trashesInBinItemStat.maxValue;

            if (isFull) OnTrashBinIsFull();
            else OnTrashBinIsNotFull();
            
            _trashesInBinItemStat.TrashesInBin = next;
        }
        
        private void OnTrashBinIsFull()
        {
            SetActiveTrashBag(true);
            
            if (!IsOwner) return;
            
            if (_wasPlayedMono) return;
            MonologueManager.instance.PlayMonologue(_myMonologue);
            _wasPlayedMono = true;
        }
        
        private void OnTrashBinIsNotFull()
        {
            SetActiveTrashBag(false);
        }
        
        [ServerRpc]
        private void SetActiveTrashBag(bool active) => SetActiveTrashBagObserversRpc(active);
        
        [ObserversRpc]
        private void SetActiveTrashBagObserversRpc(bool active)
        {
            _trashBagClosedVisual.SetActive(active);
            _trashBagOpenVisual.SetActive(!active);
        }

        #endregion
    }
}