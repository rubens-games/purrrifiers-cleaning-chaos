using FishNet.Connection;
using PlayerSystem;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace ToolSystem.Tools
{
    public class PokeBallTool : Tool
    {
        public override InputAction useAction => _useActionReference.action;
        public override InputAction freezeAction => _freezeActionReference.action;

        [SerializeField] private InputActionReference _useActionReference;
        [SerializeField] private InputActionReference _freezeActionReference;

        [Inject] private Camera _camera;

        public override void OnOwnershipClient(NetworkConnection prevOwner)
        {
            base.OnOwnershipClient(prevOwner);

            if (IsOwner) useAction.Enable();
            else useAction.Disable();
        }

        protected override void OnUse()
        {
            base.OnUse();

            if (IsOwner) HealPlayer();
        }

        private void HealPlayer()
        {
            var camTransform = _camera.transform;
            var isHit = Physics.Raycast(camTransform.position, camTransform.forward, out var hit, 100f);
            if (!isHit) return;

            var playerModel = hit.collider.GetComponent<PlayerModel>();
            if (!playerModel) return;
            
            var playerHitable = playerModel.player.playerHitable;
            
            if (playerHitable.isAlive) return;
            
            playerHitable.Heal(playerHitable.missingHitpoints);
        }
    }
}