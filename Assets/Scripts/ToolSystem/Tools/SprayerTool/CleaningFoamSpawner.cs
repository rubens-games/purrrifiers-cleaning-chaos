﻿using CleanableSystem;
using SingletonSystem.Runtime;
using UnityEngine;

namespace ToolSystem.Tools.SprayerTool
{
    public class CleaningFoamSpawner : Singleton<CleaningFoamSpawner>
    {
        [SerializeField] private CleaningFoamSpawnerNetworked _cleaningFoamSpawnerNetworked;

        public void Spawn(Vector3 spawnPosition, Vector3 forward, CleanableDecal cleanableDecal,
            Vector2 textureCord, float radius) =>
            _cleaningFoamSpawnerNetworked.SpawnFoam(spawnPosition, forward, cleanableDecal, textureCord, radius);
    }
}