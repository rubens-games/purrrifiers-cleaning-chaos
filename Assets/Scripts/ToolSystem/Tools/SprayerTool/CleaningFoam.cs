﻿using System;
using CleanableSystem;
using FishNet.Connection;
using FishNet.Object;
using UnityEngine;

namespace ToolSystem.Tools.SprayerTool
{
    public enum Durability
    {
        None = 0,
        Low = 80, // 5 razy
        Medium = 75, // 4 razy
        High = 65, // 3 razy
        VeryHigh = 50 // 2 razy
    }
    
    public class CleaningFoam : NetworkBehaviour
    {
        [SerializeField] private bool _cleanByAlpha;
        [SerializeField] private int _MaxHealth = 100;
        [SerializeField] private Durability _Durability = Durability.Low;
        [SerializeField] private float fadeDuration = 2.0f;
        
        [Header("Networked values")]        
        [SerializeField] private CleanableDecal cleanableDecal;
        [SerializeField] public Vector2 textureCords;
        [SerializeField] public float radius;


        public override void OnSpawnServer(NetworkConnection connection)
        {
            base.OnSpawnServer(connection);
            
            InitializeObserverRpc(cleanableDecal, textureCords, radius);
        }

        [ObserversRpc]
        private void InitializeObserverRpc(CleanableDecal newCleanableDecal, Vector2 newTextureCord, float newRadius) => 
            Initialize(newCleanableDecal, newTextureCord, newRadius);

        public void Initialize(CleanableDecal newCleanableDecal, Vector2 newTextureCord, float newRadius)
        {
            cleanableDecal = newCleanableDecal;
            textureCords = newTextureCord;
            radius = newRadius;
        }

        public void Clean(NetworkConnection cleanedBy)
        {
            if (!cleanableDecal)
            {
                Debug.LogError("CleanableDecal is null");
                return;
            }

            if (_cleanByAlpha) SubtractDecalAlphaServerRpc(0.35f);
            else cleanableDecal.CleanMe(textureCords, radius, cleanedBy);
            
            RemoveFoamServerRpc();
        }
        
        [ServerRpc(RequireOwnership = false)]
        private void SubtractDecalAlphaServerRpc(float alpha) => SubtractDecalAlphaObserverRpc(alpha);

        [ObserversRpc]
        private void SubtractDecalAlphaObserverRpc(float alpha)
        {
            // cleanableDecal.alphaPercentage -= alpha;
            
            var previousAlpha = cleanableDecal.decalProjector.fadeFactor;
            
            cleanableDecal.decalProjector.fadeFactor -= alpha;
            
            if (previousAlpha > 0 && cleanableDecal.decalProjector.fadeFactor <= 0)
                cleanableDecal.StartCoroutine(cleanableDecal.DisableDecal());
        }

        [ServerRpc(RequireOwnership = false)]
        private void RemoveFoamServerRpc() => Despawn(gameObject);
    }
}