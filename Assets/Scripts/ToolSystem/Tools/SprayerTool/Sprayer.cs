﻿using System;
using System.Collections;
using System.Collections.Generic;
using CleanableSystem;
using DG.Tweening;
using FishNet.Connection;
using FishNet.Object;
using PickableSystem;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace ToolSystem.Tools.SprayerTool
{
    public class Sprayer : Tool
    {
        public override InputAction useAction => _useAction.action;
        public override InputAction freezeAction => _freezeAction.action;
        
        [SerializeField] private InputActionReference _useAction;
        [SerializeField] private InputActionReference _freezeAction;
        [SerializeField] private float _sprayDistance = 5f;
        [SerializeField] private float _sprayRadius = 0.5f;
        [SerializeField] private Vector3 _foamUp = Vector3.up;

        [Header("Animation")]
        [SerializeField] private Vector3 _startingPosition;
        [SerializeField] private Vector3 _animPosOut;
        [SerializeField] private float _animDuration = 0.2f;
        [SerializeField] private Ease _animEase = Ease.OutQuad;

        [Inject] private Camera _camera;

        [SerializeField] private List<AudioClip> _clips;
        private AudioSource _audioSource;
       
        protected override void OnEnable()
        {
            base.OnEnable();
            
            OnPickupAfter.AddListener(ResetStartingAnimationPos);
            _audioSource = GetComponent<AudioSource>();
        }
        
        protected override void OnDisable()
        {
            base.OnDisable();
            
            OnPickupAfter.RemoveListener(ResetStartingAnimationPos);
        }
        
        protected override void OnUse()
        {
            base.OnUse();
            
            Animate();
            PlayRandomSFX();

            if (!IsOwner) return;
            
            var ray = _camera.ScreenPointToRay(Mouse.current.position.ReadValue());

            if (!Physics.Raycast(ray, out var hit, _sprayDistance, LayerMask.GetMask("Dirt"), QueryTriggerInteraction.Ignore)) return;
            var cleanableDecal = hit.collider.GetComponent<CleanableDecal>();

            if (!cleanableDecal) return;

            if (!cleanableDecal.cleaningAvailableTo.HasFlag(CleaningAvailableTo.Sprayer)) 
            {
                if(!_isMonologue) StartCoroutine(MonologueWhenWrongTool());
                return; 
            }
            
            if (cleanableDecal.decalProjector.fadeFactor <= 0) return;
            
            var position = hit.point;
            var forward = -hit.normal;
            var textureCord = hit.textureCoord;
            var radius = _sprayRadius;
            CleaningFoamSpawner.instance.Spawn(position, forward, cleanableDecal, textureCord, radius);
        }

        // [ServerRpc]
        // private void SpawnFoamServerRpc(Vector3 position, Quaternion rotation, CleanableDecal decal,
        //     Vector2 textureCord, float radius, NetworkConnection connection) =>
        //     SpawnFoamObserverRpc(position, rotation, decal, textureCord, radius, connection);
        //
        // [ObserversRpc]
        // private void SpawnFoamObserverRpc(Vector3 position, Quaternion rotation, CleanableDecal decal,
        //     Vector2 textureCord, float radius, NetworkConnection connection) =>
        //     CleaningFoamSpawner.instance.Spawn(position, rotation, decal, textureCord, radius, connection);
        

        private void ResetStartingAnimationPos(PickUpController controller) => _startingPosition = transform.localPosition;

        private void Animate()
        {
            transform.DOLocalMove(_animPosOut, _animDuration / 2).SetEase(_animEase)
                .OnComplete(() => transform.DOLocalMove(_startingPosition, _animDuration / 2).SetEase(_animEase));
        }

        private bool _isMonologue = false;

        //[FoldGroup("Monologue")]
        [SerializeField] private MonologueSO _myMonologue;
        private IEnumerator MonologueWhenWrongTool()
        {
            _isMonologue = true;
            yield return new WaitForSeconds(1f);
            if (_isMonologue)
            {
                MonologueManager.instance.PlayMonologue(_myMonologue);
            }
            yield return new WaitForSeconds(4f);
            _isMonologue = false;
        }
        private void PlayRandomSFX()
        {
            int randomIndex = UnityEngine.Random.Range(0, _clips.Count);
            AudioClip footstep = _clips[randomIndex];

            _audioSource.clip = footstep;
            _audioSource.Play();
        }
    }
}