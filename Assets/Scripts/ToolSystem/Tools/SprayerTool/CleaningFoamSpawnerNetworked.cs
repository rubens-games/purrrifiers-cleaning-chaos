﻿using CleanableSystem;
using FishNet;
using FishNet.Object;
using UnityEngine;

namespace ToolSystem.Tools.SprayerTool
{
    public class CleaningFoamSpawnerNetworked : NetworkBehaviour
    {
        [SerializeField] private CleaningFoam _cleaningFoamPrefab;
        
        [ServerRpc(RequireOwnership = false)]
        public void SpawnFoam(Vector3 spawnPosition, Vector3 forward, CleanableDecal cleanableDecal,
            Vector2 textureCord, float radius)
        {
            var clone = Instantiate(_cleaningFoamPrefab, spawnPosition, Quaternion.identity);
            
            clone.transform.forward = forward;

            clone.Initialize(cleanableDecal, textureCord, radius);

            // Spawn the clone on the network
            InstanceFinder.ServerManager.Spawn(clone.gameObject);
        }
    }
}