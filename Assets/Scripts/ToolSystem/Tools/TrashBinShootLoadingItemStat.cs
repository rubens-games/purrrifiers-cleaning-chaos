﻿using UISystem.HUD.ItemStats;
using UnityEngine;

namespace ToolSystem.Tools
{
    public class TrashBinShootLoadingItemStat : ItemStat
    {
        public override string statName => _statName;
        public override float maxValue => _maxShootLoading;
        public override float value => shootLoading;
        public override float minValue => 0;
        public override string statValueString => $"{shootLoading:F2}/{maxShootLoading}";
        public override Sprite statIcon => _statIcon;
        
        public float shootLoading
        {
            get => _shootLoading;
            set
            {
                if (value < 0) value = 0;
                if (value > _maxShootLoading) value = _maxShootLoading;

                _shootLoading = value;
                
                InvokeOnStatChange(this);
            }
        }
        
        public float maxShootLoading => _maxShootLoading;
        
        [SerializeField] private string _statName;
        [SerializeField] private Sprite _statIcon;
        [SerializeField] private float _maxShootLoading;

        private float _shootLoading;
    }
}