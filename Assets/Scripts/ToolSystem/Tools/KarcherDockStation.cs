using System;
using System.Collections.Generic;
using PickableSystem;
using UnityEngine;
using UnityEngine.VFX;

namespace ToolSystem.Tools
{
    public class KarcherDockStation : MonoBehaviour
    {
        public static readonly List<KarcherDockStation> allKarcherDockStations = new();
        
        public float refillSpeed => _refillSpeed;
        
        [SerializeField] private Collider _waterArea;
        [SerializeField] private float _refillSpeed = 1f;
        [SerializeField] private VisualEffect _areaEffect;

        private Pickable _pickable;
        private bool _isPlaying = true;

        private void Awake()
        {
            _pickable = GetComponent<Pickable>();
        }

        private void OnEnable()
        {
            allKarcherDockStations.Add(this);
            
            _pickable.OnPickup.AddListener(OnPickUp);
            _pickable.OnDrop.AddListener(OnDrop);
        }

        private void OnDisable()
        {
            allKarcherDockStations.Remove(this);
            
            _pickable.OnPickup.RemoveListener(OnPickUp);
            _pickable.OnDrop.RemoveListener(OnDrop);
        }

        private void OnPickUp(PickUpController controller) => _areaEffect.gameObject.SetActive(false);

        private void OnDrop(PickUpController controller) => _areaEffect.gameObject.SetActive(true);

        public static KarcherDockStation GetClosestKarcherDockStation(Vector3 position)
        {
            KarcherDockStation closestKarcherDockStation = null;
            var closestDistance = float.MaxValue;
            
            foreach (var karcherDockStation in allKarcherDockStations)
            {
                var distance = Vector3.Distance(position, karcherDockStation.transform.position);
                
                if (!(distance < closestDistance)) continue;
                
                closestKarcherDockStation = karcherDockStation;
                closestDistance = distance;
            }

            return closestKarcherDockStation;
        }
        
        public bool IsKarcherInWaterArea(Karcher karcher) => 
            _waterArea.bounds.Contains(karcher.transform.position);
    }
}
