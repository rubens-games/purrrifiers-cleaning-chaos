﻿using System.Collections;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using PlayerSystem;
using UnityEngine;

namespace ToolSystem.Tools.HitableByTools
{
    public class DamagePlayerOnKarcherHit : HitableByTool
    {
        private bool playerGotHit
        {
            get => _playerGotHit.Value;
            [ServerRpc(RequireOwnership = false)] set => _playerGotHit.Value = value;
        }
        
        [SerializeField] private int _damage = 10;
        [SerializeField] private float _damageDelayBeforeHit;
        [SerializeField] private float _damageDelayAfterHit = 1.0f;
        [SerializeField] private bool _hitOnce;

        private bool _isHitting;
        private readonly SyncVar<bool> _playerGotHit = new(false);

        internal override void Hit(Tool tool)
        {
            if (_hitOnce && playerGotHit) return;

            base.Hit(tool);
        }

        public override void OnHit(Tool tool)
        {
            var player = tool.CurrentPickUpController.Player;
            
            if (!player) return;
            
            if (!player.IsOwner) return;

            if (_isHitting) return;
            
            StartCoroutine(HitPlayer(player));
            
            base.OnHit(tool);
        }
        
        private IEnumerator HitPlayer(Player player)
        {
            _isHitting = true;

            yield return new WaitForSeconds(_damageDelayBeforeHit);
            
            player.playerHitable.Hit(_damage);
            
            playerGotHit = true;
            
            yield return new WaitForSeconds(_damageDelayAfterHit);

            _isHitting = false;
        }
    }
}