﻿using System;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using UISystem.HUD.ItemStats;
using UnityEngine;

namespace ToolSystem.Tools
{
    public class TrashBinTrashesInBinItemStat : ItemStat
    {
        public override string statName => _statName;
        public override float maxValue => _maxTrashesInBin;
        public override float value => TrashesInBin;
        public override float minValue => 0;
        public override string statValueString => $"{TrashesInBin}/{_maxTrashesInBin}";
        public override Sprite statIcon => _statIcon;
        
        public int TrashesInBin
        {
            get => SyncTrashesInBin.Value;
            [ServerRpc(RequireOwnership = false)] set
            {
                if (value == SyncTrashesInBin.Value) return;
                
                if (value < 0) value = 0;
                if (value > _maxTrashesInBin) value = _maxTrashesInBin;

                SyncTrashesInBin.Value = value;
            }
        }
        
        [SerializeField] private string _statName;
        [SerializeField] private Sprite _statIcon;
        [SerializeField] private int _maxTrashesInBin;

        public readonly SyncVar<int> SyncTrashesInBin = new(0);

        private void OnEnable() => SyncTrashesInBin.OnChange += OnSyncTrashesInBinChange;

        private void OnDisable() => SyncTrashesInBin.OnChange -= OnSyncTrashesInBinChange;

        private void OnSyncTrashesInBinChange(int oldValue, int newValue, bool asServer) => InvokeOnStatChange(this);
    }
}