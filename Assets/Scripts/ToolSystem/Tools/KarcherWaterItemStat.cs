﻿using FishNet.CodeGenerating;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using NoReleaseDate.TickerSystem.Runtime;
using PickableSystem;
using UISystem.HUD.ItemStats;
using UnityEngine;

namespace ToolSystem.Tools
{
    public class KarcherWaterItemStat : ItemStat
    {
        public override string statName => _statName;
        public override float minValue => 0;
        public override float value => _waterAmount.Value;
        public override float maxValue => _maxWaterAmount;
        public override string statValueString => $"{_waterAmount.Value:0.0}L";

        public bool isKarcherInWaterArea
        {
            get
            {
                var karcherDockStation = KarcherDockStation.GetClosestKarcherDockStation(transform.position);
            
                return karcherDockStation && karcherDockStation.IsKarcherInWaterArea(_karcher);
            }
        }
        
        public float filledPercent => _waterAmount.Value / _maxWaterAmount;

        [SerializeField] private string _statName = "Water";
        [SerializeField] public float _maxWaterAmount = 10f;

        private static Ticker ticker => Ticker.instance;
        
        private Karcher _karcher;
        private Pickable _karcherPickable;

        [AllowMutableSyncType, SerializeField] private SyncVar<float> _waterAmount = new();

       [SerializeField] private AudioSource _audioSource;

        protected override void OnValidate()
        {
            base.OnValidate();

            _waterAmount.SetInitialValues(_maxWaterAmount);
        }

        private void Awake()
        {
            _karcher = GetComponent<Karcher>();
            _karcherPickable = GetComponent<Pickable>();
        }

        private void OnEnable()
        {
            _karcherPickable.OnPickup.AddListener(OnKarcherPickup);
            _karcherPickable.OnDrop.AddListener(OnKarcherDrop);
            _waterAmount.OnChange += OnWaterAmountChange;
            
            ticker.Unregister(UpdateWaterStatServerRpc);
        }
        
        private void OnDisable()
        {
            _karcherPickable.OnPickup.RemoveListener(OnKarcherPickup);
            _karcherPickable.OnDrop.RemoveListener(OnKarcherDrop);
            _waterAmount.OnChange -= OnWaterAmountChange;
        }

        private void OnKarcherPickup(PickUpController controller)
        {
            if (!controller.IsOwner) return;
            
            ticker.Register(UpdateWaterStatServerRpc, 0.2f);
        }
        
        private void OnKarcherDrop(PickUpController controller)
        {
            if (!controller.IsOwner) return;
            
            ticker.Unregister(UpdateWaterStatServerRpc);
        }

        private void OnWaterAmountChange(float oldValue, float newValue, bool asServer) 
        { 
            if(newValue> oldValue && !_audioSource.isPlaying) { _audioSource.Play(); }
            InvokeOnStatChange(this); 
        }

        [ServerRpc]
        private void UpdateWaterStatServerRpc(float time)
        {
            var karcherDockStation = KarcherDockStation.GetClosestKarcherDockStation(transform.position);
            
            if (!isKarcherInWaterArea) return;
            if (filledPercent >= 1) return;
            
            var refillAmount = karcherDockStation.refillSpeed * time;
            AddWater(refillAmount);
        }
        
        [ServerRpc(RequireOwnership = false)]
        public void RemoveWater(float amount)
        {
            if (isKarcherInWaterArea) return;
            
            _waterAmount.Value = Mathf.Clamp(_waterAmount.Value - amount, 0, _maxWaterAmount);
        }

        [ServerRpc(RequireOwnership = false)]
        public void AddWater(float amount) => _waterAmount.Value = Mathf.Clamp(_waterAmount.Value + amount, 0, _maxWaterAmount);
    }
}