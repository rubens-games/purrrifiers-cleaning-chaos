using System;
using System.Collections;
using CleanableSystem;
using FishNet.Connection;
using FishNet.Object;
using PickableSystem;
using ToolSystem.Tools.HitableByTools;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace ToolSystem.Tools
{
    public class Karcher : Tool
    {
        public override InputAction useAction => _useActionReference.action;
        public override InputAction freezeAction => _freezeActionReference.action;

        //[FoldGroup("Interaction")]
        [SerializeField] private InputActionReference _useActionReference;
        [SerializeField] private InputActionReference _freezeActionReference;
        [SerializeField] private KarcherWaterItemStat _waterItemStat;

        //[FoldGroup("Raycast")]
        [SerializeField] private LayerMask _stainLayer;
        [SerializeField] private float _rayMaxLength = 5.0f;
        [SerializeField] private float _rayRadius = 50.0f;
        [SerializeField] private float _margin = 0.2f;
        [SerializeField] private Transform _raycastStartPoint;
        [SerializeField] private KarcherRayDistanceEffects _distanceEffects;

        //[FoldGroup("Animation")]
        [SerializeField] private ParticleSystem _waterEffect;
        [SerializeField] private Vector3 _lookAtOffset;

        [SerializeField] private float _activationDelay = 0.5f;
        [SerializeField] private float _waterRemoveSpeed = 1.0f;

        private Ray ray;
        private RaycastHit _hit;
        private readonly RaycastHit[] _hits = new RaycastHit[5];
        private float _currentRayLength;

        [Inject] private Cleaner _cleaner;
        [Inject] private Camera _camera;

        private bool _isCleaning;
        private Vector3 _lastLookAt;

        private AudioSource _audioSource;
        //[FoldGroup("AudioSource")]
        [SerializeField] private AudioClip _loadingSFX;
        [SerializeField] private AudioClip _cleaningSFX;
        [SerializeField] private AudioClip _endSFX;

        #region Initialize

        protected override void Awake()
        {
            base.Awake();
            _audioSource = GetComponent<AudioSource>();
        }
        protected override void OnEnable()
        {
            base.OnEnable();
            
            OnAnimateBefore.AddListener(IKAnimation);
            OnDrop.AddListener(OnDropExecute);
        }
        
        protected override void OnDisable()
        {
            base.OnDisable();
            
            OnAnimateBefore.RemoveListener(IKAnimation);
            OnDrop.RemoveListener(OnDropExecute);
        }

        private void Start()
        {
            _currentRayLength = _rayMaxLength;
        }

        private void FixedUpdate()
        {
            if (!_isCleaning || !IsOwner) return;

            _waterItemStat.RemoveWater(_waterRemoveSpeed * Time.fixedDeltaTime);

            // Je�li nie ma wody, zatrzymaj czyszczenie
            if (_waterItemStat.value <= 0)
            {
                StopCleaningServerRpc();
                return;
            }

            InitializeRaycast();
        }

        #endregion

        #region Raycast

        private Ray CreateRay()
        {
            return new Ray(_raycastStartPoint.position, _raycastStartPoint.forward);
        }

        public void InitializeRaycast()
        {
            ray = CreateRay();

            Debug.DrawRay(ray.origin, ray.direction * _currentRayLength, Color.blue);

            _distanceEffects.OnKarcherDistanceScale(ray, _currentRayLength);

            if (Physics.Raycast(ray, out _hit, _currentRayLength))
            {
                if (_currentRayLength <= _rayMaxLength)
                {
                    _currentRayLength = _hit.distance + _margin;
                }

                // Obs�uga innych obiekt�w (np. "DamagePlayerOnKarcherHit")
                if (_hit.collider.TryGetComponent(out DamagePlayerOnKarcherHit hitable))
                {
                    hitable.Hit(this);
                }
            }
            else
            {
                ResetRay();
            }

            ProcessHits();
        }

        private void ProcessHits()
        {
            // Liczba trafie�
            int hitCount = Physics.RaycastNonAlloc(ray, _hits, _currentRayLength + _margin, _stainLayer);

            if (hitCount > 0)
            {
                foreach (var hit in _hits)
                {
                    if (hit.collider == null) continue;
                    HandleDirtHit(hit);
                }
            }

            ClearHitsArray();
        }

        private void HandleDirtHit(RaycastHit hit)
        {
            if (hit.collider.TryGetComponent(out CleanableDecal cleanableDecal))
            {
                if (!cleanableDecal.cleaningAvailableTo.HasFlag(CleaningAvailableTo.Karcher)) 
                {
                    if(!_isMonologue) StartCoroutine(MonologueWhenWrongTool());
                    return; 
                }

                Vector2 textureCoord = hit.textureCoord;
                cleanableDecal.CleanMe(textureCoord, _rayRadius, Owner);
                CleanerStopCleaningServerRpc();
            }
        }

        private void ResetRay()
        {
            _currentRayLength = _rayMaxLength;
            ClearHitsArray();
            CleanerStopCleaningServerRpc();
        }

        private void ClearHitsArray()
        {
            Array.Clear(_hits, 0, _hits.Length);
        }

        #endregion

        #region Monologue

        private bool _isMonologue = false;

        //[FoldGroup("Monologue")]
        [SerializeField] private MonologueSO _myMonologue;
        private IEnumerator MonologueWhenWrongTool()
        {
            _isMonologue = true;
            yield return new WaitForSeconds(1f);
            if (_isMonologue)
            {
                Debug.Log("MONO");
                MonologueManager.instance.PlayMonologue(_myMonologue);
            }
            yield return new WaitForSeconds(4f);
            _isMonologue = false;
        }

        #endregion

        #region Interaction

        protected override void OnUse()
        {
            base.OnUse();

            if (!IsOwner) return;
            
            StartCleaning();
        }

        protected override void OnStopUse()
        {
            base.OnStopUse();

            StopCleaning();
        }
        
        private void OnDropExecute(PickUpController pickUpController)
        {
            StopCleaning();
        }

        private void StartCleaning()
        {
            if (_waterItemStat.value <= 0) return;
            
            StartCoroutine(StartCleaningCoroutine());
        }

        private IEnumerator StartCleaningCoroutine()
        {
            _audioSource.loop = true;
            _audioSource.clip = _loadingSFX;
            _audioSource.volume = 1;
            _audioSource.Play();
            yield return new WaitForSeconds(_activationDelay);
            _audioSource.volume = .4f;
            _audioSource.clip = _cleaningSFX;
            _audioSource.Play();
            PlayWaterEffectServerRpc(true);

            if (IsOwner) _isCleaning = true;
        }

        private void StopCleaning()
        {
            _cleaner.StopCleaning(Owner.GetHashCode());
            
            _isMonologue = false;
            StopAllCoroutines();
            _audioSource.loop = false;
            _audioSource.clip = _endSFX;
            _audioSource.volume = 1;
            _audioSource.Play();
            PlayWaterEffectServerRpc(false);

            if (IsOwner) _isCleaning = false;
        }

        #endregion

        #region Animation

        private void IKAnimation(PickUpController pickUpController)
        {
            if (!IsOwner) return;
            
            var playerLookingAt = pickUpController.Player.lookingAt.GetLookingAtPoint();
            var nextLookAt = Vector3.Lerp(_lastLookAt, playerLookingAt, Time.deltaTime * 10.0f);
            var direction = (nextLookAt + _lookAtOffset) - transform.position;
            
            transform.forward = direction;
            
            _lastLookAt = nextLookAt;
            
            UpdateKarcherPositionServerRpc(direction, _lastLookAt);
        }

        #endregion

        #region Server

        [ServerRpc]
        private void CleanerStopCleaningServerRpc() => CleanerStopCleaningObserverRpc();
        
        [ObserversRpc]
        private void CleanerStopCleaningObserverRpc() => _cleaner.StopCleaning(Owner.GetHashCode());
        
        [ServerRpc]
        private void StopCleaningServerRpc() => StopCleaningObserverRpc();

        [ObserversRpc]
        private void StopCleaningObserverRpc() => StopCleaning();

        [ServerRpc]
        private void UpdateKarcherPositionServerRpc(Vector3 direction, Vector3 lastLookAt) =>
            UpdateKarcherPositionObserverRpc(direction, lastLookAt);

        [ObserversRpc]
        private void UpdateKarcherPositionObserverRpc(Vector3 direction, Vector3 lastLookAt)
        {
            if (IsOwner) return;
            
            transform.forward = direction;
            _lastLookAt = lastLookAt;
        }

        [ServerRpc(RequireOwnership = false)]
        private void PlayWaterEffectServerRpc(bool play) => PlayWaterEffectObserverRpc(play);

        [ObserversRpc]
        private void PlayWaterEffectObserverRpc(bool play)
        {
            if (play) _waterEffect.Play();
            else _waterEffect.Stop();
        }

        #endregion
    }
}