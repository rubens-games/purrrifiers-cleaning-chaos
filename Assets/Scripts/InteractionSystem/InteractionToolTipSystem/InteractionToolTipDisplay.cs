﻿using PlayerSystem;
using UnityEngine;
using Zenject;

namespace InteractionSystem.InteractionToolTipSystem
{
    public class InteractionToolTipDisplay : MonoBehaviour
    {
        public string ToolTip;

        [Inject] private InteractionToolTipDisplayManager _interactionToolTipDisplayManager;
        
        private Interactable _interactable;

        private void Awake() => _interactable = GetComponent<Interactable>();

        private void OnEnable() => _interactable.OnObserverChange.AddListener(OnObserverChange);

        private void OnDisable() 
        { 
            _interactable.OnObserverChange.RemoveListener(OnObserverChange);
            _interactionToolTipDisplayManager.HideTooltip(this);
        }

        private void OnObserverChange(Player player)
        {
            var canShowTooltip = _interactable.CanInteract && !_interactable.IsSleeping && player && player.IsOwner &&
                                 player.interactionController.CanInteract;
            
            if (canShowTooltip) _interactionToolTipDisplayManager.ShowTooltip(this);
            else _interactionToolTipDisplayManager.HideTooltip(this);
        }
    }
}