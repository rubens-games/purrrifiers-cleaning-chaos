using UltimateClean;
using UnityEngine;

namespace InteractionSystem.InteractionToolTipSystem
{
    public class InteractionToolTipDisplayManager : MonoBehaviour
    {
        [SerializeField] private InteractionToolTipUI interactionToolTipUI;
        [SerializeField] private RectTransform _content;

        private InteractionToolTipUI[] ToolTips =>
            _content ? _content.GetComponentsInChildren<InteractionToolTipUI>() : null;

        public void ShowTooltip(InteractionToolTipDisplay toolTip)
        {
            var clone = Instantiate(interactionToolTipUI, _content);
            clone.UpdateUI(toolTip);
        }
        
        public void HideTooltip(InteractionToolTipDisplay toolTip)
        {
            if (!toolTip || ToolTips == null || ToolTips is { Length: <= 0 }) return;

            foreach (var toolTipUI in ToolTips)
            {
                if (!toolTipUI) continue;

                if (toolTipUI.ToolTip != toolTip) continue;
                
                Destroy(toolTipUI.gameObject);
                
                return;
            }
        }
    }
}
