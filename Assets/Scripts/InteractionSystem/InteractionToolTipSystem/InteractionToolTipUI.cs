﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace InteractionSystem.InteractionToolTipSystem
{
    public class InteractionToolTipUI : MonoBehaviour
    {
        public InteractionToolTipDisplay ToolTip { get; private set; }
        
        [SerializeField] private TMP_Text _text;
        
        [Header("Animation")]
        [SerializeField] private float _animDuration = 0.2f;
        [SerializeField] private Ease _animEase = Ease.OutBack;
        
        private void OnEnable()
        {
            transform.localScale = Vector3.zero;
            transform.DOScale(Vector3.one, _animDuration)
                .SetEase(_animEase)
                .SetUpdate(true)
                .SetId(GetInstanceID());
        }

        private void OnDisable()
        {
            DOTween.Kill(GetInstanceID());
        }

        public void UpdateUI(InteractionToolTipDisplay interactable)
        {
            ToolTip = interactable;
            UpdateUI(interactable.ToolTip);
        }
        
        public void UpdateUI(string toolTip, Sprite sprite = null)
        {
            _text.text = toolTip;

            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)transform);
        }
    }
}