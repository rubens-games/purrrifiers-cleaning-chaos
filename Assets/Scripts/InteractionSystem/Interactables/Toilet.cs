using System.Collections;
using System.Collections.Generic;
using CameraSystem;
using DG.Tweening;
using PlayerSystem;
using PlayerSystem.RigSystem;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace InteractionSystem.Interactables
{
    public class Toilet : Interactable, IPlayerLockHandler
    {
        #region IPlayerLockHandler

        public bool isLocked => true;

        #endregion

        #region Interactable

        public override bool CanInteract => !_isToiletOccupied;

        public override bool IsSleeping => _isToiletOccupied;

        #endregion
        
        [SerializeField] private Transform _playerSitPosition;
        [SerializeField] private Transform _cameraTarget;
        [SerializeField] private float _sitDuration = 30f;
        [SerializeField] private GameObject _sliderGO;
        [SerializeField] private Slider _slider;

        [Inject] private CameraManager _cameraManager;
        
        private bool _isToiletOccupied;
        private Transform _lastFollowTarget;
        private Transform _lastCameraFollowTarget;
        
        private void OnEnable() => OnInteract.AddListener(OnInteractHandler);

        private void OnDisable() => OnInteract.RemoveListener(OnInteractHandler);

        [SerializeField] private List<AudioClip> _clips;
        private AudioSource _audioSource;
        private void PlayRandomSFX()
        {
            int randomIndex = Random.Range(0, _clips.Count);
            AudioClip clip = _clips[randomIndex];

            _audioSource.clip = clip;
            _audioSource.Play();
        }
        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }
        private void OnInteractHandler(Player player)
        {
            if (_isToiletOccupied) return;

            if (player.IsOwner) 
                Muffin.StopAllPoisonCoroutines(player);
            
            player.StartCoroutine(SitOnToilet(player));
        }
        
        private IEnumerator SitOnToilet(Player player)
        {
            _isToiletOccupied = true;

            PlayRandomSFX();
            player.playerLock.Register(this);
            
            _lastFollowTarget = player.playerModel.followTargetSmooth.target;
            player.playerModel.followTargetSmooth.target = _playerSitPosition;

            if (player.IsOwner)
            {
                _lastCameraFollowTarget = _cameraManager.FollowTargetSmooth.target;
                _cameraManager.FollowTargetSmooth.target = _cameraTarget;
            }

            player.playerRigController.ActiveRigCombination(RigType.SittingOnToilet);
            
            player.playerAnimation.IsSitOnToilet = true;
            
            // Update slider and wait for sit duration
            SliderSetActive(true);
            SliderSetValue(0);
            yield return _slider.DOValue(1, _sitDuration).SetEase(Ease.Linear).WaitForCompletion();
            
            LeaveToilet(player);
        }
        
        private void LeaveToilet(Player player)
        {
            _isToiletOccupied = false;
            
            player.playerLock.Unregister(this);
            
            player.playerModel.followTargetSmooth.target = _lastFollowTarget;
            
            if (player.IsOwner)
                _cameraManager.FollowTargetSmooth.target = _lastCameraFollowTarget;

            player.playerRigController.ActiveRigCombination(RigType.Default);
            
            player.playerAnimation.IsSitOnToilet = false;
            
            SliderSetActive(false);
        }

        private void SliderSetActive(bool value) => _sliderGO.gameObject.SetActive(value);

        private void SliderSetValue(float value) => _slider.value = value;
    }
}
