using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PlayerSystem;
using UISystem.HUD.MyStats;
using UnityEngine;
using Zenject;

namespace InteractionSystem.Interactables
{
    public class Muffin : Interactable
    {
        [Header("Poison Settings")]
        [SerializeField] private float _poisonEffectDelay = 10f;
        [SerializeField] private float _poisonDuration = 5f;
        [SerializeField] private int _hitDamage = 10;
        [SerializeField] private float _hitInterval = 1f;
        [SerializeField] private Sprite _poisonIcon;

        [Header("Speed Settings")]
        [SerializeField] private float _speedEffectDelay;
        [SerializeField] private float _speedDuration = 500f;
        [SerializeField] private float _speedMultiplier = 1.2f;
        [SerializeField] private Sprite _speedIcon;

        [Inject] private MyStatsUI _myStatsUI;
        [Inject] private PlayerManager _playerManager;
        
        private ItemData _itemData;
        private MyStatsUIEffectsPrefab.EffectData _poisonEffectData;
        private MyStatsUIEffectsPrefab.EffectData _speedEffectData;
        
        private static Coroutine _poisonCoroutine;
        private static Coroutine _speedCoroutine;
        private static List<Coroutine> _poisonCoroutines = new();
        private static List<Coroutine> _speedCoroutines = new();
        
        private static List<MyStatsUIEffectsPrefab.EffectData> _poisonEffectDataList = new();
        private static List<MyStatsUIEffectsPrefab.EffectData> _speedEffectDataList = new();

        [SerializeField] private MonologueSO _goodMuffinMono;
        private AudioSource _audioSource;
        
        private void Awake()
        {
            _itemData = GetComponent<ItemData>();
            _audioSource = GetComponent<AudioSource>();
        }
        
        private void OnDestroy()
        {
            _poisonCoroutine = null;
            _speedCoroutine = null;
            _poisonCoroutines.Clear();
            _speedCoroutines.Clear();
            _poisonEffectDataList.Clear();
            _speedEffectDataList.Clear();
        }

        protected override void OnValidate()
        {
            base.OnValidate();
            
            if (_poisonDuration < 0)
                _poisonDuration = 0;
            
            if (_hitDamage < 0)
                _hitDamage = 0;

            if (_hitInterval < 0)
                _hitInterval = 0;
            
            if (_hitInterval > _poisonDuration)
                _hitInterval = _poisonDuration;
        }

        private void OnEnable() => OnInteract.AddListener(OnInteractHandler);

        private void OnDisable() => OnInteract.RemoveListener(OnInteractHandler);
        
        private void OnInteractHandler(Player player)
        {
            if (player.IsOwner && _itemData.CurrentState == ItemState.poisoned && _poisonCoroutine == null)
            {
                _poisonCoroutine = player.StartCoroutine(PoisonPlayer(player));
                _poisonCoroutines.Add(_poisonCoroutine);
            }
            
            if (player.IsOwner && _itemData.CurrentState == ItemState.good && _speedCoroutine == null)
            {
                _speedCoroutine = player.StartCoroutine(SpeedPlayer(player));
                _speedCoroutines.Add(_speedCoroutine);
            }
            
            if (transform.parent)
                if (transform.parent.parent)
                {
                    var muffinFoam = transform.parent.parent.GetComponent<MuffinFormInteractable>();
                    if (muffinFoam) muffinFoam.ReadyMuffins--;
                }

            gameObject.SetActive(false);
        }
        
        private IEnumerator PoisonPlayer(Player player)
        {
            _audioSource.Play();

            yield return new WaitForSeconds(_poisonEffectDelay);
            
            player.playerHitable.OnDeath.AddListener(OnPlayerDeath);
            
            OvenControler.Instance.PoisonedPlayerCount++;
            
            if (player.IsOwner)
                QuestCustomManager.instance.GoToToilet();

            _poisonEffectData = new MyStatsUIEffectsPrefab.EffectData
            {
                title = "Poisoned",
                sprite = _poisonIcon,
                startTime = DateTime.Now,
                endTime = DateTime.Now.AddSeconds(_poisonDuration)
            };
                
            _poisonEffectDataList.Add(_poisonEffectData);
            
            _myStatsUI.myStatsUIEffects.SpawnEffect(_poisonEffectData);
            
            var playerHitable = player.playerHitable;

            yield return new WaitForSeconds(3f);
            playerHitable.Hit(_hitDamage);
            
            var poisonTimer = _poisonDuration;
            var hitTimer = _hitInterval;
            
            while (poisonTimer > 0)
            {
                if (!playerHitable.isAlive) break;
                
                if (hitTimer <= 0)
                {
                    playerHitable.Hit(_hitDamage);
                    hitTimer = _hitInterval;
                }

                poisonTimer -= Time.deltaTime;
                hitTimer -= Time.deltaTime;
                
                yield return null;
            }
            
            _poisonEffectData.Dispose();
            _poisonEffectDataList.Remove(_poisonEffectData);
            
            _poisonCoroutines.Remove(_poisonCoroutine);

            OvenControler.Instance.PoisonedPlayerCount--;
            QuestCustomManager.instance.NoToilet();
            
            playerHitable.OnDeath.RemoveListener(OnPlayerDeath);
            
            _poisonCoroutine = null;
            
            yield break;

            void OnPlayerDeath(int hitpoints) => StopAllPoisonCoroutines(player);
        }
        
        private IEnumerator SpeedPlayer(Player player)
        {
            _audioSource.Play();

            yield return new WaitForSeconds(_speedEffectDelay);
            
            player.playerHitable.OnDeath.AddListener(OnPlayerDeath);
            
            MonologueManager.instance.PlayMonologue(_goodMuffinMono);

            _speedEffectData = new MyStatsUIEffectsPrefab.EffectData
            {
                title = "Speed",
                sprite = _speedIcon,
                startTime = DateTime.Now,
                endTime = DateTime.Now.AddSeconds(_speedDuration)
            };
            
            _speedEffectDataList.Add(_speedEffectData);
            
            _myStatsUI.myStatsUIEffects.SpawnEffect(_speedEffectData);
            
            player.character.maxWalkSpeed *= _speedMultiplier;
            player.character.maxWalkSprintSpeed *= _speedMultiplier;
            player.character.maxWalkCrouchedSprintSpeed *= _speedMultiplier;
            player.character.maxWalkSpeedCrouched *= _speedMultiplier;
            
            yield return new WaitForSeconds(_speedDuration);
            
            player.character.maxWalkSpeed /= _speedMultiplier;
            player.character.maxWalkSprintSpeed /= _speedMultiplier;
            player.character.maxWalkCrouchedSprintSpeed /= _speedMultiplier;
            player.character.maxWalkSpeedCrouched /= _speedMultiplier;
            
            _speedEffectData.Dispose();
            _speedEffectDataList.Remove(_poisonEffectData);
            
            player.playerHitable.OnDeath.RemoveListener(OnPlayerDeath);
            
            _speedCoroutine = null;
            
            yield break;
            
            void OnPlayerDeath(int hitpoints) => StopAllSpeedCoroutines(player);
        }
        
        public static void StopAllPoisonCoroutines(Player player)
        {
            if (_poisonCoroutines == null) return;
            
            if (!player) return;

            foreach (var coroutine in _poisonCoroutines.Where(coroutine => coroutine != null))
            {
                OvenControler.Instance.PoisonedPlayerCount--;
                player.StopCoroutine(coroutine);
            }
            
            _poisonEffectDataList.ForEach(effectData => effectData.Dispose());

            _poisonEffectDataList.Clear();
            _poisonCoroutines.Clear();
            
            _poisonCoroutine = null;
            
            QuestCustomManager.instance.NoToilet();
        }
        
        public static void StopAllSpeedCoroutines(Player player)
        {
            if (_speedCoroutines == null) return;
            if (!player) return;

            foreach (var coroutine in _speedCoroutines.Where(coroutine => coroutine != null)) 
                player.StopCoroutine(coroutine);
            
            _speedEffectDataList.ForEach(effectData => effectData.Dispose());

            _speedEffectDataList.Clear();
            _speedCoroutines.Clear();
            
            _speedCoroutine = null;
        }
    }
}
