using FishNet.Object;
using FishNet.Object.Synchronizing;
using InteractionSystem.InteractionToolTipSystem;
using OutlineSystem;
using PlayerSystem;
using UnityEngine;
using UnityEngine.Events;

namespace InteractionSystem
{
    [SelectionBase]
    [RequireComponent(typeof(InteractableOutlineController))]
    [RequireComponent(typeof(InteractionToolTipDisplay))]
    public class Interactable : NetworkBehaviour
    {
        public UnityEvent<Player> OnObserverChange = new();
        
        [field: Header("Networked")]
        [field: SerializeField] public UnityEvent<Player> OnInteractBefore { get; private set; } = new();
        [field: SerializeField] public UnityEvent<Player> OnInteract { get; private set; } = new();
        [field: SerializeField] public UnityEvent<Player> OnInteractAfter { get; private set; } = new();

        [field: SerializeField] public UnityEvent<Player> OnFailedInteractionBefore { get; private set; } = new();
        [field: SerializeField] public UnityEvent<Player> OnFailedInteraction { get; private set; } = new();
        [field: SerializeField] public UnityEvent<Player> OnFailedInteractionAfter { get; private set; } = new();
        
        [field: SerializeField] public UnityEvent<Player> OnPointerEnter { get; private set; } = new();
        [field: SerializeField] public UnityEvent<Player> OnPointerExit { get; private set; } = new();
        
        [field: Header("Server")] 
        [field: SerializeField] public UnityEvent<Player> OnServerInteractBefore { get; private set; } = new();
        [field: SerializeField] public UnityEvent<Player> OnServerInteract { get; private set; } = new();
        [field: SerializeField] public UnityEvent<Player> OnServerInteractAfter { get; private set; } = new();

        [field: SerializeField] public UnityEvent<Player> OnFailedLocalInteractBefore { get; private set; } = new();
        [field: SerializeField] public UnityEvent<Player> OnFailedLocalInteract { get; private set; } = new();
        [field: SerializeField] public UnityEvent<Player> OnFailedLocalInteractAfter { get; private set; } = new();
        
        [field: Header("Local")] 
        [field: SerializeField] public UnityEvent<Player> OnLocalInteractBefore { get; private set; } = new();
        [field: SerializeField] public UnityEvent<Player> OnLocalInteract { get; private set; } = new();
        [field: SerializeField] public UnityEvent<Player> OnLocalInteractAfter { get; private set; } = new();

        [field: SerializeField] public UnityEvent<Player> OnFailedInteractBefore { get; private set; } = new();
        [field: SerializeField] public UnityEvent<Player> OnFailedInteract { get; private set; } = new();
        [field: SerializeField] public UnityEvent<Player> OnFailedInteractAfter { get; private set; } = new();
        
        public virtual bool IsSleeping => false;
        
        public virtual bool CanInteract
        {
            get => _canInteract.Value;
            [ServerRpc(RequireOwnership = false)] 
            set => _canInteract.Value = value;
        }

        public virtual bool CanInteractWithPlayer(Player player) => CanInteract;

        protected readonly SyncVar<bool> _canInteract = new(true);

        public void Interact(Player player)
        {
            if (CanInteractWithPlayer(player))
            {
                OnLocalInteractBefore?.Invoke(player);
                OnLocalInteract?.Invoke(player);
                OnLocalInteractAfter?.Invoke(player);
            }
            else
            {
                OnFailedLocalInteractBefore?.Invoke(player);
                OnFailedLocalInteract?.Invoke(player);
                OnFailedLocalInteractAfter?.Invoke(player);
            }
            
            InteractServerRpc(player);
        }

        [ServerRpc(RequireOwnership = false)]
        private void InteractServerRpc(Player player)
        {
            if (CanInteractWithPlayer(player))
            {
                OnServerInteractBefore?.Invoke(player);
                OnServerInteract?.Invoke(player);
                OnServerInteractAfter?.Invoke(player);
            }
            else
            {
                OnFailedInteractionBefore?.Invoke(player);
                OnFailedInteraction?.Invoke(player);
                OnFailedInteractionAfter?.Invoke(player);
            }
            
            InteractObserversRpc(player);
        }

        [ObserversRpc]
        private void InteractObserversRpc(Player player)
        {
            if (CanInteractWithPlayer(player))
            {
                OnInteractBefore?.Invoke(player);
                OnInteract?.Invoke(player);
                OnInteractAfter?.Invoke(player);
            }
            else
            {
                OnFailedInteractBefore?.Invoke(player);
                OnFailedInteract?.Invoke(player);
                OnFailedInteractAfter?.Invoke(player);
            }
        }
        
        public void PointerEnter(Player player) => OnPointerEnter?.Invoke(player);

        // [ServerRpc(RequireOwnership = false)]
        // private void PointerEnterServerRpc(Player player) => PointerEnterObserversRpc(player);
        //
        // [ObserversRpc]
        // private void PointerEnterObserversRpc(Player player) => OnPointerEnter?.Invoke(player);

        public void PointerExit(Player player) => OnPointerExit?.Invoke(player);

        // [ServerRpc(RequireOwnership = false)]
        // private void PointerExitServerRpc(Player player) => PointerExitObserversRpc(player);
        //
        // [ObserversRpc]
        // private void PointerExitObserversRpc(Player player) => OnPointerExit?.Invoke(player);
    }
}
