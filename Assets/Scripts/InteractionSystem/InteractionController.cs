using FishNet.Connection;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using PlayerSystem;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace InteractionSystem
{
    public class InteractionController : NetworkBehaviour
    {
        public UnityEvent<Interactable> OnCurrentInteractableChangeBefore = new();
        public UnityEvent<Interactable> OnCurrentInteractableChange = new();
        public UnityEvent<Interactable> OnCurrentInteractableChangeAfter = new();
        
        [field: Header("Networked")] 
        [field: SerializeField] public UnityEvent OnInteractBefore { get; private set; } = new();
        [field: SerializeField] public UnityEvent OnInteract { get; private set; } = new();
        [field: SerializeField] public UnityEvent OnInteractAfter { get; private set; } = new();

        [field: SerializeField] public UnityEvent OnFailedInteractionBefore { get; private set; } = new();
        [field: SerializeField] public UnityEvent OnFailedInteraction { get; private set; } = new();
        [field: SerializeField] public UnityEvent OnFailedInteractionAfter { get; private set; } = new();
        
        [field: Header("Server")] 
        [field: SerializeField] public UnityEvent OnServerInteractBefore { get; private set; } = new();
        [field: SerializeField] public UnityEvent OnServerInteract { get; private set; } = new();
        [field: SerializeField] public UnityEvent OnServerInteractAfter { get; private set; } = new();

        [field: SerializeField] public UnityEvent OnFailedServerInteractBefore { get; private set; } = new();
        [field: SerializeField] public UnityEvent OnFailedServerInteract { get; private set; } = new();
        [field: SerializeField] public UnityEvent OnFailedServerInteractAfter { get; private set; } = new();
        
        [field: Header("Local")] 
        [field: SerializeField] public UnityEvent OnLocalInteractBefore { get; private set; } = new();
        [field: SerializeField] public UnityEvent OnLocalInteract { get; private set; } = new();
        [field: SerializeField] public UnityEvent OnLocalInteractAfter { get; private set; } = new();

        [field: SerializeField] public UnityEvent OnFailedLocalInteractBefore { get; private set; } = new();
        [field: SerializeField] public UnityEvent OnFailedLocalInteract { get; private set; } = new();
        [field: SerializeField] public UnityEvent OnFailedLocalInteractAfter { get; private set; } = new();

        public Interactable CurrentInteractable
        {
            get => _currentInteractable;
            set
            {
                if (_currentInteractable == value) return;

                var prevInteractable = _currentInteractable;
                
                OnCurrentInteractableChangeBefore?.Invoke(prevInteractable);
                
                _currentInteractable = value;

                if (!value) _interactAction.action.Disable();
                else
                {
                    if (enabled) _interactAction.action.Enable();
                    else _interactAction.action.Disable();
                }
                
                OnCurrentInteractableChange?.Invoke(_currentInteractable);

                if (prevInteractable) prevInteractable.OnObserverChange?.Invoke(null);
                if (_currentInteractable) _currentInteractable.OnObserverChange?.Invoke(_player);

                OnCurrentInteractableChangeAfter?.Invoke(_currentInteractable);
                
                if (prevInteractable) prevInteractable.PointerExit(_player);
                if (_currentInteractable) _currentInteractable.PointerEnter(_player);
            }
        }
        
        public bool CanInteract
        {
            get => _canInteract.Value;
            [ServerRpc]
            set => _canInteract.Value = value;
        }

        private readonly SyncVar<bool> _canInteract = new(true);
        
        [SerializeField] private InputActionReference _interactAction;
        [SerializeField] private InteractableDetector _interactableDetector;
        [SerializeField] private Player _player;
        
        private Interactable _currentInteractable;

        private void OnEnable()
        {
            _canInteract.OnChange += OnCanInteractChanged;
        }

        private void OnDisable()
        {
            _canInteract.OnChange -= OnCanInteractChanged;
        }

        public override void OnOwnershipClient(NetworkConnection prevOwner)
        {
            base.OnOwnershipClient(prevOwner);

            if (IsOwner)
            {
                if (CanInteract) EnableInteraction();
                else DisableInteraction();
            }
            else DisableInteraction();
        }

        private void OnCanInteractChanged(bool prevCanInteract, bool newCanInteract, bool asServer)
        {
            if (!IsOwner) return;
            
            if (newCanInteract) EnableInteraction();
            else DisableInteraction();
        }

        private void OnDetectedChange(Interactable detectedInteractable, RaycastHit raycastHit)
        {
            if (detectedInteractable && detectedInteractable.IsSleeping) return;
            
            CurrentInteractable = detectedInteractable;
        }

        private void OnInteractActionPerformed(InputAction.CallbackContext context)
        {
            if (!CurrentInteractable) return;
            
            Interact(CurrentInteractable);
        }

        [ContextMenu("Enable Interaction")]
        public void EnableInteractionContextMenu() => 
            CanInteract = true;

        private void EnableInteraction()
        {
            if (!IsOwner) return;
            
            _interactableDetector.OnDetectedChange.AddListener(OnDetectedChange);
            _interactAction.action.performed += OnInteractActionPerformed;
            _interactAction.action.Enable();
            
            CurrentInteractable = _interactableDetector.Detected;
        }

        private void DisableInteraction()
        {
            if (!IsOwner) return;
            
            _interactableDetector.OnDetectedChange.RemoveListener(OnDetectedChange);
            _interactAction.action.performed -= OnInteractActionPerformed;
            _interactAction.action.Disable();
            
            CurrentInteractable = null;
        }

        #region Interaction

        public void Interact(Interactable interactable)
        {
            if (!IsOwner) return;
            
            if (CanInteract)
            {
                OnLocalInteractBefore?.Invoke();
                OnLocalInteract?.Invoke();
                OnLocalInteractAfter?.Invoke();
            }
            else
            {
                OnFailedLocalInteractBefore?.Invoke();
                OnFailedLocalInteract?.Invoke();
                OnFailedLocalInteractAfter?.Invoke();
            }
            
            InteractServerRpc(interactable);
        }

        [ServerRpc(RequireOwnership = true)]
        private void InteractServerRpc(Interactable interactable)
        {
            if (CanInteract)
            {
                OnServerInteractBefore?.Invoke();
                OnServerInteract?.Invoke();
                OnServerInteractAfter?.Invoke();
            }
            else
            {
                OnFailedServerInteractBefore?.Invoke();
                OnFailedServerInteract?.Invoke();
                OnFailedServerInteractAfter?.Invoke();
            }
            
            InteractObserversRpc(interactable);
        }

        [ObserversRpc]
        private void InteractObserversRpc(Interactable interactable)
        {
            if (CanInteract)
            {
                OnInteractBefore?.Invoke();

                if(IsOwner) interactable.Interact(_player);
            
                OnInteract?.Invoke();
                OnInteractAfter?.Invoke();
            }
            else
            {
                OnFailedInteractionBefore?.Invoke();
                OnFailedInteraction?.Invoke();
                OnFailedInteractionAfter?.Invoke();
            }
        }

        #endregion
    }
}
