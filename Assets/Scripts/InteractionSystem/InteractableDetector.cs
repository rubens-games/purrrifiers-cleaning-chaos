﻿using DetectorSystem;
using UnityEngine;
using Zenject;

namespace InteractionSystem
{
    public class InteractableDetector : Detector<Interactable>
    {
        #region Detector

        protected override Ray Ray => new(_camera.transform.position, _camera.transform.forward);
        protected override LayerMask IgnoredLayerMask => _ignoredLayerMask;
        protected override float RayMaxDistance => _rayMaxDistance;

        #endregion
        
        [SerializeField] private LayerMask _ignoredLayerMask = -5;
        [SerializeField] private float _rayMaxDistance = 10;
        
        [Inject] private Camera _camera;
    }
}