﻿using EPOOutline;
using UnityEngine;

namespace OutlineSystem
{
    public abstract class InteractableOutline : Outlinable
    {
        protected abstract Color outlineColor { get; }
        
        protected override void OnValidate()
        {
            base.OnValidate();
            
            UpdateColor();
        }

        protected override void Awake()
        {
            base.Awake();
            
            UpdateColor();
            SetStartingState();
        }

        private void UpdateColor() => OutlineParameters.Color = outlineColor;

        private void SetStartingState() => enabled = false;
    }
}