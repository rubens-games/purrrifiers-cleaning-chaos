﻿using DetectorSystem;
using InteractionSystem;
using PlayerSystem;
using UnityEngine;
using Zenject;

namespace OutlineSystem
{
    [RequireComponent(typeof(ActiveOutline),typeof(InactiveOutline))]
    public class InteractableOutlineController : MonoBehaviour
    {
        [Inject] private GameObjectDetector _gameObjectDetector;

        private Interactable _interactable;
        private ActiveOutline _activeOutline;
        private InactiveOutline _inactiveOutline;

        private void Awake()
        {
            _interactable = GetComponent<Interactable>();
            _activeOutline = GetComponent<ActiveOutline>();
            _inactiveOutline = GetComponent<InactiveOutline>();
        }

        private void OnEnable()
        {
            _interactable.OnObserverChange.AddListener(OnObserverChange);
        }
        
        private void OnDisable()
        {
            _interactable.OnObserverChange.RemoveListener(OnObserverChange);
        }
        
        private void OnObserverChange(Player player)
        {
            if (player)
            {
                if (!_interactable)
                {
                    DisableOutline();
                    return;
                }
            
                EnableOutline(_interactable.CanInteractWithPlayer(player));
            }
            else DisableOutline();
        }

        private void EnableOutline(bool canInteract)
        {
            _activeOutline.enabled = canInteract;
            _inactiveOutline.enabled = !canInteract;
        }
        
        private void DisableOutline()
        {
            _activeOutline.enabled = false;
            _inactiveOutline.enabled = false;
        }
    }
}