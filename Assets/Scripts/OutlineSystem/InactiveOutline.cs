﻿using UnityEngine;

namespace OutlineSystem
{
    public class InactiveOutline : InteractableOutline
    {
        protected override Color outlineColor => Color.red;
    }
}