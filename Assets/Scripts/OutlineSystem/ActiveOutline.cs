﻿using UnityEngine;

namespace OutlineSystem
{
    public class ActiveOutline : InteractableOutline
    {
        protected override Color outlineColor => new Color32(0x04, 0x7C, 0xFF, 0xFF);
    }
}