﻿using System;
using UnityEngine;

namespace CursorSystem
{
    [Serializable]
    public class CursorSettings
    {
        public bool IsVisible = true;
        public CursorLockMode LockMode = CursorLockMode.None;
        public Texture2D Texture;
        public Vector2 Hotspot = Vector2.zero;
        public CursorMode CursorMode = CursorMode.Auto;

        public class Builder
        {
            private readonly CursorSettings _cursorSettings = new();
            
            public Builder SetVisible(bool isVisible)
            {
                _cursorSettings.IsVisible = isVisible;
                return this;
            }
            
            public Builder SetLockMode(CursorLockMode lockMode)
            {
                _cursorSettings.LockMode = lockMode;
                return this;
            }
            
            public Builder SetTexture(Texture2D texture)
            {
                _cursorSettings.Texture = texture;
                return this;
            }
            
            public Builder SetHotspot(Vector2 hotspot)
            {
                _cursorSettings.Hotspot = hotspot;
                return this;
            }
            
            public Builder SetCursorMode(CursorMode cursorMode)
            {
                _cursorSettings.CursorMode = cursorMode;
                return this;
            }
            
            public CursorSettings Build()
            {
                return _cursorSettings;
            }
        }
        
        public static CursorSettings ShowAndUnlock =>
            new Builder().SetVisible(true).SetLockMode(CursorLockMode.None).Build();
        
        public static CursorSettings ShowAndLock =>
            new Builder().SetVisible(true).SetLockMode(CursorLockMode.Locked).Build();
        
        public static CursorSettings ShowAndConfined =>
            new Builder().SetVisible(true).SetLockMode(CursorLockMode.Confined).Build();
        
        public static CursorSettings HideAndLock =>
            new Builder().SetVisible(false).SetLockMode(CursorLockMode.Locked).Build();
        
        public static CursorSettings HideAndUnlock =>
            new Builder().SetVisible(false).SetLockMode(CursorLockMode.None).Build();
        
        public static CursorSettings HideAndConfined =>
            new Builder().SetVisible(false).SetLockMode(CursorLockMode.Confined).Build();
    }
}