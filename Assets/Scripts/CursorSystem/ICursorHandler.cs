﻿using Zenject;

namespace CursorSystem
{
    public interface ICursorHandler
    {
        public CursorSettings CursorSettings { get; }
    }
}