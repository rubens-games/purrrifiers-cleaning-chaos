﻿using UnityEngine;
using Zenject;

namespace CursorSystem
{
    public enum UnityLifecycleMethod
    {
        None,
        Awake,
        OnEnable,
        Start,
        OnDisable,
        OnDestroy
    }
    
    public class GenericCursorLockMode : MonoBehaviour, ICursorHandler
    {
        [field: SerializeField] public CursorSettings CursorSettings { get; set; } = CursorSettings.HideAndLock;

        [SerializeField] private UnityLifecycleMethod _registerAt = UnityLifecycleMethod.OnEnable;
        [SerializeField] private UnityLifecycleMethod _unregisterAt = UnityLifecycleMethod.OnDisable;
        
        [Inject] private CursorManager _cursorManager;

        private void OnValidate()
        {
            if (_registerAt != _unregisterAt) return;
            _registerAt = UnityLifecycleMethod.None;
            Debug.LogWarning("Register and Unregister places are the same. Change RegisterAt to None.", gameObject);
        }

        private void Awake() => HandleRegisterUnregisterPlace(UnityLifecycleMethod.Awake);
        private void OnEnable() => HandleRegisterUnregisterPlace(UnityLifecycleMethod.OnEnable);
        private void Start() => HandleRegisterUnregisterPlace(UnityLifecycleMethod.Start);
        private void OnDisable() => HandleRegisterUnregisterPlace(UnityLifecycleMethod.OnDisable);
        private void OnDestroy() => HandleRegisterUnregisterPlace(UnityLifecycleMethod.OnDestroy);

        public void Register()
        {
            _cursorManager?.Register(this);
        }

        public void Unregister()
        {
            _cursorManager?.Unregister(this);
        }

        private void HandleRegisterUnregisterPlace(UnityLifecycleMethod place)
        {
            if (_registerAt == _unregisterAt)
            {
                Debug.LogError("Register and Unregister places are the same.");
                return;
            }
            
            if (place == _registerAt)
                Register();
            
            if (place == _unregisterAt)
                Unregister();
        }
    }
}