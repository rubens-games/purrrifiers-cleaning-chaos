﻿using Zenject;

namespace CursorSystem
{
    public class CursorManagerInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<CursorManager>().AsSingle().NonLazy();
        }
    }
}