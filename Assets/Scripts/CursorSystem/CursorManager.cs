﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace CursorSystem
{
    public class CursorManager : ILateDisposable, ICursorHandler
    {
        public event Action<ICursorHandler> OnRegister;
        public event Action<ICursorHandler> OnUnregister;

        public ICursorHandler CurrentCursorHandler => CursorHandlers.Count > 0 ? CursorHandlers[0] : this;
        public List<ICursorHandler> CursorHandlers { get; } = new();
        public CursorSettings CurrentCursorSettings => CurrentCursorHandler?.CursorSettings;
        public CursorSettings CursorSettings => CursorSettings.ShowAndUnlock;

        public CursorManager() => Register(this);

        public void LateDispose() => UnregisterAll();

        public void Register(ICursorHandler cursorHandler)
        {
            // TODO - Remove this two line and fix the double register on freeze mode on tool enabled
            if (CursorHandlers.Contains(cursorHandler)) return;
            
            // Add the mouse handler to be the first in the list
            CursorHandlers.Insert(0, cursorHandler);
            
            // Set the mouse settings
            ActiveCursorHandler(cursorHandler);
            
            OnRegister?.Invoke(cursorHandler);
        }
        
        public void Unregister(ICursorHandler cursorHandler)
        {
            CursorHandlers.Remove(cursorHandler);
            
            ActiveCursorHandler(CurrentCursorHandler);
            
            OnUnregister?.Invoke(cursorHandler);
        }
        
        private void UnregisterAll()
        {
            for (var i = 0; i < CursorHandlers.Count; i++) 
                Unregister(CursorHandlers[i]);
        }
        
        private void ActiveCursorHandler(ICursorHandler cursorHandler)
        {
            if (cursorHandler == null) return;
            
            // If the cursorHandler is not the CurrentCursorHandler, set the cursorHandler to be the first in the list
            if (CurrentCursorHandler != cursorHandler)
            {
                CursorHandlers.Remove(cursorHandler);
                CursorHandlers.Insert(0, cursorHandler);
            }
            
            SetCursorSettings(cursorHandler.CursorSettings);
        }
        
        private static void SetCursorSettings(CursorSettings cursorSettings)
        {
            Cursor.visible = cursorSettings.IsVisible;
            Cursor.lockState = cursorSettings.LockMode;
            Cursor.SetCursor(cursorSettings.Texture, cursorSettings.Hotspot, cursorSettings.CursorMode);
        }
    }
}