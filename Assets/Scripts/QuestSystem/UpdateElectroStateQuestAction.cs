﻿using Other;
using PixelCrushers.QuestMachine;

namespace QuestSystem
{
    public class UpdateElectroStateQuestAction : QuestAction
    {
        public override void Execute()
        {
            base.Execute();
            
            var newQuestNodeState = ElectroManager.ElectroState ? QuestNodeState.Active : QuestNodeState.True;
            
            if (questNode == null) return;
            if (questNode.GetState() == newQuestNodeState) return;
            
            questNode.SetStateRaw(newQuestNodeState);
        }
    }
}