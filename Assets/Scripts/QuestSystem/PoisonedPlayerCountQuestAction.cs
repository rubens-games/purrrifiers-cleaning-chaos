﻿using PixelCrushers.QuestMachine;

namespace QuestSystem
{
    public class PoisonedPlayerCountQuestAction : QuestAction
    {
        public override void Execute()
        {
            base.Execute();

            var state = OvenControler.Instance.PoisonedPlayerCount > 0;
            var newQuestNodeState = state ? QuestNodeState.True : QuestNodeState.Active;
            
            if (questNode == null) return;
            if (questNode.GetState() == newQuestNodeState) return;
            
            questNode.SetState(newQuestNodeState);
        }
    }
}