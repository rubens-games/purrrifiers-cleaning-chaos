﻿using PixelCrushers.QuestMachine;

namespace QuestSystem
{
    public class BlackHoleComingQuestAction : QuestAction
    {
        public override void Execute()
        {
            base.Execute();

            var state = QuestCustomManager.instance.QuestNetworked.isBlackHoleDialogueDone;
            var newQuestNodeState = state ? QuestNodeState.True : QuestNodeState.Active;
            
            if (questNode == null) return;
            if (questNode.GetState() == newQuestNodeState) return;
            
            questNode.SetState(newQuestNodeState);
        }
    }
}