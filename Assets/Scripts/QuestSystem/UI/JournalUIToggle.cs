using FishNet.Object;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace QuestSystem.UI
{
    public class JournalUIToggle : NetworkBehaviour
    {
        [SerializeField] private InputActionReference _toggleJournalInputActionReference;

        [Inject] private QuestJournalNetworked _questJournalNetworked;
        
        public override void OnStartClient()
        {
            base.OnStartClient();
            
            if (!IsOwner) return;
            
            _toggleJournalInputActionReference.action.performed += OnToggleJournalActionPerformed;
            _toggleJournalInputActionReference.action.Enable();
        }

        public override void OnStopClient()
        {
            base.OnStopClient();
            
            if (!IsOwner) return;
            
            _toggleJournalInputActionReference.action.performed -= OnToggleJournalActionPerformed;
            _toggleJournalInputActionReference.action.Disable();
        }
        
        private void OnToggleJournalActionPerformed(InputAction.CallbackContext context)
        {
            _questJournalNetworked.ToggleJournalUI();
        }
    }
}
