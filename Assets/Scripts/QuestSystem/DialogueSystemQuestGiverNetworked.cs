﻿using FishNet.Object;
using InteractionSystem;
using PixelCrushers.QuestMachine;
using PlayerSystem;
using Zenject;

namespace QuestSystem
{
    public class DialogueSystemQuestGiverNetworked : DialogueSystemQuestGiver
    {
        [Inject] private QuestJournalNetworked _questJournalNetworked;
        
        private Interactable _interactable;

        public override void Awake()
        {
            base.Awake();
            
            _interactable = GetComponent<Interactable>();
        }

        public override void OnEnable()
        {
            base.OnEnable();
            
            _interactable.OnInteract.AddListener(StartDialogueWithPlayer);
        }

        public override void OnDisable()
        {
            base.OnDisable();
            
            _interactable.OnInteract.RemoveListener(StartDialogueWithPlayer);
        }

        public void StartDialogueWithPlayer(Player networkedPlayer)
        {
            if (networkedPlayer.IsOwner)
                StartDialogue(_questJournalNetworked.gameObject);
        }

        public override void GiveQuestToQuester(Quest quest, QuestParticipantTextInfo questerTextInfo,
            QuestListContainer questerQuestListContainer) => GiveQuestToQuesterServerRpc(quest.id.value);

        [ServerRpc(RequireOwnership = false)]
        private void GiveQuestToQuesterServerRpc(string questId) => GiveQuestToQuesterObserverRpc(questId);
        
        [ObserversRpc]
        private void GiveQuestToQuesterObserverRpc(string questId)
        {
            var quests = GetOfferableQuests(gameObject);
            var quest = quests.Find(q => q.id.value == questId);
            
            var questerTextInfo = GetQuesterTextInfo();
            
            base.GiveQuestToQuester(quest, questerTextInfo, _questJournalNetworked);
        }
        
        private QuestParticipantTextInfo GetQuesterTextInfo() => new(
            QuestMachineMessages.GetID(_questJournalNetworked),
                QuestMachineMessages.GetDisplayName(_questJournalNetworked), null, null);
    }
}