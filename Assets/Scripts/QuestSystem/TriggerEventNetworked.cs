using FishNet.Object;
using Other;
using PixelCrushers;
using PlayerSystem;
using UnityEngine;
using Zenject;

namespace QuestSystem
{
    [RequireComponent(typeof(TriggerEvent))]
    public class TriggerEventNetworked : NetworkBehaviour
    {
        public GameObjectUnityEvent OnTriggerEnter = new();
        public GameObjectUnityEvent OnTriggerExit = new();

        [SerializeField] private bool _executeLocal;
        
        [Inject] private PlayerManager _playerManager;
        
        private TriggerEvent _triggerEvent;

        private void Awake()
        {
            _triggerEvent = GetComponent<TriggerEvent>();
        }

        public override void OnStartClient()
        {
            base.OnStartClient();
            
            _triggerEvent.onTriggerEnter.AddListener(OnTriggerEnterInvoke);
            _triggerEvent.onTriggerExit.AddListener(OnTriggerExitInvoke);
        }
        
        public override void OnStopClient()
        {
            base.OnStopClient();
            
            _triggerEvent.onTriggerEnter.RemoveListener(OnTriggerEnterInvoke);
            _triggerEvent.onTriggerExit.RemoveListener(OnTriggerExitInvoke);
        }
        
        private void OnTriggerEnterInvoke(GameObject go)
        {
            if (_playerManager.localConnection != go.GetConnection()) return;
            
            if (_executeLocal) OnTriggerEnter?.Invoke(go);
            else InvokeOnTriggerExitServerRpc(go);
        }
        
        private void OnTriggerExitInvoke(GameObject go)
        {
            if (_playerManager.localConnection != go.GetConnection()) return;

            if (_executeLocal) OnTriggerExit?.Invoke(go);
            else InvokeOnTriggerEnterServerRpc(go);
        }

        [ServerRpc(RequireOwnership = false)]
        private void InvokeOnTriggerEnterServerRpc(GameObject go) =>
            InvokeOnTriggerEnterObserverRpc(go);
        
        [ObserversRpc]
        private void InvokeOnTriggerEnterObserverRpc(GameObject go) => OnTriggerEnter?.Invoke(go);
        
        [ServerRpc(RequireOwnership = false)]
        private void InvokeOnTriggerExitServerRpc(GameObject go) =>
            InvokeOnTriggerExitObserverRpc(go);
        
        [ObserversRpc]
        private void InvokeOnTriggerExitObserverRpc(GameObject go) => OnTriggerExit?.Invoke(go);
    }
}
