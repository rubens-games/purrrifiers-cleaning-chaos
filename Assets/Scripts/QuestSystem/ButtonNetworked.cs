using FishNet.Object;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace QuestSystem
{
    [RequireComponent(typeof(Button))]
    public class ButtonNetworked : NetworkBehaviour
    {
        public UnityEvent OnClick = new();
        
        [SerializeField] private Button _button;

        protected override void OnValidate()
        {
            base.OnValidate();
            
            if (!_button) _button = GetComponent<Button>();
        }

        public override void OnStartClient()
        {
            base.OnStartClient();
            
            _button.onClick.AddListener(OnClickInvoked);
        }

        public override void OnStopClient()
        {
            base.OnStopClient();
            
            _button.onClick.RemoveListener(OnClickInvoked);
        }
        
        private void OnClickInvoked() => InvokeOnClickServerRpc();
        
        [ServerRpc(RequireOwnership = false)]
        private void InvokeOnClickServerRpc() => InvokeOnClickObserversRpc();

        [ObserversRpc]
        private void InvokeOnClickObserversRpc() => OnClick?.Invoke();
    }
}
