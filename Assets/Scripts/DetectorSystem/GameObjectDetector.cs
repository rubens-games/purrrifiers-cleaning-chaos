﻿using InteractionSystem;
using UnityEngine;
using Zenject;

namespace DetectorSystem
{
    public class GameObjectDetector : Detector<GameObject>
    {
        [Inject] private Camera _camera;
        
        #region Detector

        protected override Ray Ray => new(_camera.transform.position, _camera.transform.forward);

        #endregion
    }
}