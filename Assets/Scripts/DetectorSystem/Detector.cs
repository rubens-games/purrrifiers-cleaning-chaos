﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace DetectorSystem
{
    public abstract class Detector<T> : MonoBehaviour where T : class
    {
        public UnityEvent<T, RaycastHit> OnDetectedChangeBefore = new();
        public UnityEvent<T, RaycastHit> OnDetectedChange = new();
        public UnityEvent<T, RaycastHit> OnDetectedChangeAfter = new();

        public T Detected { get; private set; }

        public RaycastHit Hit { get; private set; }

        protected abstract Ray Ray { get; }

        protected virtual float RayMaxDistance => float.PositiveInfinity;

        protected virtual LayerMask IgnoredLayerMask => -5;
        
        [SerializeField] private bool _debug;

        public virtual void Update()
        {
            var isHitSomething = Physics.Raycast(Ray, out var hit, RayMaxDistance, IgnoredLayerMask, QueryTriggerInteraction.Ignore);

            HandleHit(isHitSomething, hit);
        }

        private void HandleHit(bool isHitSomething, RaycastHit hit)
        {
            Hit = hit;
            
            if (!isHitSomething)
            {
                SetDetectedObject(null, hit);
                return;
            }

            T newDetectedObject = null;
            if (typeof(T) == typeof(GameObject))
            {
                if (hit.transform) 
                    newDetectedObject = hit.transform.gameObject as T;
            }
            else if (hit.transform.TryGetComponent(out newDetectedObject)) { }
            else if (hit.transform.parent && hit.transform.parent.TryGetComponent(out newDetectedObject)) { }
            else if (hit.transform.root && hit.transform.root.TryGetComponent(out newDetectedObject)) { }

            if (newDetectedObject is Behaviour { enabled: false }) newDetectedObject = null;

            SetDetectedObject(newDetectedObject, hit);
        }

        private void SetDetectedObject(T value, RaycastHit hit)
        {
            if (Detected == value) return;

            OnDetectedChangeBefore?.Invoke(Detected, Hit);

            Hit = hit;

            Detected = value;
            
            OnDetectedChange?.Invoke(Detected, Hit);
            
            OnDetectedChangeAfter?.Invoke(Detected, Hit);
            
            if (_debug) Debug.Log($"Detected: {Detected}");
        }

        private void OnDrawGizmos()
        {
            if (!_debug) return;
            
            Gizmos.color = Detected != null ? Color.green : Color.red;
            Gizmos.DrawLine(Ray.origin, Hit.point);
            Gizmos.DrawCube(Hit.point, Vector3.one * 0.1f);
        }
    }
}