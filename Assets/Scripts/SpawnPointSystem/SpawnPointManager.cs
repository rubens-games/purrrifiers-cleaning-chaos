using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SpawnPointSystem
{
    public static class SpawnPointManager
    {
        public static UnityEvent<Transform> OnSpawnPointRegistered = new();
        public static UnityEvent<Transform> OnSpawnPointUnregistered = new();
        
        public static List<Transform> SpawnPoints = new();
        
        public static void RegisterSpawnPoint(Transform spawnPoint)
        {
            SpawnPoints.Add(spawnPoint);
            OnSpawnPointRegistered.Invoke(spawnPoint);
        }
        
        public static void UnregisterSpawnPoint(Transform spawnPoint)
        {
            SpawnPoints.Remove(spawnPoint);
            OnSpawnPointUnregistered.Invoke(spawnPoint);
        }
        
        public static Transform GetRandomSpawnPoint() => 
            SpawnPoints[Random.Range(0, SpawnPoints.Count)];
    }
}
