﻿using FishNet.Component.Spawning;
using UnityEngine;

namespace SpawnPointSystem
{
    [RequireComponent(typeof(PlayerSpawner))]
    public class SpawnPointsUpdater : MonoBehaviour
    {
        [SerializeField] private PlayerSpawner _playerSpawner;

        private void Reset() => _playerSpawner = GetComponent<PlayerSpawner>();

        private void OnEnable()
        {
            SpawnPointManager.OnSpawnPointRegistered.AddListener(OnSpawnPointRegistered);
            SpawnPointManager.OnSpawnPointUnregistered.AddListener(OnSpawnPointUnregistered);
            
            UpdateSpawnPoints();
        }
        
        private void OnDisable()
        {
            SpawnPointManager.OnSpawnPointRegistered.RemoveListener(OnSpawnPointRegistered);
            SpawnPointManager.OnSpawnPointUnregistered.RemoveListener(OnSpawnPointUnregistered);
        }
        
        private void OnSpawnPointRegistered(Transform spawnPoint) => UpdateSpawnPoints();
        
        private void OnSpawnPointUnregistered(Transform spawnPoint) => UpdateSpawnPoints();
        
        private void UpdateSpawnPoints() => 
            _playerSpawner.Spawns = SpawnPointManager.SpawnPoints.ToArray();
    }
}