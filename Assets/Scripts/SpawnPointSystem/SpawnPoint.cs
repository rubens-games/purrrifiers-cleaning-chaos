﻿using UnityEngine;

namespace SpawnPointSystem
{
    public class SpawnPoint : MonoBehaviour
    {
        private void OnEnable() => SpawnPointManager.RegisterSpawnPoint(transform);
        private void OnDisable() => SpawnPointManager.UnregisterSpawnPoint(transform);
    }
}