using DG.Tweening;
using FishNet.Object;
using Other;
using PixelCrushers;
using PixelCrushers.DialogueSystem;
using PixelCrushers.QuestMachine;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using UnityEngine;

public class QuestCustomManagerNetworked : NetworkBehaviour
{
    [SerializeField] private Collider _doorsAnnabelle;

    [SerializeField] private GameObject _cat;
    private bool _isCatHere = false;
    public bool isBlackHoleDialogueDone;
    [SerializeField] private List<ItemDock> _toysDock;

    private int _trashBagCounterBackup = 0;

    [ServerRpc(RequireOwnership =false)]
    public void SetDoorsTalkServerRPC()
    {
        SetDoorsTalkObserverRPC();
    }

    [ObserversRpc]
    public void SetDoorsTalkObserverRPC()
    {
        _doorsAnnabelle.enabled = true;
    }

    [ServerRpc(RequireOwnership = false)]
    public void BringCatServerRPC()
    {
        BringCatObserverRPC();
    }
    [ObserversRpc]
    public void BringCatObserverRPC()
    {
        if (_isCatHere) return;

        _isCatHere = true;
        _cat.transform.DOMoveY(3.913f, 0.1f);
    }

    [ServerRpc(RequireOwnership = false)]
    public void SetBlackHoleDialogueDoneServerRPC()
    {
        SetBlackHoleDialogueDoneObserverRPC();
    }
    [ObserversRpc]
    public void SetBlackHoleDialogueDoneObserverRPC()
    {
        isBlackHoleDialogueDone = true;
    }

    [ServerRpc(RequireOwnership = false)]
    public void ChangeVarDServerRPC(string p_name, bool p_state)
    {
        ChangeVarDServerObserverRPC(p_name, p_state);
    }
    [ObserversRpc]
    public void ChangeVarDServerObserverRPC(string p_name, bool p_state)
    {
        DialogueLua.SetVariable(p_name, p_state);
    }

    [ServerRpc(RequireOwnership = false)]
    public void UnlockToysServerRPC()
    {
        UnlockToysObserverRPC();
    }
    [ObserversRpc]
    public void UnlockToysObserverRPC()
    {
        foreach (ItemDock dock in _toysDock)
        {
            dock.IsSleepingDock = false;
        }
    }

    [ServerRpc(RequireOwnership = false)]
    public void SendMessageNetworkedServerRPC(string p_message, string p_parameter)
    {
        SendMessageNetworkedObserverRPC(p_message, p_parameter);
    }
    [ObserversRpc]
    public void SendMessageNetworkedObserverRPC(string p_message, string p_parameter)
    {
        MessageSystem.SendMessage(this, p_message, p_parameter);

    }

    [ServerRpc(RequireOwnership = false)]
    public void IncreaseMaxCounterServerRPC(string p_name, int p_value)
    {
        IncreaseMaxCounterObserverRPC(p_name, p_value);
    }
    [ObserversRpc]
    public void IncreaseMaxCounterObserverRPC(string p_name, int p_value)
    {
        var counter = QuestMachine.GetQuestCounter("Q1", p_name);
        counter.maxValue += p_value;
        MessageSystem.SendMessage(this, QuestMachineMessages.QuestCounterChangedMessage, string.Empty);
    }

    [ServerRpc(RequireOwnership = false)]
    public void ThrowFullTrashBagServerRPC()
    {
        ThrowFullTrashBagObserverRPC();
    }
    
    [ObserversRpc]
    public void ThrowFullTrashBagObserverRPC()
    {
        MessageSystem.SendMessage(this, "Throw", "Bag");
        _trashBagCounterBackup++;

        var counter = QuestMachine.GetQuestCounter("Q1", "Worki");
        counter.currentValue = _trashBagCounterBackup;
    }
}
