using InteractionSystem;
using PlayerSystem;
using UnityEngine;

[RequireComponent(typeof(ItemDock))]
public class DockMonoDish : MonoBehaviour
{
    [SerializeField] private MonologueSO _monologue;
    private Interactable _myItemDock;
    private static bool _wasPlayed = false;
    private void Awake()
    {
        _myItemDock = GetComponent<ItemDock>();
    }
    private void OnEnable()
    {
        _myItemDock.OnInteract.AddListener(OnPutToDockMono);
    }
    private void OnDisable()
    {
        _myItemDock.OnInteract.RemoveListener(OnPutToDockMono);
    }
    private void OnPutToDockMono(Player p_player)
    {
        QuestCustomManager.instance.PutDish(p_player.IsOwner);
        
        if (!p_player.IsOwner) return;

        if (_wasPlayed) return;
        
        MonologueManager.instance.PlayMonologue(_monologue);
        
        _wasPlayed = true;
    }
}
