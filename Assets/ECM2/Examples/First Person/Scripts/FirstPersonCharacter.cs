using System;
using System.Collections;
using System.Threading.Tasks;
using PlayerSystem;
using UnityEngine;

namespace ECM2.Examples.FirstPerson
{
    /// <summary>
    /// This example extends a Character (through inheritance), implementing a First Person control.
    /// </summary>

    public class FirstPersonCharacter : Character
    {
        public bool isSprinting => IsSprinting();
        
        public float maxWalkSprintSpeed = 5.0f;
        public float maxWalkCrouchedSprintSpeed = 2.5f;
        
        [Tooltip("The first person camera parent.")]
        public GameObject cameraParent;

        public Transform characterHeadBone;
        public Vector3 cameraPositionOffsetStanding = new(0.0f, 1.8f, 0.2f);
        public Vector3 cameraPositionOffsetCrouched = new(0.0f, 0.8f, 0.2f);
        public float cameraTransitionSpeed = 10.0f;

        public Player Player;

        [SerializeField] private LayerMask _ignoreLayers;
        
        private bool _isSprinting;
        private float _cameraPitch;
        private MovementMode _previousMovementMode;
        private bool _lateUpdateCalled;

        /// <summary>
        /// Add input (affecting Yaw).
        /// This is applied to the Character's rotation.
        /// </summary>

        public virtual void AddControlYawInput(float value)
        {
            if (value != 0.0f)
                AddYawInput(value);
        }

        /// <summary>
        /// Add input (affecting Pitch).
        /// This is applied to the cameraParent's local rotation.
        /// </summary>

        public virtual void AddControlPitchInput(float value, float minPitch = -80.0f, float maxPitch = 80.0f)
        {
            if (value != 0.0f)
                _cameraPitch = MathLib.ClampAngle(_cameraPitch + value, minPitch, maxPitch);
        }

        /// <summary>
        /// Update cameraParent local rotation applying current _cameraPitch value.
        /// </summary>

        protected virtual void UpdateCameraPitch()
        {
            // var targetPitch = Quaternion.Euler(_cameraPitch, 0.0f, 0.0f);
            // var smoothedRotation = Quaternion.Slerp(cameraParent.transform.localRotation, targetPitch,
            //     cameraTransitionSpeed * Time.fixedDeltaTime);
            // cameraParent.transform.localRotation = smoothedRotation;
            
            if (isPaused) return;
            
            cameraParent.transform.localRotation = Quaternion.Euler(_cameraPitch, 0.0f, 0.0f);
        }

        public void UpdateCameraPosition()
        {
            var cameraPositionOffset = IsCrouched()
                ? cameraPositionOffsetCrouched
                : cameraPositionOffsetStanding;

            // Calculate camera position
            // Todo: Add camera collision detection
            var newCameraPosition =
                characterHeadBone.TransformPoint(characterHeadBone.localPosition + cameraPositionOffset);

            // camera collision
            var headPoint = characterHeadBone.position;
            var distanceFromHeadToCamera = Vector3.Distance(headPoint, newCameraPosition);
            var direction = (newCameraPosition - headPoint).normalized;
            if (Physics.Raycast(headPoint, direction, out var hit, distanceFromHeadToCamera + 1, _ignoreLayers,
                    QueryTriggerInteraction.Ignore))
            {
                // if we hit something, move the camera between the head and the hit point
                var pointBetweenHeadAndHit = Vector3.Lerp(headPoint, hit.point, 0.5f);
                var isNewPositionCloser =
                    Vector3.Distance(headPoint, pointBetweenHeadAndHit) < distanceFromHeadToCamera;

                if (isNewPositionCloser) newCameraPosition = pointBetweenHeadAndHit;
            }

            // cameraParent.transform.position = Vector3.Lerp(cameraParent.transform.position, newCameraPosition,
            //     cameraTransitionSpeed * Time.deltaTime);
            
            cameraParent.transform.position = newCameraPosition;
        }

        /// <summary>
        /// If overriden, base method MUST be called.
        /// </summary>

        private void Update()
        {
            UpdateCameraPosition();
            UpdateCameraPitch();
        }

        /// <summary>
        /// If overriden, base method MUST be called.
        /// </summary>

        protected override void Reset()
        {
            // Call base method implementation

            base.Reset();

            // Disable character's rotation,
            // it is handled by the AddControlYawInput method 

            SetRotationMode(RotationMode.None);
        }

        public override void Simulate(float deltaTime)
        {
            if (!Player.IsOwner) return;

            base.Simulate(deltaTime);
        }

        public bool IsSprinting() => _isSprinting;

        public override float GetMaxSpeed()
        {
            switch (movementMode)
            {
                case MovementMode.Walking:
                    // return IsCrouched() ? maxWalkSpeedCrouched : maxWalkSpeed;
                    if (IsSprinting())
                        return IsCrouched() ? maxWalkCrouchedSprintSpeed : maxWalkSprintSpeed;
                    
                    return IsCrouched() ? maxWalkSpeedCrouched : maxWalkSpeed;

                case MovementMode.Falling:
                    return maxWalkSpeed;

                case MovementMode.Swimming:
                    return maxSwimSpeed;

                case MovementMode.Flying:
                    return maxFlySpeed;

                default:
                    return 0.0f;
            }
        }

        public float GetMaxStateSpeed() => IsCrouched() ? maxWalkCrouchedSprintSpeed : maxWalkSprintSpeed;

        public void Sprint() => _isSprinting = true;

        public void StopSprinting() => _isSprinting = false;
    }
}
